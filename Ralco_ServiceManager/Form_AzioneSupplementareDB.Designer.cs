﻿namespace Ralco_ServiceManager
{
    partial class Form_AzioneSupplementareDB
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_AzioneSupplementareDB));
            this.gb_AddAzioneAlDB = new System.Windows.Forms.GroupBox();
            this.tb_descAzSuppl = new System.Windows.Forms.TextBox();
            this.btn_addAzioneSupplDB = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dgv_listaAzioniSupplDB = new System.Windows.Forms.DataGridView();
            this.chk_Respo = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.descAzione = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_cancel = new System.Windows.Forms.Button();
            this.btn_OK = new System.Windows.Forms.Button();
            this.gb_AddAzioneAlDB.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_listaAzioniSupplDB)).BeginInit();
            this.SuspendLayout();
            // 
            // gb_AddAzioneAlDB
            // 
            this.gb_AddAzioneAlDB.Controls.Add(this.tb_descAzSuppl);
            this.gb_AddAzioneAlDB.Controls.Add(this.btn_addAzioneSupplDB);
            this.gb_AddAzioneAlDB.Location = new System.Drawing.Point(2, 12);
            this.gb_AddAzioneAlDB.Name = "gb_AddAzioneAlDB";
            this.gb_AddAzioneAlDB.Size = new System.Drawing.Size(908, 135);
            this.gb_AddAzioneAlDB.TabIndex = 1;
            this.gb_AddAzioneAlDB.TabStop = false;
            this.gb_AddAzioneAlDB.Text = "Aggiungi Azione Supplementare Al Database";
            // 
            // tb_descAzSuppl
            // 
            this.tb_descAzSuppl.Location = new System.Drawing.Point(9, 24);
            this.tb_descAzSuppl.Multiline = true;
            this.tb_descAzSuppl.Name = "tb_descAzSuppl";
            this.tb_descAzSuppl.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tb_descAzSuppl.Size = new System.Drawing.Size(835, 97);
            this.tb_descAzSuppl.TabIndex = 10;
            this.tb_descAzSuppl.Text = "N/A";
            // 
            // btn_addAzioneSupplDB
            // 
            this.btn_addAzioneSupplDB.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_addAzioneSupplDB.BackgroundImage")));
            this.btn_addAzioneSupplDB.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_addAzioneSupplDB.Location = new System.Drawing.Point(864, 56);
            this.btn_addAzioneSupplDB.Name = "btn_addAzioneSupplDB";
            this.btn_addAzioneSupplDB.Size = new System.Drawing.Size(35, 35);
            this.btn_addAzioneSupplDB.TabIndex = 9;
            this.btn_addAzioneSupplDB.UseVisualStyleBackColor = true;
            this.btn_addAzioneSupplDB.Click += new System.EventHandler(this.btn_addAzioneSupplDB_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dgv_listaAzioniSupplDB);
            this.groupBox2.Location = new System.Drawing.Point(2, 153);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(908, 409);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            // 
            // dgv_listaAzioniSupplDB
            // 
            this.dgv_listaAzioniSupplDB.AllowUserToAddRows = false;
            this.dgv_listaAzioniSupplDB.AllowUserToOrderColumns = true;
            this.dgv_listaAzioniSupplDB.BackgroundColor = System.Drawing.SystemColors.ActiveBorder;
            this.dgv_listaAzioniSupplDB.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_listaAzioniSupplDB.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.chk_Respo,
            this.descAzione});
            this.dgv_listaAzioniSupplDB.Cursor = System.Windows.Forms.Cursors.Default;
            this.dgv_listaAzioniSupplDB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_listaAzioniSupplDB.Location = new System.Drawing.Point(3, 16);
            this.dgv_listaAzioniSupplDB.Name = "dgv_listaAzioniSupplDB";
            this.dgv_listaAzioniSupplDB.ReadOnly = true;
            this.dgv_listaAzioniSupplDB.RowHeadersVisible = false;
            this.dgv_listaAzioniSupplDB.Size = new System.Drawing.Size(902, 390);
            this.dgv_listaAzioniSupplDB.TabIndex = 7;
            // 
            // chk_Respo
            // 
            this.chk_Respo.HeaderText = "";
            this.chk_Respo.Name = "chk_Respo";
            this.chk_Respo.ReadOnly = true;
            this.chk_Respo.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.chk_Respo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.chk_Respo.Width = 30;
            // 
            // descAzione
            // 
            this.descAzione.HeaderText = "Azione Supplementare";
            this.descAzione.Name = "descAzione";
            this.descAzione.ReadOnly = true;
            this.descAzione.Width = 850;
            // 
            // btn_cancel
            // 
            this.btn_cancel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_cancel.BackgroundImage")));
            this.btn_cancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_cancel.Location = new System.Drawing.Point(457, 579);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(50, 50);
            this.btn_cancel.TabIndex = 116;
            this.btn_cancel.UseVisualStyleBackColor = true;
            this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
            // 
            // btn_OK
            // 
            this.btn_OK.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_OK.BackgroundImage")));
            this.btn_OK.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_OK.Location = new System.Drawing.Point(350, 579);
            this.btn_OK.Name = "btn_OK";
            this.btn_OK.Size = new System.Drawing.Size(50, 50);
            this.btn_OK.TabIndex = 115;
            this.btn_OK.UseVisualStyleBackColor = true;
            this.btn_OK.Click += new System.EventHandler(this.btn_ok_Click);
            // 
            // Form_AzioneSupplementareDB
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(917, 650);
            this.Controls.Add(this.btn_cancel);
            this.Controls.Add(this.btn_OK);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.gb_AddAzioneAlDB);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form_AzioneSupplementareDB";
            this.Text = "Form Azione Supplementare";
            this.gb_AddAzioneAlDB.ResumeLayout(false);
            this.gb_AddAzioneAlDB.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_listaAzioniSupplDB)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gb_AddAzioneAlDB;
        private System.Windows.Forms.TextBox tb_descAzSuppl;
        private System.Windows.Forms.Button btn_addAzioneSupplDB;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dgv_listaAzioniSupplDB;
        private System.Windows.Forms.DataGridViewCheckBoxColumn chk_Respo;
        private System.Windows.Forms.DataGridViewTextBoxColumn descAzione;
        private System.Windows.Forms.Button btn_cancel;
        private System.Windows.Forms.Button btn_OK;
    }
}