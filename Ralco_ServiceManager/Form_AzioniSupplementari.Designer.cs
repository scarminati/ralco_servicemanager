﻿namespace Ralco_ServiceManager
{
    partial class Form_AzioniSupplementari
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_AzioniSupplementari));
            this.grp_AssociaAzSuppl = new System.Windows.Forms.GroupBox();
            this.btn_RemoveAzSupplCliMod = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_AddAzSupplCliMod = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dgv_azSupplCliMod = new System.Windows.Forms.DataGridView();
            this.chk_Respo = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.descAzione = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descCliente = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descModello = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmb_modelloCollimatore = new System.Windows.Forms.ComboBox();
            this.cmb_nomeCliente = new System.Windows.Forms.ComboBox();
            this.cmb_descAzioneSupplementare = new System.Windows.Forms.ComboBox();
            this.btn_cancel = new System.Windows.Forms.Button();
            this.btn_OK = new System.Windows.Forms.Button();
            this.toolStrip_MenuAllegati = new System.Windows.Forms.ToolStrip();
            this.btn_RefreshAzSuppl = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_AddDBAzSuppl = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.cmb_criterioRicerca = new System.Windows.Forms.ToolStripComboBox();
            this.tb_ricerca = new System.Windows.Forms.ToolStripTextBox();
            this.btn_SearchDBAzSuppl = new System.Windows.Forms.ToolStripButton();
            this.grp_AssociaAzSuppl.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_azSupplCliMod)).BeginInit();
            this.toolStrip_MenuAllegati.SuspendLayout();
            this.SuspendLayout();
            // 
            // grp_AssociaAzSuppl
            // 
            this.grp_AssociaAzSuppl.Controls.Add(this.btn_RemoveAzSupplCliMod);
            this.grp_AssociaAzSuppl.Controls.Add(this.label3);
            this.grp_AssociaAzSuppl.Controls.Add(this.label2);
            this.grp_AssociaAzSuppl.Controls.Add(this.label1);
            this.grp_AssociaAzSuppl.Controls.Add(this.btn_AddAzSupplCliMod);
            this.grp_AssociaAzSuppl.Controls.Add(this.groupBox2);
            this.grp_AssociaAzSuppl.Controls.Add(this.cmb_modelloCollimatore);
            this.grp_AssociaAzSuppl.Controls.Add(this.cmb_nomeCliente);
            this.grp_AssociaAzSuppl.Controls.Add(this.cmb_descAzioneSupplementare);
            this.grp_AssociaAzSuppl.Location = new System.Drawing.Point(12, 53);
            this.grp_AssociaAzSuppl.Name = "grp_AssociaAzSuppl";
            this.grp_AssociaAzSuppl.Size = new System.Drawing.Size(908, 450);
            this.grp_AssociaAzSuppl.TabIndex = 1;
            this.grp_AssociaAzSuppl.TabStop = false;
            this.grp_AssociaAzSuppl.Text = "Associa Azioni Supplementari";
            // 
            // btn_RemoveAzSupplCliMod
            // 
            this.btn_RemoveAzSupplCliMod.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_RemoveAzSupplCliMod.BackgroundImage")));
            this.btn_RemoveAzSupplCliMod.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_RemoveAzSupplCliMod.Location = new System.Drawing.Point(864, 67);
            this.btn_RemoveAzSupplCliMod.Name = "btn_RemoveAzSupplCliMod";
            this.btn_RemoveAzSupplCliMod.Size = new System.Drawing.Size(35, 35);
            this.btn_RemoveAzSupplCliMod.TabIndex = 12;
            this.btn_RemoveAzSupplCliMod.UseVisualStyleBackColor = true;
            this.btn_RemoveAzSupplCliMod.Click += new System.EventHandler(this.btn_RemoveAzSupplCliMod_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 84);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(98, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Modello Collimatore";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Nome Cliente";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Azione Supplementare";
            // 
            // btn_AddAzSupplCliMod
            // 
            this.btn_AddAzSupplCliMod.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_AddAzSupplCliMod.BackgroundImage")));
            this.btn_AddAzSupplCliMod.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_AddAzSupplCliMod.Location = new System.Drawing.Point(864, 26);
            this.btn_AddAzSupplCliMod.Name = "btn_AddAzSupplCliMod";
            this.btn_AddAzSupplCliMod.Size = new System.Drawing.Size(35, 35);
            this.btn_AddAzSupplCliMod.TabIndex = 8;
            this.btn_AddAzSupplCliMod.UseVisualStyleBackColor = true;
            this.btn_AddAzSupplCliMod.Click += new System.EventHandler(this.btn_AddAzSupplCliMod_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dgv_azSupplCliMod);
            this.groupBox2.Location = new System.Drawing.Point(6, 108);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(896, 336);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            // 
            // dgv_azSupplCliMod
            // 
            this.dgv_azSupplCliMod.AllowUserToAddRows = false;
            this.dgv_azSupplCliMod.AllowUserToOrderColumns = true;
            this.dgv_azSupplCliMod.BackgroundColor = System.Drawing.SystemColors.ActiveBorder;
            this.dgv_azSupplCliMod.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_azSupplCliMod.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.chk_Respo,
            this.descAzione,
            this.descCliente,
            this.descModello});
            this.dgv_azSupplCliMod.Cursor = System.Windows.Forms.Cursors.Default;
            this.dgv_azSupplCliMod.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_azSupplCliMod.Location = new System.Drawing.Point(3, 16);
            this.dgv_azSupplCliMod.Name = "dgv_azSupplCliMod";
            this.dgv_azSupplCliMod.ReadOnly = true;
            this.dgv_azSupplCliMod.RowHeadersVisible = false;
            this.dgv_azSupplCliMod.Size = new System.Drawing.Size(890, 317);
            this.dgv_azSupplCliMod.TabIndex = 7;
            // 
            // chk_Respo
            // 
            this.chk_Respo.HeaderText = "";
            this.chk_Respo.Name = "chk_Respo";
            this.chk_Respo.ReadOnly = true;
            this.chk_Respo.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.chk_Respo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.chk_Respo.Width = 30;
            // 
            // descAzione
            // 
            this.descAzione.HeaderText = "Azione Supplementare";
            this.descAzione.Name = "descAzione";
            this.descAzione.ReadOnly = true;
            this.descAzione.Width = 450;
            // 
            // descCliente
            // 
            this.descCliente.HeaderText = "Cliente";
            this.descCliente.Name = "descCliente";
            this.descCliente.ReadOnly = true;
            this.descCliente.Width = 200;
            // 
            // descModello
            // 
            this.descModello.HeaderText = "Modello Collimatore";
            this.descModello.Name = "descModello";
            this.descModello.ReadOnly = true;
            this.descModello.Width = 200;
            // 
            // cmb_modelloCollimatore
            // 
            this.cmb_modelloCollimatore.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmb_modelloCollimatore.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_modelloCollimatore.FormattingEnabled = true;
            this.cmb_modelloCollimatore.Location = new System.Drawing.Point(134, 81);
            this.cmb_modelloCollimatore.Name = "cmb_modelloCollimatore";
            this.cmb_modelloCollimatore.Size = new System.Drawing.Size(710, 21);
            this.cmb_modelloCollimatore.Sorted = true;
            this.cmb_modelloCollimatore.TabIndex = 6;
            // 
            // cmb_nomeCliente
            // 
            this.cmb_nomeCliente.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmb_nomeCliente.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_nomeCliente.FormattingEnabled = true;
            this.cmb_nomeCliente.Location = new System.Drawing.Point(134, 54);
            this.cmb_nomeCliente.Name = "cmb_nomeCliente";
            this.cmb_nomeCliente.Size = new System.Drawing.Size(710, 21);
            this.cmb_nomeCliente.Sorted = true;
            this.cmb_nomeCliente.TabIndex = 5;
            // 
            // cmb_descAzioneSupplementare
            // 
            this.cmb_descAzioneSupplementare.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmb_descAzioneSupplementare.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_descAzioneSupplementare.FormattingEnabled = true;
            this.cmb_descAzioneSupplementare.Location = new System.Drawing.Point(134, 27);
            this.cmb_descAzioneSupplementare.Name = "cmb_descAzioneSupplementare";
            this.cmb_descAzioneSupplementare.Size = new System.Drawing.Size(710, 21);
            this.cmb_descAzioneSupplementare.Sorted = true;
            this.cmb_descAzioneSupplementare.TabIndex = 4;
            // 
            // btn_cancel
            // 
            this.btn_cancel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_cancel.BackgroundImage")));
            this.btn_cancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_cancel.Location = new System.Drawing.Point(473, 544);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(50, 50);
            this.btn_cancel.TabIndex = 114;
            this.btn_cancel.UseVisualStyleBackColor = true;
            this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
            // 
            // btn_OK
            // 
            this.btn_OK.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_OK.BackgroundImage")));
            this.btn_OK.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_OK.Location = new System.Drawing.Point(366, 544);
            this.btn_OK.Name = "btn_OK";
            this.btn_OK.Size = new System.Drawing.Size(50, 50);
            this.btn_OK.TabIndex = 113;
            this.btn_OK.UseVisualStyleBackColor = true;
            this.btn_OK.Click += new System.EventHandler(this.btn_ok_Click);
            // 
            // toolStrip_MenuAllegati
            // 
            this.toolStrip_MenuAllegati.AutoSize = false;
            this.toolStrip_MenuAllegati.BackColor = System.Drawing.SystemColors.Control;
            this.toolStrip_MenuAllegati.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip_MenuAllegati.ImageScalingSize = new System.Drawing.Size(47, 47);
            this.toolStrip_MenuAllegati.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btn_RefreshAzSuppl,
            this.toolStripSeparator3,
            this.btn_AddDBAzSuppl,
            this.toolStripSeparator4,
            this.cmb_criterioRicerca,
            this.tb_ricerca,
            this.btn_SearchDBAzSuppl});
            this.toolStrip_MenuAllegati.Location = new System.Drawing.Point(0, 0);
            this.toolStrip_MenuAllegati.Name = "toolStrip_MenuAllegati";
            this.toolStrip_MenuAllegati.Size = new System.Drawing.Size(932, 50);
            this.toolStrip_MenuAllegati.TabIndex = 116;
            this.toolStrip_MenuAllegati.Text = "toolStrip1";
            // 
            // btn_RefreshAzSuppl
            // 
            this.btn_RefreshAzSuppl.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_RefreshAzSuppl.Image = ((System.Drawing.Image)(resources.GetObject("btn_RefreshAzSuppl.Image")));
            this.btn_RefreshAzSuppl.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_RefreshAzSuppl.Name = "btn_RefreshAzSuppl";
            this.btn_RefreshAzSuppl.Size = new System.Drawing.Size(51, 47);
            this.btn_RefreshAzSuppl.Text = "Evidenzia";
            this.btn_RefreshAzSuppl.ToolTipText = "Evidenzia";
            this.btn_RefreshAzSuppl.Click += new System.EventHandler(this.btn_RefreshAzSuppl_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 50);
            // 
            // btn_AddDBAzSuppl
            // 
            this.btn_AddDBAzSuppl.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_AddDBAzSuppl.Image = ((System.Drawing.Image)(resources.GetObject("btn_AddDBAzSuppl.Image")));
            this.btn_AddDBAzSuppl.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_AddDBAzSuppl.Name = "btn_AddDBAzSuppl";
            this.btn_AddDBAzSuppl.Size = new System.Drawing.Size(51, 47);
            this.btn_AddDBAzSuppl.Text = "toolStripButton4";
            this.btn_AddDBAzSuppl.ToolTipText = "Esporta Magazzino";
            this.btn_AddDBAzSuppl.Click += new System.EventHandler(this.btn_AddDBAzSuppl_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 50);
            // 
            // cmb_criterioRicerca
            // 
            this.cmb_criterioRicerca.Name = "cmb_criterioRicerca";
            this.cmb_criterioRicerca.Size = new System.Drawing.Size(121, 50);
            // 
            // tb_ricerca
            // 
            this.tb_ricerca.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.tb_ricerca.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.tb_ricerca.Name = "tb_ricerca";
            this.tb_ricerca.Size = new System.Drawing.Size(100, 50);
            this.tb_ricerca.Text = "Cerca...";
            this.tb_ricerca.ToolTipText = "Cerca In Magazzino";
            this.tb_ricerca.Leave += new System.EventHandler(this.Tb_ricerca_Leave);
            this.tb_ricerca.Click += new System.EventHandler(this.Tb_ricerca_Click);
            // 
            // btn_SearchDBAzSuppl
            // 
            this.btn_SearchDBAzSuppl.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_SearchDBAzSuppl.Image = ((System.Drawing.Image)(resources.GetObject("btn_SearchDBAzSuppl.Image")));
            this.btn_SearchDBAzSuppl.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_SearchDBAzSuppl.Name = "btn_SearchDBAzSuppl";
            this.btn_SearchDBAzSuppl.Size = new System.Drawing.Size(51, 47);
            this.btn_SearchDBAzSuppl.Text = "toolStripButton3";
            this.btn_SearchDBAzSuppl.Click += new System.EventHandler(this.btn_SearchDBAzSuppl_Click);
            // 
            // Form_AzioniSupplementari
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(932, 607);
            this.Controls.Add(this.toolStrip_MenuAllegati);
            this.Controls.Add(this.btn_cancel);
            this.Controls.Add(this.btn_OK);
            this.Controls.Add(this.grp_AssociaAzSuppl);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(948, 645);
            this.MinimumSize = new System.Drawing.Size(948, 645);
            this.Name = "Form_AzioniSupplementari";
            this.Text = "Form Azioni Supplementari";
            this.grp_AssociaAzSuppl.ResumeLayout(false);
            this.grp_AssociaAzSuppl.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_azSupplCliMod)).EndInit();
            this.toolStrip_MenuAllegati.ResumeLayout(false);
            this.toolStrip_MenuAllegati.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox grp_AssociaAzSuppl;
        private System.Windows.Forms.Button btn_cancel;
        private System.Windows.Forms.Button btn_OK;
        private System.Windows.Forms.ComboBox cmb_modelloCollimatore;
        private System.Windows.Forms.ComboBox cmb_nomeCliente;
        private System.Windows.Forms.ComboBox cmb_descAzioneSupplementare;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dgv_azSupplCliMod;
        private System.Windows.Forms.Button btn_AddAzSupplCliMod;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn chk_Respo;
        private System.Windows.Forms.DataGridViewTextBoxColumn descAzione;
        private System.Windows.Forms.DataGridViewTextBoxColumn descCliente;
        private System.Windows.Forms.DataGridViewTextBoxColumn descModello;
        private System.Windows.Forms.Button btn_RemoveAzSupplCliMod;
        private System.Windows.Forms.ToolStrip toolStrip_MenuAllegati;
        private System.Windows.Forms.ToolStripButton btn_RefreshAzSuppl;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton btn_AddDBAzSuppl;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripComboBox cmb_criterioRicerca;
        private System.Windows.Forms.ToolStripTextBox tb_ricerca;
        private System.Windows.Forms.ToolStripButton btn_SearchDBAzSuppl;
    }
}