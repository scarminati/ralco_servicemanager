﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Ralco_ServiceManager
{
    public partial class Form_AzioniSupplementari : Form
    {
        private SQLConnector conn;
        private String queryConditions = "";
        public List<RMA_AzioneSupplementare> supplActions { get; set; }
        private List<RMA_AzioneSupplementare> azSupplList;
        public Form_AzioniSupplementari()
        {
            try
            {
                InitializeComponent();
                conn = new SQLConnector();
                btn_RefreshAzSuppl_Click(null, null);
                setWidgetDescriptions();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void setWidgetDescriptions()
        {
            try
            {
                #region Form
                this.Text = Utils.resourcemanager.GetString("Form_AzioneSupplementare");
                #endregion
                #region TooltipsPerBottoni
                ToolTip tooltip = new System.Windows.Forms.ToolTip();
                btn_RefreshAzSuppl.ToolTipText = Utils.resourcemanager.GetString("btn_RefreshAzSuppl");
                btn_AddDBAzSuppl.ToolTipText = Utils.resourcemanager.GetString("btn_addAzioneSupplDB");
                btn_SearchDBAzSuppl.ToolTipText = Utils.resourcemanager.GetString("btn_SearchDBAzSuppl");
                tooltip.SetToolTip(btn_AddAzSupplCliMod, Utils.resourcemanager.GetString("btn_AddAzSupplCliMod"));
                tooltip.SetToolTip(btn_RemoveAzSupplCliMod, Utils.resourcemanager.GetString("btn_RemoveAzSupplCliMod"));
                tooltip.SetToolTip(btn_OK, Utils.resourcemanager.GetString("btn_OK"));
                tooltip.SetToolTip(btn_cancel, Utils.resourcemanager.GetString("btn_cancel"));
                #endregion
                #region Groupbox
                grp_AssociaAzSuppl.Text = Utils.resourcemanager.GetString("grp_AssociaAzSuppl");
                #endregion
                #region Combobox
                cmb_criterioRicerca.Items.Add(Utils.resourcemanager.GetString("dgv_azSupplCliMod_descAzione"));
                cmb_criterioRicerca.Items.Add(Utils.resourcemanager.GetString("dgv_azSupplCliMod_descCliente"));
                cmb_criterioRicerca.Items.Add(Utils.resourcemanager.GetString("dgv_azSupplCliMod_descModello"));
                #endregion
                #region DataGridViewColumns
                dgv_azSupplCliMod.Columns["descAzione"].HeaderText = Utils.resourcemanager.GetString("dgv_azSupplCliMod_descAzione");
                dgv_azSupplCliMod.Columns["descCliente"].HeaderText = Utils.resourcemanager.GetString("dgv_azSupplCliMod_descCliente");
                dgv_azSupplCliMod.Columns["descModello"].HeaderText = Utils.resourcemanager.GetString("dgv_azSupplCliMod_descModello");
                #endregion
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void setComboboxes()
        {
            try
            {
                cmb_descAzioneSupplementare.Items.Clear();
                cmb_nomeCliente.Items.Clear();
                cmb_modelloCollimatore.Items.Clear();
                //Aggiungo gli * per indicare che la selezione riguarderà tutti gli items della combobox
                cmb_nomeCliente.Items.Add("*");
                cmb_modelloCollimatore.Items.Add("*");
                List<String> azioniSupplList = new List<String>();
                List<String> cliList = new List<String>();
                List<String> modCollList = new List<String>();
                azioniSupplList = (List<String>)conn.CreateCommand("SELECT [descrizioneAzioneSuppl] FROM [Autotest].[dbo].[RMA_ElencoAzioniSupplementari]", azioniSupplList);
                cliList = (List<String>)conn.CreateCommand("SELECT DISTINCT [Dsc] FROM [archivi].[dbo].[VISTA_SM_CLIENTI]", cliList);
                modCollList = (List<String>)conn.CreateCommand("SELECT DISTINCT [Model] FROM [archivi].[dbo].[VISTA_SM_FC_2]", modCollList);
                if (azioniSupplList != null)
                    foreach (string s in azioniSupplList)
                        cmb_descAzioneSupplementare.Items.Add(s);
                if (cliList != null)
                    foreach (string s in cliList)
                        cmb_nomeCliente.Items.Add(s);
                if (modCollList != null)
                    foreach (string s in modCollList)
                        cmb_modelloCollimatore.Items.Add(s);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_RefreshAzSuppl_Click(object sender, EventArgs e)
        {
            try
            {
                queryConditions = "";
                setComboboxes();
                refreshListData(queryConditions);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void refreshListData(string queryConditions)
        {
            try
            {
                dgv_azSupplCliMod.Rows.Clear();
                azSupplList = new List<RMA_AzioneSupplementare>();
                azSupplList = (List<RMA_AzioneSupplementare>)conn.CreateCommand("SELECT * FROM [Autotest].[dbo].[SM_VISTA_CLI_MOD_AZSUPPL]" + queryConditions, azSupplList);
                if (azSupplList != null)
                    foreach (RMA_AzioneSupplementare azSuppl in azSupplList)
                        this.dgv_azSupplCliMod.Rows.Insert(0, false, azSuppl.descrizioneAzioneSuppl, azSuppl.nomeCliente, azSuppl.modelloColl);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_AddDBAzSuppl_Click(object sender, EventArgs e)
        {
            try
            {
                using (var form = new Form_AzioneSupplementareDB())
                {
                    var result = form.ShowDialog();
                    if (result == DialogResult.OK)
                    {
                        btn_RefreshAzSuppl_Click(null, null);
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void btn_SearchDBAzSuppl_Click(object sender, EventArgs e)
        {
            try
            {
                if (cmb_criterioRicerca.SelectedItem.Equals(""))
                    MessageBox.Show("Selezionare un criterio per la ricerca nel Database", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else if (tb_ricerca.Text.Equals(""))
                    MessageBox.Show("Inserire l'elemento da ricercare", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                {
                    String[] query = new String[1];
                    string nomeCampoDB = "";
                    if (cmb_criterioRicerca.SelectedItem.Equals(Utils.resourcemanager.GetString("dgv_azSupplCliMod_descAzione")))
                        nomeCampoDB = ".[descrizioneAzioneSuppl]";
                    else if (cmb_criterioRicerca.SelectedItem.Equals(Utils.resourcemanager.GetString("dgv_azSupplCliMod_descCliente")))
                        nomeCampoDB = ".[nomeCliente]";
                    else if (cmb_criterioRicerca.SelectedItem.Equals(Utils.resourcemanager.GetString("dgv_azSupplCliMod_descModello")))
                        nomeCampoDB = ".[modelloColl]";
                    if (!nomeCampoDB.Equals(""))
                    {
                        if (this.queryConditions.Equals(""))
                            this.queryConditions = "WHERE [AutoTest].[dbo].[SM_VISTA_RMA_DESCAZSUPPL_CLI_MODCOLL]" + nomeCampoDB + " LIKE '%" + tb_ricerca.Text + "%'";
                        else
                            this.queryConditions = this.queryConditions + " AND [AutoTest].[dbo].[SM_VISTA_RMA_DESCAZSUPPL_CLI_MODCOLL]" + nomeCampoDB + " LIKE '%" + tb_ricerca.Text + "%'";
                        refreshListData(queryConditions);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_AddAzSupplCliMod_Click(object sender, EventArgs e)
        {
            try
            {
                if (cmb_descAzioneSupplementare.SelectedIndex.Equals(-1))
                    MessageBox.Show("Inserire una azione supplementare da aggiungere", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else if (cmb_nomeCliente.SelectedIndex.Equals(-1))
                    MessageBox.Show("Inserire il nome del cliente da associare all'azione supplementare", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else if (cmb_modelloCollimatore.SelectedIndex.Equals(-1))
                    MessageBox.Show("Inserire il modello del collimatore da associare all'azione supplementare ed al cliente", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                {
                    int idxAzSuppl = 0;
                    idxAzSuppl = (int)conn.CreateCommand("SELECT [idProgressivo] FROM [Autotest].[dbo].[RMA_ElencoAzioniSupplementari] WHERE [descrizioneAzioneSuppl] = '" + cmb_descAzioneSupplementare.SelectedItem.ToString().Replace("'", "''") + "'", idxAzSuppl);
                    if (!idxAzSuppl.Equals(0))
                    {
                        conn.CreateCommand("INSERT INTO [Autotest].[dbo].[RMA_ElencoAzSupplClienteModello] " +
                        "([nomeCliente],[modelloColl],[idAzioneSuppl]) " +
                        "VALUES ('" + cmb_nomeCliente.SelectedItem.ToString() + "','" + cmb_modelloCollimatore.SelectedItem.ToString() + "','" + idxAzSuppl + "')", null);
                        refreshListData(queryConditions);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_RemoveAzSupplCliMod_Click(object sender, EventArgs e)
        {
            try
            {
                if (cmb_descAzioneSupplementare.SelectedIndex.Equals(-1))
                    MessageBox.Show("Inserire una azione supplementare da aggiungere", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else if (cmb_nomeCliente.SelectedIndex.Equals(-1))
                    MessageBox.Show("Inserire il nome del cliente da associare all'azione supplementare", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else if (cmb_modelloCollimatore.SelectedIndex.Equals(-1))
                    MessageBox.Show("Inserire il modello del collimatore da associare all'azione supplementare ed al cliente", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                {
                    int idxAzSuppl = 0;
                    idxAzSuppl = (int)conn.CreateCommand("SELECT [idProgressivo] FROM [Autotest].[dbo].[RMA_ElencoAzioniSupplementari] WHERE [descrizioneAzioneSuppl] = '" + cmb_descAzioneSupplementare.SelectedItem.ToString() + "'", idxAzSuppl);
                    if (!idxAzSuppl.Equals(0))
                    {
                        conn.CreateCommand("DELETE FROM [AutoTest].[dbo].[RMA_ElencoAzSupplClienteModello] " +
                            "WHERE [AutoTest].[dbo].[RMA_ElencoAzSupplClienteModello].[nomeCliente] = '" + cmb_nomeCliente.SelectedItem.ToString() + "'" +
                            "AND [AutoTest].[dbo].[RMA_ElencoAzSupplClienteModello].[modelloColl] = '" + cmb_modelloCollimatore.SelectedItem.ToString() + "'" +
                            "AND [AutoTest].[dbo].[RMA_ElencoAzSupplClienteModello].[idAzioneSuppl] = '" + idxAzSuppl + "'", null);
                        refreshListData(queryConditions);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            try
            {
                this.DialogResult = DialogResult.OK;
                this.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_cancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.DialogResult = DialogResult.Cancel;
                this.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Tb_ricerca_Click(object sender, EventArgs e)
        {
            tb_ricerca.Text = "";
            tb_ricerca.ForeColor = Color.Black;
        }

        private void Tb_ricerca_Leave(object sender, EventArgs e)
        {
            if (tb_ricerca.Text.Equals(""))
            {
                tb_ricerca.Text = "Cerca...";
                tb_ricerca.ForeColor = SystemColors.ControlDark;
            }
        }

    }
}
