﻿namespace Ralco_ServiceManager
{
    partial class Form_Log
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Log));
            this.lbl_dataAgg = new System.Windows.Forms.Label();
            this.lbl_aggDa = new System.Windows.Forms.Label();
            this.lbl_note = new System.Windows.Forms.Label();
            this.grp_dettRip = new System.Windows.Forms.GroupBox();
            this.dtp_dataAggiornamento = new System.Windows.Forms.DateTimePicker();
            this.lbl_aggiornatoDa = new System.Windows.Forms.Label();
            this.cmb_statoRMA = new System.Windows.Forms.ComboBox();
            this.lbl_statoRMA = new System.Windows.Forms.Label();
            this.tb_noteLog = new System.Windows.Forms.TextBox();
            this.btn_OK = new System.Windows.Forms.Button();
            this.btn_cancel = new System.Windows.Forms.Button();
            this.grp_dettRip.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbl_dataAgg
            // 
            this.lbl_dataAgg.AutoSize = true;
            this.lbl_dataAgg.Location = new System.Drawing.Point(8, 81);
            this.lbl_dataAgg.Name = "lbl_dataAgg";
            this.lbl_dataAgg.Size = new System.Drawing.Size(104, 13);
            this.lbl_dataAgg.TabIndex = 1;
            this.lbl_dataAgg.Text = "Data Aggiornamento";
            // 
            // lbl_aggDa
            // 
            this.lbl_aggDa.AutoSize = true;
            this.lbl_aggDa.Location = new System.Drawing.Point(8, 35);
            this.lbl_aggDa.Name = "lbl_aggDa";
            this.lbl_aggDa.Size = new System.Drawing.Size(75, 13);
            this.lbl_aggDa.TabIndex = 2;
            this.lbl_aggDa.Text = "Aggiornato Da";
            // 
            // lbl_note
            // 
            this.lbl_note.AutoSize = true;
            this.lbl_note.Location = new System.Drawing.Point(8, 173);
            this.lbl_note.Name = "lbl_note";
            this.lbl_note.Size = new System.Drawing.Size(30, 13);
            this.lbl_note.TabIndex = 4;
            this.lbl_note.Text = "Note";
            // 
            // grp_dettRip
            // 
            this.grp_dettRip.Controls.Add(this.dtp_dataAggiornamento);
            this.grp_dettRip.Controls.Add(this.lbl_aggiornatoDa);
            this.grp_dettRip.Controls.Add(this.cmb_statoRMA);
            this.grp_dettRip.Controls.Add(this.lbl_statoRMA);
            this.grp_dettRip.Controls.Add(this.tb_noteLog);
            this.grp_dettRip.Controls.Add(this.lbl_dataAgg);
            this.grp_dettRip.Controls.Add(this.lbl_note);
            this.grp_dettRip.Controls.Add(this.lbl_aggDa);
            this.grp_dettRip.Location = new System.Drawing.Point(12, 12);
            this.grp_dettRip.Name = "grp_dettRip";
            this.grp_dettRip.Size = new System.Drawing.Size(337, 238);
            this.grp_dettRip.TabIndex = 7;
            this.grp_dettRip.TabStop = false;
            this.grp_dettRip.Text = "Dettagli della riparazione";
            // 
            // dtp_dataAggiornamento
            // 
            this.dtp_dataAggiornamento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtp_dataAggiornamento.Location = new System.Drawing.Point(168, 75);
            this.dtp_dataAggiornamento.MaximumSize = new System.Drawing.Size(150, 21);
            this.dtp_dataAggiornamento.MinimumSize = new System.Drawing.Size(150, 21);
            this.dtp_dataAggiornamento.Name = "dtp_dataAggiornamento";
            this.dtp_dataAggiornamento.Size = new System.Drawing.Size(150, 21);
            this.dtp_dataAggiornamento.TabIndex = 9;
            this.dtp_dataAggiornamento.Value = new System.DateTime(2018, 1, 16, 13, 29, 36, 0);
            // 
            // lbl_aggiornatoDa
            // 
            this.lbl_aggiornatoDa.AutoSize = true;
            this.lbl_aggiornatoDa.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_aggiornatoDa.Location = new System.Drawing.Point(168, 35);
            this.lbl_aggiornatoDa.MaximumSize = new System.Drawing.Size(150, 21);
            this.lbl_aggiornatoDa.MinimumSize = new System.Drawing.Size(150, 21);
            this.lbl_aggiornatoDa.Name = "lbl_aggiornatoDa";
            this.lbl_aggiornatoDa.Size = new System.Drawing.Size(150, 21);
            this.lbl_aggiornatoDa.TabIndex = 3;
            // 
            // cmb_statoRMA
            // 
            this.cmb_statoRMA.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmb_statoRMA.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_statoRMA.FormattingEnabled = true;
            this.cmb_statoRMA.Items.AddRange(new object[] {
            "Aperto",
            "Attesa Analisi Problema",
            "Emettere Preventivo",
            "Spedito Preventivo",
            "Attesa Approvazione Preventivo",
            "Preventivo Approvato",
            "In Riparazione",
            "Riparazione Completata",
            "Parzialmente Evaso",
            "In Spedizione",
            "Attesa Rientro Parte",
            "Chiuso",
            "Annullato"});
            this.cmb_statoRMA.Location = new System.Drawing.Point(168, 114);
            this.cmb_statoRMA.MaximumSize = new System.Drawing.Size(150, 0);
            this.cmb_statoRMA.MinimumSize = new System.Drawing.Size(150, 0);
            this.cmb_statoRMA.Name = "cmb_statoRMA";
            this.cmb_statoRMA.Size = new System.Drawing.Size(150, 21);
            this.cmb_statoRMA.TabIndex = 4;
            // 
            // lbl_statoRMA
            // 
            this.lbl_statoRMA.AutoSize = true;
            this.lbl_statoRMA.Location = new System.Drawing.Point(8, 117);
            this.lbl_statoRMA.Name = "lbl_statoRMA";
            this.lbl_statoRMA.Size = new System.Drawing.Size(59, 13);
            this.lbl_statoRMA.TabIndex = 8;
            this.lbl_statoRMA.Text = "Stato RMA";
            // 
            // tb_noteLog
            // 
            this.tb_noteLog.Location = new System.Drawing.Point(168, 156);
            this.tb_noteLog.Multiline = true;
            this.tb_noteLog.Name = "tb_noteLog";
            this.tb_noteLog.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tb_noteLog.Size = new System.Drawing.Size(150, 65);
            this.tb_noteLog.TabIndex = 5;
            // 
            // btn_OK
            // 
            this.btn_OK.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_OK.BackgroundImage")));
            this.btn_OK.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_OK.Location = new System.Drawing.Point(102, 269);
            this.btn_OK.Name = "btn_OK";
            this.btn_OK.Size = new System.Drawing.Size(50, 50);
            this.btn_OK.TabIndex = 6;
            this.btn_OK.UseVisualStyleBackColor = true;
            this.btn_OK.Click += new System.EventHandler(this.btn_okRiparazione_Click);
            // 
            // btn_cancel
            // 
            this.btn_cancel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_cancel.BackgroundImage")));
            this.btn_cancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_cancel.Location = new System.Drawing.Point(207, 270);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(50, 50);
            this.btn_cancel.TabIndex = 112;
            this.btn_cancel.UseVisualStyleBackColor = true;
            this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
            // 
            // Form_Log
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(361, 332);
            this.Controls.Add(this.btn_cancel);
            this.Controls.Add(this.btn_OK);
            this.Controls.Add(this.grp_dettRip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(377, 370);
            this.MinimumSize = new System.Drawing.Size(377, 370);
            this.Name = "Form_Log";
            this.Text = "Modifica Note";
            this.grp_dettRip.ResumeLayout(false);
            this.grp_dettRip.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lbl_dataAgg;
        private System.Windows.Forms.Label lbl_aggDa;
        private System.Windows.Forms.Label lbl_note;
        private System.Windows.Forms.GroupBox grp_dettRip;
        private System.Windows.Forms.TextBox tb_noteLog;
        private System.Windows.Forms.Button btn_OK;
        private System.Windows.Forms.ComboBox cmb_statoRMA;
        private System.Windows.Forms.Label lbl_statoRMA;
        private System.Windows.Forms.Label lbl_aggiornatoDa;
        private System.Windows.Forms.DateTimePicker dtp_dataAggiornamento;
        private System.Windows.Forms.Button btn_cancel;
    }
}