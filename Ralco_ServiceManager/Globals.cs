﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ralco_ServiceManager
{
    public class Globals
    {
        //Variabili comuni a tutti i form del progetto
        public static Utente user;

        //Accessor / muthator methods
        public static void setUser(Utente user)
        {
            Globals.user = new Utente(user);
        }
    }
}
