﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Configuration;//per leggere il file di configurazione Ralco_Centratura.config

namespace Ralco_ServiceManager
{
    class SQLConnector
    {
        private Object retObject;
        private RMA_View rmaView;
        private RMA_Problema prob;
        private RMA_Preventivo prev;
        private RMA_Riparazione rip;
        private RMA_Collimatore coll;
        private RMA_Parte part;
        private RMA_FaseProduzione faseProd;
        private RMA_Articolo art;
        private RMA_Generali rmaGen;
        private RMA_InformativaCliente infoCli;
        private RMA_NoteQualita noteQualita;
        private RMA_GaranziaRalco garanziaRalco;
        private RMA_CostiExtra costoExtra;
        private List<Utente> users;
        private List<RMA_View> rmaList;
        private List<RMA_Collimatore> collList;
        private List<RMA_Parte> partList;
        private List<RMA_Problema> probList;
        private List<RMA_Preventivo> prevList;
        private List<RMA_Riparazione> ripList;
        private List<RMA_ClientiRalco> cliList;
        private List<RMA_TestDaEseguire> testDaEseguireList;
        private List<RMA_EsitiTestPreAnalisi> esitiTestPreAnalisi;
        private List<RMA_EsitiAzioniSupplementari> esitiAzioniSupplementari;
        private List<RALCO_ModelliProduttivi> modProdList;
        private List<RMA_Articolo> artList;
        private List<string> listString;
        private List<RMA_Log> logList;
        private List<RMA_DifettoRiscontrato> difList;
        private List<RMA_CostiExtra> costiExtraList;
        private List<RMA_AzioneSupplementare> azSupplList;
        private List<RMA_Allegato> allegatiList;
        private string singoloDatoString;
        private int singoloDatoInt;
        private DateTime date;
        private decimal singoloDatoDecimal;

        public object CreateCommand(string queryString, Object obj)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                listString = new List<String>();
                singoloDatoString = "";
                singoloDatoInt = 0;
                singoloDatoDecimal = 0;
                rmaList = null;
                collList = null;
                partList = null;
                probList = null;
                prevList = null;
                ripList = null;
                artList = null;
                cliList = null;
                logList = null;
                difList = null;
                modProdList = null;
                testDaEseguireList = null;
                esitiTestPreAnalisi = null;
                esitiAzioniSupplementari = null;
                azSupplList = null;
                prob = null;
                prev = null;
                rip = null;
                coll = null;
                part = null;
                faseProd = null;
                art = null;
                rmaGen = null;
                infoCli = null;
                noteQualita = null;
                garanziaRalco = null;
                costoExtra = null;
                rmaView = null;
                allegatiList = null;
                string connectionString = "";
                if (ConfigurationManager.AppSettings["LOCAL_TEST"].Equals("false"))
                    connectionString = ConfigurationManager.ConnectionStrings["RALCO_DB"].ConnectionString;
                else
                    connectionString = ConfigurationManager.ConnectionStrings["LOCAL_DB"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand command = new SqlCommand(queryString, connection);
                    command.Connection.Open();
                    if (queryString.Contains("UPDATE") || queryString.Contains("INSERT") || queryString.Contains("DELETE"))
                    {
                        Console.WriteLine(queryString);
                        command.ExecuteNonQuery();
                        command.Connection.Close();
                    }    
                    else if (queryString.Contains("SELECT"))
                    {
                        SqlDataReader reader = command.ExecuteReader();
                        if (reader != null && reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                if (obj.GetType() == typeof(List<Utente>))
                                {
                                    if (users == null)
                                        users = new List<Utente>();
                                    users.Add(new Utente());
                                    users[users.Count - 1].idUtente = Convert.ToInt16(reader["idUtente"]);
                                    users[users.Count - 1].idApplicativo = Convert.ToInt16(reader["idApplicativo"]);
                                    users[users.Count - 1].nome = Convert.ToString(reader["nome"]);
                                    users[users.Count - 1].cognome = Convert.ToString(reader["cognome"]);
                                    users[users.Count - 1].password = Convert.ToString(reader["password"]);
                                    users[users.Count - 1].credenziale = Convert.ToString(reader["credenziale"]);
                                    users[users.Count - 1].location = Convert.ToString(reader["location"]);
                                    users[users.Count - 1].language = Convert.ToString(reader["lingua"]);
                                }
                                else if (obj.GetType() == typeof(RMA_View))
                                {
                                    if (rmaView == null)
                                        rmaView = new RMA_View();
                                    if (rmaView.idRMA == null)
                                    {
                                        rmaView.idRMA = reader.GetString(0);
                                        rmaView.tipoRMA = reader.GetString(1);
                                        if (rmaView.modelloCollim == null)
                                        {
                                            rmaView.modelloCollim = new List<String>();
                                            rmaView.modelloCollim.Add(reader.GetString(2));
                                        }
                                        if (rmaView.serialeCollim == null)
                                        {
                                            rmaView.serialeCollim = new List<String>();
                                            rmaView.serialeCollim.Add(reader.GetString(3));
                                        }
                                        if (rmaView.numFlowChart == null)
                                        {
                                            rmaView.numFlowChart = new List<String>();
                                            rmaView.numFlowChart.Add(reader.GetString(4));
                                        }
                                        rmaView.dataCreazione = reader.GetDateTime(5);
                                        rmaView.creatoDa = reader.GetString(6);
                                        rmaView.dataAggiornamento = reader.GetDateTime(7);
                                        rmaView.aggiornatoDa = reader.GetString(8);
                                        if (!reader.IsDBNull(9))
                                            rmaView.dataScadenza = reader.GetDateTime(9);
                                        rmaView.priorita = reader.GetString(10);
                                        rmaView.stato = reader.GetString(11);
                                        rmaView.location = reader.GetString(12);
                                        if (!reader.IsDBNull(13))
                                            rmaView.nomeCliente = reader.GetString(13);
                                        else
                                            rmaView.nomeCliente = "";
                                        if (!reader.IsDBNull(14))
                                            rmaView.noteCliente = reader.GetString(14);
                                        else
                                            rmaView.noteCliente = "";
                                        if (!reader.IsDBNull(15))
                                            rmaView.numSCAR = reader.GetString(15);
                                        else
                                            rmaView.numSCAR = "";
                                        if (!reader.IsDBNull(16))
                                            rmaView.docReso = reader.GetString(16);
                                        else
                                            rmaView.docReso = "";
                                        if (!reader.IsDBNull(17))
                                            rmaView.numOrdRip = reader.GetString(17);
                                        else
                                            rmaView.numOrdRip = "";
                                        //if (reader["noteProblema"] != null)
                                        //{
                                        //    rmaView.difRiscDaRalco = new List<String>();
                                        //    rmaView.difRiscDaRalco.Add(reader.GetString(19));
                                        //}
                                        //if (reader["causaProblema"] != null)
                                        //{
                                        //    rmaView.causaProblema = new List<String>();
                                        //    rmaView.causaProblema.Add(reader.GetString(18));
                                        //}
                                    }
                                    else
                                    {
                                        rmaView.modelloCollim.Add(reader.GetString(2));
                                        rmaView.serialeCollim.Add(reader.GetString(3));
                                        rmaView.numFlowChart.Add(reader.GetString(4));
                                        rmaView.difRiscDaRalco.Add(reader.GetString(18));
                                        rmaView.causaProblema.Add(reader.GetString(19));
                                    }
                                }
                                else if (obj.GetType() == typeof(List<RMA_View>))
                                {
                                    if (rmaList == null)
                                        rmaList = new List<RMA_View>();
                                    if (rmaList.Count.Equals(0) || !rmaList[rmaList.Count - 1].idRMA.Contains(reader["idRMA"].ToString()))
                                    {
                                        rmaList.Add(new RMA_View());
                                        rmaList[rmaList.Count - 1].idRMA = reader["idRMA"].ToString();
                                        rmaList[rmaList.Count - 1].tipoRMA = reader["tipoRMA"].ToString();
                                        if (rmaList[rmaList.Count - 1].modelloCollim == null)
                                        {
                                            rmaList[rmaList.Count - 1].modelloCollim = new List<String>();
                                            rmaList[rmaList.Count - 1].modelloCollim.Add(reader["modelloColl"].ToString());
                                        }
                                        if (rmaList[rmaList.Count - 1].serialeCollim == null)
                                        {
                                            rmaList[rmaList.Count - 1].serialeCollim = new List<String>();
                                            rmaList[rmaList.Count - 1].serialeCollim.Add(reader["serialeColl"].ToString());
                                        }
                                        if (rmaList[rmaList.Count - 1].numFlowChart == null)
                                        {
                                            rmaList[rmaList.Count - 1].numFlowChart = new List<String>();
                                            rmaList[rmaList.Count - 1].numFlowChart.Add(reader["numFlowChart"].ToString());
                                        }
                                        rmaList[rmaList.Count - 1].dataCreazione = Convert.ToDateTime(reader["dataCreazione"]);
                                        rmaList[rmaList.Count - 1].creatoDa = reader["creatoDa"].ToString();
                                        rmaList[rmaList.Count - 1].dataAggiornamento = Convert.ToDateTime(reader["dataAggiornamento"]);
                                        rmaList[rmaList.Count - 1].aggiornatoDa = reader["aggiornatoDa"].ToString();
                                        if (!reader.IsDBNull(9))
                                            rmaList[rmaList.Count - 1].dataScadenza = Convert.ToDateTime(reader["dataScadenza"]);
                                        rmaList[rmaList.Count - 1].priorita = reader["priorita"].ToString();
                                        rmaList[rmaList.Count - 1].stato = reader["stato"].ToString();
                                        rmaList[rmaList.Count - 1].location = reader["location"].ToString();
                                        if (!reader.IsDBNull(13))
                                            rmaList[rmaList.Count - 1].nomeCliente = reader["nomeCliente"].ToString();
                                        else
                                            rmaList[rmaList.Count - 1].nomeCliente = "";
                                        if (!reader.IsDBNull(14))
                                            rmaList[rmaList.Count - 1].noteCliente = reader["noteCliente"].ToString();
                                        else
                                            rmaList[rmaList.Count - 1].noteCliente = "";
                                        if (!reader.IsDBNull(15))
                                            rmaList[rmaList.Count - 1].numSCAR = reader["numScarCli"].ToString();
                                        else
                                            rmaList[rmaList.Count - 1].numSCAR = "";
                                        if (!reader.IsDBNull(16))
                                            rmaList[rmaList.Count - 1].docReso = reader["docReso"].ToString();
                                        else
                                            rmaList[rmaList.Count - 1].docReso = "";
                                        if (!reader.IsDBNull(17))
                                            rmaList[rmaList.Count - 1].numOrdRip = reader["ordRiparazione"].ToString();
                                        else
                                            rmaList[rmaList.Count - 1].numOrdRip = "";
                                        if (!reader.IsDBNull(18))
                                            rmaList[rmaList.Count - 1].statoGaranzia = reader["statoGaranzia"].ToString();
                                        else
                                            rmaList[rmaList.Count - 1].statoGaranzia = "";
                                        if (rmaList[rmaList.Count - 1].difRiscDaRalco == null)
                                        {
                                            rmaList[rmaList.Count - 1].difRiscDaRalco = new List<String>();
                                            if (!reader.IsDBNull(19))
                                                rmaList[rmaList.Count - 1].difRiscDaRalco.Add(reader["noteProblema"].ToString());
                                            else
                                                rmaList[rmaList.Count - 1].difRiscDaRalco.Add("");
                                        }
                                        if (rmaList[rmaList.Count - 1].causaProblema == null)
                                        {
                                            rmaList[rmaList.Count - 1].causaProblema = new List<String>();
                                            if (!reader.IsDBNull(20))
                                                rmaList[rmaList.Count - 1].causaProblema.Add(reader["causaProblema"].ToString());
                                            else
                                                rmaList[rmaList.Count - 1].causaProblema.Add("");
                                        }
                                        if (!reader.IsDBNull(21))
                                            rmaList[rmaList.Count - 1].provCollimatore = reader["provCollimatore"].ToString();
                                        else
                                            rmaList[rmaList.Count - 1].provCollimatore = "";
                                        //if (!reader.IsDBNull(21))
                                        //    rmaList[rmaList.Count - 1].costoRiparazione = Convert.ToDecimal(reader["costoRiparazione"]);
                                        //else
                                        //    rmaList[rmaList.Count - 1].costoRiparazione = 0;
                                    }
                                    else
                                    {
                                        rmaList[rmaList.Count - 1].modelloCollim.Add(reader["modelloColl"].ToString());
                                        rmaList[rmaList.Count - 1].serialeCollim.Add(reader["serialeColl"].ToString());
                                        rmaList[rmaList.Count - 1].numFlowChart.Add(reader["numFlowChart"].ToString());
                                        if (!reader.IsDBNull(19))
                                            rmaList[rmaList.Count - 1].difRiscDaRalco.Add(reader["noteProblema"].ToString());
                                        else
                                            rmaList[rmaList.Count - 1].difRiscDaRalco.Add("");
                                        if (!reader.IsDBNull(20))
                                            rmaList[rmaList.Count - 1].causaProblema.Add(reader["causaProblema"].ToString());
                                        else
                                            rmaList[rmaList.Count - 1].causaProblema.Add("");
                                    }
                                }
                                else if (obj.GetType() == typeof(RMA_Generali))
                                {
                                    if (rmaGen == null)
                                    {
                                        rmaGen = new RMA_Generali();
                                        rmaGen.idGenerali = reader.GetInt64(0);
                                        rmaGen.idRMA = reader.GetString(1);
                                        rmaGen.tipoRMA = reader.GetString(2);
                                        rmaGen.dataCreazione = reader.GetDateTime(3);
                                        rmaGen.creatoDa = reader.GetString(4);
                                        rmaGen.dataAggiornamento = reader.GetDateTime(5);
                                        rmaGen.aggiornatoDa = reader.GetString(6);
                                        rmaGen.location = reader.GetString(7);
                                        rmaGen.priorita = reader.GetString(8);
                                        rmaGen.note = reader.GetString(9);
                                        rmaGen.stato = reader.GetString(10);
                                        if (rmaGen.seriali == null)
                                            rmaGen.seriali = new List<String>();
                                        if (!reader.IsDBNull(11))
                                            rmaGen.seriali.Add(reader.GetString(11));
                                        if (rmaGen.progrParte == null)
                                            rmaGen.progrParte = new List<int>();
                                        if (!reader.IsDBNull(12))
                                            rmaGen.progrParte.Add(reader.GetInt32(12));
                                    }
                                    else
                                    {
                                        if (!reader.IsDBNull(11))
                                            rmaGen.seriali.Add(reader.GetString(11));
                                        if (!reader.IsDBNull(12))
                                            rmaGen.progrParte.Add(reader.GetInt32(12));
                                    }
                                }
                                //leggo il singolo collimatore dalla VISTA_SM_FC 
                                else if (obj.GetType() == typeof(RMA_Collimatore))
                                {
                                    coll = new RMA_Collimatore();
                                    coll.idRMA = reader.GetString(1);
                                    coll.seriale = reader.GetString(2);
                                    if (reader.IsDBNull(3))
                                        coll.modelloColl = "";
                                    else
                                        coll.modelloColl = reader.GetString(3);
                                    if (reader.IsDBNull(4))
                                        coll.dataProduzione = DateTime.MinValue;
                                    else
                                        coll.dataProduzione = reader.GetDateTime(4);
                                    coll.cliente = new RMA_ClientiRalco();
                                    coll.cliente.IdCliente = reader.GetInt32(5);
                                    coll.cliente.nomeCliente = reader.GetString(6);
                                    if (reader.IsDBNull(7))//perchè
                                        coll.codCollCliente = "";
                                    else
                                        coll.codCollCliente = reader.GetString(7);
                                    coll.numFlowChart = reader.GetString(8).Trim();
                                    coll.da_N = reader.GetInt64(9);
                                    coll.a_N = reader.GetInt64(10);
                                    coll.qta = reader.GetInt16(11);
                                }
                                //leggo dalla tabella dei collimatori per i quali esistono degli RMA
                                else if (obj.GetType() == typeof(List<RMA_Collimatore>))
                                {
                                    if (collList == null)
                                        collList = new List<RMA_Collimatore>();
                                    collList.Add(new RMA_Collimatore());
                                    collList[collList.Count - 1].idRMA = reader.GetString(1);
                                    collList[collList.Count - 1].seriale = reader.GetString(2);
                                    if (reader.IsDBNull(3))
                                        collList[collList.Count - 1].modelloColl = "";
                                    else
                                        collList[collList.Count - 1].modelloColl = reader.GetString(3);
                                    if (reader.IsDBNull(4))
                                        collList[collList.Count - 1].dataProduzione = DateTime.MinValue;
                                    else
                                        collList[collList.Count - 1].dataProduzione = reader.GetDateTime(4);
                                    collList[collList.Count - 1].cliente = new RMA_ClientiRalco();
                                    collList[collList.Count - 1].cliente.IdCliente = reader.GetInt32(5);
                                    collList[collList.Count - 1].cliente.nomeCliente = reader.GetString(6);
                                    if (reader.IsDBNull(7))//perchè
                                        collList[collList.Count - 1].codCollCliente = "";
                                    else
                                        collList[collList.Count - 1].codCollCliente = reader.GetString(7);
                                    collList[collList.Count - 1].numFlowChart = reader.GetString(8).Trim();
                                    collList[collList.Count - 1].da_N = reader.GetInt64(9);
                                    collList[collList.Count - 1].a_N = reader.GetInt64(10);
                                    collList[collList.Count - 1].qta = reader.GetInt16(11);
                                }
                                else if (obj.GetType() == typeof(RMA_Parte))
                                {
                                    part = new RMA_Parte();
                                    part.idProgressivo = reader.GetInt32(0);
                                    part.idRMA = reader.GetString(1);
                                    part.idParte = reader.GetString(2);
                                    if (reader.IsDBNull(3))
                                        part.descParte = "";
                                    else
                                        part.descParte = reader.GetString(3);
                                    if (reader.IsDBNull(4))
                                        part.serialeParte = "";
                                    else
                                        part.serialeParte = reader.GetString(4);
                                    if (reader.IsDBNull(5))
                                        part.nomeCliente = "";
                                    else
                                        part.nomeCliente = reader.GetString(5);
                                    if (reader.IsDBNull(6))
                                        part.numBolla = "";
                                    else
                                        part.numBolla = reader.GetString(6);
                                    part.dataBolla = reader.GetDateTime(7);
                                    part.dataVendita = reader.GetDateTime(8);
                                    part.quantita = reader.GetInt16(9);
                                    if (reader.IsDBNull(10))
                                        part.numFlowChart = "";
                                    else
                                        part.numFlowChart = reader.GetString(10);
                                }
                                //leggo dalla tabella dei collimatori per i quali esistono degli RMA
                                else if (obj.GetType() == typeof(List<RMA_Parte>))
                                {
                                    if (partList == null)
                                        partList = new List<RMA_Parte>();
                                    partList.Add(new RMA_Parte());
                                    partList[partList.Count - 1].idProgressivo = reader.GetInt32(0);
                                    partList[partList.Count - 1].idRMA = reader.GetString(1);
                                    partList[partList.Count - 1].idParte = reader.GetString(2);
                                    if (reader.IsDBNull(3))
                                        partList[partList.Count - 1].descParte = "";
                                    else
                                        partList[partList.Count - 1].descParte = reader.GetString(3);
                                    if (reader.IsDBNull(4))
                                        partList[partList.Count - 1].serialeParte = "";
                                    else
                                        partList[partList.Count - 1].serialeParte = reader.GetString(4);
                                    if (reader.IsDBNull(5))
                                        partList[partList.Count - 1].nomeCliente = "";
                                    else
                                        partList[partList.Count - 1].nomeCliente = reader.GetString(5);
                                    if (reader.IsDBNull(6))
                                        partList[partList.Count - 1].numBolla = "";
                                    else
                                        partList[partList.Count - 1].numBolla = reader.GetString(6);
                                    partList[partList.Count - 1].dataBolla = reader.GetDateTime(7);
                                    partList[partList.Count - 1].dataVendita = reader.GetDateTime(8);
                                    partList[partList.Count - 1].quantita = reader.GetInt16(9);
                                    if (reader.IsDBNull(10))
                                        partList[partList.Count - 1].numFlowChart = "";
                                    else
                                        partList[partList.Count - 1].numFlowChart = reader.GetString(10);
                                }
                                else if (obj.GetType() == typeof(RMA_InformativaCliente))
                                {
                                    if (infoCli == null)
                                        infoCli = new RMA_InformativaCliente();
                                    infoCli.idRMA = Convert.ToString(reader["idRMA"]);
                                    infoCli.cliente = new RMA_ClientiRalco();
                                    infoCli.cliente.nomeCliente = Convert.ToString(reader["nomeCliente"]);
                                    infoCli.cliente.IdCliente = Convert.ToInt32(reader["idCliente"]);
                                    infoCli.decisioneCliente = Convert.ToString(reader["decisioneCliente"]);
                                    infoCli.numScarCli = Convert.ToString(reader["numScarCli"]);
                                    infoCli.SCARCliente = Convert.ToString(reader["SCARCliente"]);
                                    infoCli.ordRiparazione = Convert.ToString(reader["ordRiparazione"]);
                                    infoCli.dataOrdineRip = Convert.ToDateTime(reader["dataOrdineRip"]);
                                    infoCli.docReso = Convert.ToString(reader["docReso"]);
                                    infoCli.dataSpedizione = Convert.ToDateTime(reader["dataSpedizione"]);
                                    infoCli.speseSpedizione = Convert.ToString(reader["speseSpedizione"]);
                                    infoCli.noteCliente = Convert.ToString(reader["noteCliente"]);
                                    infoCli.dataArrivoCollim = Convert.ToDateTime(reader["dataArrivoCollim"]);
                                    infoCli.dataScadenzaRMA = Convert.ToDateTime(reader["dataScadenzaRMA"]);
                                    infoCli.dataDocReso = Convert.ToDateTime(reader["dataDocReso"]);
                                    infoCli.tipoTrasporto = Convert.ToString(reader["tipoTrasporto"]);
                                    infoCli.codCollCli = Convert.ToString(reader["codCollCli"]);
                                    infoCli.tipoImballaggio = Convert.ToString(reader["tipoImballaggio"]);
                                    infoCli.noteImballaggio = Convert.ToString(reader["noteImballaggio"]);
                                }
                                else if (obj.GetType() == typeof(RMA_NoteQualita))
                                {
                                    if (noteQualita == null)
                                        noteQualita = new RMA_NoteQualita();
                                    noteQualita.idRMA = reader.GetString(1);
                                    noteQualita.sicurezzaProdotto = reader.GetString(2);
                                    noteQualita.segnalazIncidente = reader.GetString(3);
                                    noteQualita.invioNotaInf = reader.GetString(4);
                                    noteQualita.revDocValutazRischi = reader.GetString(5);
                                    noteQualita.statoAzCorrett = reader.GetString(6);
                                    noteQualita.rifAzCorrett = reader.GetString(7);
                                    noteQualita.descAzCorrett = reader.GetString(8);
                                    noteQualita.note = reader.GetString(9);
                                }
                                else if (obj.GetType() == typeof(RMA_GaranziaRalco))
                                {
                                    if (garanziaRalco == null)
                                        garanziaRalco = new RMA_GaranziaRalco();
                                    garanziaRalco.idRMA = reader.GetString(1);
                                    garanziaRalco.provCollimatore = reader.GetString(2);
                                    garanziaRalco.anniVitaCollimatore = reader.GetString(3);
                                    garanziaRalco.respDannoCollimatore = reader.GetString(4);
                                    garanziaRalco.noteGaranziaRalco = reader.GetString(5);
                                    garanziaRalco.statoGaranzia = reader.GetString(6);
                                    garanziaRalco.azioniSupplRalco = reader.GetString(7);
                                }
                                else if (obj.GetType() == typeof(RMA_Preventivo))
                                {
                                    prev = new RMA_Preventivo();
                                    prev.numPreventivo = reader.GetInt32(2);
                                    prev.revisionePrev = reader.GetString(3);
                                    prev.dataAggPrev = reader.GetDateTime(4);
                                    prev.stato = reader.GetString(5);
                                    prev.totale = reader.GetDecimal(6);
                                    prev.totaleCostoRalco = reader.GetDecimal(7);
                                    prev.nomeTecnicoRalco = reader.GetString(8);
                                }
                                else if (obj.GetType() == typeof(RMA_Problema))
                                {
                                    prob = new RMA_Problema();
                                    prob.idProblema = reader.GetInt32(0);
                                    prob.idRMA = reader.GetString(1);
                                    prob.dataAggProb = reader.GetDateTime(2);
                                    prob.faseProduzione = reader.GetString(3);
                                    prob.codParteControllo = reader.GetString(4);
                                    prob.rifFlowChart = reader.GetString(5);
                                    prob.causaProblema = reader.GetString(6);
                                    prob.gravitaProblema = reader.GetString(7);
                                    if (prob.nomeResponsabile == null)
                                        prob.nomeResponsabile = new List<string>();
                                    if (!reader.IsDBNull(8))
                                        prob.nomeResponsabile.Add(reader.GetString(8));
                                    //prob.nomeResponsabile = reader.GetString(8);
                                    prob.nomeTecnicoRalco = reader.GetString(9);
                                    prob.faseIndividuazioneProblema = reader.GetString(10);
                                    prob.difRiscDaRalco = reader.GetString(11);
                                    //aggiungo altri 4 nomi responsabili possibili
                                    if (!reader.IsDBNull(12))
                                        prob.nomeResponsabile.Add(reader.GetString(12));
                                    if (!reader.IsDBNull(13))
                                        prob.nomeResponsabile.Add(reader.GetString(13));
                                    if (!reader.IsDBNull(14))
                                        prob.nomeResponsabile.Add(reader.GetString(14));
                                    if (!reader.IsDBNull(15))
                                        prob.nomeResponsabile.Add(reader.GetString(15));
                                    if (!reader.IsDBNull(16))
                                        prob.descParte = reader.GetString(16);
                                    else
                                        prob.descParte = "";
                                }
                                else if (obj.GetType() == typeof(RMA_Riparazione))
                                {
                                    rip = new RMA_Riparazione();
                                    rip.idRiparazione = reader.GetInt32(0);
                                    rip.idRMA = reader.GetString(1);
                                    rip.dataAggRip = reader.GetDateTime(2);
                                    rip.descRip = reader.GetString(3);
                                    rip.idParte = reader.GetString(4);
                                    rip.descParte = reader.GetString(5);
                                    rip.quantita = reader.GetInt16(6);
                                    rip.statoRip = reader.GetString(7);
                                    rip.noteRip = reader.GetString(8);
                                    rip.nomeTecnicoRalco = reader.GetString(9);
                                    rip.prezzoUnitario = reader.GetDecimal(10);
                                }
                                //leggo dalla tabella dei collimatori per i quali esistono degli RMA
                                else if (obj.GetType() == typeof(List<RMA_Problema>))
                                {
                                    if (probList == null)
                                        probList = new List<RMA_Problema>();
                                    probList.Add(new RMA_Problema());
                                    probList[probList.Count - 1].idProblema = reader.GetInt32(0);
                                    probList[probList.Count - 1].idRMA = reader.GetString(1);
                                    probList[probList.Count - 1].dataAggProb = reader.GetDateTime(2);
                                    probList[probList.Count - 1].faseProduzione = reader.GetString(3);
                                    probList[probList.Count - 1].codParteControllo = reader.GetString(4);
                                    probList[probList.Count - 1].rifFlowChart = reader.GetString(5);
                                    probList[probList.Count - 1].causaProblema = reader.GetString(6);
                                    probList[probList.Count - 1].gravitaProblema = reader.GetString(7);
                                    if (probList[probList.Count - 1].nomeResponsabile == null)
                                        probList[probList.Count - 1].nomeResponsabile = new List<string>();
                                    if (!reader.IsDBNull(8))
                                        probList[probList.Count - 1].nomeResponsabile.Add(reader.GetString(8));
                                    probList[probList.Count - 1].nomeTecnicoRalco = reader.GetString(9);
                                    probList[probList.Count - 1].faseIndividuazioneProblema = reader.GetString(10);
                                    probList[probList.Count - 1].difRiscDaRalco = reader.GetString(11);
                                    if (!reader.IsDBNull(12))
                                        probList[probList.Count - 1].nomeResponsabile.Add(reader.GetString(12));
                                    if (!reader.IsDBNull(13))
                                        probList[probList.Count - 1].nomeResponsabile.Add(reader.GetString(13));
                                    if (!reader.IsDBNull(14))
                                        probList[probList.Count - 1].nomeResponsabile.Add(reader.GetString(14));
                                    if (!reader.IsDBNull(15))
                                        probList[probList.Count - 1].nomeResponsabile.Add(reader.GetString(15));
                                    if (!reader.IsDBNull(16))
                                        probList[probList.Count - 1].descParte = reader.GetString(16);
                                    else
                                        probList[probList.Count - 1].descParte = "";
                                }
                                //leggo dalla tabella dei collimatori per i quali esistono degli RMA
                                else if (obj.GetType() == typeof(List<RMA_Preventivo>))
                                {
                                    if (prevList == null)
                                        prevList = new List<RMA_Preventivo>();
                                    prevList.Add(new RMA_Preventivo());
                                    prevList[prevList.Count - 1].idProgressivo = reader.GetInt32(0);
                                    prevList[prevList.Count - 1].idRMA = reader.GetString(1);
                                    prevList[prevList.Count - 1].numPreventivo = reader.GetInt32(2);
                                    prevList[prevList.Count - 1].revisionePrev = reader.GetString(3);
                                    prevList[prevList.Count - 1].dataAggPrev = reader.GetDateTime(4);
                                    prevList[prevList.Count - 1].stato = reader.GetString(5);
                                    prevList[prevList.Count - 1].totale = reader.GetDecimal(6);
                                    prevList[prevList.Count - 1].totaleCostoRalco = reader.GetDecimal(7);
                                    prevList[prevList.Count - 1].nomeTecnicoRalco = reader.GetString(8);
                                }
                                else if (obj.GetType() == typeof(List<RMA_Riparazione>))
                                {
                                    if (ripList == null)
                                        ripList = new List<RMA_Riparazione>();
                                    ripList.Add(new RMA_Riparazione());
                                    ripList[ripList.Count - 1].idRiparazione = reader.GetInt32(0);
                                    ripList[ripList.Count - 1].idRMA = reader.GetString(1);
                                    ripList[ripList.Count - 1].dataAggRip = reader.GetDateTime(2);
                                    ripList[ripList.Count - 1].descRip = reader.GetString(3);
                                    ripList[ripList.Count - 1].idParte = reader.GetString(4);
                                    ripList[ripList.Count - 1].descParte = reader.GetString(5);
                                    ripList[ripList.Count - 1].quantita = reader.GetInt16(6);
                                    ripList[ripList.Count - 1].statoRip = reader.GetString(7);
                                    ripList[ripList.Count - 1].noteRip = reader.GetString(8);
                                    ripList[ripList.Count - 1].nomeTecnicoRalco = reader.GetString(9);
                                    ripList[ripList.Count - 1].prezzoUnitario = reader.GetDecimal(10);
                                }
                                else if (obj.GetType() == typeof(List<String>))
                                {
                                    if (!reader.IsDBNull(0))
                                        listString.Add(reader.GetString(0));
                                }
                                else if (obj.GetType() == typeof(string))
                                    singoloDatoString = reader.GetString(0);
                                else if (obj.GetType() == typeof(decimal))
                                    singoloDatoDecimal = reader.GetDecimal(0);
                                else if (obj.GetType() == typeof(int))
                                    singoloDatoInt = reader.GetInt32(0);
                                else if (obj.GetType() == typeof(DateTime))
                                    date = reader.GetDateTime(0);
                                else if (obj.GetType() == typeof(RMA_Articolo))
                                {
                                    art = new RMA_Articolo();
                                    art.idArticolo = reader.GetString(1);
                                    art.descrizione = reader.GetString(2);
                                    if (!reader.IsDBNull(4))
                                        art.prezzoUnitario = reader.GetDecimal(4);
                                    else
                                        art.prezzoUnitario = 0;
                                }
                                else if (obj.GetType() == typeof(List<RMA_Articolo>))
                                {
                                    if (artList == null)
                                        artList = new List<RMA_Articolo>();
                                    artList.Add(new RMA_Articolo());
                                    artList[artList.Count - 1].idArticolo = reader.GetString(1);
                                    artList[artList.Count - 1].descrizione = reader.GetString(2);
                                    if (!reader.IsDBNull(3))
                                        artList[artList.Count - 1].quantita = reader.GetInt16(3);
                                    else
                                        artList[artList.Count - 1].quantita = 1;
                                    if (!reader.IsDBNull(4))
                                        artList[artList.Count - 1].prezzoUnitario = reader.GetDecimal(4);
                                    else
                                        artList[artList.Count - 1].prezzoUnitario = 0;
                                }
                                else if (obj.GetType() == typeof(List<RALCO_ModelliProduttivi>))
                                {
                                    if (modProdList == null)
                                        modProdList = new List<RALCO_ModelliProduttivi>();
                                    modProdList.Add(new RALCO_ModelliProduttivi());
                                    modProdList[modProdList.Count - 1].padre = reader.GetString(0);
                                    modProdList[modProdList.Count - 1].descPadre = reader.GetString(1);
                                    modProdList[modProdList.Count - 1].figlio = reader.GetString(2);
                                    modProdList[modProdList.Count - 1].descFiglio = reader.GetString(3);
                                    if (!reader.IsDBNull(4))
                                        modProdList[modProdList.Count - 1].costo = reader.GetDecimal(4);
                                    else
                                        modProdList[modProdList.Count - 1].costo = 0;
                                }
                                else if (obj.GetType() == typeof(List<RMA_ClientiRalco>))
                                {
                                    if (cliList == null)
                                        cliList = new List<RMA_ClientiRalco>();
                                    cliList.Add(new RMA_ClientiRalco());
                                    cliList[cliList.Count - 1].IdCliente = reader.GetInt32(0);
                                    cliList[cliList.Count - 1].nomeCliente = reader.GetString(1);
                                    if (!reader.IsDBNull(2))
                                        cliList[cliList.Count - 1].scontoCli = reader.GetDecimal(2);
                                    else
                                        cliList[cliList.Count - 1].scontoCli = 0;
                                    if (!reader.IsDBNull(3))
                                        cliList[cliList.Count - 1].scontoMod = reader.GetDecimal(3);
                                    else
                                        cliList[cliList.Count - 1].scontoMod = 0;
                                }
                                else if (obj.GetType() == typeof(List<RMA_Log>))
                                {
                                    if (logList == null)
                                        logList = new List<RMA_Log>();
                                    logList.Add(new RMA_Log());
                                    logList[logList.Count - 1].idProgressivo = reader.GetInt64(0);
                                    logList[logList.Count - 1].idRMA = reader.GetString(1);
                                    logList[logList.Count - 1].dataAggiornamento = reader.GetDateTime(2);
                                    logList[logList.Count - 1].aggiornatoDa = reader.GetString(3);
                                    logList[logList.Count - 1].stato = reader.GetString(4);
                                    logList[logList.Count - 1].note = reader.GetString(5);
                                }
                                else if (obj.GetType() == typeof(RMA_FaseProduzione))
                                {
                                    if (faseProd == null)
                                        faseProd = new RMA_FaseProduzione();
                                    if (!reader.IsDBNull(0))
                                        faseProd.idProgressivo = reader.GetInt64(0);
                                    if (!reader.IsDBNull(1))
                                        faseProd.idRMA = reader.GetString(1);
                                    if (faseProd.minFaseProd == null)
                                        faseProd.minFaseProd = new List<int>();
                                    if (faseProd.minFaseProd.Count.Equals(0))
                                        for (int i = 0; i < 12; i++)
                                            faseProd.minFaseProd.Add(new int());
                                    for (int i = 0; i < 12; i++)
                                        faseProd.minFaseProd[i] = reader.GetInt32(i + 2);
                                }
                                else if (obj.GetType() == typeof(List<RMA_TestDaEseguire>))
                                {
                                    if (testDaEseguireList == null)
                                        testDaEseguireList = new List<RMA_TestDaEseguire>();
                                    testDaEseguireList.Add(new RMA_TestDaEseguire());
                                    if (!reader.IsDBNull(0))
                                        testDaEseguireList[testDaEseguireList.Count - 1].idProgressivo = reader.GetInt64(0);
                                    if (!reader.IsDBNull(1))
                                        testDaEseguireList[testDaEseguireList.Count - 1].idRMA = reader.GetString(1);
                                    if (!reader.IsDBNull(2))
                                        testDaEseguireList[testDaEseguireList.Count - 1].descTestDaEseguire = reader.GetString(2);
                                }
                                else if (obj.GetType() == typeof(List<RMA_EsitiTestPreAnalisi>))
                                {
                                    if (esitiTestPreAnalisi == null)
                                        esitiTestPreAnalisi = new List<RMA_EsitiTestPreAnalisi>();
                                    esitiTestPreAnalisi.Add(new RMA_EsitiTestPreAnalisi());
                                    if (!reader.IsDBNull(0))
                                        esitiTestPreAnalisi[esitiTestPreAnalisi.Count - 1].idProgressivo = reader.GetInt64(0);
                                    if (!reader.IsDBNull(1))
                                        esitiTestPreAnalisi[esitiTestPreAnalisi.Count - 1].idRMA = reader.GetString(1);
                                    if (!reader.IsDBNull(2))
                                        esitiTestPreAnalisi[esitiTestPreAnalisi.Count - 1].idProblema = reader.GetInt32(2);
                                    if (!reader.IsDBNull(3))
                                        esitiTestPreAnalisi[esitiTestPreAnalisi.Count - 1].descTestDaEseguire = reader.GetString(3);
                                    if (!reader.IsDBNull(4))
                                        esitiTestPreAnalisi[esitiTestPreAnalisi.Count - 1].esitoTest = reader.GetString(4);
                                }
                                else if (obj.GetType() == typeof(List<RMA_EsitiAzioniSupplementari>))
                                {
                                    if (esitiAzioniSupplementari == null)
                                        esitiAzioniSupplementari = new List<RMA_EsitiAzioniSupplementari>();
                                    esitiAzioniSupplementari.Add(new RMA_EsitiAzioniSupplementari());
                                    if (!reader.IsDBNull(0))
                                        esitiAzioniSupplementari[esitiAzioniSupplementari.Count - 1].idProgressivo = reader.GetInt64(0);
                                    if (!reader.IsDBNull(1))
                                        esitiAzioniSupplementari[esitiAzioniSupplementari.Count - 1].idRMA = reader.GetString(1);
                                    if (!reader.IsDBNull(2))
                                        esitiAzioniSupplementari[esitiAzioniSupplementari.Count - 1].descAzSuppl = reader.GetString(2);
                                    if (!reader.IsDBNull(3))
                                        esitiAzioniSupplementari[esitiAzioniSupplementari.Count - 1].esitoAzSuppl = reader.GetString(3);
                                }
                                else if (obj.GetType() == typeof(List<RMA_DifettoRiscontrato>))
                                {
                                    if (difList == null)
                                        difList = new List<RMA_DifettoRiscontrato>();
                                    difList.Add(new RMA_DifettoRiscontrato());
                                    if (!reader.IsDBNull(0))
                                        difList[difList.Count - 1].fase = reader.GetString(0);
                                    else
                                        difList[difList.Count - 1].fase = "";
                                    if (!reader.IsDBNull(1))
                                        difList[difList.Count - 1].descDifetto = reader.GetString(1);
                                    else
                                        difList[difList.Count - 1].descDifetto = "";
                                    if (!reader.IsDBNull(2))
                                        difList[difList.Count - 1].codParte = reader.GetString(2);
                                    else
                                        difList[difList.Count - 1].codParte = "";
                                }
                                else if (obj.GetType() == typeof(List<RMA_CostiExtra>))
                                {
                                    if (costiExtraList == null)
                                        costiExtraList = new List<RMA_CostiExtra>();
                                    costiExtraList.Add(new RMA_CostiExtra());
                                    costiExtraList[costiExtraList.Count - 1].idExtra = reader.GetInt32(0);
                                    costiExtraList[costiExtraList.Count - 1].descrizione = reader.GetString(1);
                                    if (!reader.IsDBNull(2))
                                        costiExtraList[costiExtraList.Count - 1].prezzoUnitario = reader.GetDecimal(2);
                                    else
                                        costiExtraList[costiExtraList.Count - 1].prezzoUnitario = 0;
                                    if (!reader.IsDBNull(3))
                                        costiExtraList[costiExtraList.Count - 1].quantita = reader.GetInt16(3);
                                    else
                                        costiExtraList[costiExtraList.Count - 1].quantita = 1;
                                }
                                else if (obj.GetType() == typeof(RMA_CostiExtra))
                                {
                                    costoExtra = new RMA_CostiExtra();
                                    costoExtra.idExtra = reader.GetInt32(0);
                                    costoExtra.descrizione = reader.GetString(1);
                                    if (!reader.IsDBNull(2))
                                        costoExtra.prezzoUnitario = reader.GetDecimal(2);
                                    else
                                        costoExtra.prezzoUnitario = 0;
                                    if (!reader.IsDBNull(3))
                                        costoExtra.quantita = reader.GetInt16(3);
                                    else
                                        costoExtra.quantita = 1;
                                }
                                else if (obj.GetType() == typeof(List<RMA_AzioneSupplementare>))
                                {
                                    if (azSupplList == null)
                                        azSupplList = new List<RMA_AzioneSupplementare>();
                                    azSupplList.Add(new RMA_AzioneSupplementare());
                                    azSupplList[azSupplList.Count - 1].nomeCliente = Convert.ToString(reader["nomeCliente"]);
                                    azSupplList[azSupplList.Count - 1].modelloColl = Convert.ToString(reader["modelloColl"]);
                                    azSupplList[azSupplList.Count - 1].descrizioneAzioneSuppl = Convert.ToString(reader["descrizioneAzioneSuppl"]);
                                }
                                else if (obj.GetType() == typeof(List<RMA_Allegato>))
                                {
                                    if (allegatiList == null)
                                        allegatiList = new List<RMA_Allegato>();
                                    allegatiList.Add(new RMA_Allegato());
                                    allegatiList[allegatiList.Count - 1].idProgressivo = Convert.ToInt32(reader["idProgressivo"]);
                                    allegatiList[allegatiList.Count - 1].idRMA = Convert.ToString(reader["idRMA"]);
                                    allegatiList[allegatiList.Count - 1].pathAllegato = Convert.ToString(reader["pathAllegato"]);
                                }
                            }
                            command.Connection.Close();
                        }
                        if (obj == null)
                            return null;
                        else
                        {
                            if (obj.GetType() == typeof(List<Utente>))
                            {
                                retObject = null;
                                Convert.ChangeType(retObject, typeof(List<Utente>));
                                retObject = users;
                            }
                            else if (obj.GetType() == typeof(RMA_View))
                            {
                                retObject = null;
                                Convert.ChangeType(retObject, typeof(RMA_View));
                                retObject = rmaView;
                            }
                            else if (obj.GetType() == typeof(List<RMA_View>))
                            {
                                retObject = null;
                                Convert.ChangeType(retObject, typeof(List<RMA_View>));
                                retObject = rmaList;
                            }
                            else if (obj.GetType() == typeof(RMA_Collimatore))
                            {
                                retObject = null;
                                Convert.ChangeType(retObject, typeof(RMA_Collimatore));
                                retObject = coll;
                            }
                            else if (obj.GetType() == typeof(List<RMA_Collimatore>))
                            {
                                retObject = null;
                                Convert.ChangeType(retObject, typeof(List<RMA_Collimatore>));
                                retObject = collList;
                            }
                            else if (obj.GetType() == typeof(RMA_Parte))
                            {
                                retObject = null;
                                Convert.ChangeType(retObject, typeof(RMA_Parte));
                                retObject = part;
                            }
                            else if (obj.GetType() == typeof(List<RMA_Parte>))
                            {
                                retObject = null;
                                Convert.ChangeType(retObject, typeof(List<RMA_Parte>));
                                retObject = partList;
                            }
                            else if (obj.GetType() == typeof(RMA_InformativaCliente))
                            {
                                retObject = null;
                                Convert.ChangeType(retObject, typeof(RMA_InformativaCliente));
                                retObject = infoCli;
                            }
                            else if (obj.GetType() == typeof(RMA_NoteQualita))
                            {
                                retObject = null;
                                Convert.ChangeType(retObject, typeof(RMA_NoteQualita));
                                retObject = noteQualita;
                            }
                            else if (obj.GetType() == typeof(RMA_GaranziaRalco))
                            {
                                retObject = null;
                                Convert.ChangeType(retObject, typeof(RMA_GaranziaRalco));
                                retObject = garanziaRalco;
                            }
                            else if (obj.GetType() == typeof(string))
                            {
                                retObject = null;
                                Convert.ChangeType(retObject, typeof(string));
                                retObject = singoloDatoString;
                            }
                            else if (obj.GetType() == typeof(decimal))
                            {
                                retObject = 0;
                                Convert.ChangeType(retObject, typeof(decimal));
                                retObject = singoloDatoDecimal;
                            }
                            else if (obj.GetType() == typeof(int))
                            {
                                retObject = 0;
                                Convert.ChangeType(retObject, typeof(Int32));
                                retObject = singoloDatoInt;
                            }
                            else if (obj.GetType() == typeof(DateTime))
                            {
                                retObject = 0;
                                Convert.ChangeType(retObject, typeof(DateTime));
                                retObject = date;
                            }
                            else if (obj.GetType() == typeof(RMA_Articolo))
                            {
                                retObject = null;
                                Convert.ChangeType(retObject, typeof(RMA_Articolo));
                                retObject = art;
                            }
                            else if (obj.GetType() == typeof(List<RMA_Articolo>))
                            {
                                retObject = null;
                                Convert.ChangeType(retObject, typeof(List<RMA_Articolo>));
                                retObject = artList;
                            }
                            else if (obj.GetType() == typeof(RMA_Preventivo))
                            {
                                retObject = null;
                                Convert.ChangeType(retObject, typeof(RMA_Preventivo));
                                retObject = prev;
                            }
                            else if (obj.GetType() == typeof(RMA_Problema))
                            {
                                retObject = null;
                                Convert.ChangeType(retObject, typeof(RMA_Problema));
                                retObject = prob;
                            }
                            else if (obj.GetType() == typeof(List<RMA_Problema>))
                            {
                                retObject = null;
                                Convert.ChangeType(retObject, typeof(List<RMA_Problema>));
                                retObject = probList;
                            }
                            else if (obj.GetType() == typeof(List<RMA_Preventivo>))
                            {
                                retObject = null;
                                Convert.ChangeType(retObject, typeof(List<RMA_Preventivo>));
                                retObject = prevList;
                            }
                            else if (obj.GetType() == typeof(List<String>))
                            {
                                retObject = null;
                                Convert.ChangeType(retObject, typeof(List<String>));
                                retObject = listString;
                            }
                            else if (obj.GetType() == typeof(RMA_Generali))
                            {
                                retObject = null;
                                Convert.ChangeType(retObject, typeof(RMA_Generali));
                                retObject = rmaGen;
                            }
                            else if (obj.GetType() == typeof(List<RMA_Riparazione>))
                            {
                                retObject = null;
                                Convert.ChangeType(retObject, typeof(List<RMA_Riparazione>));
                                retObject = ripList;
                            }
                            else if (obj.GetType() == typeof(List<RMA_ClientiRalco>))
                            {
                                retObject = null;
                                Convert.ChangeType(retObject, typeof(List<RMA_ClientiRalco>));
                                retObject = cliList;
                            }
                            else if (obj.GetType() == typeof(List<RMA_Log>))
                            {
                                retObject = null;
                                Convert.ChangeType(retObject, typeof(List<RMA_Log>));
                                retObject = logList;
                            }
                            else if (obj.GetType() == typeof(RMA_FaseProduzione))
                            {
                                retObject = null;
                                Convert.ChangeType(retObject, typeof(RMA_FaseProduzione));
                                retObject = faseProd;
                            }
                            else if (obj.GetType() == typeof(List<RMA_TestDaEseguire>))
                            {
                                retObject = null;
                                Convert.ChangeType(retObject, typeof(List<RMA_TestDaEseguire>));
                                retObject = testDaEseguireList;
                            }
                            else if (obj.GetType() == typeof(List<RMA_EsitiTestPreAnalisi>))
                            {
                                retObject = null;
                                Convert.ChangeType(retObject, typeof(List<RMA_EsitiTestPreAnalisi>));
                                retObject = esitiTestPreAnalisi;
                            }
                            else if (obj.GetType() == typeof(List<RMA_EsitiAzioniSupplementari>))
                            {
                                retObject = null;
                                Convert.ChangeType(retObject, typeof(List<RMA_EsitiTestPreAnalisi>));
                                retObject = esitiAzioniSupplementari;
                            }
                            else if (obj.GetType() == typeof(List<RMA_DifettoRiscontrato>))
                            {
                                retObject = null;
                                Convert.ChangeType(retObject, typeof(List<RMA_DifettoRiscontrato>));
                                retObject = difList;
                            }
                            else if (obj.GetType() == typeof(List<RALCO_ModelliProduttivi>))
                            {
                                retObject = null;
                                Convert.ChangeType(retObject, typeof(List<RALCO_ModelliProduttivi>));
                                retObject = modProdList;
                            }
                            else if (obj.GetType() == typeof(List<RMA_CostiExtra>))
                            {
                                retObject = null;
                                Convert.ChangeType(retObject, typeof(List<RMA_CostiExtra>));
                                retObject = costiExtraList;
                            }
                            else if (obj.GetType() == typeof(RMA_CostiExtra))
                            {
                                retObject = null;
                                Convert.ChangeType(retObject, typeof(RMA_CostiExtra));
                                retObject = costoExtra;
                            }
                            else if (obj.GetType() == typeof(List<RMA_AzioneSupplementare>))
                            {
                                retObject = null;
                                Convert.ChangeType(retObject, typeof(List<RMA_AzioneSupplementare>));
                                retObject = azSupplList;
                            }
                            else if (obj.GetType() == typeof(List<RMA_Allegato>))
                            {
                                retObject = null;
                                Convert.ChangeType(retObject, typeof(List<RMA_Allegato>));
                                retObject = allegatiList;
                            }
                            return (retObject);
                        }
                    }
                }
                return (null);
                Cursor.Current = Cursors.Default;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return (null);
            }
        }
    }
}
