﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ralco_ServiceManager
{
    public class RMA_EsitiAzioniSupplementari : IEquatable<RMA_EsitiAzioniSupplementari>
    {
        public Int64 idProgressivo { get; set; }
        public string idRMA { get; set; }
        public string descAzSuppl { get; set; }
        public string esitoAzSuppl { get; set; }

        //costruttore di default
        public RMA_EsitiAzioniSupplementari()
        {
            //do nothing...
        }

        //overload costruttore di default
        public RMA_EsitiAzioniSupplementari(RMA_EsitiAzioniSupplementari test)
        {
            this.idProgressivo = test.idProgressivo;
            this.idRMA = test.idRMA;
            this.descAzSuppl = test.descAzSuppl;
            this.esitoAzSuppl = test.esitoAzSuppl;
        }

        //overload costruttore di default
        public RMA_EsitiAzioniSupplementari(string idRMA, string descTestDaEseguire, string esitoTest)
        {
            this.idRMA = idRMA;
            this.descAzSuppl = descTestDaEseguire;
            this.esitoAzSuppl = esitoTest;
        }

        public bool Equals(RMA_EsitiAzioniSupplementari obj)
        {
            if (obj == null)
                return false;
            else if (obj.idRMA.Equals(this.idRMA) && obj.descAzSuppl.Equals(this.descAzSuppl))
                return true;
            else
                return false;
        }

    }
}
