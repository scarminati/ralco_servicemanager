﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Ralco_ServiceManager
{
    public partial class Form_Parte : Form
    {
        public List<RMA_Parte> partList;
        private List<RMA_Articolo> artList;
        private List<RMA_ClientiRalco> cliList;
        private List<String> seriali;
        private SQLConnector conn;
        private string idRMA;
        private bool partListToUpdate = false;
        public Form_Parte(string idRMA)
        {
            try
            {
                InitializeComponent();
                conn = new SQLConnector();
                setWidgetDescriptions();
                this.idRMA = idRMA;
                seriali = new List<string>();
                partList = new List<RMA_Parte>();
                initComboboxValues();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public Form_Parte(List<RMA_Parte> partList)
        {
            try
            {
                InitializeComponent();
                conn = new SQLConnector();
                setWidgetDescriptions();
                initComboboxValues();
                this.partList = new List<RMA_Parte>();
                foreach (RMA_Parte p in partList)
                    this.partList.Add(p);
                partListToUpdate = true;
                setFormValues();
                nud_Quantita.Enabled = false;//modifico solo una parte per volta
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public Form_Parte(string idRMA, List<RMA_Parte> partList)
        {
            try
            {
                InitializeComponent();
                conn = new SQLConnector();
                this.partList = new List<RMA_Parte>();
                foreach (RMA_Parte p in partList)
                    this.partList.Add(p);
                this.idRMA = idRMA;
                commitDB();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void setWidgetDescriptions()
        {
            try
            {
                #region Form
                this.Text = Utils.resourcemanager.GetString("Form_Parte");
                #endregion
                #region TooltipsPerBottoni
                ToolTip tooltip = new System.Windows.Forms.ToolTip();
                tooltip.SetToolTip(btn_OK, Utils.resourcemanager.GetString("btn_OK"));
                tooltip.SetToolTip(btn_cancel, Utils.resourcemanager.GetString("btn_cancel"));
                tooltip.SetToolTip(btn_AddSeriale, Utils.resourcemanager.GetString("btn_AddSeriale"));
                tooltip.SetToolTip(btn_RemoveSeriale, Utils.resourcemanager.GetString("btn_RemoveSeriale"));
                #endregion
                #region Label
                lbl_IDParte.Text = Utils.resourcemanager.GetString("lbl_IDParte");
                lbl_Desc.Text = Utils.resourcemanager.GetString("lbl_Desc");
                lbl_qta.Text = Utils.resourcemanager.GetString("lbl_qta");
                lbl_FC.Text = Utils.resourcemanager.GetString("lbl_FC");
                lbl_nomeCli.Text = Utils.resourcemanager.GetString("lbl_nomeCli");
                lbl_numBolla.Text = Utils.resourcemanager.GetString("lbl_numBolla");
                lbl_dataBolla.Text = Utils.resourcemanager.GetString("lbl_dataBolla");
                lbl_dataVendita.Text = Utils.resourcemanager.GetString("lbl_dataVendita");
                #endregion
                #region DataGridViewColumns
                dgv_listaSeriali.Columns["numSer"].HeaderText = Utils.resourcemanager.GetString("dgv_listaSeriali_numSer");
                dgv_ListaRifRSRO.Columns["codice"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaNonConf_IDRMA");
                dgv_ListaRifRSRO.Columns["descr"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaNonConf_Cliente");
                #endregion
                #region Groupbox
                grp_RSRO_Rif.Text = Utils.resourcemanager.GetString("grp_RSRO_Rif");
                #endregion
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void setFormValues()
        {
            try
            {
                if (partList != null)
                {
                    cmb_IDParti.Text = partList[0].idParte;
                    dgv_listaSeriali.Rows.Insert(0, true, partList[0].serialeParte);
                    nud_Quantita.Value = partList.Count;
                    tb_FC.Text = partList[0].numFlowChart;
                    cmb_nomeCliente.Text = partList[0].nomeCliente;
                    tb_numBolla.Text = partList[0].numBolla;
                    if (!partList[0].dataBolla.Equals(DateTime.MinValue))
                        tb_dataBolla.Text = partList[0].dataBolla.ToShortDateString();
                    else
                        tb_dataBolla.Text = "";
                    if (!partList[0].dataVendita.Equals(DateTime.MinValue))
                        tb_dataVendita.Text = partList[0].dataVendita.ToShortDateString();
                    else
                        tb_dataVendita.Text = "";
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void initComboboxValues()
        {
            try
            {
                artList = new List<RMA_Articolo>();
                cliList = new List<RMA_ClientiRalco>();
                artList = (List<RMA_Articolo>)conn.CreateCommand("SELECT DISTINCT ID_AZIENDA, ID_ARTICOLO, UPPER (DESCRIZIONE), ID_SCELTA, ID_SCELTA FROM [archivi].[dbo].[vistaListaArticoliPanth] WHERE ID_LINGUA = 'it' ORDER BY ID_ARTICOLO", artList);
                cliList = (List<RMA_ClientiRalco>)conn.CreateCommand("SELECT * FROM [archivi].[dbo].[VISTA_SM_CLIENTI] order by ID", cliList);
                if (artList != null)
                {
                    cmb_IDParti.Items.Clear();
                    foreach (RMA_Articolo a in artList)
                        cmb_IDParti.Items.Add(a.idArticolo);
                }
                if (cliList != null)
                {
                    cmb_nomeCliente.Items.Clear();
                    foreach (RMA_ClientiRalco c in cliList)
                        cmb_nomeCliente.Items.Add(c.nomeCliente);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cmb_IDParti_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cmb_IDParti.SelectedItem != null)
                {
                    lbl_descrizione.Text = artList.Single(c => c.idArticolo.Equals(cmb_IDParti.SelectedItem.ToString())).descrizione;
                    dgv_ListaRifRSRO.Rows.Clear();
                    List<RMA_Articolo> rsro_Rif = new List<RMA_Articolo>();
                    rsro_Rif = (List<RMA_Articolo>)conn.CreateCommand("SELECT [ID_AZIENDA],[PADRE],[DSC_PADRE],[PROGRESSIVO],[COSTO] FROM [archivi].[dbo].[VISTA_SM_MODPRO_PANTH] WHERE [FIGLIO] = '" + cmb_IDParti.SelectedItem.ToString() + "'", rsro_Rif);
                    if (rsro_Rif != null)
                    {
                        foreach (RMA_Articolo r in rsro_Rif)
                            if (r.idArticolo.StartsWith("RS") || r.idArticolo.StartsWith("RO"))
                                dgv_ListaRifRSRO.Rows.Insert(0, false, r.idArticolo, r.descrizione);
                    }
                }
                else
                    MessageBox.Show("ID articolo non corretto", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dtp_dataBolla_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                tb_dataBolla.Text = dtp_dataBolla.Value.ToShortDateString();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dtp_dataVendita_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                tb_dataVendita.Text = dtp_dataVendita.Value.ToShortDateString();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_AddSeriale_Click(object sender, EventArgs e)
        {
            try
            {
                using (var form = new Form_Seriale())
                {
                    var result = form.ShowDialog();
                    if (result == DialogResult.OK)
                    {
                        this.seriali.AddRange(form.seriali);
                    }
                }
                refreshDataGridView();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_CancelSeriale_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow row in dgv_listaSeriali.Rows)
                {
                    if (row.Cells["chk_Ser"].Value != null && row.Cells["chk_Ser"].Value.Equals(true))
                    {
                        var itemToRemove = row.Cells["numSer"].Value;
                        if (itemToRemove != null)
                        {
                            seriali.Remove(itemToRemove.ToString());
                        }
                    }
                }
                refreshDataGridView();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void refreshDataGridView()
        {
            try
            {
                dgv_listaSeriali.Rows.Clear();
                foreach (String s in seriali)
                    dgv_listaSeriali.Rows.Insert(0, true, s);
                nud_Quantita.Value = seriali.Count;
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void updateVariabilePubblica()
        {
            try
            {
                if (cmb_IDParti.SelectedItem != null)
                {
                    if (partList == null || partList.Count.Equals(0))
                    {
                        partList = new List<RMA_Parte>();
                        DateTime dataBolla;
                        if (tb_dataBolla.Text.Equals(""))
                            dataBolla = DateTime.MinValue;
                        else
                            dataBolla = dtp_dataBolla.Value;
                        DateTime dataVendita;
                        if (tb_dataVendita.Text.Equals(""))
                            dataVendita = DateTime.MinValue;
                        else
                            dataVendita = dtp_dataVendita.Value;
                        string nomeCliente = "";
                        if (!cmb_nomeCliente.SelectedIndex.Equals(-1))
                            nomeCliente = cmb_nomeCliente.SelectedItem.ToString();
                        if (seriali == null || seriali.Count.Equals(0))
                        {
                            partList.Add(new RMA_Parte(cmb_IDParti.SelectedItem.ToString(), lbl_descrizione.Text, "", nomeCliente, Utils.parseUselessChars(tb_numBolla.Text), dataBolla, dataVendita, Convert.ToInt16(nud_Quantita.Value), tb_FC.Text));
                        }
                        else
                        {
                            foreach (String s in seriali)
                                partList.Add(new RMA_Parte(cmb_IDParti.SelectedItem.ToString(), lbl_descrizione.Text, s, nomeCliente, Utils.parseUselessChars(tb_numBolla.Text), dataBolla, dataVendita, 1, tb_FC.Text));
                        }
                    }
                    else
                    {
                        //ammessa la modifica di 1 sola parte alla volta
                        partList[0].idParte = cmb_IDParti.Text;
                        partList[0].descParte = lbl_descrizione.Text;
                        partList[0].serialeParte = dgv_listaSeriali.Rows[0].Cells["numSer"].Value.ToString();
                        partList[0].numFlowChart = tb_FC.Text;
                        partList[0].nomeCliente = cmb_nomeCliente.Text;
                        partList[0].numBolla = tb_numBolla.Text;
                        if (!tb_dataBolla.Text.Equals(""))
                            partList[0].dataBolla = Convert.ToDateTime(tb_dataBolla.Text);
                        else
                            partList[0].dataBolla = DateTime.MinValue;
                        if (!tb_dataVendita.Text.Equals(""))
                            partList[0].dataVendita = Convert.ToDateTime(tb_dataVendita.Text);
                        else
                            partList[0].dataVendita = DateTime.MinValue;
                    }
                }
                else
                {
                    MessageBox.Show("Selezionare almeno una parte prima di procedere", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void commitDB()
        {
            try
            {
                foreach (RMA_Parte p in partList)
                {
                    if (!partListToUpdate)
                    {
                        p.idRMA = idRMA;
                        for (int i = 0; i < p.quantita; i++)
                        {
                            conn.CreateCommand("INSERT INTO [Autotest].[dbo].[RMA_Parte] " +
                            "([idRMA],[idParte],[descParte],[serialeParte],[nomeCliente],[numBolla],[dataBolla],[dataVendita],[quantita],[numFlowChart]) " +
                            "VALUES ('" + p.idRMA + "','" + p.idParte + "','" + p.descParte + "','" + p.serialeParte + "','" + p.nomeCliente + "','" + p.numBolla + "','" + p.dataBolla.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "','" + p.dataVendita.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "','" + "1" + "','" + p.numFlowChart + "')", null);
                            p.idProgressivo = (int)conn.CreateCommand("SELECT TOP(1) [idProgressivo] FROM [Autotest].[dbo].[RMA_Parte] ORDER BY idProgressivo DESC", p.idProgressivo);
                        }
                    }
                    else
                    {
                        conn.CreateCommand("UPDATE [Autotest].[dbo].[RMA_Parte] SET " +
                        "[idRMA] = '" + p.idRMA + "'" +
                        ",[idParte] = '" + p.idParte + "'" +
                        ",[descParte] = '" + p.descParte + "'" +
                        ",[serialeParte] = '" + p.serialeParte + "'" +
                        ",[nomeCliente] = '" + p.nomeCliente + "'" +
                        ",[numBolla] = '" + p.numBolla + "'" +
                        ",[dataBolla] = '" + p.dataBolla.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "'" +
                        ",[dataVendita] = '" + p.dataVendita.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "'" +
                        ",[quantita] = '" + p.quantita + "'" +
                        ",[numFlowChart] = '" + p.numFlowChart + "'" +
                        " WHERE ([Autotest].[dbo].[RMA_Parte].[idRMA] = '" + p.idRMA + "' AND [Autotest].[dbo].[RMA_Parte].[serialeParte] = '" + p.serialeParte + "')", null);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_saveAndClose_Click(object sender, EventArgs e)
        {
            try
            {
                updateVariabilePubblica();
                if (partList != null && !partList.Count.Equals(0))
                {
                    commitDB();
                    this.DialogResult = DialogResult.OK;
                    this.Dispose();
                }
                else
                    MessageBox.Show("Selezionare almeno una parte tra quelli disponibili", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_cancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.DialogResult = DialogResult.Cancel;
                this.Dispose();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
