﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ralco_ServiceManager
{
    public class RMA_EsitiTestPreAnalisi : IEquatable<RMA_EsitiTestPreAnalisi>
    {
        public Int64 idProgressivo { get; set; }
        public string idRMA { get; set; }
        public int idProblema { get; set; }
        public string descTestDaEseguire { get; set; }
        public string esitoTest { get; set; }

        //costruttore di default
        public RMA_EsitiTestPreAnalisi()
        {
            //do nothing...
        }

        //overload costruttore di default
        public RMA_EsitiTestPreAnalisi(RMA_EsitiTestPreAnalisi test)
        {
            this.idProgressivo = test.idProgressivo;
            this.idRMA = test.idRMA;
            this.idProblema = test.idProblema;
            this.descTestDaEseguire = test.descTestDaEseguire;
            this.esitoTest = test.esitoTest;
        }

        //overload costruttore di default
        public RMA_EsitiTestPreAnalisi(string idRMA, string descTestDaEseguire, string esitoTest)
        {
            this.idRMA = idRMA;
            this.idProblema = idProblema;
            this.descTestDaEseguire = descTestDaEseguire;
            this.esitoTest = esitoTest;
        }

        public bool Equals(RMA_EsitiTestPreAnalisi obj)
        {
            if (obj == null)
                return false;
            else if (obj.idRMA.Equals(this.idRMA) && obj.descTestDaEseguire.Equals(this.descTestDaEseguire))
                return true;
            else
                return false;
        }

    }
}
