﻿#define USEPANTHERA
//#undef USEPANTHERA
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace Ralco_ServiceManager
{
    public partial class Form_Collimatore : Form
    {
        public List<RMA_Collimatore> collimators { get; set; }
        private List<RMA_Collimatore> collList { get; set; }
        private SQLConnector conn;
        private string idRMA;
        private bool isJustForConsulting = false;

        public Form_Collimatore(string idRMA)
        {
            try
            {
                InitializeComponent();
                conn = new SQLConnector();
                this.idRMA = idRMA;
                setWidgetDescriptions();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public Form_Collimatore(string idRMA, List<RMA_Collimatore> collimators)
        {
            try
            {
                InitializeComponent();
                conn = new SQLConnector();
                this.collimators = new List<RMA_Collimatore>();
                foreach (RMA_Collimatore c in collimators)
                    this.collimators.Add(c);
                this.idRMA = idRMA;
                commitDB();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public Form_Collimatore()
        {
            try
            {
                InitializeComponent();
                conn = new SQLConnector();
                this.isJustForConsulting = true;
                setWidgetDescriptions();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void setWidgetDescriptions()
        {
            try
            {
                #region Form
                this.Text = Utils.resourcemanager.GetString("Form_Collimatore");
                #endregion
                #region TooltipsPerBottoni
                ToolTip tooltip = new System.Windows.Forms.ToolTip();
                tooltip.SetToolTip(btn_OK, Utils.resourcemanager.GetString("btn_OK"));
                tooltip.SetToolTip(btn_cancel, Utils.resourcemanager.GetString("btn_cancel"));
                tooltip.SetToolTip(btn_cercaColl, Utils.resourcemanager.GetString("btn_cercaColl"));
                tooltip.SetToolTip(btn_AddCollimators, Utils.resourcemanager.GetString("btn_AddCollimators"));
                #endregion
                #region Label
                lbl_SN.Text = Utils.resourcemanager.GetString("lbl_SN");
                lbl_FC.Text = Utils.resourcemanager.GetString("lbl_FC");
                lbl_fromSN.Text = Utils.resourcemanager.GetString("lbl_fromSN");
                lbl_toSN.Text = Utils.resourcemanager.GetString("lbl_toSN");
                #endregion
                #region Groupbox
                gb_cercaColl.Text = Utils.resourcemanager.GetString("gb_cercaColl");
                gb_addColl.Text = Utils.resourcemanager.GetString("gb_cercaColl");
                #endregion
                #region DataGridViewColumns
                dgv_ListaCollimatoriRMA.Columns["serialNr"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaCollimatoriRMA_serialNr");
                dgv_ListaCollimatoriRMA.Columns["modello"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaCollimatoriRMA_modello");
                dgv_ListaCollimatoriRMA.Columns["dataProd"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaCollimatoriRMA_dataProd");
                dgv_ListaCollimatoriRMA.Columns["cliente"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaCollimatoriRMA_cliente");
                dgv_ListaCollimatoriRMA.Columns["codColl"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaCollimatoriRMA_codColl");
                dgv_ListaCollimatoriRMA.Columns["numFC"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaCollimatoriRMA_numFC");
                dgv_ListaCollimatoriRMA.Columns["da_SN"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaCollimatoriRMA_da_SN");
                dgv_ListaCollimatoriRMA.Columns["al_SN"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaCollimatoriRMA_al_SN");
                dgv_ListaCollimatoriRMA.Columns["qta"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaCollimatoriRMA_qta");
                dgv_ListaCollimatoriRMA.Columns["rmaAttivi"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaCollimatoriRMA_rmaAttivi");
                #endregion
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgv_ListaCollimatoriRMA_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgv_ListaCollimatoriRMA.CurrentCell.ColumnIndex.Equals(0))
                {
                    if (dgv_ListaCollimatoriRMA.CurrentCell.Value == (null) || dgv_ListaCollimatoriRMA.CurrentCell.Value.Equals(false))
                        dgv_ListaCollimatoriRMA.CurrentCell.Value = true;
                    else
                        dgv_ListaCollimatoriRMA.CurrentCell.Value = false;
                }
                else if (dgv_ListaCollimatoriRMA.CurrentCell.ColumnIndex.Equals(10))
                {
                    if (!dgv_ListaCollimatoriRMA.CurrentCell.Value.Equals(""))
                    {
                        String[] RMA_IDs_fromDGV;
                        RMA_IDs_fromDGV = dgv_ListaCollimatoriRMA.CurrentCell.Value.ToString().Split('\n');
                        List<RMA_View> itemsListToCheck = new List<RMA_View>();
                        for (int i = 0; i < RMA_IDs_fromDGV.Count(); i++)
                        {
                            RMA_View itemToCheck = new RMA_View();
                            itemToCheck = (RMA_View)conn.CreateCommand("SELECT * FROM [AutoTest].[dbo].[SM_MainView] WHERE [AutoTest].[dbo].[SM_MainView].idRMA = '" + RMA_IDs_fromDGV[i] + "'", itemToCheck);
                            itemsListToCheck.Add(itemToCheck);
                        }
                        if (itemsListToCheck != null && itemsListToCheck.Count >= 1)
                        {
                            foreach (RMA_View rma in itemsListToCheck)
                            {
                                using (var form = new Form_RMA(rma))
                                {
                                    var result = form.ShowDialog();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_cercaColl_Click(object sender, EventArgs e)
        {
            try
            {
                dgv_ListaCollimatoriRMA.Rows.Clear();
                if (!tb_numSeriale.Text.Equals(""))
                {
                    collList = new List<RMA_Collimatore>();
                    collList.Clear();
                    collList = (List<RMA_Collimatore>)conn.CreateCommand("SELECT PROGR, nFlow,'" + tb_numSeriale.Text + "' AS Serial, Model, dtCEf, " +
                                                             "Idcliente, Ragsoc, CodCli, NumeroFormattato, Da_N, A_N, " +
                                                              "Qta, Data FROM [archivi].[dbo].[VISTA_SM_FC_2] WHERE [archivi].[dbo].[VISTA_SM_FC_2].[Da_N] <= '" + tb_numSeriale.Text.Replace("_","").TrimStart().TrimEnd() + "' AND [archivi].[dbo].[VISTA_SM_FC_2].[A_N] >= '" + tb_numSeriale.Text.Replace("_", "").TrimStart().TrimEnd() + "'", collList);
                    if (collList != null)
                    {
                        foreach (RMA_Collimatore c in collList)
                        {
                            //elimina nei modelli collimatore la parte -[0-9]+; es : P 225 ACS DHHS-451220110751, elimina tutto ciò che è presente a partire dal -
                            if (c.modelloColl.Contains("-"))
                            {
                                string pattern = @"-[0-9]+";
                                string modello = Regex.Replace(c.modelloColl, pattern, "");
                                c.modelloColl = modello;
                            }
                            List<String> RMA_List = new List<String>();
                            string RMA_ID_Hyperlinks = "";
                            RMA_List = (List<String>)conn.CreateCommand("SELECT idRMA FROM [AutoTest].[dbo].[RMA_Collimatore] WHERE [AutoTest].[dbo].[RMA_Collimatore].[serialeColl] = '" + c.seriale + "' AND [AutoTest].[dbo].[RMA_Collimatore].[modelloColl] LIKE '%" + c.modelloColl + "%'" , RMA_List);
                            if (RMA_List != null)
                                foreach (String s in RMA_List)
                                    if (RMA_ID_Hyperlinks.Equals(""))
                                        RMA_ID_Hyperlinks = RMA_ID_Hyperlinks + s;
                                    else
                                        RMA_ID_Hyperlinks = RMA_ID_Hyperlinks + "\n" + s;
                            dgv_ListaCollimatoriRMA.Rows.Insert(0, false, c.seriale, c.modelloColl, c.dataProduzione, c.cliente.nomeCliente, c.codCollCliente, c.numFlowChart, c.da_N, c.a_N, c.qta, RMA_ID_Hyperlinks);
                        }
                        tb_flowChart.Text = collList[0].numFlowChart;
                        tb_dalNumSeriale.Text= collList[0].da_N.ToString();
                        tb_alNumSeriale.Text = collList[0].a_N.ToString();
                    }
                    else
                    {
                        MessageBox.Show("Numero seriale non corretto", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                    MessageBox.Show("Inserisci il numero seriale del collimatore", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_AddMultipleCollimators_Click(object sender, EventArgs e)
        {
            try
            {
                dgv_ListaCollimatoriRMA.Rows.Clear();
                if (tb_flowChart.Text.Equals(""))
                {
                    MessageBox.Show("Inserisci il numero di flow chart", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else if (tb_dalNumSeriale.Text.Equals(""))
                {
                    MessageBox.Show("Inserisci il primo numero seriale dei collimatori da aggiungere", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else if (tb_alNumSeriale.Text.Equals(""))
                {
                    MessageBox.Show("Inserisci l'ultimo numero seriale dei collimatori da aggiungere", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    collList = new List<RMA_Collimatore>();
                    collList.Clear();
                    Int64 serCollFirst = Convert.ToInt64(tb_dalNumSeriale.Text);
                    Int64 serCollLast = Convert.ToInt64(tb_alNumSeriale.Text);
                    for (Int64 i = serCollFirst; i <= serCollLast; i++)
                    {
                        RMA_Collimatore coll = new RMA_Collimatore();
                        coll = (RMA_Collimatore)conn.CreateCommand("SELECT PROGR, nFlow,'" + i + "' AS Serial, Model, dtCEf, " +
                                                             "Idcliente, Ragsoc, CodCli, NumeroFormattato, Da_N, A_N, " +
                                                              "Qta, Data FROM [archivi].[dbo].[VISTA_SM_FC_2] WHERE [archivi].[dbo].[VISTA_SM_FC_2].[Da_N] <= '" + tb_dalNumSeriale.Text.Replace("_", "").TrimStart().TrimEnd() + "' AND [archivi].[dbo].[VISTA_SM_FC_2].[A_N] >= '" + tb_alNumSeriale.Text.Replace("_", "").TrimStart().TrimEnd() + "' AND NumeroFormattato LIKE '%" + tb_flowChart.Text + "%'", coll);
                        if (coll!=null)
                            collList.Add(coll);
                    }
                    
                    if (collList != null)
                    {
                        foreach (RMA_Collimatore c in collList)
                            dgv_ListaCollimatoriRMA.Rows.Insert(0, true, c.seriale, c.modelloColl, c.dataProduzione, c.cliente.nomeCliente, c.codCollCliente, c.numFlowChart, c.da_N, c.a_N, c.qta);
                    }
                    else
                    {
                        MessageBox.Show("Numero seriale non corretto", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void updateVariabilePubblica()
        {
            try
            {
                collimators = new List<RMA_Collimatore>();
                foreach (DataGridViewRow row in dgv_ListaCollimatoriRMA.Rows)
                {
                    if (row.Cells.GetCellValueFromColumnHeader("").Equals(true))
                    {
                        collimators.Add(collList.Single(r => (r.seriale.Equals(row.Cells.GetCellValueFromColumnHeader("Seriale")) && r.dataProduzione.Equals(row.Cells.GetCellValueFromColumnHeader("Data Produzione")) && (r.modelloColl.Equals(row.Cells.GetCellValueFromColumnHeader("Modello"))) && (r.numFlowChart.Equals(row.Cells.GetCellValueFromColumnHeader("Flow Chart"))))));
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void commitDB()
        {
            try
            {
                List<RMA_Collimatore> collToRemoveList = new List<RMA_Collimatore>();
                foreach (RMA_Collimatore c in collimators)
                {
                    RMA_Collimatore coll = new RMA_Collimatore();
                    coll = (RMA_Collimatore)conn.CreateCommand("SELECT * FROM [Autotest].[dbo].[RMA_Collimatore] WHERE [idRMA] = '" + idRMA + "' AND [serialeColl] = '" + c.seriale + "'", coll);
                    if (coll == null)
                    {
                        c.idRMA = idRMA;
                        conn.CreateCommand("INSERT INTO [Autotest].[dbo].[RMA_Collimatore] " +
                        "([idRMA],[serialeColl],[modelloColl],[dataProduzione],[nomeCliente],[codCollCli],[idCliente],[numFlowChart],[da_NFC],[a_NFC],[quantita]) " +
                        "VALUES ('" + c.idRMA + "','" + c.seriale + "','" + c.modelloColl + "','" + c.dataProduzione.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "','" + c.cliente.nomeCliente + "','" + c.codCollCliente + "','" + c.cliente.IdCliente + "','" + c.numFlowChart + "','" + c.da_N + "','" + c.a_N + "','" + c.qta + "')", null);
#if USEPANTHERA
                        string[] flowChartSplit = c.numFlowChart.Split('/');
                        string flowChart = flowChartSplit[1] + "_" + flowChartSplit[0];
                        //inserimento dei dati nella vista RMA_TES_MATRICOLE
                        conn.CreateCommand("INSERT INTO [Autotest].[dbo].[RMA_TES_MATRICOLE] " +
                        "([ID_AZIENDA],[ID_RMA],[ID_MATRICOLA],[ID_ARTICOLO],[NOTE],[R_LOTTO]) " +
                        "VALUES ('001','" + c.idRMA.Replace('/', '_') + "','" + c.seriale + "','" + c.modelloColl + "','" + "N/A" + "','" + flowChart + "')", null);
#endif

                    }
                    else
                    {
                        collToRemoveList.Add(collimators.SingleOrDefault(clmt => clmt.seriale.Equals(coll.seriale)));
                    }
                }
                if (collToRemoveList != null)
                    foreach (RMA_Collimatore c in collToRemoveList)
                        collimators.Remove(c);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_okColl_Click(object sender, EventArgs e)
        {
            try
            {
                if (!this.isJustForConsulting)
                {
                    updateVariabilePubblica();
                    if (collimators != null && !collimators.Count.Equals(0))
                    {
                        commitDB();
                        this.DialogResult = DialogResult.OK;
                        this.Dispose();
                    }
                    else
                        MessageBox.Show("Selezionare almeno un collimatore tra quelli disponibili", "Informazione", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    this.DialogResult = DialogResult.OK;
                    this.Dispose();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_cancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.DialogResult = DialogResult.Cancel;
                this.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
