﻿namespace Ralco_ServiceManager
{
    partial class Form_RMA
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_RMA));
            this.tabctrl_RMA = new System.Windows.Forms.TabControl();
            this.tab_generalInfo = new System.Windows.Forms.TabPage();
            this.tabctrl_ItemGenerali = new System.Windows.Forms.TabControl();
            this.tabCollimatore = new System.Windows.Forms.TabPage();
            this.dgv_ListaCollimatoriRMA = new System.Windows.Forms.DataGridView();
            this.chk_coll = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.serialNr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.modello = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataProd = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cliente = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codColl = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numFC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.da_SN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.al_SN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.qta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolStrip_MenuCollimatore = new System.Windows.Forms.ToolStrip();
            this.btn_RefreshCollRMA = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_AddCollRMA = new System.Windows.Forms.ToolStripButton();
            this.btn_CancelCollRMA = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_StampaEtichetteColl = new System.Windows.Forms.ToolStripButton();
            this.cmb_CollSelection = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_visualizzaScansioneFlowChart = new System.Windows.Forms.ToolStripButton();
            this.btn_visualizzaFlowTree = new System.Windows.Forms.ToolStripButton();
            this.btn_DHR = new System.Windows.Forms.ToolStripButton();
            this.tabParte = new System.Windows.Forms.TabPage();
            this.dgv_ListaPartiRMA = new System.Windows.Forms.DataGridView();
            this.chk_Parte = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.idParte = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descrizParte = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SNParte = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cliParte = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FC_Parte = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numBolla = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataBolla = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataVendita = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.qtyPart = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolStrip_MenuParte = new System.Windows.Forms.ToolStrip();
            this.btn_RefreshParteRMA = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_AddParteRMA = new System.Windows.Forms.ToolStripButton();
            this.btn_EditParteRMA = new System.Windows.Forms.ToolStripButton();
            this.btn_CancelParteRMA = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_StampaEtichetteParte = new System.Windows.Forms.ToolStripButton();
            this.cmb_ParteSelection = new System.Windows.Forms.ToolStripComboBox();
            this.gb_InfoGenerali = new System.Windows.Forms.GroupBox();
            this.lbl_DataCreazioneRMA = new System.Windows.Forms.Label();
            this.lbl_dataCreazRMA = new System.Windows.Forms.Label();
            this.tb_noteGenerali = new System.Windows.Forms.TextBox();
            this.lbl_noteGen = new System.Windows.Forms.Label();
            this.cmb_tipoRMA = new System.Windows.Forms.ComboBox();
            this.lbl_NumRMA = new System.Windows.Forms.Label();
            this.lbl_numeroRMA = new System.Windows.Forms.Label();
            this.lbl_tipoRMA = new System.Windows.Forms.Label();
            this.tab_infoCliente = new System.Windows.Forms.TabPage();
            this.gb_dettagli = new System.Windows.Forms.GroupBox();
            this.tb_DataOrdineRiparazione = new System.Windows.Forms.MaskedTextBox();
            this.lbl_codCollCli = new System.Windows.Forms.Label();
            this.tb_DataDocumentoDiReso = new System.Windows.Forms.MaskedTextBox();
            this.lbl_idCli = new System.Windows.Forms.Label();
            this.tb_DataScadenzaRMA = new System.Windows.Forms.MaskedTextBox();
            this.tb_DataArrivoCollimatore = new System.Windows.Forms.MaskedTextBox();
            this.lbl_codCollCliente = new System.Windows.Forms.Label();
            this.lbl_idCliente = new System.Windows.Forms.Label();
            this.lbl_dataDocReso = new System.Windows.Forms.Label();
            this.dtp_dataDocReso = new System.Windows.Forms.DateTimePicker();
            this.dtp_DataArrivoCollimatore = new System.Windows.Forms.DateTimePicker();
            this.lbl_dataArrColl = new System.Windows.Forms.Label();
            this.dtp_dataScadenzaRMA = new System.Windows.Forms.DateTimePicker();
            this.lbl_dataScadRMA = new System.Windows.Forms.Label();
            this.dtp_dataOrdRip = new System.Windows.Forms.DateTimePicker();
            this.lbl_dataOrdRip = new System.Windows.Forms.Label();
            this.lbl_ordRip = new System.Windows.Forms.Label();
            this.cmb_nomeCliente = new System.Windows.Forms.ComboBox();
            this.tb_ordRiparaz = new System.Windows.Forms.TextBox();
            this.lbl_nomeCliente = new System.Windows.Forms.Label();
            this.tb_numScarCLI = new System.Windows.Forms.TextBox();
            this.lbl_numSCARcli = new System.Windows.Forms.Label();
            this.cmb_decisCliente = new System.Windows.Forms.ComboBox();
            this.lbl_decisCli = new System.Windows.Forms.Label();
            this.tb_noteCliente = new System.Windows.Forms.TextBox();
            this.tb_docReso = new System.Windows.Forms.TextBox();
            this.tb_scarCliente = new System.Windows.Forms.TextBox();
            this.lbl_docReso = new System.Windows.Forms.Label();
            this.lbl_SCARcli = new System.Windows.Forms.Label();
            this.lbl_difRiscCli = new System.Windows.Forms.Label();
            this.tab_Allegati = new System.Windows.Forms.TabPage();
            this.dgv_ListaAllegatiRMA = new System.Windows.Forms.DataGridView();
            this.chkAll = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.nomeFile = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolStrip_MenuAllegati = new System.Windows.Forms.ToolStrip();
            this.btn_RefreshAllegati = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_AddAllegati = new System.Windows.Forms.ToolStripButton();
            this.btn_DeleteAllegati = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_viewAllegati = new System.Windows.Forms.ToolStripButton();
            this.tab_detGaranzia = new System.Windows.Forms.TabPage();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.cmb_statoGaranzia = new System.Windows.Forms.ComboBox();
            this.rb_statoGaranzia = new System.Windows.Forms.RadioButton();
            this.cmb_azioniSupplRalco = new System.Windows.Forms.ComboBox();
            this.lbl_azSupplRalco = new System.Windows.Forms.Label();
            this.cmb_respDannoCollimatore = new System.Windows.Forms.ComboBox();
            this.tb_noteGaranziaRalco = new System.Windows.Forms.TextBox();
            this.lbl_note = new System.Windows.Forms.Label();
            this.lbl_respDannoColl = new System.Windows.Forms.Label();
            this.cmb_provCollimatore = new System.Windows.Forms.ComboBox();
            this.lbl_provColl = new System.Windows.Forms.Label();
            this.cmb_anniVitaCollimatore = new System.Windows.Forms.ComboBox();
            this.lbl_anniVitaColl = new System.Windows.Forms.Label();
            this.tab_Analisi = new System.Windows.Forms.TabPage();
            this.dgv_ListaProblemiRMA = new System.Windows.Forms.DataGridView();
            this.chk_Prob = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.difettoProbl = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataAggProb = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gravitaProb = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idProblema = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolStrip_MenuAnalisiProblema = new System.Windows.Forms.ToolStrip();
            this.btn_RefreshProbl = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_AddProbl = new System.Windows.Forms.ToolStripButton();
            this.btn_importProbFromTemplateDB = new System.Windows.Forms.ToolStripButton();
            this.btn_CloneProblema = new System.Windows.Forms.ToolStripButton();
            this.btn_EditProbl = new System.Windows.Forms.ToolStripButton();
            this.btn_DeleteProbl = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_addTemplateProbDB = new System.Windows.Forms.ToolStripButton();
            this.btn_editTemplateProbDB = new System.Windows.Forms.ToolStripButton();
            this.tab_FasiDaRieseguire = new System.Windows.Forms.TabPage();
            this.dgv_FasiDaRieseguire = new System.Windows.Forms.DataGridView();
            this.dataGridViewCheckBoxColumn2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.descFase = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.minLavoro = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tab_TestDaEseguire = new System.Windows.Forms.TabPage();
            this.dgv_testDaEseguire = new System.Windows.Forms.DataGridView();
            this.checkTest = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.descTest = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tab_Riparazioni = new System.Windows.Forms.TabPage();
            this.dgv_ListaRiparazRMA = new System.Windows.Forms.DataGridView();
            this.chk_Rip = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.descRip = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descParte = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.qtaParti = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statoRip = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.noteRip = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataRip = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idRiparazione = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolStrip_MenuRiparazioniDaEseguire = new System.Windows.Forms.ToolStrip();
            this.btn_RefreshRiparazione = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_AddRiparazione = new System.Windows.Forms.ToolStripButton();
            this.btn_importRipFromTemplateDB = new System.Windows.Forms.ToolStripButton();
            this.btn_CloneRiparazione = new System.Windows.Forms.ToolStripButton();
            this.btn_EditRiparazione = new System.Windows.Forms.ToolStripButton();
            this.btn_DeleteRiparazione = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_addTemplateRipDB = new System.Windows.Forms.ToolStripButton();
            this.btn_editTemplateRipDB = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator12 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_PrintRiparazioni = new System.Windows.Forms.ToolStripButton();
            this.tab_AzioniSuppl = new System.Windows.Forms.TabPage();
            this.dgv_azioniSupplementari = new System.Windows.Forms.DataGridView();
            this.dataGridViewCheckBoxColumn3 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statoAzioneSuppl = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.toolStrip_MenuAzioniSupplementari = new System.Windows.Forms.ToolStrip();
            this.btn_RefreshAzSuppl = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator13 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_GestisciAzSuppl = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.tab_Preventivi = new System.Windows.Forms.TabPage();
            this.dgv_ListaPreventiviRMA = new System.Windows.Forms.DataGridView();
            this.chk_Prev = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.idPreventivo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.revisionePrev = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataAggPrev = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.totale = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stato = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idPrev = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolStrip_MenuPreventivi = new System.Windows.Forms.ToolStrip();
            this.btn_RefreshPreventivi = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator14 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_AddPreventivo = new System.Windows.Forms.ToolStripButton();
            this.btn_ClonePreventivo = new System.Windows.Forms.ToolStripButton();
            this.btn_EditPreventivo = new System.Windows.Forms.ToolStripButton();
            this.btn_DeletePreventivo = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator15 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_PrintPreventivo = new System.Windows.Forms.ToolStripButton();
            this.cmb_PrevPrezzi = new System.Windows.Forms.ToolStripComboBox();
            this.tab_Trasporto = new System.Windows.Forms.TabPage();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.tb_noteImballaggio = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cmb_tipoImballaggio = new System.Windows.Forms.ComboBox();
            this.tb_noteTrasporto = new System.Windows.Forms.TextBox();
            this.tb_DataSpedizione = new System.Windows.Forms.MaskedTextBox();
            this.lbl_dataSped = new System.Windows.Forms.Label();
            this.lbl_speseSped = new System.Windows.Forms.Label();
            this.lbl_noteTrasporto = new System.Windows.Forms.Label();
            this.cmb_speseSpediz = new System.Windows.Forms.ComboBox();
            this.dtp_dataSpedizione = new System.Windows.Forms.DateTimePicker();
            this.tab_Qualita = new System.Windows.Forms.TabPage();
            this.tb_noteQualita = new System.Windows.Forms.TextBox();
            this.lbl_noteQualita = new System.Windows.Forms.Label();
            this.gb_azCorrPrev = new System.Windows.Forms.GroupBox();
            this.cmb_rifAzCorr = new System.Windows.Forms.ComboBox();
            this.lbl_descAzCorrett = new System.Windows.Forms.Label();
            this.lbl_descAzCorr = new System.Windows.Forms.Label();
            this.cmb_statoAzCorr = new System.Windows.Forms.ComboBox();
            this.lbl_statoAzCorr = new System.Windows.Forms.Label();
            this.lbl_rif = new System.Windows.Forms.Label();
            this.gb_impattoSCAR = new System.Windows.Forms.GroupBox();
            this.cmb_revDocValutRischi = new System.Windows.Forms.ComboBox();
            this.cmb_segIncidente = new System.Windows.Forms.ComboBox();
            this.lbl_revDocValutazRischi = new System.Windows.Forms.Label();
            this.lbl_segnIncid = new System.Windows.Forms.Label();
            this.cmb_invioNotaInf = new System.Windows.Forms.ComboBox();
            this.cmb_sicProdotto = new System.Windows.Forms.ComboBox();
            this.lbl_invioNotaInf = new System.Windows.Forms.Label();
            this.lbl_sicProd = new System.Windows.Forms.Label();
            this.tab_NoteRMA = new System.Windows.Forms.TabPage();
            this.dgv_noteRMA = new System.Windows.Forms.DataGridView();
            this.dataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.logAggDa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.noteLog = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idNote = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btn_RefreshNoteRMA = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator16 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_AddNoteRMA = new System.Windows.Forms.ToolStripButton();
            this.btn_EditNoteRMA = new System.Windows.Forms.ToolStripButton();
            this.btn_DeleteNoteRMA = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator17 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_PrintGenericInfo = new System.Windows.Forms.ToolStripButton();
            this.cmb_prioritaRMA = new System.Windows.Forms.ComboBox();
            this.lbl_priorita = new System.Windows.Forms.Label();
            this.cmb_statoRMA = new System.Windows.Forms.ComboBox();
            this.lbl_stato = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.btn_SaveAndClose = new System.Windows.Forms.Button();
            this.btn_cancel = new System.Windows.Forms.Button();
            this.btn_saveRMA = new System.Windows.Forms.Button();
            this.cb_modManNumRMA = new System.Windows.Forms.CheckBox();
            this.tb_ModManNumRMA = new System.Windows.Forms.TextBox();
            this.tabctrl_RMA.SuspendLayout();
            this.tab_generalInfo.SuspendLayout();
            this.tabctrl_ItemGenerali.SuspendLayout();
            this.tabCollimatore.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ListaCollimatoriRMA)).BeginInit();
            this.toolStrip_MenuCollimatore.SuspendLayout();
            this.tabParte.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ListaPartiRMA)).BeginInit();
            this.toolStrip_MenuParte.SuspendLayout();
            this.gb_InfoGenerali.SuspendLayout();
            this.tab_infoCliente.SuspendLayout();
            this.gb_dettagli.SuspendLayout();
            this.tab_Allegati.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ListaAllegatiRMA)).BeginInit();
            this.toolStrip_MenuAllegati.SuspendLayout();
            this.tab_detGaranzia.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.tab_Analisi.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ListaProblemiRMA)).BeginInit();
            this.toolStrip_MenuAnalisiProblema.SuspendLayout();
            this.tab_FasiDaRieseguire.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_FasiDaRieseguire)).BeginInit();
            this.tab_TestDaEseguire.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_testDaEseguire)).BeginInit();
            this.tab_Riparazioni.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ListaRiparazRMA)).BeginInit();
            this.toolStrip_MenuRiparazioniDaEseguire.SuspendLayout();
            this.tab_AzioniSuppl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_azioniSupplementari)).BeginInit();
            this.toolStrip_MenuAzioniSupplementari.SuspendLayout();
            this.tab_Preventivi.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ListaPreventiviRMA)).BeginInit();
            this.toolStrip_MenuPreventivi.SuspendLayout();
            this.tab_Trasporto.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.tab_Qualita.SuspendLayout();
            this.gb_azCorrPrev.SuspendLayout();
            this.gb_impattoSCAR.SuspendLayout();
            this.tab_NoteRMA.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_noteRMA)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabctrl_RMA
            // 
            this.tabctrl_RMA.Controls.Add(this.tab_generalInfo);
            this.tabctrl_RMA.Controls.Add(this.tab_infoCliente);
            this.tabctrl_RMA.Controls.Add(this.tab_Allegati);
            this.tabctrl_RMA.Controls.Add(this.tab_detGaranzia);
            this.tabctrl_RMA.Controls.Add(this.tab_Analisi);
            this.tabctrl_RMA.Controls.Add(this.tab_FasiDaRieseguire);
            this.tabctrl_RMA.Controls.Add(this.tab_TestDaEseguire);
            this.tabctrl_RMA.Controls.Add(this.tab_Riparazioni);
            this.tabctrl_RMA.Controls.Add(this.tab_AzioniSuppl);
            this.tabctrl_RMA.Controls.Add(this.tab_Preventivi);
            this.tabctrl_RMA.Controls.Add(this.tab_Trasporto);
            this.tabctrl_RMA.Controls.Add(this.tab_Qualita);
            this.tabctrl_RMA.Controls.Add(this.tab_NoteRMA);
            this.tabctrl_RMA.Location = new System.Drawing.Point(12, 60);
            this.tabctrl_RMA.Name = "tabctrl_RMA";
            this.tabctrl_RMA.SelectedIndex = 0;
            this.tabctrl_RMA.Size = new System.Drawing.Size(1054, 438);
            this.tabctrl_RMA.TabIndex = 3;
            this.tabctrl_RMA.SelectedIndexChanged += new System.EventHandler(this.tabctrl_RMA_SelectedIndexChanged);
            // 
            // tab_generalInfo
            // 
            this.tab_generalInfo.BackColor = System.Drawing.Color.Transparent;
            this.tab_generalInfo.Controls.Add(this.tabctrl_ItemGenerali);
            this.tab_generalInfo.Controls.Add(this.gb_InfoGenerali);
            this.tab_generalInfo.Location = new System.Drawing.Point(4, 22);
            this.tab_generalInfo.Name = "tab_generalInfo";
            this.tab_generalInfo.Padding = new System.Windows.Forms.Padding(3);
            this.tab_generalInfo.Size = new System.Drawing.Size(1046, 412);
            this.tab_generalInfo.TabIndex = 0;
            this.tab_generalInfo.Text = "Generali";
            // 
            // tabctrl_ItemGenerali
            // 
            this.tabctrl_ItemGenerali.Controls.Add(this.tabCollimatore);
            this.tabctrl_ItemGenerali.Controls.Add(this.tabParte);
            this.tabctrl_ItemGenerali.Location = new System.Drawing.Point(6, 142);
            this.tabctrl_ItemGenerali.Name = "tabctrl_ItemGenerali";
            this.tabctrl_ItemGenerali.SelectedIndex = 0;
            this.tabctrl_ItemGenerali.Size = new System.Drawing.Size(1034, 264);
            this.tabctrl_ItemGenerali.TabIndex = 11;
            // 
            // tabCollimatore
            // 
            this.tabCollimatore.BackColor = System.Drawing.Color.Transparent;
            this.tabCollimatore.Controls.Add(this.dgv_ListaCollimatoriRMA);
            this.tabCollimatore.Controls.Add(this.toolStrip_MenuCollimatore);
            this.tabCollimatore.Location = new System.Drawing.Point(4, 22);
            this.tabCollimatore.Name = "tabCollimatore";
            this.tabCollimatore.Padding = new System.Windows.Forms.Padding(3);
            this.tabCollimatore.Size = new System.Drawing.Size(1026, 238);
            this.tabCollimatore.TabIndex = 0;
            this.tabCollimatore.Text = "Collimatore";
            // 
            // dgv_ListaCollimatoriRMA
            // 
            this.dgv_ListaCollimatoriRMA.AllowUserToAddRows = false;
            this.dgv_ListaCollimatoriRMA.AllowUserToOrderColumns = true;
            this.dgv_ListaCollimatoriRMA.BackgroundColor = System.Drawing.SystemColors.ActiveBorder;
            this.dgv_ListaCollimatoriRMA.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_ListaCollimatoriRMA.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.chk_coll,
            this.serialNr,
            this.modello,
            this.dataProd,
            this.cliente,
            this.codColl,
            this.numFC,
            this.da_SN,
            this.al_SN,
            this.qta});
            this.dgv_ListaCollimatoriRMA.Cursor = System.Windows.Forms.Cursors.Default;
            this.dgv_ListaCollimatoriRMA.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_ListaCollimatoriRMA.Location = new System.Drawing.Point(3, 53);
            this.dgv_ListaCollimatoriRMA.Name = "dgv_ListaCollimatoriRMA";
            this.dgv_ListaCollimatoriRMA.ReadOnly = true;
            this.dgv_ListaCollimatoriRMA.RowHeadersVisible = false;
            this.dgv_ListaCollimatoriRMA.Size = new System.Drawing.Size(1020, 182);
            this.dgv_ListaCollimatoriRMA.TabIndex = 16;
            this.dgv_ListaCollimatoriRMA.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_ListaCollimatoriRMA_CellContentClick);
            // 
            // chk_coll
            // 
            this.chk_coll.HeaderText = "";
            this.chk_coll.Name = "chk_coll";
            this.chk_coll.ReadOnly = true;
            this.chk_coll.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.chk_coll.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.chk_coll.Width = 30;
            // 
            // serialNr
            // 
            this.serialNr.HeaderText = "Seriale";
            this.serialNr.Name = "serialNr";
            this.serialNr.ReadOnly = true;
            this.serialNr.Width = 60;
            // 
            // modello
            // 
            this.modello.HeaderText = "Modello";
            this.modello.Name = "modello";
            this.modello.ReadOnly = true;
            this.modello.Width = 160;
            // 
            // dataProd
            // 
            this.dataProd.HeaderText = "Data Produzione";
            this.dataProd.Name = "dataProd";
            this.dataProd.ReadOnly = true;
            this.dataProd.Width = 110;
            // 
            // cliente
            // 
            this.cliente.HeaderText = "Cliente";
            this.cliente.Name = "cliente";
            this.cliente.ReadOnly = true;
            this.cliente.Width = 165;
            // 
            // codColl
            // 
            this.codColl.HeaderText = "Codice Collimatore";
            this.codColl.Name = "codColl";
            this.codColl.ReadOnly = true;
            this.codColl.Width = 120;
            // 
            // numFC
            // 
            this.numFC.HeaderText = "Flow Chart";
            this.numFC.Name = "numFC";
            this.numFC.ReadOnly = true;
            this.numFC.Width = 80;
            // 
            // da_SN
            // 
            this.da_SN.HeaderText = "Dal SN";
            this.da_SN.Name = "da_SN";
            this.da_SN.ReadOnly = true;
            this.da_SN.Width = 65;
            // 
            // al_SN
            // 
            this.al_SN.HeaderText = "Al SN";
            this.al_SN.Name = "al_SN";
            this.al_SN.ReadOnly = true;
            this.al_SN.Width = 60;
            // 
            // qta
            // 
            this.qta.HeaderText = "Quantità";
            this.qta.Name = "qta";
            this.qta.ReadOnly = true;
            this.qta.Width = 65;
            // 
            // toolStrip_MenuCollimatore
            // 
            this.toolStrip_MenuCollimatore.AutoSize = false;
            this.toolStrip_MenuCollimatore.BackColor = System.Drawing.SystemColors.Control;
            this.toolStrip_MenuCollimatore.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip_MenuCollimatore.ImageScalingSize = new System.Drawing.Size(47, 47);
            this.toolStrip_MenuCollimatore.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btn_RefreshCollRMA,
            this.toolStripSeparator10,
            this.btn_AddCollRMA,
            this.btn_CancelCollRMA,
            this.toolStripSeparator11,
            this.btn_StampaEtichetteColl,
            this.cmb_CollSelection,
            this.toolStripSeparator5,
            this.btn_visualizzaScansioneFlowChart,
            this.btn_visualizzaFlowTree,
            this.btn_DHR});
            this.toolStrip_MenuCollimatore.Location = new System.Drawing.Point(3, 3);
            this.toolStrip_MenuCollimatore.Name = "toolStrip_MenuCollimatore";
            this.toolStrip_MenuCollimatore.Size = new System.Drawing.Size(1020, 50);
            this.toolStrip_MenuCollimatore.TabIndex = 15;
            this.toolStrip_MenuCollimatore.Text = "toolStrip1";
            // 
            // btn_RefreshCollRMA
            // 
            this.btn_RefreshCollRMA.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_RefreshCollRMA.Image = ((System.Drawing.Image)(resources.GetObject("btn_RefreshCollRMA.Image")));
            this.btn_RefreshCollRMA.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_RefreshCollRMA.Name = "btn_RefreshCollRMA";
            this.btn_RefreshCollRMA.Size = new System.Drawing.Size(51, 47);
            this.btn_RefreshCollRMA.Text = "Evidenzia";
            this.btn_RefreshCollRMA.ToolTipText = "Evidenzia";
            this.btn_RefreshCollRMA.Click += new System.EventHandler(this.btn_RefreshCollRMA_Click);
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(6, 50);
            // 
            // btn_AddCollRMA
            // 
            this.btn_AddCollRMA.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_AddCollRMA.Image = ((System.Drawing.Image)(resources.GetObject("btn_AddCollRMA.Image")));
            this.btn_AddCollRMA.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_AddCollRMA.Name = "btn_AddCollRMA";
            this.btn_AddCollRMA.Size = new System.Drawing.Size(51, 47);
            this.btn_AddCollRMA.Text = "toolStripButton4";
            this.btn_AddCollRMA.ToolTipText = "Esporta Magazzino";
            this.btn_AddCollRMA.Click += new System.EventHandler(this.btn_AddCollRMA_Click);
            // 
            // btn_CancelCollRMA
            // 
            this.btn_CancelCollRMA.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_CancelCollRMA.Image = ((System.Drawing.Image)(resources.GetObject("btn_CancelCollRMA.Image")));
            this.btn_CancelCollRMA.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_CancelCollRMA.Name = "btn_CancelCollRMA";
            this.btn_CancelCollRMA.Size = new System.Drawing.Size(51, 47);
            this.btn_CancelCollRMA.Text = "toolStripButton2";
            this.btn_CancelCollRMA.ToolTipText = "Rimuovi Da Magazzino";
            this.btn_CancelCollRMA.Click += new System.EventHandler(this.btn_CancelCollRMA_Click);
            // 
            // toolStripSeparator11
            // 
            this.toolStripSeparator11.Name = "toolStripSeparator11";
            this.toolStripSeparator11.Size = new System.Drawing.Size(6, 50);
            // 
            // btn_StampaEtichetteColl
            // 
            this.btn_StampaEtichetteColl.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_StampaEtichetteColl.Image = ((System.Drawing.Image)(resources.GetObject("btn_StampaEtichetteColl.Image")));
            this.btn_StampaEtichetteColl.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_StampaEtichetteColl.Name = "btn_StampaEtichetteColl";
            this.btn_StampaEtichetteColl.Size = new System.Drawing.Size(51, 47);
            this.btn_StampaEtichetteColl.Text = "Clona Record";
            this.btn_StampaEtichetteColl.Click += new System.EventHandler(this.btn_StampaEtichetteColl_Click);
            // 
            // cmb_CollSelection
            // 
            this.cmb_CollSelection.AutoCompleteCustomSource.AddRange(new string[] {
            "Seleziona Tutti",
            "Deseleziona Tutti"});
            this.cmb_CollSelection.Items.AddRange(new object[] {
            "Seleziona Tutti",
            "Deseleziona Tutti"});
            this.cmb_CollSelection.Name = "cmb_CollSelection";
            this.cmb_CollSelection.Size = new System.Drawing.Size(121, 50);
            this.cmb_CollSelection.TextChanged += new System.EventHandler(this.Cmb_CollSelection_TextChanged);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 50);
            // 
            // btn_visualizzaScansioneFlowChart
            // 
            this.btn_visualizzaScansioneFlowChart.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_visualizzaScansioneFlowChart.Image = ((System.Drawing.Image)(resources.GetObject("btn_visualizzaScansioneFlowChart.Image")));
            this.btn_visualizzaScansioneFlowChart.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_visualizzaScansioneFlowChart.Name = "btn_visualizzaScansioneFlowChart";
            this.btn_visualizzaScansioneFlowChart.Size = new System.Drawing.Size(51, 47);
            this.btn_visualizzaScansioneFlowChart.ToolTipText = "Stampa Test";
            this.btn_visualizzaScansioneFlowChart.Click += new System.EventHandler(this.btn_visualizzaScansioneFlowChart_Click);
            // 
            // btn_visualizzaFlowTree
            // 
            this.btn_visualizzaFlowTree.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_visualizzaFlowTree.Image = ((System.Drawing.Image)(resources.GetObject("btn_visualizzaFlowTree.Image")));
            this.btn_visualizzaFlowTree.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_visualizzaFlowTree.Name = "btn_visualizzaFlowTree";
            this.btn_visualizzaFlowTree.Size = new System.Drawing.Size(51, 47);
            this.btn_visualizzaFlowTree.Text = "toolStripButton1";
            this.btn_visualizzaFlowTree.Click += new System.EventHandler(this.btn_visualizzaFlowTree_Click);
            // 
            // btn_DHR
            // 
            this.btn_DHR.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_DHR.Image = ((System.Drawing.Image)(resources.GetObject("btn_DHR.Image")));
            this.btn_DHR.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_DHR.Name = "btn_DHR";
            this.btn_DHR.Size = new System.Drawing.Size(51, 47);
            this.btn_DHR.Text = "toolStripButton2";
            this.btn_DHR.Click += new System.EventHandler(this.btn_DHR_Click);
            // 
            // tabParte
            // 
            this.tabParte.BackColor = System.Drawing.Color.Transparent;
            this.tabParte.Controls.Add(this.dgv_ListaPartiRMA);
            this.tabParte.Controls.Add(this.toolStrip_MenuParte);
            this.tabParte.Location = new System.Drawing.Point(4, 22);
            this.tabParte.Name = "tabParte";
            this.tabParte.Padding = new System.Windows.Forms.Padding(3);
            this.tabParte.Size = new System.Drawing.Size(1026, 238);
            this.tabParte.TabIndex = 1;
            this.tabParte.Text = "Parte";
            // 
            // dgv_ListaPartiRMA
            // 
            this.dgv_ListaPartiRMA.AllowUserToAddRows = false;
            this.dgv_ListaPartiRMA.AllowUserToOrderColumns = true;
            this.dgv_ListaPartiRMA.BackgroundColor = System.Drawing.SystemColors.ActiveBorder;
            this.dgv_ListaPartiRMA.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_ListaPartiRMA.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.chk_Parte,
            this.idParte,
            this.descrizParte,
            this.SNParte,
            this.cliParte,
            this.FC_Parte,
            this.numBolla,
            this.dataBolla,
            this.dataVendita,
            this.qtyPart});
            this.dgv_ListaPartiRMA.Cursor = System.Windows.Forms.Cursors.Default;
            this.dgv_ListaPartiRMA.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_ListaPartiRMA.Location = new System.Drawing.Point(3, 53);
            this.dgv_ListaPartiRMA.Name = "dgv_ListaPartiRMA";
            this.dgv_ListaPartiRMA.ReadOnly = true;
            this.dgv_ListaPartiRMA.RowHeadersVisible = false;
            this.dgv_ListaPartiRMA.Size = new System.Drawing.Size(1020, 182);
            this.dgv_ListaPartiRMA.TabIndex = 19;
            this.dgv_ListaPartiRMA.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_ListaPartiRMA_CellContentClick);
            // 
            // chk_Parte
            // 
            this.chk_Parte.HeaderText = "";
            this.chk_Parte.Name = "chk_Parte";
            this.chk_Parte.ReadOnly = true;
            this.chk_Parte.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.chk_Parte.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.chk_Parte.Width = 30;
            // 
            // idParte
            // 
            this.idParte.HeaderText = "ID Parte";
            this.idParte.Name = "idParte";
            this.idParte.ReadOnly = true;
            this.idParte.Width = 80;
            // 
            // descrizParte
            // 
            this.descrizParte.HeaderText = "Descrizione";
            this.descrizParte.Name = "descrizParte";
            this.descrizParte.ReadOnly = true;
            this.descrizParte.Width = 160;
            // 
            // SNParte
            // 
            this.SNParte.HeaderText = "Seriale";
            this.SNParte.Name = "SNParte";
            this.SNParte.ReadOnly = true;
            this.SNParte.Width = 80;
            // 
            // cliParte
            // 
            this.cliParte.HeaderText = "Cliente";
            this.cliParte.Name = "cliParte";
            this.cliParte.ReadOnly = true;
            this.cliParte.Width = 165;
            // 
            // FC_Parte
            // 
            this.FC_Parte.HeaderText = "Flow Chart";
            this.FC_Parte.Name = "FC_Parte";
            this.FC_Parte.ReadOnly = true;
            this.FC_Parte.Width = 80;
            // 
            // numBolla
            // 
            this.numBolla.HeaderText = "Numero Bolla";
            this.numBolla.Name = "numBolla";
            this.numBolla.ReadOnly = true;
            // 
            // dataBolla
            // 
            this.dataBolla.HeaderText = "Data Bolla";
            this.dataBolla.Name = "dataBolla";
            this.dataBolla.ReadOnly = true;
            this.dataBolla.Width = 90;
            // 
            // dataVendita
            // 
            this.dataVendita.HeaderText = "Data Vendita";
            this.dataVendita.Name = "dataVendita";
            this.dataVendita.ReadOnly = true;
            // 
            // qtyPart
            // 
            this.qtyPart.HeaderText = "Quantità";
            this.qtyPart.Name = "qtyPart";
            this.qtyPart.ReadOnly = true;
            this.qtyPart.Width = 65;
            // 
            // toolStrip_MenuParte
            // 
            this.toolStrip_MenuParte.AutoSize = false;
            this.toolStrip_MenuParte.BackColor = System.Drawing.SystemColors.Control;
            this.toolStrip_MenuParte.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip_MenuParte.ImageScalingSize = new System.Drawing.Size(47, 47);
            this.toolStrip_MenuParte.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btn_RefreshParteRMA,
            this.toolStripSeparator1,
            this.btn_AddParteRMA,
            this.btn_EditParteRMA,
            this.btn_CancelParteRMA,
            this.toolStripSeparator2,
            this.btn_StampaEtichetteParte,
            this.cmb_ParteSelection});
            this.toolStrip_MenuParte.Location = new System.Drawing.Point(3, 3);
            this.toolStrip_MenuParte.Name = "toolStrip_MenuParte";
            this.toolStrip_MenuParte.Size = new System.Drawing.Size(1020, 50);
            this.toolStrip_MenuParte.TabIndex = 18;
            this.toolStrip_MenuParte.Text = "toolStrip1";
            // 
            // btn_RefreshParteRMA
            // 
            this.btn_RefreshParteRMA.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_RefreshParteRMA.Image = ((System.Drawing.Image)(resources.GetObject("btn_RefreshParteRMA.Image")));
            this.btn_RefreshParteRMA.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_RefreshParteRMA.Name = "btn_RefreshParteRMA";
            this.btn_RefreshParteRMA.Size = new System.Drawing.Size(51, 47);
            this.btn_RefreshParteRMA.Text = "Evidenzia";
            this.btn_RefreshParteRMA.ToolTipText = "Evidenzia";
            this.btn_RefreshParteRMA.Click += new System.EventHandler(this.btn_RefreshParteRMA_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 50);
            // 
            // btn_AddParteRMA
            // 
            this.btn_AddParteRMA.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_AddParteRMA.Image = ((System.Drawing.Image)(resources.GetObject("btn_AddParteRMA.Image")));
            this.btn_AddParteRMA.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_AddParteRMA.Name = "btn_AddParteRMA";
            this.btn_AddParteRMA.Size = new System.Drawing.Size(51, 47);
            this.btn_AddParteRMA.Text = "toolStripButton4";
            this.btn_AddParteRMA.ToolTipText = "Esporta Magazzino";
            this.btn_AddParteRMA.Click += new System.EventHandler(this.btn_AddParteRMA_Click);
            // 
            // btn_EditParteRMA
            // 
            this.btn_EditParteRMA.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_EditParteRMA.Image = ((System.Drawing.Image)(resources.GetObject("btn_EditParteRMA.Image")));
            this.btn_EditParteRMA.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_EditParteRMA.Name = "btn_EditParteRMA";
            this.btn_EditParteRMA.Size = new System.Drawing.Size(51, 47);
            this.btn_EditParteRMA.ToolTipText = "Stampa Test";
            this.btn_EditParteRMA.Click += new System.EventHandler(this.btn_EditParteRMA_Click);
            // 
            // btn_CancelParteRMA
            // 
            this.btn_CancelParteRMA.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_CancelParteRMA.Image = ((System.Drawing.Image)(resources.GetObject("btn_CancelParteRMA.Image")));
            this.btn_CancelParteRMA.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_CancelParteRMA.Name = "btn_CancelParteRMA";
            this.btn_CancelParteRMA.Size = new System.Drawing.Size(51, 47);
            this.btn_CancelParteRMA.Text = "toolStripButton2";
            this.btn_CancelParteRMA.ToolTipText = "Rimuovi Da Magazzino";
            this.btn_CancelParteRMA.Click += new System.EventHandler(this.btn_CancelParteRMA_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 50);
            // 
            // btn_StampaEtichetteParte
            // 
            this.btn_StampaEtichetteParte.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_StampaEtichetteParte.Image = ((System.Drawing.Image)(resources.GetObject("btn_StampaEtichetteParte.Image")));
            this.btn_StampaEtichetteParte.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_StampaEtichetteParte.Name = "btn_StampaEtichetteParte";
            this.btn_StampaEtichetteParte.Size = new System.Drawing.Size(51, 47);
            this.btn_StampaEtichetteParte.Text = "Clona Record";
            this.btn_StampaEtichetteParte.Click += new System.EventHandler(this.btn_StampaEtichetteParte_Click);
            // 
            // cmb_ParteSelection
            // 
            this.cmb_ParteSelection.AutoCompleteCustomSource.AddRange(new string[] {
            "Seleziona Tutti",
            "Deseleziona Tutti"});
            this.cmb_ParteSelection.Items.AddRange(new object[] {
            "Seleziona Tutti",
            "Deseleziona Tutti"});
            this.cmb_ParteSelection.Name = "cmb_ParteSelection";
            this.cmb_ParteSelection.Size = new System.Drawing.Size(121, 50);
            this.cmb_ParteSelection.TextChanged += new System.EventHandler(this.Cmb_ParteSelection_TextChanged);
            // 
            // gb_InfoGenerali
            // 
            this.gb_InfoGenerali.Controls.Add(this.lbl_DataCreazioneRMA);
            this.gb_InfoGenerali.Controls.Add(this.lbl_dataCreazRMA);
            this.gb_InfoGenerali.Controls.Add(this.tb_noteGenerali);
            this.gb_InfoGenerali.Controls.Add(this.lbl_noteGen);
            this.gb_InfoGenerali.Controls.Add(this.cmb_tipoRMA);
            this.gb_InfoGenerali.Controls.Add(this.lbl_NumRMA);
            this.gb_InfoGenerali.Controls.Add(this.lbl_numeroRMA);
            this.gb_InfoGenerali.Controls.Add(this.lbl_tipoRMA);
            this.gb_InfoGenerali.Location = new System.Drawing.Point(6, 6);
            this.gb_InfoGenerali.Name = "gb_InfoGenerali";
            this.gb_InfoGenerali.Size = new System.Drawing.Size(1034, 130);
            this.gb_InfoGenerali.TabIndex = 10;
            this.gb_InfoGenerali.TabStop = false;
            this.gb_InfoGenerali.Text = "Informazioni Generali";
            // 
            // lbl_DataCreazioneRMA
            // 
            this.lbl_DataCreazioneRMA.AutoSize = true;
            this.lbl_DataCreazioneRMA.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_DataCreazioneRMA.Location = new System.Drawing.Point(498, 22);
            this.lbl_DataCreazioneRMA.MaximumSize = new System.Drawing.Size(180, 20);
            this.lbl_DataCreazioneRMA.MinimumSize = new System.Drawing.Size(180, 20);
            this.lbl_DataCreazioneRMA.Name = "lbl_DataCreazioneRMA";
            this.lbl_DataCreazioneRMA.Size = new System.Drawing.Size(180, 20);
            this.lbl_DataCreazioneRMA.TabIndex = 2;
            // 
            // lbl_dataCreazRMA
            // 
            this.lbl_dataCreazRMA.AutoSize = true;
            this.lbl_dataCreazRMA.Location = new System.Drawing.Point(351, 22);
            this.lbl_dataCreazRMA.Name = "lbl_dataCreazRMA";
            this.lbl_dataCreazRMA.Size = new System.Drawing.Size(107, 13);
            this.lbl_dataCreazRMA.TabIndex = 16;
            this.lbl_dataCreazRMA.Text = "Data Creazione RMA";
            // 
            // tb_noteGenerali
            // 
            this.tb_noteGenerali.Location = new System.Drawing.Point(133, 56);
            this.tb_noteGenerali.Multiline = true;
            this.tb_noteGenerali.Name = "tb_noteGenerali";
            this.tb_noteGenerali.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tb_noteGenerali.Size = new System.Drawing.Size(882, 64);
            this.tb_noteGenerali.TabIndex = 4;
            this.tb_noteGenerali.Text = "N/A";
            // 
            // lbl_noteGen
            // 
            this.lbl_noteGen.AutoSize = true;
            this.lbl_noteGen.Location = new System.Drawing.Point(22, 80);
            this.lbl_noteGen.Name = "lbl_noteGen";
            this.lbl_noteGen.Size = new System.Drawing.Size(72, 13);
            this.lbl_noteGen.TabIndex = 14;
            this.lbl_noteGen.Text = "Note Generali";
            // 
            // cmb_tipoRMA
            // 
            this.cmb_tipoRMA.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmb_tipoRMA.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_tipoRMA.FormattingEnabled = true;
            this.cmb_tipoRMA.Location = new System.Drawing.Point(835, 22);
            this.cmb_tipoRMA.MaximumSize = new System.Drawing.Size(180, 0);
            this.cmb_tipoRMA.MinimumSize = new System.Drawing.Size(180, 0);
            this.cmb_tipoRMA.Name = "cmb_tipoRMA";
            this.cmb_tipoRMA.Size = new System.Drawing.Size(180, 21);
            this.cmb_tipoRMA.TabIndex = 3;
            // 
            // lbl_NumRMA
            // 
            this.lbl_NumRMA.AutoSize = true;
            this.lbl_NumRMA.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_NumRMA.Location = new System.Drawing.Point(133, 22);
            this.lbl_NumRMA.MaximumSize = new System.Drawing.Size(180, 20);
            this.lbl_NumRMA.MinimumSize = new System.Drawing.Size(180, 20);
            this.lbl_NumRMA.Name = "lbl_NumRMA";
            this.lbl_NumRMA.Size = new System.Drawing.Size(180, 20);
            this.lbl_NumRMA.TabIndex = 1;
            // 
            // lbl_numeroRMA
            // 
            this.lbl_numeroRMA.AutoSize = true;
            this.lbl_numeroRMA.Location = new System.Drawing.Point(22, 22);
            this.lbl_numeroRMA.Name = "lbl_numeroRMA";
            this.lbl_numeroRMA.Size = new System.Drawing.Size(71, 13);
            this.lbl_numeroRMA.TabIndex = 0;
            this.lbl_numeroRMA.Text = "Numero RMA";
            // 
            // lbl_tipoRMA
            // 
            this.lbl_tipoRMA.AutoSize = true;
            this.lbl_tipoRMA.Location = new System.Drawing.Point(746, 22);
            this.lbl_tipoRMA.Name = "lbl_tipoRMA";
            this.lbl_tipoRMA.Size = new System.Drawing.Size(55, 13);
            this.lbl_tipoRMA.TabIndex = 8;
            this.lbl_tipoRMA.Text = "Tipo RMA";
            // 
            // tab_infoCliente
            // 
            this.tab_infoCliente.BackColor = System.Drawing.Color.Transparent;
            this.tab_infoCliente.Controls.Add(this.gb_dettagli);
            this.tab_infoCliente.Location = new System.Drawing.Point(4, 22);
            this.tab_infoCliente.Name = "tab_infoCliente";
            this.tab_infoCliente.Padding = new System.Windows.Forms.Padding(3);
            this.tab_infoCliente.Size = new System.Drawing.Size(1046, 412);
            this.tab_infoCliente.TabIndex = 1;
            this.tab_infoCliente.Text = "Informativa Cliente";
            // 
            // gb_dettagli
            // 
            this.gb_dettagli.Controls.Add(this.tb_DataOrdineRiparazione);
            this.gb_dettagli.Controls.Add(this.lbl_codCollCli);
            this.gb_dettagli.Controls.Add(this.tb_DataDocumentoDiReso);
            this.gb_dettagli.Controls.Add(this.lbl_idCli);
            this.gb_dettagli.Controls.Add(this.tb_DataScadenzaRMA);
            this.gb_dettagli.Controls.Add(this.tb_DataArrivoCollimatore);
            this.gb_dettagli.Controls.Add(this.lbl_codCollCliente);
            this.gb_dettagli.Controls.Add(this.lbl_idCliente);
            this.gb_dettagli.Controls.Add(this.lbl_dataDocReso);
            this.gb_dettagli.Controls.Add(this.dtp_dataDocReso);
            this.gb_dettagli.Controls.Add(this.dtp_DataArrivoCollimatore);
            this.gb_dettagli.Controls.Add(this.lbl_dataArrColl);
            this.gb_dettagli.Controls.Add(this.dtp_dataScadenzaRMA);
            this.gb_dettagli.Controls.Add(this.lbl_dataScadRMA);
            this.gb_dettagli.Controls.Add(this.dtp_dataOrdRip);
            this.gb_dettagli.Controls.Add(this.lbl_dataOrdRip);
            this.gb_dettagli.Controls.Add(this.lbl_ordRip);
            this.gb_dettagli.Controls.Add(this.cmb_nomeCliente);
            this.gb_dettagli.Controls.Add(this.tb_ordRiparaz);
            this.gb_dettagli.Controls.Add(this.lbl_nomeCliente);
            this.gb_dettagli.Controls.Add(this.tb_numScarCLI);
            this.gb_dettagli.Controls.Add(this.lbl_numSCARcli);
            this.gb_dettagli.Controls.Add(this.cmb_decisCliente);
            this.gb_dettagli.Controls.Add(this.lbl_decisCli);
            this.gb_dettagli.Controls.Add(this.tb_noteCliente);
            this.gb_dettagli.Controls.Add(this.tb_docReso);
            this.gb_dettagli.Controls.Add(this.tb_scarCliente);
            this.gb_dettagli.Controls.Add(this.lbl_docReso);
            this.gb_dettagli.Controls.Add(this.lbl_SCARcli);
            this.gb_dettagli.Controls.Add(this.lbl_difRiscCli);
            this.gb_dettagli.Location = new System.Drawing.Point(6, 6);
            this.gb_dettagli.Name = "gb_dettagli";
            this.gb_dettagli.Size = new System.Drawing.Size(1034, 400);
            this.gb_dettagli.TabIndex = 0;
            this.gb_dettagli.TabStop = false;
            this.gb_dettagli.Text = "Dettagli";
            // 
            // tb_DataOrdineRiparazione
            // 
            this.tb_DataOrdineRiparazione.Location = new System.Drawing.Point(755, 185);
            this.tb_DataOrdineRiparazione.Mask = "00/00/0000";
            this.tb_DataOrdineRiparazione.Name = "tb_DataOrdineRiparazione";
            this.tb_DataOrdineRiparazione.Size = new System.Drawing.Size(211, 20);
            this.tb_DataOrdineRiparazione.TabIndex = 8;
            this.tb_DataOrdineRiparazione.ValidatingType = typeof(System.DateTime);
            // 
            // lbl_codCollCli
            // 
            this.lbl_codCollCli.AutoSize = true;
            this.lbl_codCollCli.Location = new System.Drawing.Point(11, 62);
            this.lbl_codCollCli.Name = "lbl_codCollCli";
            this.lbl_codCollCli.Size = new System.Drawing.Size(129, 13);
            this.lbl_codCollCli.TabIndex = 34;
            this.lbl_codCollCli.Text = "Codice Collimatore Cliente";
            // 
            // tb_DataDocumentoDiReso
            // 
            this.tb_DataDocumentoDiReso.Location = new System.Drawing.Point(755, 226);
            this.tb_DataDocumentoDiReso.Mask = "00/00/0000";
            this.tb_DataDocumentoDiReso.Name = "tb_DataDocumentoDiReso";
            this.tb_DataDocumentoDiReso.Size = new System.Drawing.Size(211, 20);
            this.tb_DataDocumentoDiReso.TabIndex = 10;
            this.tb_DataDocumentoDiReso.ValidatingType = typeof(System.DateTime);
            // 
            // lbl_idCli
            // 
            this.lbl_idCli.AutoSize = true;
            this.lbl_idCli.Location = new System.Drawing.Point(566, 23);
            this.lbl_idCli.Name = "lbl_idCli";
            this.lbl_idCli.Size = new System.Drawing.Size(51, 13);
            this.lbl_idCli.TabIndex = 33;
            this.lbl_idCli.Text = "Id Cliente";
            // 
            // tb_DataScadenzaRMA
            // 
            this.tb_DataScadenzaRMA.Location = new System.Drawing.Point(755, 98);
            this.tb_DataScadenzaRMA.Mask = "00/00/0000";
            this.tb_DataScadenzaRMA.Name = "tb_DataScadenzaRMA";
            this.tb_DataScadenzaRMA.Size = new System.Drawing.Size(211, 20);
            this.tb_DataScadenzaRMA.TabIndex = 4;
            this.tb_DataScadenzaRMA.ValidatingType = typeof(System.DateTime);
            // 
            // tb_DataArrivoCollimatore
            // 
            this.tb_DataArrivoCollimatore.Location = new System.Drawing.Point(170, 98);
            this.tb_DataArrivoCollimatore.Mask = "00/00/0000";
            this.tb_DataArrivoCollimatore.Name = "tb_DataArrivoCollimatore";
            this.tb_DataArrivoCollimatore.Size = new System.Drawing.Size(211, 20);
            this.tb_DataArrivoCollimatore.TabIndex = 3;
            this.tb_DataArrivoCollimatore.ValidatingType = typeof(System.DateTime);
            // 
            // lbl_codCollCliente
            // 
            this.lbl_codCollCliente.AutoSize = true;
            this.lbl_codCollCliente.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_codCollCliente.Location = new System.Drawing.Point(170, 59);
            this.lbl_codCollCliente.MaximumSize = new System.Drawing.Size(250, 20);
            this.lbl_codCollCliente.MinimumSize = new System.Drawing.Size(250, 20);
            this.lbl_codCollCliente.Name = "lbl_codCollCliente";
            this.lbl_codCollCliente.Size = new System.Drawing.Size(250, 20);
            this.lbl_codCollCliente.TabIndex = 32;
            // 
            // lbl_idCliente
            // 
            this.lbl_idCliente.AutoSize = true;
            this.lbl_idCliente.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_idCliente.Location = new System.Drawing.Point(755, 20);
            this.lbl_idCliente.MaximumSize = new System.Drawing.Size(250, 20);
            this.lbl_idCliente.MinimumSize = new System.Drawing.Size(250, 20);
            this.lbl_idCliente.Name = "lbl_idCliente";
            this.lbl_idCliente.Size = new System.Drawing.Size(250, 20);
            this.lbl_idCliente.TabIndex = 31;
            // 
            // lbl_dataDocReso
            // 
            this.lbl_dataDocReso.AutoSize = true;
            this.lbl_dataDocReso.Location = new System.Drawing.Point(566, 229);
            this.lbl_dataDocReso.Name = "lbl_dataDocReso";
            this.lbl_dataDocReso.Size = new System.Drawing.Size(127, 13);
            this.lbl_dataDocReso.TabIndex = 30;
            this.lbl_dataDocReso.Text = "Data Documento di Reso";
            // 
            // dtp_dataDocReso
            // 
            this.dtp_dataDocReso.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_dataDocReso.Location = new System.Drawing.Point(755, 226);
            this.dtp_dataDocReso.MaximumSize = new System.Drawing.Size(250, 20);
            this.dtp_dataDocReso.MinimumSize = new System.Drawing.Size(250, 20);
            this.dtp_dataDocReso.Name = "dtp_dataDocReso";
            this.dtp_dataDocReso.Size = new System.Drawing.Size(250, 20);
            this.dtp_dataDocReso.TabIndex = 10;
            this.dtp_dataDocReso.Value = new System.DateTime(2018, 4, 19, 0, 0, 0, 0);
            this.dtp_dataDocReso.ValueChanged += new System.EventHandler(this.dtp_dataDocReso_ValueChanged);
            // 
            // dtp_DataArrivoCollimatore
            // 
            this.dtp_DataArrivoCollimatore.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_DataArrivoCollimatore.Location = new System.Drawing.Point(170, 98);
            this.dtp_DataArrivoCollimatore.Name = "dtp_DataArrivoCollimatore";
            this.dtp_DataArrivoCollimatore.Size = new System.Drawing.Size(250, 20);
            this.dtp_DataArrivoCollimatore.TabIndex = 3;
            this.dtp_DataArrivoCollimatore.Value = new System.DateTime(2018, 4, 19, 0, 0, 0, 0);
            this.dtp_DataArrivoCollimatore.ValueChanged += new System.EventHandler(this.dtp_DataArrivoCollimatore_ValueChanged);
            // 
            // lbl_dataArrColl
            // 
            this.lbl_dataArrColl.AutoSize = true;
            this.lbl_dataArrColl.Location = new System.Drawing.Point(11, 104);
            this.lbl_dataArrColl.Name = "lbl_dataArrColl";
            this.lbl_dataArrColl.Size = new System.Drawing.Size(114, 13);
            this.lbl_dataArrColl.TabIndex = 28;
            this.lbl_dataArrColl.Text = "Data Arrivo Collimatore";
            // 
            // dtp_dataScadenzaRMA
            // 
            this.dtp_dataScadenzaRMA.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_dataScadenzaRMA.Location = new System.Drawing.Point(755, 98);
            this.dtp_dataScadenzaRMA.MaximumSize = new System.Drawing.Size(250, 20);
            this.dtp_dataScadenzaRMA.MinimumSize = new System.Drawing.Size(250, 20);
            this.dtp_dataScadenzaRMA.Name = "dtp_dataScadenzaRMA";
            this.dtp_dataScadenzaRMA.Size = new System.Drawing.Size(250, 20);
            this.dtp_dataScadenzaRMA.TabIndex = 4;
            this.dtp_dataScadenzaRMA.Value = new System.DateTime(2018, 4, 19, 0, 0, 0, 0);
            this.dtp_dataScadenzaRMA.ValueChanged += new System.EventHandler(this.dtp_dataScadenzaRMA_ValueChanged);
            // 
            // lbl_dataScadRMA
            // 
            this.lbl_dataScadRMA.AutoSize = true;
            this.lbl_dataScadRMA.Location = new System.Drawing.Point(566, 104);
            this.lbl_dataScadRMA.Name = "lbl_dataScadRMA";
            this.lbl_dataScadRMA.Size = new System.Drawing.Size(108, 13);
            this.lbl_dataScadRMA.TabIndex = 13;
            this.lbl_dataScadRMA.Text = "Data Scadenza RMA";
            // 
            // dtp_dataOrdRip
            // 
            this.dtp_dataOrdRip.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_dataOrdRip.Location = new System.Drawing.Point(755, 185);
            this.dtp_dataOrdRip.MaximumSize = new System.Drawing.Size(250, 20);
            this.dtp_dataOrdRip.MinimumSize = new System.Drawing.Size(250, 20);
            this.dtp_dataOrdRip.Name = "dtp_dataOrdRip";
            this.dtp_dataOrdRip.Size = new System.Drawing.Size(250, 20);
            this.dtp_dataOrdRip.TabIndex = 8;
            this.dtp_dataOrdRip.Value = new System.DateTime(2018, 4, 19, 0, 0, 0, 0);
            this.dtp_dataOrdRip.ValueChanged += new System.EventHandler(this.dtp_dataOrdRip_ValueChanged);
            // 
            // lbl_dataOrdRip
            // 
            this.lbl_dataOrdRip.AutoSize = true;
            this.lbl_dataOrdRip.Location = new System.Drawing.Point(566, 188);
            this.lbl_dataOrdRip.Name = "lbl_dataOrdRip";
            this.lbl_dataOrdRip.Size = new System.Drawing.Size(123, 13);
            this.lbl_dataOrdRip.TabIndex = 20;
            this.lbl_dataOrdRip.Text = "Data Ordine Riparazione";
            // 
            // lbl_ordRip
            // 
            this.lbl_ordRip.AutoSize = true;
            this.lbl_ordRip.Location = new System.Drawing.Point(11, 188);
            this.lbl_ordRip.Name = "lbl_ordRip";
            this.lbl_ordRip.Size = new System.Drawing.Size(97, 13);
            this.lbl_ordRip.TabIndex = 4;
            this.lbl_ordRip.Text = "Ordine Riparazione";
            // 
            // cmb_nomeCliente
            // 
            this.cmb_nomeCliente.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmb_nomeCliente.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_nomeCliente.FormattingEnabled = true;
            this.cmb_nomeCliente.Items.AddRange(new object[] {
            "Invio al cliente di materiale per la riparazione",
            "Invio al cliente di personale Ralco per riparazione On Site",
            "Riparazione effettuata da Ralco",
            "Riparazione effettuata dal cliente"});
            this.cmb_nomeCliente.Location = new System.Drawing.Point(170, 17);
            this.cmb_nomeCliente.MaximumSize = new System.Drawing.Size(250, 0);
            this.cmb_nomeCliente.MinimumSize = new System.Drawing.Size(250, 0);
            this.cmb_nomeCliente.Name = "cmb_nomeCliente";
            this.cmb_nomeCliente.Size = new System.Drawing.Size(250, 21);
            this.cmb_nomeCliente.Sorted = true;
            this.cmb_nomeCliente.TabIndex = 1;
            this.cmb_nomeCliente.SelectedIndexChanged += new System.EventHandler(this.cmb_nomeCliente_SelectedIndexChanged);
            // 
            // tb_ordRiparaz
            // 
            this.tb_ordRiparaz.Location = new System.Drawing.Point(170, 185);
            this.tb_ordRiparaz.MaximumSize = new System.Drawing.Size(250, 20);
            this.tb_ordRiparaz.MinimumSize = new System.Drawing.Size(250, 20);
            this.tb_ordRiparaz.Name = "tb_ordRiparaz";
            this.tb_ordRiparaz.Size = new System.Drawing.Size(250, 20);
            this.tb_ordRiparaz.TabIndex = 7;
            this.tb_ordRiparaz.Text = "N/A";
            // 
            // lbl_nomeCliente
            // 
            this.lbl_nomeCliente.AutoSize = true;
            this.lbl_nomeCliente.Location = new System.Drawing.Point(11, 20);
            this.lbl_nomeCliente.Name = "lbl_nomeCliente";
            this.lbl_nomeCliente.Size = new System.Drawing.Size(70, 13);
            this.lbl_nomeCliente.TabIndex = 23;
            this.lbl_nomeCliente.Text = "Nome Cliente";
            // 
            // tb_numScarCLI
            // 
            this.tb_numScarCLI.Location = new System.Drawing.Point(755, 142);
            this.tb_numScarCLI.MaximumSize = new System.Drawing.Size(250, 20);
            this.tb_numScarCLI.MinimumSize = new System.Drawing.Size(250, 20);
            this.tb_numScarCLI.Name = "tb_numScarCLI";
            this.tb_numScarCLI.Size = new System.Drawing.Size(250, 20);
            this.tb_numScarCLI.TabIndex = 6;
            this.tb_numScarCLI.Text = "N/A";
            // 
            // lbl_numSCARcli
            // 
            this.lbl_numSCARcli.AutoSize = true;
            this.lbl_numSCARcli.Location = new System.Drawing.Point(566, 146);
            this.lbl_numSCARcli.Name = "lbl_numSCARcli";
            this.lbl_numSCARcli.Size = new System.Drawing.Size(111, 13);
            this.lbl_numSCARcli.TabIndex = 18;
            this.lbl_numSCARcli.Text = "Numero SCAR Cliente";
            // 
            // cmb_decisCliente
            // 
            this.cmb_decisCliente.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmb_decisCliente.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_decisCliente.FormattingEnabled = true;
            this.cmb_decisCliente.Items.AddRange(new object[] {
            "Invio al cliente di materiale per la riparazione / Sending repairment material to" +
                " the customer",
            "Invio al cliente di personale Ralco per riparazione On Site / Sending Ralco perso" +
                "nnel to the customer for repairment On Site",
            "Riparazione effettuata dal cliente / Repairment carried out by the customer",
            "Riparazione effettuata in Ralco / Repairment carried out by Ralco"});
            this.cmb_decisCliente.Location = new System.Drawing.Point(755, 62);
            this.cmb_decisCliente.MaximumSize = new System.Drawing.Size(250, 0);
            this.cmb_decisCliente.MinimumSize = new System.Drawing.Size(250, 0);
            this.cmb_decisCliente.Name = "cmb_decisCliente";
            this.cmb_decisCliente.Size = new System.Drawing.Size(250, 21);
            this.cmb_decisCliente.Sorted = true;
            this.cmb_decisCliente.TabIndex = 2;
            // 
            // lbl_decisCli
            // 
            this.lbl_decisCli.AutoSize = true;
            this.lbl_decisCli.Location = new System.Drawing.Point(566, 65);
            this.lbl_decisCli.Name = "lbl_decisCli";
            this.lbl_decisCli.Size = new System.Drawing.Size(89, 13);
            this.lbl_decisCli.TabIndex = 16;
            this.lbl_decisCli.Text = "Decisione Cliente";
            // 
            // tb_noteCliente
            // 
            this.tb_noteCliente.Location = new System.Drawing.Point(170, 278);
            this.tb_noteCliente.Multiline = true;
            this.tb_noteCliente.Name = "tb_noteCliente";
            this.tb_noteCliente.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tb_noteCliente.Size = new System.Drawing.Size(842, 110);
            this.tb_noteCliente.TabIndex = 11;
            this.tb_noteCliente.Text = "N/A";
            // 
            // tb_docReso
            // 
            this.tb_docReso.Location = new System.Drawing.Point(170, 226);
            this.tb_docReso.MaximumSize = new System.Drawing.Size(250, 20);
            this.tb_docReso.MinimumSize = new System.Drawing.Size(250, 20);
            this.tb_docReso.Name = "tb_docReso";
            this.tb_docReso.Size = new System.Drawing.Size(250, 20);
            this.tb_docReso.TabIndex = 9;
            this.tb_docReso.Text = "N/A";
            // 
            // tb_scarCliente
            // 
            this.tb_scarCliente.Location = new System.Drawing.Point(170, 143);
            this.tb_scarCliente.MaximumSize = new System.Drawing.Size(250, 20);
            this.tb_scarCliente.MinimumSize = new System.Drawing.Size(250, 20);
            this.tb_scarCliente.Name = "tb_scarCliente";
            this.tb_scarCliente.Size = new System.Drawing.Size(250, 20);
            this.tb_scarCliente.TabIndex = 5;
            this.tb_scarCliente.Text = "N/A";
            // 
            // lbl_docReso
            // 
            this.lbl_docReso.AutoSize = true;
            this.lbl_docReso.Location = new System.Drawing.Point(11, 229);
            this.lbl_docReso.Name = "lbl_docReso";
            this.lbl_docReso.Size = new System.Drawing.Size(101, 13);
            this.lbl_docReso.TabIndex = 3;
            this.lbl_docReso.Text = "Documento di Reso";
            // 
            // lbl_SCARcli
            // 
            this.lbl_SCARcli.AutoSize = true;
            this.lbl_SCARcli.Location = new System.Drawing.Point(11, 146);
            this.lbl_SCARcli.Name = "lbl_SCARcli";
            this.lbl_SCARcli.Size = new System.Drawing.Size(71, 13);
            this.lbl_SCARcli.TabIndex = 1;
            this.lbl_SCARcli.Text = "SCAR Cliente";
            // 
            // lbl_difRiscCli
            // 
            this.lbl_difRiscCli.AutoSize = true;
            this.lbl_difRiscCli.Location = new System.Drawing.Point(11, 326);
            this.lbl_difRiscCli.Name = "lbl_difRiscCli";
            this.lbl_difRiscCli.Size = new System.Drawing.Size(149, 13);
            this.lbl_difRiscCli.TabIndex = 0;
            this.lbl_difRiscCli.Text = "Difetto Riscontrato Dal Cliente";
            // 
            // tab_Allegati
            // 
            this.tab_Allegati.BackColor = System.Drawing.Color.Transparent;
            this.tab_Allegati.Controls.Add(this.dgv_ListaAllegatiRMA);
            this.tab_Allegati.Controls.Add(this.toolStrip_MenuAllegati);
            this.tab_Allegati.Location = new System.Drawing.Point(4, 22);
            this.tab_Allegati.Name = "tab_Allegati";
            this.tab_Allegati.Size = new System.Drawing.Size(1046, 412);
            this.tab_Allegati.TabIndex = 12;
            this.tab_Allegati.Text = "Allegati";
            // 
            // dgv_ListaAllegatiRMA
            // 
            this.dgv_ListaAllegatiRMA.AllowUserToAddRows = false;
            this.dgv_ListaAllegatiRMA.AllowUserToOrderColumns = true;
            this.dgv_ListaAllegatiRMA.BackgroundColor = System.Drawing.SystemColors.ActiveBorder;
            this.dgv_ListaAllegatiRMA.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_ListaAllegatiRMA.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.chkAll,
            this.nomeFile});
            this.dgv_ListaAllegatiRMA.Cursor = System.Windows.Forms.Cursors.Default;
            this.dgv_ListaAllegatiRMA.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_ListaAllegatiRMA.Location = new System.Drawing.Point(0, 50);
            this.dgv_ListaAllegatiRMA.Name = "dgv_ListaAllegatiRMA";
            this.dgv_ListaAllegatiRMA.ReadOnly = true;
            this.dgv_ListaAllegatiRMA.RowHeadersVisible = false;
            this.dgv_ListaAllegatiRMA.Size = new System.Drawing.Size(1046, 362);
            this.dgv_ListaAllegatiRMA.TabIndex = 24;
            this.dgv_ListaAllegatiRMA.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_ListaAllegatiRMA_CellContentClick);
            // 
            // chkAll
            // 
            this.chkAll.HeaderText = "";
            this.chkAll.Name = "chkAll";
            this.chkAll.ReadOnly = true;
            this.chkAll.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.chkAll.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.chkAll.Width = 30;
            // 
            // nomeFile
            // 
            this.nomeFile.HeaderText = "Nome File";
            this.nomeFile.Name = "nomeFile";
            this.nomeFile.ReadOnly = true;
            this.nomeFile.Width = 980;
            // 
            // toolStrip_MenuAllegati
            // 
            this.toolStrip_MenuAllegati.AutoSize = false;
            this.toolStrip_MenuAllegati.BackColor = System.Drawing.SystemColors.Control;
            this.toolStrip_MenuAllegati.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip_MenuAllegati.ImageScalingSize = new System.Drawing.Size(47, 47);
            this.toolStrip_MenuAllegati.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btn_RefreshAllegati,
            this.toolStripSeparator3,
            this.btn_AddAllegati,
            this.btn_DeleteAllegati,
            this.toolStripSeparator4,
            this.btn_viewAllegati});
            this.toolStrip_MenuAllegati.Location = new System.Drawing.Point(0, 0);
            this.toolStrip_MenuAllegati.Name = "toolStrip_MenuAllegati";
            this.toolStrip_MenuAllegati.Size = new System.Drawing.Size(1046, 50);
            this.toolStrip_MenuAllegati.TabIndex = 26;
            this.toolStrip_MenuAllegati.Text = "toolStrip1";
            // 
            // btn_RefreshAllegati
            // 
            this.btn_RefreshAllegati.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_RefreshAllegati.Image = ((System.Drawing.Image)(resources.GetObject("btn_RefreshAllegati.Image")));
            this.btn_RefreshAllegati.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_RefreshAllegati.Name = "btn_RefreshAllegati";
            this.btn_RefreshAllegati.Size = new System.Drawing.Size(51, 47);
            this.btn_RefreshAllegati.Text = "Evidenzia";
            this.btn_RefreshAllegati.ToolTipText = "Evidenzia";
            this.btn_RefreshAllegati.Click += new System.EventHandler(this.btn_RefreshAllegati_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 50);
            // 
            // btn_AddAllegati
            // 
            this.btn_AddAllegati.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_AddAllegati.Image = ((System.Drawing.Image)(resources.GetObject("btn_AddAllegati.Image")));
            this.btn_AddAllegati.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_AddAllegati.Name = "btn_AddAllegati";
            this.btn_AddAllegati.Size = new System.Drawing.Size(51, 47);
            this.btn_AddAllegati.Text = "toolStripButton4";
            this.btn_AddAllegati.ToolTipText = "Esporta Magazzino";
            this.btn_AddAllegati.Click += new System.EventHandler(this.btn_AddAllegati_Click);
            // 
            // btn_DeleteAllegati
            // 
            this.btn_DeleteAllegati.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_DeleteAllegati.Image = ((System.Drawing.Image)(resources.GetObject("btn_DeleteAllegati.Image")));
            this.btn_DeleteAllegati.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_DeleteAllegati.Name = "btn_DeleteAllegati";
            this.btn_DeleteAllegati.Size = new System.Drawing.Size(51, 47);
            this.btn_DeleteAllegati.Text = "toolStripButton2";
            this.btn_DeleteAllegati.ToolTipText = "Rimuovi Da Magazzino";
            this.btn_DeleteAllegati.Click += new System.EventHandler(this.btn_DeleteAllegati_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 50);
            // 
            // btn_viewAllegati
            // 
            this.btn_viewAllegati.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_viewAllegati.Image = ((System.Drawing.Image)(resources.GetObject("btn_viewAllegati.Image")));
            this.btn_viewAllegati.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_viewAllegati.Name = "btn_viewAllegati";
            this.btn_viewAllegati.Size = new System.Drawing.Size(51, 47);
            this.btn_viewAllegati.Text = "Clona Record";
            this.btn_viewAllegati.Click += new System.EventHandler(this.Btn_viewAllegati_Click);
            // 
            // tab_detGaranzia
            // 
            this.tab_detGaranzia.BackColor = System.Drawing.Color.Transparent;
            this.tab_detGaranzia.Controls.Add(this.groupBox12);
            this.tab_detGaranzia.Location = new System.Drawing.Point(4, 22);
            this.tab_detGaranzia.Name = "tab_detGaranzia";
            this.tab_detGaranzia.Size = new System.Drawing.Size(1046, 412);
            this.tab_detGaranzia.TabIndex = 7;
            this.tab_detGaranzia.Text = "Garanzia";
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.cmb_statoGaranzia);
            this.groupBox12.Controls.Add(this.rb_statoGaranzia);
            this.groupBox12.Controls.Add(this.cmb_azioniSupplRalco);
            this.groupBox12.Controls.Add(this.lbl_azSupplRalco);
            this.groupBox12.Controls.Add(this.cmb_respDannoCollimatore);
            this.groupBox12.Controls.Add(this.tb_noteGaranziaRalco);
            this.groupBox12.Controls.Add(this.lbl_note);
            this.groupBox12.Controls.Add(this.lbl_respDannoColl);
            this.groupBox12.Controls.Add(this.cmb_provCollimatore);
            this.groupBox12.Controls.Add(this.lbl_provColl);
            this.groupBox12.Controls.Add(this.cmb_anniVitaCollimatore);
            this.groupBox12.Controls.Add(this.lbl_anniVitaColl);
            this.groupBox12.Location = new System.Drawing.Point(6, 6);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(1037, 400);
            this.groupBox12.TabIndex = 8;
            this.groupBox12.TabStop = false;
            // 
            // cmb_statoGaranzia
            // 
            this.cmb_statoGaranzia.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmb_statoGaranzia.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple;
            this.cmb_statoGaranzia.Enabled = false;
            this.cmb_statoGaranzia.FormattingEnabled = true;
            this.cmb_statoGaranzia.Items.AddRange(new object[] {
            "In Garanzia / In Warranty",
            "Fuori Garanzia / Out Of Warranty"});
            this.cmb_statoGaranzia.Location = new System.Drawing.Point(497, 314);
            this.cmb_statoGaranzia.Name = "cmb_statoGaranzia";
            this.cmb_statoGaranzia.Size = new System.Drawing.Size(405, 21);
            this.cmb_statoGaranzia.TabIndex = 25;
            // 
            // rb_statoGaranzia
            // 
            this.rb_statoGaranzia.AutoSize = true;
            this.rb_statoGaranzia.Location = new System.Drawing.Point(97, 315);
            this.rb_statoGaranzia.Name = "rb_statoGaranzia";
            this.rb_statoGaranzia.Size = new System.Drawing.Size(95, 17);
            this.rb_statoGaranzia.TabIndex = 24;
            this.rb_statoGaranzia.TabStop = true;
            this.rb_statoGaranzia.Text = "Stato Garanzia";
            this.rb_statoGaranzia.UseVisualStyleBackColor = true;
            this.rb_statoGaranzia.CheckedChanged += new System.EventHandler(this.rb_statoGaranzia_CheckedChanged);
            // 
            // cmb_azioniSupplRalco
            // 
            this.cmb_azioniSupplRalco.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmb_azioniSupplRalco.FormattingEnabled = true;
            this.cmb_azioniSupplRalco.Items.AddRange(new object[] {
            "Garanzia con addebito / Warranty with charge",
            "Riparazione in deroga Ralco / Repair in derogation"});
            this.cmb_azioniSupplRalco.Location = new System.Drawing.Point(497, 363);
            this.cmb_azioniSupplRalco.Name = "cmb_azioniSupplRalco";
            this.cmb_azioniSupplRalco.Size = new System.Drawing.Size(405, 21);
            this.cmb_azioniSupplRalco.Sorted = true;
            this.cmb_azioniSupplRalco.TabIndex = 6;
            // 
            // lbl_azSupplRalco
            // 
            this.lbl_azSupplRalco.AutoSize = true;
            this.lbl_azSupplRalco.Location = new System.Drawing.Point(94, 363);
            this.lbl_azSupplRalco.Name = "lbl_azSupplRalco";
            this.lbl_azSupplRalco.Size = new System.Drawing.Size(134, 13);
            this.lbl_azSupplRalco.TabIndex = 22;
            this.lbl_azSupplRalco.Text = "Azioni supplementari Ralco";
            // 
            // cmb_respDannoCollimatore
            // 
            this.cmb_respDannoCollimatore.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmb_respDannoCollimatore.FormattingEnabled = true;
            this.cmb_respDannoCollimatore.Items.AddRange(new object[] {
            "Cliente / Customer",
            "Ralco"});
            this.cmb_respDannoCollimatore.Location = new System.Drawing.Point(497, 111);
            this.cmb_respDannoCollimatore.Name = "cmb_respDannoCollimatore";
            this.cmb_respDannoCollimatore.Size = new System.Drawing.Size(405, 21);
            this.cmb_respDannoCollimatore.Sorted = true;
            this.cmb_respDannoCollimatore.TabIndex = 3;
            this.cmb_respDannoCollimatore.SelectedIndexChanged += new System.EventHandler(this.cmb_respDannoCollimatore_SelectedIndexChanged);
            // 
            // tb_noteGaranziaRalco
            // 
            this.tb_noteGaranziaRalco.Location = new System.Drawing.Point(497, 159);
            this.tb_noteGaranziaRalco.Multiline = true;
            this.tb_noteGaranziaRalco.Name = "tb_noteGaranziaRalco";
            this.tb_noteGaranziaRalco.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tb_noteGaranziaRalco.Size = new System.Drawing.Size(405, 130);
            this.tb_noteGaranziaRalco.TabIndex = 4;
            this.tb_noteGaranziaRalco.Text = "N/A";
            // 
            // lbl_note
            // 
            this.lbl_note.AutoSize = true;
            this.lbl_note.Location = new System.Drawing.Point(94, 212);
            this.lbl_note.Name = "lbl_note";
            this.lbl_note.Size = new System.Drawing.Size(30, 13);
            this.lbl_note.TabIndex = 19;
            this.lbl_note.Text = "Note";
            // 
            // lbl_respDannoColl
            // 
            this.lbl_respDannoColl.AutoSize = true;
            this.lbl_respDannoColl.Location = new System.Drawing.Point(94, 111);
            this.lbl_respDannoColl.Name = "lbl_respDannoColl";
            this.lbl_respDannoColl.Size = new System.Drawing.Size(245, 13);
            this.lbl_respDannoColl.TabIndex = 17;
            this.lbl_respDannoColl.Text = "Responsabilità del danneggiamento del collimatore";
            // 
            // cmb_provCollimatore
            // 
            this.cmb_provCollimatore.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmb_provCollimatore.FormattingEnabled = true;
            this.cmb_provCollimatore.Items.AddRange(new object[] {
            "Cliente / Customer",
            "Service"});
            this.cmb_provCollimatore.Location = new System.Drawing.Point(497, 24);
            this.cmb_provCollimatore.Name = "cmb_provCollimatore";
            this.cmb_provCollimatore.Size = new System.Drawing.Size(405, 21);
            this.cmb_provCollimatore.Sorted = true;
            this.cmb_provCollimatore.TabIndex = 1;
            // 
            // lbl_provColl
            // 
            this.lbl_provColl.AutoSize = true;
            this.lbl_provColl.Location = new System.Drawing.Point(94, 24);
            this.lbl_provColl.Name = "lbl_provColl";
            this.lbl_provColl.Size = new System.Drawing.Size(136, 13);
            this.lbl_provColl.TabIndex = 16;
            this.lbl_provColl.Text = "Provenienza del collimatore";
            // 
            // cmb_anniVitaCollimatore
            // 
            this.cmb_anniVitaCollimatore.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmb_anniVitaCollimatore.FormattingEnabled = true;
            this.cmb_anniVitaCollimatore.Items.AddRange(new object[] {
            "Meno di 2 / Less than 2",
            "Più di 2 / More than 2"});
            this.cmb_anniVitaCollimatore.Location = new System.Drawing.Point(497, 66);
            this.cmb_anniVitaCollimatore.Name = "cmb_anniVitaCollimatore";
            this.cmb_anniVitaCollimatore.Size = new System.Drawing.Size(405, 21);
            this.cmb_anniVitaCollimatore.Sorted = true;
            this.cmb_anniVitaCollimatore.TabIndex = 2;
            this.cmb_anniVitaCollimatore.SelectedIndexChanged += new System.EventHandler(this.cmb_anniVitaCollimatore_SelectedIndexChanged);
            // 
            // lbl_anniVitaColl
            // 
            this.lbl_anniVitaColl.AutoSize = true;
            this.lbl_anniVitaColl.Location = new System.Drawing.Point(94, 66);
            this.lbl_anniVitaColl.Name = "lbl_anniVitaColl";
            this.lbl_anniVitaColl.Size = new System.Drawing.Size(129, 13);
            this.lbl_anniVitaColl.TabIndex = 7;
            this.lbl_anniVitaColl.Text = "Anni di vita del collimatore";
            // 
            // tab_Analisi
            // 
            this.tab_Analisi.BackColor = System.Drawing.Color.Transparent;
            this.tab_Analisi.Controls.Add(this.dgv_ListaProblemiRMA);
            this.tab_Analisi.Controls.Add(this.toolStrip_MenuAnalisiProblema);
            this.tab_Analisi.Location = new System.Drawing.Point(4, 22);
            this.tab_Analisi.Name = "tab_Analisi";
            this.tab_Analisi.Size = new System.Drawing.Size(1046, 412);
            this.tab_Analisi.TabIndex = 5;
            this.tab_Analisi.Text = "Analisi del Problema";
            // 
            // dgv_ListaProblemiRMA
            // 
            this.dgv_ListaProblemiRMA.AllowUserToAddRows = false;
            this.dgv_ListaProblemiRMA.AllowUserToOrderColumns = true;
            this.dgv_ListaProblemiRMA.BackgroundColor = System.Drawing.SystemColors.ActiveBorder;
            this.dgv_ListaProblemiRMA.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_ListaProblemiRMA.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.chk_Prob,
            this.difettoProbl,
            this.dataAggProb,
            this.gravitaProb,
            this.idProblema});
            this.dgv_ListaProblemiRMA.Cursor = System.Windows.Forms.Cursors.Default;
            this.dgv_ListaProblemiRMA.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_ListaProblemiRMA.Location = new System.Drawing.Point(0, 50);
            this.dgv_ListaProblemiRMA.Name = "dgv_ListaProblemiRMA";
            this.dgv_ListaProblemiRMA.ReadOnly = true;
            this.dgv_ListaProblemiRMA.RowHeadersVisible = false;
            this.dgv_ListaProblemiRMA.Size = new System.Drawing.Size(1046, 362);
            this.dgv_ListaProblemiRMA.TabIndex = 27;
            this.dgv_ListaProblemiRMA.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_ListaProblemiRMA_CellContentClick);
            // 
            // chk_Prob
            // 
            this.chk_Prob.HeaderText = "";
            this.chk_Prob.Name = "chk_Prob";
            this.chk_Prob.ReadOnly = true;
            this.chk_Prob.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.chk_Prob.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.chk_Prob.Width = 30;
            // 
            // difettoProbl
            // 
            this.difettoProbl.HeaderText = "Descrizione";
            this.difettoProbl.Name = "difettoProbl";
            this.difettoProbl.ReadOnly = true;
            this.difettoProbl.Width = 780;
            // 
            // dataAggProb
            // 
            this.dataAggProb.HeaderText = "Data Aggiornamento";
            this.dataAggProb.Name = "dataAggProb";
            this.dataAggProb.ReadOnly = true;
            this.dataAggProb.Width = 130;
            // 
            // gravitaProb
            // 
            this.gravitaProb.HeaderText = "Gravità";
            this.gravitaProb.Name = "gravitaProb";
            this.gravitaProb.ReadOnly = true;
            this.gravitaProb.Width = 70;
            // 
            // idProblema
            // 
            this.idProblema.HeaderText = "ID PROBLEMA";
            this.idProblema.Name = "idProblema";
            this.idProblema.ReadOnly = true;
            this.idProblema.Visible = false;
            // 
            // toolStrip_MenuAnalisiProblema
            // 
            this.toolStrip_MenuAnalisiProblema.AutoSize = false;
            this.toolStrip_MenuAnalisiProblema.BackColor = System.Drawing.SystemColors.Control;
            this.toolStrip_MenuAnalisiProblema.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip_MenuAnalisiProblema.ImageScalingSize = new System.Drawing.Size(47, 47);
            this.toolStrip_MenuAnalisiProblema.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btn_RefreshProbl,
            this.toolStripSeparator6,
            this.btn_AddProbl,
            this.btn_importProbFromTemplateDB,
            this.btn_CloneProblema,
            this.btn_EditProbl,
            this.btn_DeleteProbl,
            this.toolStripSeparator7,
            this.btn_addTemplateProbDB,
            this.btn_editTemplateProbDB});
            this.toolStrip_MenuAnalisiProblema.Location = new System.Drawing.Point(0, 0);
            this.toolStrip_MenuAnalisiProblema.Name = "toolStrip_MenuAnalisiProblema";
            this.toolStrip_MenuAnalisiProblema.Size = new System.Drawing.Size(1046, 50);
            this.toolStrip_MenuAnalisiProblema.TabIndex = 26;
            this.toolStrip_MenuAnalisiProblema.Text = "toolStrip2";
            // 
            // btn_RefreshProbl
            // 
            this.btn_RefreshProbl.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_RefreshProbl.Image = ((System.Drawing.Image)(resources.GetObject("btn_RefreshProbl.Image")));
            this.btn_RefreshProbl.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_RefreshProbl.Name = "btn_RefreshProbl";
            this.btn_RefreshProbl.Size = new System.Drawing.Size(51, 47);
            this.btn_RefreshProbl.Text = "Evidenzia";
            this.btn_RefreshProbl.ToolTipText = "Evidenzia";
            this.btn_RefreshProbl.Click += new System.EventHandler(this.btn_RefreshProbl_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 50);
            // 
            // btn_AddProbl
            // 
            this.btn_AddProbl.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_AddProbl.Image = ((System.Drawing.Image)(resources.GetObject("btn_AddProbl.Image")));
            this.btn_AddProbl.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_AddProbl.Name = "btn_AddProbl";
            this.btn_AddProbl.Size = new System.Drawing.Size(51, 47);
            this.btn_AddProbl.Text = "toolStripButton4";
            this.btn_AddProbl.ToolTipText = "Esporta Magazzino";
            this.btn_AddProbl.Click += new System.EventHandler(this.btn_AddProbl_Click);
            // 
            // btn_importProbFromTemplateDB
            // 
            this.btn_importProbFromTemplateDB.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_importProbFromTemplateDB.Image = ((System.Drawing.Image)(resources.GetObject("btn_importProbFromTemplateDB.Image")));
            this.btn_importProbFromTemplateDB.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_importProbFromTemplateDB.Name = "btn_importProbFromTemplateDB";
            this.btn_importProbFromTemplateDB.Size = new System.Drawing.Size(51, 47);
            this.btn_importProbFromTemplateDB.Text = "toolStripButton5";
            this.btn_importProbFromTemplateDB.Click += new System.EventHandler(this.btn_importProbFromTemplateDB_Click);
            // 
            // btn_CloneProblema
            // 
            this.btn_CloneProblema.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_CloneProblema.Image = ((System.Drawing.Image)(resources.GetObject("btn_CloneProblema.Image")));
            this.btn_CloneProblema.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_CloneProblema.Name = "btn_CloneProblema";
            this.btn_CloneProblema.Size = new System.Drawing.Size(51, 47);
            this.btn_CloneProblema.Text = "toolStripButton5";
            this.btn_CloneProblema.Click += new System.EventHandler(this.btn_CloneProblema_Click);
            // 
            // btn_EditProbl
            // 
            this.btn_EditProbl.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_EditProbl.Image = ((System.Drawing.Image)(resources.GetObject("btn_EditProbl.Image")));
            this.btn_EditProbl.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_EditProbl.Name = "btn_EditProbl";
            this.btn_EditProbl.Size = new System.Drawing.Size(51, 47);
            this.btn_EditProbl.Text = "toolStripButton5";
            this.btn_EditProbl.Click += new System.EventHandler(this.btn_EditProbl_Click);
            // 
            // btn_DeleteProbl
            // 
            this.btn_DeleteProbl.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_DeleteProbl.Image = ((System.Drawing.Image)(resources.GetObject("btn_DeleteProbl.Image")));
            this.btn_DeleteProbl.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_DeleteProbl.Name = "btn_DeleteProbl";
            this.btn_DeleteProbl.Size = new System.Drawing.Size(51, 47);
            this.btn_DeleteProbl.Text = "toolStripButton2";
            this.btn_DeleteProbl.ToolTipText = "Rimuovi Da Magazzino";
            this.btn_DeleteProbl.Click += new System.EventHandler(this.btn_DeleteProbl_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 50);
            // 
            // btn_addTemplateProbDB
            // 
            this.btn_addTemplateProbDB.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_addTemplateProbDB.Image = ((System.Drawing.Image)(resources.GetObject("btn_addTemplateProbDB.Image")));
            this.btn_addTemplateProbDB.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_addTemplateProbDB.Name = "btn_addTemplateProbDB";
            this.btn_addTemplateProbDB.Size = new System.Drawing.Size(51, 47);
            this.btn_addTemplateProbDB.Text = "Clona Record";
            this.btn_addTemplateProbDB.Click += new System.EventHandler(this.btn_addTemplateProbDB_Click);
            // 
            // btn_editTemplateProbDB
            // 
            this.btn_editTemplateProbDB.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_editTemplateProbDB.Image = ((System.Drawing.Image)(resources.GetObject("btn_editTemplateProbDB.Image")));
            this.btn_editTemplateProbDB.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_editTemplateProbDB.Name = "btn_editTemplateProbDB";
            this.btn_editTemplateProbDB.Size = new System.Drawing.Size(51, 47);
            this.btn_editTemplateProbDB.Text = "toolStripButton5";
            this.btn_editTemplateProbDB.Click += new System.EventHandler(this.btn_editTemplateProbDB_Click);
            // 
            // tab_FasiDaRieseguire
            // 
            this.tab_FasiDaRieseguire.BackColor = System.Drawing.Color.Transparent;
            this.tab_FasiDaRieseguire.Controls.Add(this.dgv_FasiDaRieseguire);
            this.tab_FasiDaRieseguire.Location = new System.Drawing.Point(4, 22);
            this.tab_FasiDaRieseguire.Name = "tab_FasiDaRieseguire";
            this.tab_FasiDaRieseguire.Size = new System.Drawing.Size(1046, 412);
            this.tab_FasiDaRieseguire.TabIndex = 8;
            this.tab_FasiDaRieseguire.Text = "Fasi Da Rieseguire";
            // 
            // dgv_FasiDaRieseguire
            // 
            this.dgv_FasiDaRieseguire.AllowUserToAddRows = false;
            this.dgv_FasiDaRieseguire.AllowUserToOrderColumns = true;
            this.dgv_FasiDaRieseguire.BackgroundColor = System.Drawing.SystemColors.ActiveBorder;
            this.dgv_FasiDaRieseguire.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_FasiDaRieseguire.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewCheckBoxColumn2,
            this.descFase,
            this.minLavoro});
            this.dgv_FasiDaRieseguire.Cursor = System.Windows.Forms.Cursors.Default;
            this.dgv_FasiDaRieseguire.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_FasiDaRieseguire.Location = new System.Drawing.Point(0, 0);
            this.dgv_FasiDaRieseguire.Name = "dgv_FasiDaRieseguire";
            this.dgv_FasiDaRieseguire.RowHeadersVisible = false;
            this.dgv_FasiDaRieseguire.Size = new System.Drawing.Size(1046, 412);
            this.dgv_FasiDaRieseguire.TabIndex = 7;
            this.dgv_FasiDaRieseguire.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_FasiDaRieseguire_CellContentClick);
            // 
            // dataGridViewCheckBoxColumn2
            // 
            this.dataGridViewCheckBoxColumn2.HeaderText = "";
            this.dataGridViewCheckBoxColumn2.Name = "dataGridViewCheckBoxColumn2";
            this.dataGridViewCheckBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewCheckBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewCheckBoxColumn2.Width = 30;
            // 
            // descFase
            // 
            this.descFase.HeaderText = "Descrizione Fase";
            this.descFase.Name = "descFase";
            this.descFase.ReadOnly = true;
            this.descFase.Width = 860;
            // 
            // minLavoro
            // 
            this.minLavoro.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.minLavoro.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.minLavoro.HeaderText = "Tempo (m)";
            this.minLavoro.Items.AddRange(new object[] {
            "0",
            "15",
            "30",
            "45",
            "60",
            "75",
            "90",
            "105",
            "120",
            "135",
            "150",
            "165",
            "180"});
            this.minLavoro.Name = "minLavoro";
            this.minLavoro.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.minLavoro.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // tab_TestDaEseguire
            // 
            this.tab_TestDaEseguire.BackColor = System.Drawing.Color.Transparent;
            this.tab_TestDaEseguire.Controls.Add(this.dgv_testDaEseguire);
            this.tab_TestDaEseguire.Location = new System.Drawing.Point(4, 22);
            this.tab_TestDaEseguire.Name = "tab_TestDaEseguire";
            this.tab_TestDaEseguire.Size = new System.Drawing.Size(1046, 412);
            this.tab_TestDaEseguire.TabIndex = 10;
            this.tab_TestDaEseguire.Text = "Test Da Eseguire";
            // 
            // dgv_testDaEseguire
            // 
            this.dgv_testDaEseguire.AllowUserToAddRows = false;
            this.dgv_testDaEseguire.AllowUserToOrderColumns = true;
            this.dgv_testDaEseguire.BackgroundColor = System.Drawing.SystemColors.ActiveBorder;
            this.dgv_testDaEseguire.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_testDaEseguire.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.checkTest,
            this.descTest});
            this.dgv_testDaEseguire.Cursor = System.Windows.Forms.Cursors.Default;
            this.dgv_testDaEseguire.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_testDaEseguire.Location = new System.Drawing.Point(0, 0);
            this.dgv_testDaEseguire.Name = "dgv_testDaEseguire";
            this.dgv_testDaEseguire.RowHeadersVisible = false;
            this.dgv_testDaEseguire.Size = new System.Drawing.Size(1046, 412);
            this.dgv_testDaEseguire.TabIndex = 7;
            // 
            // checkTest
            // 
            this.checkTest.HeaderText = "";
            this.checkTest.Name = "checkTest";
            this.checkTest.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.checkTest.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.checkTest.Width = 30;
            // 
            // descTest
            // 
            this.descTest.HeaderText = "Descrizione Test";
            this.descTest.Name = "descTest";
            this.descTest.ReadOnly = true;
            this.descTest.Width = 960;
            // 
            // tab_Riparazioni
            // 
            this.tab_Riparazioni.BackColor = System.Drawing.Color.Transparent;
            this.tab_Riparazioni.Controls.Add(this.dgv_ListaRiparazRMA);
            this.tab_Riparazioni.Controls.Add(this.toolStrip_MenuRiparazioniDaEseguire);
            this.tab_Riparazioni.Location = new System.Drawing.Point(4, 22);
            this.tab_Riparazioni.Name = "tab_Riparazioni";
            this.tab_Riparazioni.Size = new System.Drawing.Size(1046, 412);
            this.tab_Riparazioni.TabIndex = 2;
            this.tab_Riparazioni.Text = "Riparazioni Da Eseguire";
            // 
            // dgv_ListaRiparazRMA
            // 
            this.dgv_ListaRiparazRMA.AllowUserToAddRows = false;
            this.dgv_ListaRiparazRMA.AllowUserToOrderColumns = true;
            this.dgv_ListaRiparazRMA.BackgroundColor = System.Drawing.SystemColors.ActiveBorder;
            this.dgv_ListaRiparazRMA.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_ListaRiparazRMA.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.chk_Rip,
            this.descRip,
            this.descParte,
            this.qtaParti,
            this.statoRip,
            this.noteRip,
            this.dataRip,
            this.idRiparazione});
            this.dgv_ListaRiparazRMA.Cursor = System.Windows.Forms.Cursors.Default;
            this.dgv_ListaRiparazRMA.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_ListaRiparazRMA.Location = new System.Drawing.Point(0, 50);
            this.dgv_ListaRiparazRMA.Name = "dgv_ListaRiparazRMA";
            this.dgv_ListaRiparazRMA.ReadOnly = true;
            this.dgv_ListaRiparazRMA.RowHeadersVisible = false;
            this.dgv_ListaRiparazRMA.Size = new System.Drawing.Size(1046, 362);
            this.dgv_ListaRiparazRMA.TabIndex = 28;
            this.dgv_ListaRiparazRMA.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_ListaRiparazRMA_CellContentClick);
            // 
            // chk_Rip
            // 
            this.chk_Rip.HeaderText = "";
            this.chk_Rip.Name = "chk_Rip";
            this.chk_Rip.ReadOnly = true;
            this.chk_Rip.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.chk_Rip.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.chk_Rip.Width = 30;
            // 
            // descRip
            // 
            this.descRip.HeaderText = "Riparazione";
            this.descRip.Name = "descRip";
            this.descRip.ReadOnly = true;
            this.descRip.Width = 120;
            // 
            // descParte
            // 
            this.descParte.HeaderText = "Parte";
            this.descParte.Name = "descParte";
            this.descParte.ReadOnly = true;
            this.descParte.Width = 450;
            // 
            // qtaParti
            // 
            this.qtaParti.HeaderText = "Quantità";
            this.qtaParti.Name = "qtaParti";
            this.qtaParti.ReadOnly = true;
            this.qtaParti.Width = 60;
            // 
            // statoRip
            // 
            this.statoRip.HeaderText = "Stato";
            this.statoRip.Name = "statoRip";
            this.statoRip.ReadOnly = true;
            // 
            // noteRip
            // 
            this.noteRip.HeaderText = "Note";
            this.noteRip.Name = "noteRip";
            this.noteRip.ReadOnly = true;
            this.noteRip.Width = 230;
            // 
            // dataRip
            // 
            this.dataRip.HeaderText = "Data Aggiornamento";
            this.dataRip.Name = "dataRip";
            this.dataRip.ReadOnly = true;
            this.dataRip.Visible = false;
            this.dataRip.Width = 140;
            // 
            // idRiparazione
            // 
            this.idRiparazione.HeaderText = "ID RIPARAZIONE";
            this.idRiparazione.Name = "idRiparazione";
            this.idRiparazione.ReadOnly = true;
            this.idRiparazione.Visible = false;
            // 
            // toolStrip_MenuRiparazioniDaEseguire
            // 
            this.toolStrip_MenuRiparazioniDaEseguire.AutoSize = false;
            this.toolStrip_MenuRiparazioniDaEseguire.BackColor = System.Drawing.SystemColors.Control;
            this.toolStrip_MenuRiparazioniDaEseguire.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip_MenuRiparazioniDaEseguire.ImageScalingSize = new System.Drawing.Size(47, 47);
            this.toolStrip_MenuRiparazioniDaEseguire.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btn_RefreshRiparazione,
            this.toolStripSeparator8,
            this.btn_AddRiparazione,
            this.btn_importRipFromTemplateDB,
            this.btn_CloneRiparazione,
            this.btn_EditRiparazione,
            this.btn_DeleteRiparazione,
            this.toolStripSeparator9,
            this.btn_addTemplateRipDB,
            this.btn_editTemplateRipDB,
            this.toolStripSeparator12,
            this.btn_PrintRiparazioni});
            this.toolStrip_MenuRiparazioniDaEseguire.Location = new System.Drawing.Point(0, 0);
            this.toolStrip_MenuRiparazioniDaEseguire.Name = "toolStrip_MenuRiparazioniDaEseguire";
            this.toolStrip_MenuRiparazioniDaEseguire.Size = new System.Drawing.Size(1046, 50);
            this.toolStrip_MenuRiparazioniDaEseguire.TabIndex = 27;
            this.toolStrip_MenuRiparazioniDaEseguire.Text = "toolStrip2";
            // 
            // btn_RefreshRiparazione
            // 
            this.btn_RefreshRiparazione.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_RefreshRiparazione.Image = ((System.Drawing.Image)(resources.GetObject("btn_RefreshRiparazione.Image")));
            this.btn_RefreshRiparazione.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_RefreshRiparazione.Name = "btn_RefreshRiparazione";
            this.btn_RefreshRiparazione.Size = new System.Drawing.Size(51, 47);
            this.btn_RefreshRiparazione.Text = "Evidenzia";
            this.btn_RefreshRiparazione.ToolTipText = "Evidenzia";
            this.btn_RefreshRiparazione.Click += new System.EventHandler(this.btn_RefreshRiparazione_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(6, 50);
            // 
            // btn_AddRiparazione
            // 
            this.btn_AddRiparazione.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_AddRiparazione.Image = ((System.Drawing.Image)(resources.GetObject("btn_AddRiparazione.Image")));
            this.btn_AddRiparazione.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_AddRiparazione.Name = "btn_AddRiparazione";
            this.btn_AddRiparazione.Size = new System.Drawing.Size(51, 47);
            this.btn_AddRiparazione.Text = "toolStripButton4";
            this.btn_AddRiparazione.ToolTipText = "Esporta Magazzino";
            this.btn_AddRiparazione.Click += new System.EventHandler(this.btn_AddRiparazione_Click);
            // 
            // btn_importRipFromTemplateDB
            // 
            this.btn_importRipFromTemplateDB.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_importRipFromTemplateDB.Image = ((System.Drawing.Image)(resources.GetObject("btn_importRipFromTemplateDB.Image")));
            this.btn_importRipFromTemplateDB.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_importRipFromTemplateDB.Name = "btn_importRipFromTemplateDB";
            this.btn_importRipFromTemplateDB.Size = new System.Drawing.Size(51, 47);
            this.btn_importRipFromTemplateDB.Text = "toolStripButton5";
            this.btn_importRipFromTemplateDB.Click += new System.EventHandler(this.btn_importRipFromTemplateDB_Click);
            // 
            // btn_CloneRiparazione
            // 
            this.btn_CloneRiparazione.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_CloneRiparazione.Image = ((System.Drawing.Image)(resources.GetObject("btn_CloneRiparazione.Image")));
            this.btn_CloneRiparazione.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_CloneRiparazione.Name = "btn_CloneRiparazione";
            this.btn_CloneRiparazione.Size = new System.Drawing.Size(51, 47);
            this.btn_CloneRiparazione.Text = "toolStripButton5";
            this.btn_CloneRiparazione.Click += new System.EventHandler(this.btn_CloneRiparazione_Click);
            // 
            // btn_EditRiparazione
            // 
            this.btn_EditRiparazione.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_EditRiparazione.Image = ((System.Drawing.Image)(resources.GetObject("btn_EditRiparazione.Image")));
            this.btn_EditRiparazione.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_EditRiparazione.Name = "btn_EditRiparazione";
            this.btn_EditRiparazione.Size = new System.Drawing.Size(51, 47);
            this.btn_EditRiparazione.Text = "toolStripButton5";
            this.btn_EditRiparazione.Click += new System.EventHandler(this.btn_EditRiparazione_Click);
            // 
            // btn_DeleteRiparazione
            // 
            this.btn_DeleteRiparazione.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_DeleteRiparazione.Image = ((System.Drawing.Image)(resources.GetObject("btn_DeleteRiparazione.Image")));
            this.btn_DeleteRiparazione.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_DeleteRiparazione.Name = "btn_DeleteRiparazione";
            this.btn_DeleteRiparazione.Size = new System.Drawing.Size(51, 47);
            this.btn_DeleteRiparazione.Text = "toolStripButton2";
            this.btn_DeleteRiparazione.ToolTipText = "Rimuovi Da Magazzino";
            this.btn_DeleteRiparazione.Click += new System.EventHandler(this.btn_DeleteRiparazione_Click);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(6, 50);
            // 
            // btn_addTemplateRipDB
            // 
            this.btn_addTemplateRipDB.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_addTemplateRipDB.Image = ((System.Drawing.Image)(resources.GetObject("btn_addTemplateRipDB.Image")));
            this.btn_addTemplateRipDB.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_addTemplateRipDB.Name = "btn_addTemplateRipDB";
            this.btn_addTemplateRipDB.Size = new System.Drawing.Size(51, 47);
            this.btn_addTemplateRipDB.Text = "Clona Record";
            this.btn_addTemplateRipDB.Click += new System.EventHandler(this.btn_addTemplateRipDB_Click);
            // 
            // btn_editTemplateRipDB
            // 
            this.btn_editTemplateRipDB.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_editTemplateRipDB.Image = ((System.Drawing.Image)(resources.GetObject("btn_editTemplateRipDB.Image")));
            this.btn_editTemplateRipDB.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_editTemplateRipDB.Name = "btn_editTemplateRipDB";
            this.btn_editTemplateRipDB.Size = new System.Drawing.Size(51, 47);
            this.btn_editTemplateRipDB.Text = "toolStripButton5";
            this.btn_editTemplateRipDB.Click += new System.EventHandler(this.btn_editTemplateRipDB_Click);
            // 
            // toolStripSeparator12
            // 
            this.toolStripSeparator12.Name = "toolStripSeparator12";
            this.toolStripSeparator12.Size = new System.Drawing.Size(6, 50);
            // 
            // btn_PrintRiparazioni
            // 
            this.btn_PrintRiparazioni.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_PrintRiparazioni.Image = ((System.Drawing.Image)(resources.GetObject("btn_PrintRiparazioni.Image")));
            this.btn_PrintRiparazioni.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_PrintRiparazioni.Name = "btn_PrintRiparazioni";
            this.btn_PrintRiparazioni.Size = new System.Drawing.Size(51, 47);
            this.btn_PrintRiparazioni.Text = "toolStripButton5";
            this.btn_PrintRiparazioni.Click += new System.EventHandler(this.btn_printRiparazioni_Click);
            // 
            // tab_AzioniSuppl
            // 
            this.tab_AzioniSuppl.BackColor = System.Drawing.Color.Transparent;
            this.tab_AzioniSuppl.Controls.Add(this.dgv_azioniSupplementari);
            this.tab_AzioniSuppl.Controls.Add(this.toolStrip_MenuAzioniSupplementari);
            this.tab_AzioniSuppl.Location = new System.Drawing.Point(4, 22);
            this.tab_AzioniSuppl.Name = "tab_AzioniSuppl";
            this.tab_AzioniSuppl.Padding = new System.Windows.Forms.Padding(3);
            this.tab_AzioniSuppl.Size = new System.Drawing.Size(1046, 412);
            this.tab_AzioniSuppl.TabIndex = 11;
            this.tab_AzioniSuppl.Text = "Azioni Supplementari";
            // 
            // dgv_azioniSupplementari
            // 
            this.dgv_azioniSupplementari.AllowUserToAddRows = false;
            this.dgv_azioniSupplementari.AllowUserToOrderColumns = true;
            this.dgv_azioniSupplementari.BackgroundColor = System.Drawing.SystemColors.ActiveBorder;
            this.dgv_azioniSupplementari.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_azioniSupplementari.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewCheckBoxColumn3,
            this.dataGridViewTextBoxColumn1,
            this.statoAzioneSuppl});
            this.dgv_azioniSupplementari.Cursor = System.Windows.Forms.Cursors.Default;
            this.dgv_azioniSupplementari.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_azioniSupplementari.Location = new System.Drawing.Point(3, 53);
            this.dgv_azioniSupplementari.Name = "dgv_azioniSupplementari";
            this.dgv_azioniSupplementari.RowHeadersVisible = false;
            this.dgv_azioniSupplementari.Size = new System.Drawing.Size(1040, 356);
            this.dgv_azioniSupplementari.TabIndex = 27;
            this.dgv_azioniSupplementari.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgv_azioniSupplementari_CurrentCellDirtyStateChanged);
            // 
            // dataGridViewCheckBoxColumn3
            // 
            this.dataGridViewCheckBoxColumn3.HeaderText = "";
            this.dataGridViewCheckBoxColumn3.Name = "dataGridViewCheckBoxColumn3";
            this.dataGridViewCheckBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewCheckBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewCheckBoxColumn3.Width = 30;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Descrizione Azione Supplementare";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 790;
            // 
            // statoAzioneSuppl
            // 
            this.statoAzioneSuppl.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.statoAzioneSuppl.HeaderText = "Stato";
            this.statoAzioneSuppl.Items.AddRange(new object[] {
            "Aggiungi a Note Trasporto",
            "Aggiungi a Note Imballaggio"});
            this.statoAzioneSuppl.Name = "statoAzioneSuppl";
            this.statoAzioneSuppl.Width = 170;
            // 
            // toolStrip_MenuAzioniSupplementari
            // 
            this.toolStrip_MenuAzioniSupplementari.AutoSize = false;
            this.toolStrip_MenuAzioniSupplementari.BackColor = System.Drawing.SystemColors.Control;
            this.toolStrip_MenuAzioniSupplementari.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip_MenuAzioniSupplementari.ImageScalingSize = new System.Drawing.Size(47, 47);
            this.toolStrip_MenuAzioniSupplementari.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btn_RefreshAzSuppl,
            this.toolStripSeparator13,
            this.btn_GestisciAzSuppl,
            this.toolStripButton2});
            this.toolStrip_MenuAzioniSupplementari.Location = new System.Drawing.Point(3, 3);
            this.toolStrip_MenuAzioniSupplementari.Name = "toolStrip_MenuAzioniSupplementari";
            this.toolStrip_MenuAzioniSupplementari.Size = new System.Drawing.Size(1040, 50);
            this.toolStrip_MenuAzioniSupplementari.TabIndex = 26;
            this.toolStrip_MenuAzioniSupplementari.Text = "toolStrip1";
            // 
            // btn_RefreshAzSuppl
            // 
            this.btn_RefreshAzSuppl.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_RefreshAzSuppl.Image = ((System.Drawing.Image)(resources.GetObject("btn_RefreshAzSuppl.Image")));
            this.btn_RefreshAzSuppl.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_RefreshAzSuppl.Name = "btn_RefreshAzSuppl";
            this.btn_RefreshAzSuppl.Size = new System.Drawing.Size(51, 47);
            this.btn_RefreshAzSuppl.Text = "Evidenzia";
            this.btn_RefreshAzSuppl.ToolTipText = "Evidenzia";
            this.btn_RefreshAzSuppl.Click += new System.EventHandler(this.btn_RefreshAzSuppl_Click);
            // 
            // toolStripSeparator13
            // 
            this.toolStripSeparator13.Name = "toolStripSeparator13";
            this.toolStripSeparator13.Size = new System.Drawing.Size(6, 50);
            // 
            // btn_GestisciAzSuppl
            // 
            this.btn_GestisciAzSuppl.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_GestisciAzSuppl.Image = ((System.Drawing.Image)(resources.GetObject("btn_GestisciAzSuppl.Image")));
            this.btn_GestisciAzSuppl.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_GestisciAzSuppl.Name = "btn_GestisciAzSuppl";
            this.btn_GestisciAzSuppl.Size = new System.Drawing.Size(51, 47);
            this.btn_GestisciAzSuppl.Text = "Evidenzia";
            this.btn_GestisciAzSuppl.ToolTipText = "Evidenzia";
            this.btn_GestisciAzSuppl.Click += new System.EventHandler(this.btn_EditAzSuppl_Click);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(23, 47);
            this.toolStripButton2.Text = "toolStripButton4";
            this.toolStripButton2.ToolTipText = "Esporta Magazzino";
            // 
            // tab_Preventivi
            // 
            this.tab_Preventivi.BackColor = System.Drawing.Color.Transparent;
            this.tab_Preventivi.Controls.Add(this.dgv_ListaPreventiviRMA);
            this.tab_Preventivi.Controls.Add(this.toolStrip_MenuPreventivi);
            this.tab_Preventivi.Location = new System.Drawing.Point(4, 22);
            this.tab_Preventivi.Name = "tab_Preventivi";
            this.tab_Preventivi.Size = new System.Drawing.Size(1046, 412);
            this.tab_Preventivi.TabIndex = 4;
            this.tab_Preventivi.Text = "Preventivi";
            // 
            // dgv_ListaPreventiviRMA
            // 
            this.dgv_ListaPreventiviRMA.AllowUserToAddRows = false;
            this.dgv_ListaPreventiviRMA.AllowUserToOrderColumns = true;
            this.dgv_ListaPreventiviRMA.BackgroundColor = System.Drawing.SystemColors.ActiveBorder;
            this.dgv_ListaPreventiviRMA.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_ListaPreventiviRMA.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.chk_Prev,
            this.idPreventivo,
            this.revisionePrev,
            this.dataAggPrev,
            this.totale,
            this.stato,
            this.idPrev});
            this.dgv_ListaPreventiviRMA.Cursor = System.Windows.Forms.Cursors.Default;
            this.dgv_ListaPreventiviRMA.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_ListaPreventiviRMA.Location = new System.Drawing.Point(0, 50);
            this.dgv_ListaPreventiviRMA.Name = "dgv_ListaPreventiviRMA";
            this.dgv_ListaPreventiviRMA.ReadOnly = true;
            this.dgv_ListaPreventiviRMA.RowHeadersVisible = false;
            this.dgv_ListaPreventiviRMA.Size = new System.Drawing.Size(1046, 362);
            this.dgv_ListaPreventiviRMA.TabIndex = 29;
            this.dgv_ListaPreventiviRMA.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_ListaPreventiviRMA_CellContentClick);
            // 
            // chk_Prev
            // 
            this.chk_Prev.HeaderText = "";
            this.chk_Prev.Name = "chk_Prev";
            this.chk_Prev.ReadOnly = true;
            this.chk_Prev.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.chk_Prev.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.chk_Prev.Width = 30;
            // 
            // idPreventivo
            // 
            this.idPreventivo.HeaderText = "Preventivo";
            this.idPreventivo.Name = "idPreventivo";
            this.idPreventivo.ReadOnly = true;
            this.idPreventivo.Width = 90;
            // 
            // revisionePrev
            // 
            this.revisionePrev.HeaderText = "Revisione";
            this.revisionePrev.Name = "revisionePrev";
            this.revisionePrev.ReadOnly = true;
            // 
            // dataAggPrev
            // 
            this.dataAggPrev.HeaderText = "Data Aggiornamento";
            this.dataAggPrev.Name = "dataAggPrev";
            this.dataAggPrev.ReadOnly = true;
            this.dataAggPrev.Width = 140;
            // 
            // totale
            // 
            this.totale.HeaderText = "Totale (€)";
            this.totale.Name = "totale";
            this.totale.ReadOnly = true;
            this.totale.Width = 90;
            // 
            // stato
            // 
            this.stato.HeaderText = "Stato";
            this.stato.Name = "stato";
            this.stato.ReadOnly = true;
            this.stato.Width = 90;
            // 
            // idPrev
            // 
            this.idPrev.HeaderText = "ID PREVENTIVO";
            this.idPrev.Name = "idPrev";
            this.idPrev.ReadOnly = true;
            this.idPrev.Visible = false;
            // 
            // toolStrip_MenuPreventivi
            // 
            this.toolStrip_MenuPreventivi.AutoSize = false;
            this.toolStrip_MenuPreventivi.BackColor = System.Drawing.SystemColors.Control;
            this.toolStrip_MenuPreventivi.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip_MenuPreventivi.ImageScalingSize = new System.Drawing.Size(47, 47);
            this.toolStrip_MenuPreventivi.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btn_RefreshPreventivi,
            this.toolStripSeparator14,
            this.btn_AddPreventivo,
            this.btn_ClonePreventivo,
            this.btn_EditPreventivo,
            this.btn_DeletePreventivo,
            this.toolStripSeparator15,
            this.btn_PrintPreventivo,
            this.cmb_PrevPrezzi});
            this.toolStrip_MenuPreventivi.Location = new System.Drawing.Point(0, 0);
            this.toolStrip_MenuPreventivi.Name = "toolStrip_MenuPreventivi";
            this.toolStrip_MenuPreventivi.Size = new System.Drawing.Size(1046, 50);
            this.toolStrip_MenuPreventivi.TabIndex = 28;
            this.toolStrip_MenuPreventivi.Text = "toolStrip2";
            // 
            // btn_RefreshPreventivi
            // 
            this.btn_RefreshPreventivi.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_RefreshPreventivi.Image = ((System.Drawing.Image)(resources.GetObject("btn_RefreshPreventivi.Image")));
            this.btn_RefreshPreventivi.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_RefreshPreventivi.Name = "btn_RefreshPreventivi";
            this.btn_RefreshPreventivi.Size = new System.Drawing.Size(51, 47);
            this.btn_RefreshPreventivi.Text = "Evidenzia";
            this.btn_RefreshPreventivi.ToolTipText = "Evidenzia";
            this.btn_RefreshPreventivi.Click += new System.EventHandler(this.btn_RefreshPrev_Click);
            // 
            // toolStripSeparator14
            // 
            this.toolStripSeparator14.Name = "toolStripSeparator14";
            this.toolStripSeparator14.Size = new System.Drawing.Size(6, 50);
            // 
            // btn_AddPreventivo
            // 
            this.btn_AddPreventivo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_AddPreventivo.Image = ((System.Drawing.Image)(resources.GetObject("btn_AddPreventivo.Image")));
            this.btn_AddPreventivo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_AddPreventivo.Name = "btn_AddPreventivo";
            this.btn_AddPreventivo.Size = new System.Drawing.Size(51, 47);
            this.btn_AddPreventivo.Text = "toolStripButton4";
            this.btn_AddPreventivo.ToolTipText = "Esporta Magazzino";
            this.btn_AddPreventivo.Click += new System.EventHandler(this.btn_AddPreventivo_Click);
            // 
            // btn_ClonePreventivo
            // 
            this.btn_ClonePreventivo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_ClonePreventivo.Image = ((System.Drawing.Image)(resources.GetObject("btn_ClonePreventivo.Image")));
            this.btn_ClonePreventivo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_ClonePreventivo.Name = "btn_ClonePreventivo";
            this.btn_ClonePreventivo.Size = new System.Drawing.Size(51, 47);
            this.btn_ClonePreventivo.Text = "toolStripButton5";
            this.btn_ClonePreventivo.Click += new System.EventHandler(this.btn_ClonePreventivo_Click);
            // 
            // btn_EditPreventivo
            // 
            this.btn_EditPreventivo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_EditPreventivo.Image = ((System.Drawing.Image)(resources.GetObject("btn_EditPreventivo.Image")));
            this.btn_EditPreventivo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_EditPreventivo.Name = "btn_EditPreventivo";
            this.btn_EditPreventivo.Size = new System.Drawing.Size(51, 47);
            this.btn_EditPreventivo.Text = "toolStripButton5";
            this.btn_EditPreventivo.Click += new System.EventHandler(this.btn_EditPreventivo_Click);
            // 
            // btn_DeletePreventivo
            // 
            this.btn_DeletePreventivo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_DeletePreventivo.Image = ((System.Drawing.Image)(resources.GetObject("btn_DeletePreventivo.Image")));
            this.btn_DeletePreventivo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_DeletePreventivo.Name = "btn_DeletePreventivo";
            this.btn_DeletePreventivo.Size = new System.Drawing.Size(51, 47);
            this.btn_DeletePreventivo.Text = "toolStripButton2";
            this.btn_DeletePreventivo.ToolTipText = "Rimuovi Da Magazzino";
            this.btn_DeletePreventivo.Click += new System.EventHandler(this.btn_DeletePreventivo_Click);
            // 
            // toolStripSeparator15
            // 
            this.toolStripSeparator15.Name = "toolStripSeparator15";
            this.toolStripSeparator15.Size = new System.Drawing.Size(6, 50);
            // 
            // btn_PrintPreventivo
            // 
            this.btn_PrintPreventivo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_PrintPreventivo.Image = ((System.Drawing.Image)(resources.GetObject("btn_PrintPreventivo.Image")));
            this.btn_PrintPreventivo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_PrintPreventivo.Name = "btn_PrintPreventivo";
            this.btn_PrintPreventivo.Size = new System.Drawing.Size(51, 47);
            this.btn_PrintPreventivo.Text = "toolStripButton5";
            this.btn_PrintPreventivo.Click += new System.EventHandler(this.btn_printPreventivo_Click);
            // 
            // cmb_PrevPrezzi
            // 
            this.cmb_PrevPrezzi.AutoCompleteCustomSource.AddRange(new string[] {
            "Seleziona Tutti",
            "Deseleziona Tutti"});
            this.cmb_PrevPrezzi.Items.AddRange(new object[] {
            "Con Prezzi",
            "Senza Prezzi"});
            this.cmb_PrevPrezzi.Name = "cmb_PrevPrezzi";
            this.cmb_PrevPrezzi.Size = new System.Drawing.Size(121, 50);
            // 
            // tab_Trasporto
            // 
            this.tab_Trasporto.BackColor = System.Drawing.Color.Transparent;
            this.tab_Trasporto.Controls.Add(this.groupBox14);
            this.tab_Trasporto.Location = new System.Drawing.Point(4, 22);
            this.tab_Trasporto.Name = "tab_Trasporto";
            this.tab_Trasporto.Size = new System.Drawing.Size(1046, 412);
            this.tab_Trasporto.TabIndex = 9;
            this.tab_Trasporto.Text = "Trasporto";
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.tb_noteImballaggio);
            this.groupBox14.Controls.Add(this.label2);
            this.groupBox14.Controls.Add(this.label1);
            this.groupBox14.Controls.Add(this.cmb_tipoImballaggio);
            this.groupBox14.Controls.Add(this.tb_noteTrasporto);
            this.groupBox14.Controls.Add(this.tb_DataSpedizione);
            this.groupBox14.Controls.Add(this.lbl_dataSped);
            this.groupBox14.Controls.Add(this.lbl_speseSped);
            this.groupBox14.Controls.Add(this.lbl_noteTrasporto);
            this.groupBox14.Controls.Add(this.cmb_speseSpediz);
            this.groupBox14.Controls.Add(this.dtp_dataSpedizione);
            this.groupBox14.Location = new System.Drawing.Point(6, 6);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(1037, 400);
            this.groupBox14.TabIndex = 33;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "Dettagli";
            // 
            // tb_noteImballaggio
            // 
            this.tb_noteImballaggio.Location = new System.Drawing.Point(150, 250);
            this.tb_noteImballaggio.Multiline = true;
            this.tb_noteImballaggio.Name = "tb_noteImballaggio";
            this.tb_noteImballaggio.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tb_noteImballaggio.Size = new System.Drawing.Size(872, 140);
            this.tb_noteImballaggio.TabIndex = 5;
            this.tb_noteImballaggio.Text = "N/A";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 313);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 13);
            this.label2.TabIndex = 35;
            this.label2.Text = "Note Imballaggio";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(721, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 13);
            this.label1.TabIndex = 34;
            this.label1.Text = "Tipo Imballaggio";
            // 
            // cmb_tipoImballaggio
            // 
            this.cmb_tipoImballaggio.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmb_tipoImballaggio.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_tipoImballaggio.FormattingEnabled = true;
            this.cmb_tipoImballaggio.ItemHeight = 13;
            this.cmb_tipoImballaggio.Items.AddRange(new object[] {
            "Carta / Paper",
            "Cassa di legno / Wood box",
            "Imballo Kodak / Kodak packaging",
            "Instapack"});
            this.cmb_tipoImballaggio.Location = new System.Drawing.Point(842, 40);
            this.cmb_tipoImballaggio.MaximumSize = new System.Drawing.Size(180, 0);
            this.cmb_tipoImballaggio.MinimumSize = new System.Drawing.Size(180, 0);
            this.cmb_tipoImballaggio.Name = "cmb_tipoImballaggio";
            this.cmb_tipoImballaggio.Size = new System.Drawing.Size(180, 21);
            this.cmb_tipoImballaggio.Sorted = true;
            this.cmb_tipoImballaggio.TabIndex = 4;
            // 
            // tb_noteTrasporto
            // 
            this.tb_noteTrasporto.Location = new System.Drawing.Point(150, 90);
            this.tb_noteTrasporto.Multiline = true;
            this.tb_noteTrasporto.Name = "tb_noteTrasporto";
            this.tb_noteTrasporto.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tb_noteTrasporto.Size = new System.Drawing.Size(872, 140);
            this.tb_noteTrasporto.TabIndex = 6;
            this.tb_noteTrasporto.Text = "N/A";
            // 
            // tb_DataSpedizione
            // 
            this.tb_DataSpedizione.Location = new System.Drawing.Point(150, 40);
            this.tb_DataSpedizione.Mask = "00/00/0000";
            this.tb_DataSpedizione.Name = "tb_DataSpedizione";
            this.tb_DataSpedizione.ResetOnSpace = false;
            this.tb_DataSpedizione.Size = new System.Drawing.Size(141, 20);
            this.tb_DataSpedizione.TabIndex = 1;
            this.tb_DataSpedizione.ValidatingType = typeof(System.DateTime);
            // 
            // lbl_dataSped
            // 
            this.lbl_dataSped.AutoSize = true;
            this.lbl_dataSped.Location = new System.Drawing.Point(22, 43);
            this.lbl_dataSped.Name = "lbl_dataSped";
            this.lbl_dataSped.Size = new System.Drawing.Size(96, 13);
            this.lbl_dataSped.TabIndex = 28;
            this.lbl_dataSped.Text = "Data di Spedizione";
            // 
            // lbl_speseSped
            // 
            this.lbl_speseSped.AutoSize = true;
            this.lbl_speseSped.Location = new System.Drawing.Point(369, 43);
            this.lbl_speseSped.Name = "lbl_speseSped";
            this.lbl_speseSped.Size = new System.Drawing.Size(103, 13);
            this.lbl_speseSped.TabIndex = 27;
            this.lbl_speseSped.Text = "Spese di Spedizione";
            // 
            // lbl_noteTrasporto
            // 
            this.lbl_noteTrasporto.AutoSize = true;
            this.lbl_noteTrasporto.Location = new System.Drawing.Point(22, 157);
            this.lbl_noteTrasporto.Name = "lbl_noteTrasporto";
            this.lbl_noteTrasporto.Size = new System.Drawing.Size(78, 13);
            this.lbl_noteTrasporto.TabIndex = 31;
            this.lbl_noteTrasporto.Text = "Note Trasporto";
            // 
            // cmb_speseSpediz
            // 
            this.cmb_speseSpediz.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmb_speseSpediz.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_speseSpediz.FormattingEnabled = true;
            this.cmb_speseSpediz.ItemHeight = 13;
            this.cmb_speseSpediz.Items.AddRange(new object[] {
            "Cliente / Customer",
            "Ralco"});
            this.cmb_speseSpediz.Location = new System.Drawing.Point(504, 40);
            this.cmb_speseSpediz.MaximumSize = new System.Drawing.Size(180, 0);
            this.cmb_speseSpediz.MinimumSize = new System.Drawing.Size(180, 0);
            this.cmb_speseSpediz.Name = "cmb_speseSpediz";
            this.cmb_speseSpediz.Size = new System.Drawing.Size(180, 21);
            this.cmb_speseSpediz.Sorted = true;
            this.cmb_speseSpediz.TabIndex = 3;
            // 
            // dtp_dataSpedizione
            // 
            this.dtp_dataSpedizione.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_dataSpedizione.Location = new System.Drawing.Point(150, 40);
            this.dtp_dataSpedizione.MaximumSize = new System.Drawing.Size(180, 20);
            this.dtp_dataSpedizione.MinimumSize = new System.Drawing.Size(180, 20);
            this.dtp_dataSpedizione.Name = "dtp_dataSpedizione";
            this.dtp_dataSpedizione.Size = new System.Drawing.Size(180, 20);
            this.dtp_dataSpedizione.TabIndex = 2;
            this.dtp_dataSpedizione.Value = new System.DateTime(2018, 4, 19, 0, 0, 0, 0);
            this.dtp_dataSpedizione.ValueChanged += new System.EventHandler(this.dtp_dataSpedizione_ValueChanged);
            // 
            // tab_Qualita
            // 
            this.tab_Qualita.BackColor = System.Drawing.Color.Transparent;
            this.tab_Qualita.Controls.Add(this.tb_noteQualita);
            this.tab_Qualita.Controls.Add(this.lbl_noteQualita);
            this.tab_Qualita.Controls.Add(this.gb_azCorrPrev);
            this.tab_Qualita.Controls.Add(this.gb_impattoSCAR);
            this.tab_Qualita.Location = new System.Drawing.Point(4, 22);
            this.tab_Qualita.Name = "tab_Qualita";
            this.tab_Qualita.Size = new System.Drawing.Size(1046, 412);
            this.tab_Qualita.TabIndex = 3;
            this.tab_Qualita.Text = "Note Qualità";
            // 
            // tb_noteQualita
            // 
            this.tb_noteQualita.Location = new System.Drawing.Point(127, 271);
            this.tb_noteQualita.Multiline = true;
            this.tb_noteQualita.Name = "tb_noteQualita";
            this.tb_noteQualita.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tb_noteQualita.Size = new System.Drawing.Size(916, 127);
            this.tb_noteQualita.TabIndex = 8;
            this.tb_noteQualita.Text = "N/A";
            // 
            // lbl_noteQualita
            // 
            this.lbl_noteQualita.AutoSize = true;
            this.lbl_noteQualita.Location = new System.Drawing.Point(28, 327);
            this.lbl_noteQualita.Name = "lbl_noteQualita";
            this.lbl_noteQualita.Size = new System.Drawing.Size(66, 13);
            this.lbl_noteQualita.TabIndex = 11;
            this.lbl_noteQualita.Text = "Note Qualità";
            // 
            // gb_azCorrPrev
            // 
            this.gb_azCorrPrev.Controls.Add(this.cmb_rifAzCorr);
            this.gb_azCorrPrev.Controls.Add(this.lbl_descAzCorrett);
            this.gb_azCorrPrev.Controls.Add(this.lbl_descAzCorr);
            this.gb_azCorrPrev.Controls.Add(this.cmb_statoAzCorr);
            this.gb_azCorrPrev.Controls.Add(this.lbl_statoAzCorr);
            this.gb_azCorrPrev.Controls.Add(this.lbl_rif);
            this.gb_azCorrPrev.Location = new System.Drawing.Point(6, 158);
            this.gb_azCorrPrev.Name = "gb_azCorrPrev";
            this.gb_azCorrPrev.Size = new System.Drawing.Size(1037, 102);
            this.gb_azCorrPrev.TabIndex = 1;
            this.gb_azCorrPrev.TabStop = false;
            this.gb_azCorrPrev.Text = "Azione Correttiva / Preventiva";
            // 
            // cmb_rifAzCorr
            // 
            this.cmb_rifAzCorr.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmb_rifAzCorr.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_rifAzCorr.FormattingEnabled = true;
            this.cmb_rifAzCorr.Location = new System.Drawing.Point(770, 24);
            this.cmb_rifAzCorr.MaximumSize = new System.Drawing.Size(250, 0);
            this.cmb_rifAzCorr.MinimumSize = new System.Drawing.Size(250, 0);
            this.cmb_rifAzCorr.Name = "cmb_rifAzCorr";
            this.cmb_rifAzCorr.Size = new System.Drawing.Size(250, 21);
            this.cmb_rifAzCorr.Sorted = true;
            this.cmb_rifAzCorr.TabIndex = 6;
            // 
            // lbl_descAzCorrett
            // 
            this.lbl_descAzCorrett.AutoSize = true;
            this.lbl_descAzCorrett.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_descAzCorrett.Location = new System.Drawing.Point(121, 71);
            this.lbl_descAzCorrett.MaximumSize = new System.Drawing.Size(895, 20);
            this.lbl_descAzCorrett.MinimumSize = new System.Drawing.Size(895, 20);
            this.lbl_descAzCorrett.Name = "lbl_descAzCorrett";
            this.lbl_descAzCorrett.Size = new System.Drawing.Size(895, 20);
            this.lbl_descAzCorrett.TabIndex = 7;
            this.lbl_descAzCorrett.Text = "N/A";
            // 
            // lbl_descAzCorr
            // 
            this.lbl_descAzCorr.AutoSize = true;
            this.lbl_descAzCorr.Location = new System.Drawing.Point(22, 71);
            this.lbl_descAzCorr.Name = "lbl_descAzCorr";
            this.lbl_descAzCorr.Size = new System.Drawing.Size(62, 13);
            this.lbl_descAzCorr.TabIndex = 14;
            this.lbl_descAzCorr.Text = "Descrizione";
            // 
            // cmb_statoAzCorr
            // 
            this.cmb_statoAzCorr.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmb_statoAzCorr.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_statoAzCorr.FormattingEnabled = true;
            this.cmb_statoAzCorr.Items.AddRange(new object[] {
            "Da Emettere / To Be Emitted",
            "Da Valutare / To Be Evaluated",
            "Emessa / Emitted",
            "Non Necessaria / Not Necessary"});
            this.cmb_statoAzCorr.Location = new System.Drawing.Point(121, 29);
            this.cmb_statoAzCorr.MaximumSize = new System.Drawing.Size(250, 0);
            this.cmb_statoAzCorr.MinimumSize = new System.Drawing.Size(250, 0);
            this.cmb_statoAzCorr.Name = "cmb_statoAzCorr";
            this.cmb_statoAzCorr.Size = new System.Drawing.Size(250, 21);
            this.cmb_statoAzCorr.Sorted = true;
            this.cmb_statoAzCorr.TabIndex = 5;
            // 
            // lbl_statoAzCorr
            // 
            this.lbl_statoAzCorr.AutoSize = true;
            this.lbl_statoAzCorr.Location = new System.Drawing.Point(22, 32);
            this.lbl_statoAzCorr.Name = "lbl_statoAzCorr";
            this.lbl_statoAzCorr.Size = new System.Drawing.Size(32, 13);
            this.lbl_statoAzCorr.TabIndex = 13;
            this.lbl_statoAzCorr.Text = "Stato";
            // 
            // lbl_rif
            // 
            this.lbl_rif.AutoSize = true;
            this.lbl_rif.Location = new System.Drawing.Point(644, 29);
            this.lbl_rif.Name = "lbl_rif";
            this.lbl_rif.Size = new System.Drawing.Size(60, 13);
            this.lbl_rif.TabIndex = 12;
            this.lbl_rif.Text = "Riferimento";
            // 
            // gb_impattoSCAR
            // 
            this.gb_impattoSCAR.Controls.Add(this.cmb_revDocValutRischi);
            this.gb_impattoSCAR.Controls.Add(this.cmb_segIncidente);
            this.gb_impattoSCAR.Controls.Add(this.lbl_revDocValutazRischi);
            this.gb_impattoSCAR.Controls.Add(this.lbl_segnIncid);
            this.gb_impattoSCAR.Controls.Add(this.cmb_invioNotaInf);
            this.gb_impattoSCAR.Controls.Add(this.cmb_sicProdotto);
            this.gb_impattoSCAR.Controls.Add(this.lbl_invioNotaInf);
            this.gb_impattoSCAR.Controls.Add(this.lbl_sicProd);
            this.gb_impattoSCAR.Location = new System.Drawing.Point(6, 6);
            this.gb_impattoSCAR.Name = "gb_impattoSCAR";
            this.gb_impattoSCAR.Size = new System.Drawing.Size(1037, 146);
            this.gb_impattoSCAR.TabIndex = 0;
            this.gb_impattoSCAR.TabStop = false;
            this.gb_impattoSCAR.Text = "Impatto SCAR";
            // 
            // cmb_revDocValutRischi
            // 
            this.cmb_revDocValutRischi.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmb_revDocValutRischi.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_revDocValutRischi.FormattingEnabled = true;
            this.cmb_revDocValutRischi.Items.AddRange(new object[] {
            "Necessaria / Necessary",
            "Non Necessaria / Not Necessary"});
            this.cmb_revDocValutRischi.Location = new System.Drawing.Point(770, 97);
            this.cmb_revDocValutRischi.MaximumSize = new System.Drawing.Size(250, 0);
            this.cmb_revDocValutRischi.MinimumSize = new System.Drawing.Size(250, 0);
            this.cmb_revDocValutRischi.Name = "cmb_revDocValutRischi";
            this.cmb_revDocValutRischi.Size = new System.Drawing.Size(250, 21);
            this.cmb_revDocValutRischi.Sorted = true;
            this.cmb_revDocValutRischi.TabIndex = 4;
            // 
            // cmb_segIncidente
            // 
            this.cmb_segIncidente.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmb_segIncidente.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_segIncidente.FormattingEnabled = true;
            this.cmb_segIncidente.Items.AddRange(new object[] {
            "Necessaria / Necessary",
            "Non Necessaria / Not Necessary"});
            this.cmb_segIncidente.Location = new System.Drawing.Point(770, 37);
            this.cmb_segIncidente.MaximumSize = new System.Drawing.Size(250, 0);
            this.cmb_segIncidente.MinimumSize = new System.Drawing.Size(250, 0);
            this.cmb_segIncidente.Name = "cmb_segIncidente";
            this.cmb_segIncidente.Size = new System.Drawing.Size(250, 21);
            this.cmb_segIncidente.Sorted = true;
            this.cmb_segIncidente.TabIndex = 2;
            // 
            // lbl_revDocValutazRischi
            // 
            this.lbl_revDocValutazRischi.AutoSize = true;
            this.lbl_revDocValutazRischi.Location = new System.Drawing.Point(644, 92);
            this.lbl_revDocValutazRischi.Name = "lbl_revDocValutazRischi";
            this.lbl_revDocValutazRischi.Size = new System.Drawing.Size(110, 26);
            this.lbl_revDocValutazRischi.TabIndex = 22;
            this.lbl_revDocValutazRischi.Text = "Revisione documento\r\nvalutazione dei rischi";
            // 
            // lbl_segnIncid
            // 
            this.lbl_segnIncid.AutoSize = true;
            this.lbl_segnIncid.Location = new System.Drawing.Point(644, 35);
            this.lbl_segnIncid.Name = "lbl_segnIncid";
            this.lbl_segnIncid.Size = new System.Drawing.Size(71, 26);
            this.lbl_segnIncid.TabIndex = 20;
            this.lbl_segnIncid.Text = "Segnalazione\r\ndell\'incidente";
            // 
            // cmb_invioNotaInf
            // 
            this.cmb_invioNotaInf.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmb_invioNotaInf.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_invioNotaInf.FormattingEnabled = true;
            this.cmb_invioNotaInf.Items.AddRange(new object[] {
            "Necessaria / Necessary",
            "Non Necessaria / Not Necessary"});
            this.cmb_invioNotaInf.Location = new System.Drawing.Point(121, 97);
            this.cmb_invioNotaInf.MaximumSize = new System.Drawing.Size(250, 0);
            this.cmb_invioNotaInf.MinimumSize = new System.Drawing.Size(250, 0);
            this.cmb_invioNotaInf.Name = "cmb_invioNotaInf";
            this.cmb_invioNotaInf.Size = new System.Drawing.Size(250, 21);
            this.cmb_invioNotaInf.Sorted = true;
            this.cmb_invioNotaInf.TabIndex = 3;
            // 
            // cmb_sicProdotto
            // 
            this.cmb_sicProdotto.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmb_sicProdotto.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_sicProdotto.FormattingEnabled = true;
            this.cmb_sicProdotto.Items.AddRange(new object[] {
            "Compromessa / Compromised",
            "Non Compromessa / Not Compromised"});
            this.cmb_sicProdotto.Location = new System.Drawing.Point(121, 37);
            this.cmb_sicProdotto.MaximumSize = new System.Drawing.Size(250, 0);
            this.cmb_sicProdotto.MinimumSize = new System.Drawing.Size(250, 0);
            this.cmb_sicProdotto.Name = "cmb_sicProdotto";
            this.cmb_sicProdotto.Size = new System.Drawing.Size(250, 21);
            this.cmb_sicProdotto.Sorted = true;
            this.cmb_sicProdotto.TabIndex = 1;
            // 
            // lbl_invioNotaInf
            // 
            this.lbl_invioNotaInf.AutoSize = true;
            this.lbl_invioNotaInf.Location = new System.Drawing.Point(22, 92);
            this.lbl_invioNotaInf.Name = "lbl_invioNotaInf";
            this.lbl_invioNotaInf.Size = new System.Drawing.Size(59, 26);
            this.lbl_invioNotaInf.TabIndex = 16;
            this.lbl_invioNotaInf.Text = "Invio Nota\r\nInformativa";
            // 
            // lbl_sicProd
            // 
            this.lbl_sicProd.AutoSize = true;
            this.lbl_sicProd.Location = new System.Drawing.Point(22, 35);
            this.lbl_sicProd.Name = "lbl_sicProd";
            this.lbl_sicProd.Size = new System.Drawing.Size(70, 26);
            this.lbl_sicProd.TabIndex = 14;
            this.lbl_sicProd.Text = "Sicurezza del\r\nprodotto";
            // 
            // tab_NoteRMA
            // 
            this.tab_NoteRMA.BackColor = System.Drawing.Color.Transparent;
            this.tab_NoteRMA.Controls.Add(this.dgv_noteRMA);
            this.tab_NoteRMA.Controls.Add(this.toolStrip1);
            this.tab_NoteRMA.Location = new System.Drawing.Point(4, 22);
            this.tab_NoteRMA.Name = "tab_NoteRMA";
            this.tab_NoteRMA.Size = new System.Drawing.Size(1046, 412);
            this.tab_NoteRMA.TabIndex = 6;
            this.tab_NoteRMA.Text = "Note RMA";
            // 
            // dgv_noteRMA
            // 
            this.dgv_noteRMA.AllowUserToAddRows = false;
            this.dgv_noteRMA.AllowUserToOrderColumns = true;
            this.dgv_noteRMA.BackgroundColor = System.Drawing.SystemColors.ActiveBorder;
            this.dgv_noteRMA.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_noteRMA.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewCheckBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.logAggDa,
            this.dataGridViewTextBoxColumn4,
            this.noteLog,
            this.idNote});
            this.dgv_noteRMA.Cursor = System.Windows.Forms.Cursors.Default;
            this.dgv_noteRMA.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_noteRMA.Location = new System.Drawing.Point(0, 50);
            this.dgv_noteRMA.Name = "dgv_noteRMA";
            this.dgv_noteRMA.ReadOnly = true;
            this.dgv_noteRMA.RowHeadersVisible = false;
            this.dgv_noteRMA.Size = new System.Drawing.Size(1046, 362);
            this.dgv_noteRMA.TabIndex = 30;
            this.dgv_noteRMA.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_noteRMA_CellContentClick);
            // 
            // dataGridViewCheckBoxColumn1
            // 
            this.dataGridViewCheckBoxColumn1.HeaderText = "";
            this.dataGridViewCheckBoxColumn1.Name = "dataGridViewCheckBoxColumn1";
            this.dataGridViewCheckBoxColumn1.ReadOnly = true;
            this.dataGridViewCheckBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewCheckBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewCheckBoxColumn1.Width = 30;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Data Aggiornamento";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 130;
            // 
            // logAggDa
            // 
            this.logAggDa.HeaderText = "Aggiornato Da";
            this.logAggDa.Name = "logAggDa";
            this.logAggDa.ReadOnly = true;
            this.logAggDa.Width = 150;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "Stato";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Width = 130;
            // 
            // noteLog
            // 
            this.noteLog.HeaderText = "Note";
            this.noteLog.Name = "noteLog";
            this.noteLog.ReadOnly = true;
            this.noteLog.Width = 560;
            // 
            // idNote
            // 
            this.idNote.HeaderText = "ID NOTE";
            this.idNote.Name = "idNote";
            this.idNote.ReadOnly = true;
            this.idNote.Visible = false;
            // 
            // toolStrip1
            // 
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.BackColor = System.Drawing.SystemColors.Control;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(47, 47);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btn_RefreshNoteRMA,
            this.toolStripSeparator16,
            this.btn_AddNoteRMA,
            this.btn_EditNoteRMA,
            this.btn_DeleteNoteRMA,
            this.toolStripSeparator17,
            this.btn_PrintGenericInfo});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1046, 50);
            this.toolStrip1.TabIndex = 29;
            this.toolStrip1.Text = "toolStrip2";
            // 
            // btn_RefreshNoteRMA
            // 
            this.btn_RefreshNoteRMA.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_RefreshNoteRMA.Image = ((System.Drawing.Image)(resources.GetObject("btn_RefreshNoteRMA.Image")));
            this.btn_RefreshNoteRMA.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_RefreshNoteRMA.Name = "btn_RefreshNoteRMA";
            this.btn_RefreshNoteRMA.Size = new System.Drawing.Size(51, 47);
            this.btn_RefreshNoteRMA.Text = "Evidenzia";
            this.btn_RefreshNoteRMA.ToolTipText = "Evidenzia";
            this.btn_RefreshNoteRMA.Click += new System.EventHandler(this.btn_RefreshNoteRMA_Click);
            // 
            // toolStripSeparator16
            // 
            this.toolStripSeparator16.Name = "toolStripSeparator16";
            this.toolStripSeparator16.Size = new System.Drawing.Size(6, 50);
            // 
            // btn_AddNoteRMA
            // 
            this.btn_AddNoteRMA.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_AddNoteRMA.Image = ((System.Drawing.Image)(resources.GetObject("btn_AddNoteRMA.Image")));
            this.btn_AddNoteRMA.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_AddNoteRMA.Name = "btn_AddNoteRMA";
            this.btn_AddNoteRMA.Size = new System.Drawing.Size(51, 47);
            this.btn_AddNoteRMA.Text = "toolStripButton4";
            this.btn_AddNoteRMA.ToolTipText = "Esporta Magazzino";
            this.btn_AddNoteRMA.Click += new System.EventHandler(this.btn_AddNoteRMA_Click);
            // 
            // btn_EditNoteRMA
            // 
            this.btn_EditNoteRMA.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_EditNoteRMA.Image = ((System.Drawing.Image)(resources.GetObject("btn_EditNoteRMA.Image")));
            this.btn_EditNoteRMA.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_EditNoteRMA.Name = "btn_EditNoteRMA";
            this.btn_EditNoteRMA.Size = new System.Drawing.Size(51, 47);
            this.btn_EditNoteRMA.Text = "toolStripButton5";
            this.btn_EditNoteRMA.Click += new System.EventHandler(this.btn_EditNoteRMA_Click);
            // 
            // btn_DeleteNoteRMA
            // 
            this.btn_DeleteNoteRMA.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_DeleteNoteRMA.Image = ((System.Drawing.Image)(resources.GetObject("btn_DeleteNoteRMA.Image")));
            this.btn_DeleteNoteRMA.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_DeleteNoteRMA.Name = "btn_DeleteNoteRMA";
            this.btn_DeleteNoteRMA.Size = new System.Drawing.Size(51, 47);
            this.btn_DeleteNoteRMA.Text = "toolStripButton2";
            this.btn_DeleteNoteRMA.ToolTipText = "Rimuovi Da Magazzino";
            this.btn_DeleteNoteRMA.Click += new System.EventHandler(this.btn_DeleteNoteRMA_Click);
            // 
            // toolStripSeparator17
            // 
            this.toolStripSeparator17.Name = "toolStripSeparator17";
            this.toolStripSeparator17.Size = new System.Drawing.Size(6, 50);
            // 
            // btn_PrintGenericInfo
            // 
            this.btn_PrintGenericInfo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_PrintGenericInfo.Image = ((System.Drawing.Image)(resources.GetObject("btn_PrintGenericInfo.Image")));
            this.btn_PrintGenericInfo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_PrintGenericInfo.Name = "btn_PrintGenericInfo";
            this.btn_PrintGenericInfo.Size = new System.Drawing.Size(51, 47);
            this.btn_PrintGenericInfo.Text = "toolStripButton5";
            this.btn_PrintGenericInfo.Click += new System.EventHandler(this.btn_PrintGeneralInfo_Click);
            // 
            // cmb_prioritaRMA
            // 
            this.cmb_prioritaRMA.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmb_prioritaRMA.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_prioritaRMA.FormattingEnabled = true;
            this.cmb_prioritaRMA.Items.AddRange(new object[] {
            "1 - Alta",
            "2 - Media",
            "3 - Bassa"});
            this.cmb_prioritaRMA.Location = new System.Drawing.Point(133, 20);
            this.cmb_prioritaRMA.MaximumSize = new System.Drawing.Size(180, 0);
            this.cmb_prioritaRMA.MinimumSize = new System.Drawing.Size(180, 0);
            this.cmb_prioritaRMA.Name = "cmb_prioritaRMA";
            this.cmb_prioritaRMA.Size = new System.Drawing.Size(180, 21);
            this.cmb_prioritaRMA.TabIndex = 1;
            // 
            // lbl_priorita
            // 
            this.lbl_priorita.AutoSize = true;
            this.lbl_priorita.Location = new System.Drawing.Point(19, 23);
            this.lbl_priorita.Name = "lbl_priorita";
            this.lbl_priorita.Size = new System.Drawing.Size(39, 13);
            this.lbl_priorita.TabIndex = 12;
            this.lbl_priorita.Text = "Priorità";
            // 
            // cmb_statoRMA
            // 
            this.cmb_statoRMA.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmb_statoRMA.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_statoRMA.FormattingEnabled = true;
            this.cmb_statoRMA.Items.AddRange(new object[] {
            "Aperto",
            "Attesa Analisi Problema",
            "Emettere Preventivo",
            "Attesa Approvazione Preventivo",
            "Preventivo Approvato",
            "In Riparazione",
            "Riparazione Completata",
            "Parzialmente Evaso",
            "In Spedizione",
            "Attesa Rientro Parte",
            "Chiuso",
            "Annullato"});
            this.cmb_statoRMA.Location = new System.Drawing.Point(882, 20);
            this.cmb_statoRMA.MaximumSize = new System.Drawing.Size(180, 0);
            this.cmb_statoRMA.MinimumSize = new System.Drawing.Size(180, 0);
            this.cmb_statoRMA.Name = "cmb_statoRMA";
            this.cmb_statoRMA.Size = new System.Drawing.Size(180, 21);
            this.cmb_statoRMA.TabIndex = 2;
            this.cmb_statoRMA.SelectedIndexChanged += new System.EventHandler(this.cmb_statoRMA_SelectedIndexChanged);
            // 
            // lbl_stato
            // 
            this.lbl_stato.AutoSize = true;
            this.lbl_stato.Location = new System.Drawing.Point(785, 23);
            this.lbl_stato.Name = "lbl_stato";
            this.lbl_stato.Size = new System.Drawing.Size(32, 13);
            this.lbl_stato.TabIndex = 10;
            this.lbl_stato.Text = "Stato";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(274, 31);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(32, 13);
            this.label10.TabIndex = 9;
            this.label10.Text = "Stato";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(21, 28);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(39, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Priorità";
            // 
            // btn_SaveAndClose
            // 
            this.btn_SaveAndClose.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_SaveAndClose.BackgroundImage")));
            this.btn_SaveAndClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_SaveAndClose.Location = new System.Drawing.Point(488, 513);
            this.btn_SaveAndClose.Name = "btn_SaveAndClose";
            this.btn_SaveAndClose.Size = new System.Drawing.Size(50, 50);
            this.btn_SaveAndClose.TabIndex = 101;
            this.btn_SaveAndClose.UseVisualStyleBackColor = true;
            this.btn_SaveAndClose.Click += new System.EventHandler(this.btn_saveAndCloseRMA_Click);
            // 
            // btn_cancel
            // 
            this.btn_cancel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_cancel.BackgroundImage")));
            this.btn_cancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_cancel.Location = new System.Drawing.Point(593, 513);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(50, 50);
            this.btn_cancel.TabIndex = 102;
            this.btn_cancel.UseVisualStyleBackColor = true;
            this.btn_cancel.Click += new System.EventHandler(this.btn_cancelRMA_Click);
            // 
            // btn_saveRMA
            // 
            this.btn_saveRMA.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_saveRMA.BackgroundImage")));
            this.btn_saveRMA.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_saveRMA.Location = new System.Drawing.Point(383, 513);
            this.btn_saveRMA.Name = "btn_saveRMA";
            this.btn_saveRMA.Size = new System.Drawing.Size(50, 50);
            this.btn_saveRMA.TabIndex = 100;
            this.btn_saveRMA.UseVisualStyleBackColor = true;
            this.btn_saveRMA.Click += new System.EventHandler(this.btn_saveRMA_Click);
            // 
            // cb_modManNumRMA
            // 
            this.cb_modManNumRMA.AutoSize = true;
            this.cb_modManNumRMA.CheckAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.cb_modManNumRMA.Location = new System.Drawing.Point(366, 12);
            this.cb_modManNumRMA.Name = "cb_modManNumRMA";
            this.cb_modManNumRMA.Size = new System.Drawing.Size(185, 31);
            this.cb_modManNumRMA.TabIndex = 103;
            this.cb_modManNumRMA.Text = "Modifica Manualmente Numero RMA";
            this.cb_modManNumRMA.UseVisualStyleBackColor = true;
            this.cb_modManNumRMA.CheckedChanged += new System.EventHandler(this.Cb_modManNumRMA_CheckedChanged);
            // 
            // tb_ModManNumRMA
            // 
            this.tb_ModManNumRMA.Location = new System.Drawing.Point(581, 21);
            this.tb_ModManNumRMA.Name = "tb_ModManNumRMA";
            this.tb_ModManNumRMA.Size = new System.Drawing.Size(100, 20);
            this.tb_ModManNumRMA.TabIndex = 105;
            // 
            // Form_RMA
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1076, 577);
            this.Controls.Add(this.tb_ModManNumRMA);
            this.Controls.Add(this.cb_modManNumRMA);
            this.Controls.Add(this.btn_saveRMA);
            this.Controls.Add(this.btn_cancel);
            this.Controls.Add(this.btn_SaveAndClose);
            this.Controls.Add(this.tabctrl_RMA);
            this.Controls.Add(this.cmb_statoRMA);
            this.Controls.Add(this.cmb_prioritaRMA);
            this.Controls.Add(this.lbl_priorita);
            this.Controls.Add(this.lbl_stato);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(1092, 615);
            this.MinimumSize = new System.Drawing.Size(1092, 615);
            this.Name = "Form_RMA";
            this.Text = "RMA";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form_RMA_FormClosing);
            this.tabctrl_RMA.ResumeLayout(false);
            this.tab_generalInfo.ResumeLayout(false);
            this.tabctrl_ItemGenerali.ResumeLayout(false);
            this.tabCollimatore.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ListaCollimatoriRMA)).EndInit();
            this.toolStrip_MenuCollimatore.ResumeLayout(false);
            this.toolStrip_MenuCollimatore.PerformLayout();
            this.tabParte.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ListaPartiRMA)).EndInit();
            this.toolStrip_MenuParte.ResumeLayout(false);
            this.toolStrip_MenuParte.PerformLayout();
            this.gb_InfoGenerali.ResumeLayout(false);
            this.gb_InfoGenerali.PerformLayout();
            this.tab_infoCliente.ResumeLayout(false);
            this.gb_dettagli.ResumeLayout(false);
            this.gb_dettagli.PerformLayout();
            this.tab_Allegati.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ListaAllegatiRMA)).EndInit();
            this.toolStrip_MenuAllegati.ResumeLayout(false);
            this.toolStrip_MenuAllegati.PerformLayout();
            this.tab_detGaranzia.ResumeLayout(false);
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.tab_Analisi.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ListaProblemiRMA)).EndInit();
            this.toolStrip_MenuAnalisiProblema.ResumeLayout(false);
            this.toolStrip_MenuAnalisiProblema.PerformLayout();
            this.tab_FasiDaRieseguire.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_FasiDaRieseguire)).EndInit();
            this.tab_TestDaEseguire.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_testDaEseguire)).EndInit();
            this.tab_Riparazioni.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ListaRiparazRMA)).EndInit();
            this.toolStrip_MenuRiparazioniDaEseguire.ResumeLayout(false);
            this.toolStrip_MenuRiparazioniDaEseguire.PerformLayout();
            this.tab_AzioniSuppl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_azioniSupplementari)).EndInit();
            this.toolStrip_MenuAzioniSupplementari.ResumeLayout(false);
            this.toolStrip_MenuAzioniSupplementari.PerformLayout();
            this.tab_Preventivi.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ListaPreventiviRMA)).EndInit();
            this.toolStrip_MenuPreventivi.ResumeLayout(false);
            this.toolStrip_MenuPreventivi.PerformLayout();
            this.tab_Trasporto.ResumeLayout(false);
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            this.tab_Qualita.ResumeLayout(false);
            this.tab_Qualita.PerformLayout();
            this.gb_azCorrPrev.ResumeLayout(false);
            this.gb_azCorrPrev.PerformLayout();
            this.gb_impattoSCAR.ResumeLayout(false);
            this.gb_impattoSCAR.PerformLayout();
            this.tab_NoteRMA.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_noteRMA)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabctrl_RMA;
        private System.Windows.Forms.TabPage tab_generalInfo;
        private System.Windows.Forms.TabPage tab_infoCliente;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lbl_numeroRMA;
        private System.Windows.Forms.GroupBox gb_InfoGenerali;
        private System.Windows.Forms.Label lbl_tipoRMA;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lbl_NumRMA;
        private System.Windows.Forms.TabPage tab_Riparazioni;
        private System.Windows.Forms.GroupBox gb_dettagli;
        private System.Windows.Forms.Label lbl_ordRip;
        private System.Windows.Forms.Label lbl_docReso;
        private System.Windows.Forms.Label lbl_SCARcli;
        private System.Windows.Forms.Label lbl_difRiscCli;
        private System.Windows.Forms.TextBox tb_scarCliente;
        private System.Windows.Forms.TextBox tb_ordRiparaz;
        private System.Windows.Forms.TextBox tb_docReso;
        private System.Windows.Forms.TextBox tb_noteCliente;
        private System.Windows.Forms.TabPage tab_Qualita;
        private System.Windows.Forms.GroupBox gb_impattoSCAR;
        private System.Windows.Forms.GroupBox gb_azCorrPrev;
        private System.Windows.Forms.ComboBox cmb_statoAzCorr;
        private System.Windows.Forms.Label lbl_statoAzCorr;
        private System.Windows.Forms.Label lbl_rif;
        private System.Windows.Forms.Label lbl_sicProd;
        private System.Windows.Forms.Label lbl_invioNotaInf;
        private System.Windows.Forms.ComboBox cmb_invioNotaInf;
        private System.Windows.Forms.ComboBox cmb_sicProdotto;
        private System.Windows.Forms.Label lbl_revDocValutazRischi;
        private System.Windows.Forms.Label lbl_segnIncid;
        private System.Windows.Forms.ComboBox cmb_revDocValutRischi;
        private System.Windows.Forms.ComboBox cmb_segIncidente;
        private System.Windows.Forms.TextBox tb_numScarCLI;
        private System.Windows.Forms.Label lbl_numSCARcli;
        private System.Windows.Forms.ComboBox cmb_decisCliente;
        private System.Windows.Forms.Label lbl_decisCli;
        private System.Windows.Forms.Label lbl_dataOrdRip;
        private System.Windows.Forms.DateTimePicker dtp_dataOrdRip;
        private System.Windows.Forms.Button btn_SaveAndClose;
        private System.Windows.Forms.Button btn_cancel;
        private System.Windows.Forms.ComboBox cmb_tipoRMA;
        private System.Windows.Forms.TabPage tab_Preventivi;
        private System.Windows.Forms.ComboBox cmb_prioritaRMA;
        private System.Windows.Forms.Label lbl_priorita;
        private System.Windows.Forms.ComboBox cmb_statoRMA;
        private System.Windows.Forms.Label lbl_stato;
        private System.Windows.Forms.DateTimePicker dtp_dataScadenzaRMA;
        private System.Windows.Forms.Label lbl_dataScadRMA;
        private System.Windows.Forms.Label lbl_noteGen;
        private System.Windows.Forms.TextBox tb_noteQualita;
        private System.Windows.Forms.Label lbl_noteQualita;
        private System.Windows.Forms.TabPage tab_Analisi;
        private System.Windows.Forms.ComboBox cmb_nomeCliente;
        private System.Windows.Forms.Label lbl_nomeCliente;
        private System.Windows.Forms.TabPage tab_NoteRMA;
        private System.Windows.Forms.TabPage tab_detGaranzia;
        private System.Windows.Forms.TabPage tab_FasiDaRieseguire;
        private System.Windows.Forms.Label lbl_descAzCorrett;
        private System.Windows.Forms.Label lbl_descAzCorr;
        private System.Windows.Forms.ComboBox cmb_rifAzCorr;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.TextBox tb_noteGaranziaRalco;
        private System.Windows.Forms.Label lbl_note;
        private System.Windows.Forms.Label lbl_respDannoColl;
        private System.Windows.Forms.ComboBox cmb_provCollimatore;
        private System.Windows.Forms.Label lbl_provColl;
        private System.Windows.Forms.ComboBox cmb_anniVitaCollimatore;
        private System.Windows.Forms.Label lbl_anniVitaColl;
        private System.Windows.Forms.ComboBox cmb_respDannoCollimatore;
        private System.Windows.Forms.ComboBox cmb_azioniSupplRalco;
        private System.Windows.Forms.Label lbl_azSupplRalco;
        private System.Windows.Forms.TextBox tb_noteGenerali;
        private System.Windows.Forms.DateTimePicker dtp_DataArrivoCollimatore;
        private System.Windows.Forms.Label lbl_dataArrColl;
        private System.Windows.Forms.Label lbl_dataDocReso;
        private System.Windows.Forms.DateTimePicker dtp_dataDocReso;
        private System.Windows.Forms.TabPage tab_Trasporto;
        private System.Windows.Forms.Label lbl_noteTrasporto;
        private System.Windows.Forms.DateTimePicker dtp_dataSpedizione;
        private System.Windows.Forms.ComboBox cmb_speseSpediz;
        private System.Windows.Forms.Label lbl_dataSped;
        private System.Windows.Forms.Label lbl_speseSped;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.TextBox tb_noteTrasporto;
        private System.Windows.Forms.Label lbl_codCollCliente;
        private System.Windows.Forms.Label lbl_idCliente;
        private System.Windows.Forms.Label lbl_codCollCli;
        private System.Windows.Forms.Label lbl_idCli;
        private System.Windows.Forms.Button btn_saveRMA;
        private System.Windows.Forms.TabPage tab_TestDaEseguire;
        private System.Windows.Forms.RadioButton rb_statoGaranzia;
        private System.Windows.Forms.Label lbl_DataCreazioneRMA;
        private System.Windows.Forms.Label lbl_dataCreazRMA;
        private System.Windows.Forms.ComboBox cmb_statoGaranzia;
        private System.Windows.Forms.TabControl tabctrl_ItemGenerali;
        private System.Windows.Forms.TabPage tabCollimatore;
        private System.Windows.Forms.TabPage tabParte;
        private System.Windows.Forms.TabPage tab_AzioniSuppl;
        private System.Windows.Forms.MaskedTextBox tb_DataArrivoCollimatore;
        private System.Windows.Forms.MaskedTextBox tb_DataScadenzaRMA;
        private System.Windows.Forms.MaskedTextBox tb_DataDocumentoDiReso;
        private System.Windows.Forms.MaskedTextBox tb_DataSpedizione;
        private System.Windows.Forms.MaskedTextBox tb_DataOrdineRiparazione;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmb_tipoImballaggio;
        private System.Windows.Forms.TextBox tb_noteImballaggio;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ToolStrip toolStrip_MenuCollimatore;
        private System.Windows.Forms.ToolStripButton btn_RefreshCollRMA;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripButton btn_AddCollRMA;
        private System.Windows.Forms.ToolStripButton btn_CancelCollRMA;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;
        private System.Windows.Forms.ToolStripButton btn_StampaEtichetteColl;
        private System.Windows.Forms.ToolStripComboBox cmb_CollSelection;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripButton btn_visualizzaScansioneFlowChart;
        private System.Windows.Forms.ToolStripButton btn_visualizzaFlowTree;
        private System.Windows.Forms.ToolStripButton btn_DHR;
        private System.Windows.Forms.ToolStrip toolStrip_MenuParte;
        private System.Windows.Forms.ToolStripButton btn_RefreshParteRMA;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton btn_AddParteRMA;
        private System.Windows.Forms.ToolStripButton btn_CancelParteRMA;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton btn_StampaEtichetteParte;
        private System.Windows.Forms.ToolStripComboBox cmb_ParteSelection;
        private System.Windows.Forms.ToolStripButton btn_EditParteRMA;
        private System.Windows.Forms.DataGridView dgv_ListaAllegatiRMA;
        private System.Windows.Forms.DataGridViewCheckBoxColumn chkAll;
        private System.Windows.Forms.DataGridViewTextBoxColumn nomeFile;
        private System.Windows.Forms.DataGridView dgv_ListaProblemiRMA;
        private System.Windows.Forms.DataGridViewCheckBoxColumn chk_Prob;
        private System.Windows.Forms.DataGridViewTextBoxColumn difettoProbl;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataAggProb;
        private System.Windows.Forms.DataGridViewTextBoxColumn gravitaProb;
        private System.Windows.Forms.DataGridViewTextBoxColumn idProblema;
        private System.Windows.Forms.ToolStrip toolStrip_MenuAnalisiProblema;
        private System.Windows.Forms.ToolStripButton btn_RefreshProbl;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripButton btn_AddProbl;
        private System.Windows.Forms.ToolStripButton btn_DeleteProbl;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripButton btn_addTemplateProbDB;
        private System.Windows.Forms.ToolStripButton btn_importProbFromTemplateDB;
        private System.Windows.Forms.ToolStripButton btn_CloneProblema;
        private System.Windows.Forms.ToolStripButton btn_EditProbl;
        private System.Windows.Forms.ToolStripButton btn_editTemplateProbDB;
        private System.Windows.Forms.DataGridView dgv_ListaCollimatoriRMA;
        private System.Windows.Forms.DataGridViewCheckBoxColumn chk_coll;
        private System.Windows.Forms.DataGridViewTextBoxColumn serialNr;
        private System.Windows.Forms.DataGridViewTextBoxColumn modello;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataProd;
        private System.Windows.Forms.DataGridViewTextBoxColumn cliente;
        private System.Windows.Forms.DataGridViewTextBoxColumn codColl;
        private System.Windows.Forms.DataGridViewTextBoxColumn numFC;
        private System.Windows.Forms.DataGridViewTextBoxColumn da_SN;
        private System.Windows.Forms.DataGridViewTextBoxColumn al_SN;
        private System.Windows.Forms.DataGridViewTextBoxColumn qta;
        private System.Windows.Forms.DataGridView dgv_ListaPartiRMA;
        private System.Windows.Forms.DataGridViewCheckBoxColumn chk_Parte;
        private System.Windows.Forms.DataGridViewTextBoxColumn idParte;
        private System.Windows.Forms.DataGridViewTextBoxColumn descrizParte;
        private System.Windows.Forms.DataGridViewTextBoxColumn SNParte;
        private System.Windows.Forms.DataGridViewTextBoxColumn cliParte;
        private System.Windows.Forms.DataGridViewTextBoxColumn FC_Parte;
        private System.Windows.Forms.DataGridViewTextBoxColumn numBolla;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataBolla;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataVendita;
        private System.Windows.Forms.DataGridViewTextBoxColumn qtyPart;
        private System.Windows.Forms.DataGridView dgv_FasiDaRieseguire;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn descFase;
        private System.Windows.Forms.DataGridViewComboBoxColumn minLavoro;
        private System.Windows.Forms.DataGridView dgv_testDaEseguire;
        private System.Windows.Forms.DataGridViewCheckBoxColumn checkTest;
        private System.Windows.Forms.DataGridViewTextBoxColumn descTest;
        private System.Windows.Forms.ToolStrip toolStrip_MenuRiparazioniDaEseguire;
        private System.Windows.Forms.ToolStripButton btn_RefreshRiparazione;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripButton btn_AddRiparazione;
        private System.Windows.Forms.ToolStripButton btn_importRipFromTemplateDB;
        private System.Windows.Forms.ToolStripButton btn_CloneRiparazione;
        private System.Windows.Forms.ToolStripButton btn_EditRiparazione;
        private System.Windows.Forms.ToolStripButton btn_DeleteRiparazione;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripButton btn_addTemplateRipDB;
        private System.Windows.Forms.ToolStripButton btn_editTemplateRipDB;
        private System.Windows.Forms.DataGridView dgv_ListaRiparazRMA;
        private System.Windows.Forms.DataGridViewCheckBoxColumn chk_Rip;
        private System.Windows.Forms.DataGridViewTextBoxColumn descRip;
        private System.Windows.Forms.DataGridViewTextBoxColumn descParte;
        private System.Windows.Forms.DataGridViewTextBoxColumn qtaParti;
        private System.Windows.Forms.DataGridViewTextBoxColumn statoRip;
        private System.Windows.Forms.DataGridViewTextBoxColumn noteRip;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataRip;
        private System.Windows.Forms.DataGridViewTextBoxColumn idRiparazione;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator12;
        private System.Windows.Forms.ToolStripButton btn_PrintRiparazioni;
        private System.Windows.Forms.DataGridView dgv_azioniSupplementari;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewComboBoxColumn statoAzioneSuppl;
        private System.Windows.Forms.ToolStrip toolStrip_MenuAzioniSupplementari;
        private System.Windows.Forms.ToolStripButton btn_RefreshAzSuppl;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator13;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripButton btn_GestisciAzSuppl;
        private System.Windows.Forms.ToolStrip toolStrip_MenuPreventivi;
        private System.Windows.Forms.ToolStripButton btn_RefreshPreventivi;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator14;
        private System.Windows.Forms.ToolStripButton btn_AddPreventivo;
        private System.Windows.Forms.ToolStripButton btn_ClonePreventivo;
        private System.Windows.Forms.ToolStripButton btn_EditPreventivo;
        private System.Windows.Forms.ToolStripButton btn_DeletePreventivo;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator15;
        private System.Windows.Forms.ToolStripButton btn_PrintPreventivo;
        private System.Windows.Forms.ToolStripComboBox cmb_PrevPrezzi;
        private System.Windows.Forms.DataGridView dgv_ListaPreventiviRMA;
        private System.Windows.Forms.DataGridViewCheckBoxColumn chk_Prev;
        private System.Windows.Forms.DataGridViewTextBoxColumn idPreventivo;
        private System.Windows.Forms.DataGridViewTextBoxColumn revisionePrev;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataAggPrev;
        private System.Windows.Forms.DataGridViewTextBoxColumn totale;
        private System.Windows.Forms.DataGridViewTextBoxColumn stato;
        private System.Windows.Forms.DataGridViewTextBoxColumn idPrev;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btn_RefreshNoteRMA;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator16;
        private System.Windows.Forms.ToolStripButton btn_AddNoteRMA;
        private System.Windows.Forms.ToolStripButton btn_EditNoteRMA;
        private System.Windows.Forms.ToolStripButton btn_DeleteNoteRMA;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator17;
        private System.Windows.Forms.ToolStripButton btn_PrintGenericInfo;
        private System.Windows.Forms.DataGridView dgv_noteRMA;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn logAggDa;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn noteLog;
        private System.Windows.Forms.DataGridViewTextBoxColumn idNote;
        private System.Windows.Forms.TabPage tab_Allegati;
        private System.Windows.Forms.ToolStrip toolStrip_MenuAllegati;
        private System.Windows.Forms.ToolStripButton btn_RefreshAllegati;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton btn_AddAllegati;
        private System.Windows.Forms.ToolStripButton btn_DeleteAllegati;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton btn_viewAllegati;
        private System.Windows.Forms.CheckBox cb_modManNumRMA;
        private System.Windows.Forms.TextBox tb_ModManNumRMA;
    }
}