﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ralco_ServiceManager
{
    public class RMA_Parte : IEquatable <RMA_Parte>
    {
        public int idProgressivo { get; set; }
        public string idRMA { get; set; }
        public string idParte { get; set; }
        public string descParte { get; set; }
        public string serialeParte { get; set; }
        public string nomeCliente { get; set; }
        public string numBolla { get; set; }
        public DateTime dataBolla { get; set; }
        public DateTime dataVendita { get; set; }
        public Int16 quantita { get; set; }
        public string numFlowChart { get; set; }

        //costruttore di default
        public RMA_Parte()
        {
            //do nothing...
        }

        //override del costruttore di default
        public RMA_Parte(string idParte, string descParte, string serialeParte, string nomeCliente, string numBolla, DateTime dataBolla, DateTime dataVendita, Int16 quantita, String numFlowChart)
        {
            this.idParte = idParte;
            this.descParte = descParte;
            this.serialeParte = serialeParte;
            this.nomeCliente = nomeCliente;
            this.numBolla = numBolla;
            this.dataBolla = dataBolla;
            this.dataVendita = dataVendita;
            this.quantita = quantita;
            this.numFlowChart = numFlowChart;
        }

        public bool Equals(RMA_Parte obj)
        {
            if (obj == null)
                return false;
            else if (obj.idParte.Equals(this.idParte) && obj.descParte.Equals(this.descParte)
                    && obj.serialeParte.Equals(this.serialeParte) && obj.nomeCliente.Equals(this.nomeCliente)
                    && obj.numBolla.Equals(this.numBolla) && obj.dataBolla.Equals(this.dataBolla) 
                    && obj.dataVendita.Equals(this.dataVendita) && obj.quantita.Equals(this.quantita))
                return true;
            else
                return false;
        }
    }
}
