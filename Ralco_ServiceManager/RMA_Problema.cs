﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ralco_ServiceManager
{
    public class RMA_Problema
    {
        public string idRMA { get; set; }
        public int idProblema { get; set; }
        public DateTime dataAggProb { get; set; }
        public string faseProduzione { get; set; }
        public string codParteControllo { get; set; }
        public string rifFlowChart { get; set; }
        public string causaProblema { get; set; }
        public string gravitaProblema { get; set; }
        public List<String> nomeResponsabile { get; set; }
        public string nomeTecnicoRalco { get; set; }
        public string faseIndividuazioneProblema { get; set; }
        public string difRiscDaRalco { get; set; }
        public string descParte { get; set; }

        //costruttore di default
        public RMA_Problema()
        {
            //do nothing...
        }

        //override del costruttore
        public RMA_Problema(RMA_Problema prob)
        {
            this.idRMA = prob.idRMA;
            this.idProblema = prob.idProblema;
            this.dataAggProb = DateTime.Now;
            this.faseProduzione = prob.faseProduzione;
            this.codParteControllo = prob.codParteControllo;
            this.descParte = prob.descParte;
            this.rifFlowChart = prob.rifFlowChart;
            this.causaProblema = prob.causaProblema;
            this.gravitaProblema = prob.gravitaProblema;
            this.nomeResponsabile = new List<string>();
            foreach (string s in prob.nomeResponsabile)
                this.nomeResponsabile.Add(s);
            this.nomeTecnicoRalco = prob.nomeTecnicoRalco;
            this.faseIndividuazioneProblema = prob.faseIndividuazioneProblema;
            this.difRiscDaRalco = prob.difRiscDaRalco;
        }
    }
}
