﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ralco_ServiceManager
{
    public partial class Form_ListaTemplatesDB : Form
    {
        public List<RMA_Problema> problemsList { get; set; }
        public List<RMA_Riparazione> repairsList { get; set; }
        private List<RMA_Problema> probList;
        private List<RMA_Riparazione> ripList;
        private SQLConnector conn;
        private int main;
        public Form_ListaTemplatesDB(int main)
        {
            try
            {
                InitializeComponent();
                conn = new SQLConnector();
                this.main = main;
                createDataGridViewColumns();
                setDataGridViewColumnsParameters();
                setWidgetDescriptions();
                fill_dgv_ListaTemplates();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void createDataGridViewColumns()
        {
            try
            {
                switch (main)
                {
                    case 1://Template Problema
                        dgv_ListaTemplates.Columns.Add("id", "");
                        dgv_ListaTemplates.Columns.Add("difRisc", "");
                        dgv_ListaTemplates.Columns.Add("causaProb", "");
                        dgv_ListaTemplates.Columns.Add("faseProd", "");
                        dgv_ListaTemplates.Columns.Add("rifFC", "");
                        dgv_ListaTemplates.Columns.Add("codParte", "");
                        dgv_ListaTemplates.Columns.Add("descParte", "");
                        dgv_ListaTemplates.Columns.Add("gravitaProb", "");
                        break;
                    case 2://Template Riparazione
                        dgv_ListaTemplates.Columns.Add("id", "");
                        dgv_ListaTemplates.Columns.Add("descRip", "");
                        dgv_ListaTemplates.Columns.Add("parteSost", "");
                        dgv_ListaTemplates.Columns.Add("descParte", "");
                        dgv_ListaTemplates.Columns.Add("qty", "");
                        dgv_ListaTemplates.Columns.Add("statoRip", "");
                        dgv_ListaTemplates.Columns.Add("noteRip", "");
                        break;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void setDataGridViewColumnsParameters()
        {
            try
            {
                switch (main)
                {
                    case 1://Template Problema
                        dgv_ListaTemplates.Columns["id"].Width = 50;
                        dgv_ListaTemplates.Columns["difRisc"].Width = 130;
                        dgv_ListaTemplates.Columns["causaProb"].Width = 130;
                        dgv_ListaTemplates.Columns["faseProd"].Width = 130;
                        dgv_ListaTemplates.Columns["rifFC"].Width = 130;
                        dgv_ListaTemplates.Columns["codParte"].Width = 130;
                        dgv_ListaTemplates.Columns["descParte"].Width = 130;
                        dgv_ListaTemplates.Columns["gravitaProb"].Width = 130;
                        break;
                    case 2://Template Riparazione
                        dgv_ListaTemplates.Columns["id"].Width = 50;
                        dgv_ListaTemplates.Columns["descRip"].Width = 230;
                        dgv_ListaTemplates.Columns["parteSost"].Width = 130;
                        dgv_ListaTemplates.Columns["descParte"].Width = 230;
                        dgv_ListaTemplates.Columns["qty"].Width = 50;
                        dgv_ListaTemplates.Columns["statoRip"].Width = 130;
                        dgv_ListaTemplates.Columns["noteRip"].Width = 130;
                        break;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void setWidgetDescriptions()
        {
            try
            {
                #region PartiComuni
                ToolTip tooltip = new System.Windows.Forms.ToolTip();
                tooltip.SetToolTip(btn_OK, Utils.resourcemanager.GetString("btn_OK"));
                tooltip.SetToolTip(btn_cancel, Utils.resourcemanager.GetString("btn_cancel"));
                #endregion
                #region PartiCustom
                switch (main)
                {
                    case 1://Template Problema
                        this.Text = Utils.resourcemanager.GetString("Form_ListaTemplatesDB_Problema");
                        dgv_ListaTemplates.Columns["id"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaTemplates_id");
                        dgv_ListaTemplates.Columns["difRisc"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaTemplates_difRisc");
                        dgv_ListaTemplates.Columns["causaProb"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaTemplates_causaProb");
                        dgv_ListaTemplates.Columns["faseProd"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaTemplates_faseProd");
                        dgv_ListaTemplates.Columns["rifFC"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaTemplates_rifFC");
                        dgv_ListaTemplates.Columns["codParte"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaTemplates_codParte");
                        dgv_ListaTemplates.Columns["descParte"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaTemplates_descParte");
                        dgv_ListaTemplates.Columns["gravitaProb"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaTemplates_gravitaProb");
                        break;
                    case 2://Template Riparazione
                        this.Text = Utils.resourcemanager.GetString("Form_ListaTemplatesDB_Riparazione");
                        dgv_ListaTemplates.Columns["id"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaTemplates_id");
                        dgv_ListaTemplates.Columns["descRip"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaTemplates_descRip");
                        dgv_ListaTemplates.Columns["parteSost"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaTemplates_parteSost");
                        dgv_ListaTemplates.Columns["descParte"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaTemplates_descParte");
                        dgv_ListaTemplates.Columns["qty"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaTemplates_qty");
                        dgv_ListaTemplates.Columns["statoRip"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaTemplates_statoRip");
                        dgv_ListaTemplates.Columns["noteRip"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaTemplates_noteRip");
                        break;
                }
                #endregion
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void fill_dgv_ListaTemplates()
        {
            try
            {
                switch (main)
                {
                    case 1://Template Problema
                        {
                            probList = new List<RMA_Problema>();
                            probList = (List<RMA_Problema>)conn.CreateCommand("SELECT * FROM [Autotest].[dbo].[RMA_ProblemaTemplateDB]", probList);
                            if (probList != null)
                            {
                                foreach (RMA_Problema p in probList)
                                {
                                    dgv_ListaTemplates.Rows.Insert(0, false, p.idProblema, p.difRiscDaRalco, p.causaProblema, p.faseProduzione, p.rifFlowChart, p.codParteControllo, p.descParte, p.gravitaProblema);
                                }
                                dgv_ListaTemplates.Refresh();
                            }
                        }
                        break;
                    case 2://Template Riparazione
                        {
                            ripList = new List<RMA_Riparazione>();
                            ripList = (List<RMA_Riparazione>)conn.CreateCommand("SELECT * FROM [Autotest].[dbo].[RMA_RiparazioneTemplateDB]", ripList);
                            if (ripList != null)
                            {
                                foreach (RMA_Riparazione r in ripList)
                                {
                                    dgv_ListaTemplates.Rows.Insert(0, false, r.idRiparazione, r.descRip, r.idParte, r.descParte, r.quantita, r.statoRip, r.noteRip);
                                }
                                dgv_ListaTemplates.Refresh();
                            }
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgv_ListaTemplates_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                Utils.DataGridView_CellContentClick(dgv_ListaTemplates, 1);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void updateVariabilePubblica()
        {
            try
            {
                switch (main)
                {
                    case 1://Template Problema
                        problemsList = new List<RMA_Problema>();
                        foreach (DataGridViewRow row in dgv_ListaTemplates.Rows)
                        {
                            if (row.Cells.GetCellValueFromColumnHeader("").Equals(true))
                            {
                                problemsList.Add(probList.Single(r => (r.idProblema.Equals(row.Cells.GetCellValueFromColumnHeader(dgv_ListaTemplates.Columns["id"].HeaderText)))));
                            }
                        }
                        break;
                    case 2://Template Riparazione
                        repairsList = new List<RMA_Riparazione>();
                        foreach (DataGridViewRow row in dgv_ListaTemplates.Rows)
                        {
                            if (row.Cells.GetCellValueFromColumnHeader("").Equals(true))
                            {
                                repairsList.Add(ripList.Single(r => (r.idRiparazione.Equals(row.Cells.GetCellValueFromColumnHeader(dgv_ListaTemplates.Columns["id"].HeaderText)))));
                            }
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_okColl_Click(object sender, EventArgs e)
        {
            try
            {
                updateVariabilePubblica();
                switch (main)
                {
                    case 1://Template Problema
                        {
                            if (problemsList == null || problemsList.Count.Equals(0))
                                MessageBox.Show("Selezionare almeno un elemento tra quelli disponibili", "Informazione", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            else if (problemsList.Count >= 2)
                                MessageBox.Show("Selezionare un solo elemento tra quelli disponibili", "Informazione", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            else
                            {
                                this.DialogResult = DialogResult.OK;
                                this.Dispose();
                            }
                        }
                        break;
                    case 2://Template Riparazione
                        {
                            if (repairsList == null || repairsList.Count.Equals(0))
                                MessageBox.Show("Selezionare almeno un elemento tra quelli disponibili", "Informazione", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            else if (repairsList.Count >= 2)
                                MessageBox.Show("Selezionare un solo elemento tra quelli disponibili", "Informazione", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            else
                            {
                                this.DialogResult = DialogResult.OK;
                                this.Dispose();
                            }
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_cancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.DialogResult = DialogResult.Cancel;
                this.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

    }
}
