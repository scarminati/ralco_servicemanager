﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ralco_ServiceManager
{
    public class RMA_FaseProduzione
    {
        public Int64 idProgressivo { get; set; }
        public string idRMA { get; set; }
        public List<int> minFaseProd { get; set; }

        //costruttore di default
        public RMA_FaseProduzione()
        {
            //do nothing...
        }

        //overload costruttore di default
        public RMA_FaseProduzione(RMA_FaseProduzione fase)
        {
            this.idProgressivo = fase.idProgressivo;
            this.idRMA = fase.idRMA;
            this.minFaseProd = fase.minFaseProd;
        }
    }
}
