﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ralco_ServiceManager
{
    public partial class Form_CostiExtra : Form
    {
        public List<RMA_CostiExtra> costiExtraList { get; set; }
        private List<RMA_CostiExtra> costiExtraCmbList = new List<RMA_CostiExtra>();
        private SQLConnector conn;
        public Form_CostiExtra()
        {
            try
            {
                InitializeComponent();
                conn = new SQLConnector();
                setWidgetDescriptions();
                setCmbCostiExtra();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void setWidgetDescriptions()
        {
            try
            {
                #region Form
                this.Text = Utils.resourcemanager.GetString("Form_CostiExtra");
                #endregion
                #region TooltipsPerBottoni
                ToolTip tooltip = new System.Windows.Forms.ToolTip();
                tooltip.SetToolTip(btn_OK, Utils.resourcemanager.GetString("btn_OK"));
                tooltip.SetToolTip(btn_cancel, Utils.resourcemanager.GetString("btn_cancel"));
                tooltip.SetToolTip(btn_addExtra, Utils.resourcemanager.GetString("btn_addExtra"));
                tooltip.SetToolTip(btn_addExtraToDB, Utils.resourcemanager.GetString("btn_addExtraToDB"));
                #endregion
                #region Label
                lbl_Desc.Text = Utils.resourcemanager.GetString("lbl_Desc");
                lbl_prezzoCostoExtra.Text = Utils.resourcemanager.GetString("lbl_prezzoCostoExtra");
                #endregion
                #region Groupbox
                gb_selectExtra.Text = Utils.resourcemanager.GetString("gb_selectExtra");
                gb_addExtra.Text = Utils.resourcemanager.GetString("gb_addExtra");
                #endregion
                #region DataGridViewColumns
                dgv_ListaCostiExtra.Columns["idExtra"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaCostiExtra_idExtra");
                dgv_ListaCostiExtra.Columns["qta"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaCostiExtra_qta");
                dgv_ListaCostiExtra.Columns["descCostoExtra"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaCostiExtra_descCostoExtra");
                dgv_ListaCostiExtra.Columns["prezzoCostoExtra"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaCostiExtra_prezzoCostoExtra");
                #endregion
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void setCmbCostiExtra()
        {
            try
            {
                cmb_costoExtra.Items.Clear();
                cmb_costoExtra.SelectedIndex = -1;
                costiExtraCmbList.Clear();
                costiExtraCmbList = (List<RMA_CostiExtra>)conn.CreateCommand("SELECT * FROM [Autotest].[dbo].[RMA_CostiExtra]", costiExtraCmbList);
                if (costiExtraCmbList != null)
                    foreach (RMA_CostiExtra c in costiExtraCmbList)
                        cmb_costoExtra.Items.Add(c.descrizione);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgv_ListaCostiExtra_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                Utils.DataGridView_CellContentClick(dgv_ListaCostiExtra, 1);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_addExtra_Click(object sender, EventArgs e)
        {
            try
            {
                if (!cmb_costoExtra.SelectedIndex.Equals(-1))
                {
                    RMA_CostiExtra costoExtra = new RMA_CostiExtra();
                    costoExtra = (RMA_CostiExtra)conn.CreateCommand("SELECT * FROM [Autotest].[dbo].[RMA_CostiExtra] WHERE [Autotest].[dbo].[RMA_CostiExtra].[descrizione] = '" + cmb_costoExtra.SelectedItem + "'", costoExtra);
                    dgv_ListaCostiExtra.Rows.Insert(0, true, "EXTRA" + costoExtra.idExtra, costoExtra.quantita, costoExtra.descrizione, costoExtra.prezzoUnitario);
                }
                else
                    MessageBox.Show("Seleziona un costo extra da aggiungere", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_addExtraToDB_Click(object sender, EventArgs e)
        {
            try
            {
                if (tb_DescCostiExtra.Text.Equals(""))
                    MessageBox.Show("Inserisci la descrizione prima di procedere", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else if (tb_PrezzoCostiExtra.Text.Equals("    ,"))
                    MessageBox.Show("Inserisci il prezzo prima di procedere", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                {
                    conn.CreateCommand("INSERT INTO [Autotest].[dbo].[RMA_CostiExtra] " +
                    "([descrizione],[prezzoUnitario],[quantita]) " +
                    "VALUES ('" + Utils.parseUselessChars(tb_DescCostiExtra.Text) + "','" + tb_PrezzoCostiExtra.Text.Trim().Replace (',','.') + "','" + "1" + "')", null);
                }
                setCmbCostiExtra();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_okCostiExtra_Click(object sender, EventArgs e)
        {
            try
            {
                costiExtraList = new List<RMA_CostiExtra>();
                foreach (DataGridViewRow row in dgv_ListaCostiExtra.Rows)
                {
                    if (row.Cells.GetCellValueFromColumnHeader("").Equals(true))
                    {
                        costiExtraList.Add(costiExtraCmbList.Single(r => (r.descrizione.Equals(row.Cells.GetCellValueFromColumnHeader(dgv_ListaCostiExtra.Columns["descCostoExtra"].HeaderText)))));
                    }
                }
                this.DialogResult = DialogResult.OK;
                this.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_cancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.DialogResult = DialogResult.Cancel;
                this.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
