﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ralco_ServiceManager
{
    public class RMA_ClientiRalco
    {
        public int IdCliente { get; set; }
        public string nomeCliente { get; set; }
        public decimal scontoCli { get; set; }
        public decimal scontoMod { get; set; }

        //costruttore di default
        public RMA_ClientiRalco()
        {
            //do nothing
        }

        //overload costruttore
        public RMA_ClientiRalco(RMA_ClientiRalco cli)
        {
            this.IdCliente = cli.IdCliente;
            this.nomeCliente = cli.nomeCliente;
            this.scontoCli = cli.scontoCli;
            this.scontoMod = cli.scontoMod;
        }
    }
}
