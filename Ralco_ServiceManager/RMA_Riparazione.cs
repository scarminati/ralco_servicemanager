﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ralco_ServiceManager
{
    public class RMA_Riparazione
    {
        public string idRMA { get; set; }
        public int idRiparazione { get; set; }
        public DateTime dataAggRip { get; set; }
        public string descRip { get; set; }
        public string idParte { get; set; }
        public string descParte { get; set; }
        public int quantita { get; set; }
        public string statoRip { get; set; }
        public string noteRip { get; set; }
        public string nomeTecnicoRalco { get; set; }
        public Decimal prezzoUnitario { get; set; }

        //costruttore di default
        public RMA_Riparazione()
        {

        }

        public RMA_Riparazione(RMA_Riparazione rip)
        {
            this.idRMA = rip.idRMA;
            this.idRiparazione = rip.idRiparazione;
            this.dataAggRip = DateTime.Now.AddMilliseconds(DateTime.Now.Millisecond);
            this.descRip = rip.descRip;
            this.quantita = rip.quantita;
            this.idParte = rip.idParte;
            this.descParte = rip.descParte;
            this.statoRip = rip.statoRip;
            this.noteRip = rip.noteRip;
            this.prezzoUnitario = rip.prezzoUnitario;
        }
    }
}
