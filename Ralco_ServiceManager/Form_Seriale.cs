﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Ralco_ServiceManager
{
    public partial class Form_Seriale : Form
    {
        public List<String> seriali;
        private SQLConnector conn;
        public Form_Seriale()
        {
            try
            {
                InitializeComponent();
                conn = new SQLConnector();
                seriali = new List<String>();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void setWidgetDescriptions()
        {
            try
            {
                #region Form
                this.Text = Utils.resourcemanager.GetString("Form_Seriale");
                #endregion
                #region TooltipsPerBottoni
                ToolTip tooltip = new System.Windows.Forms.ToolTip();
                tooltip.SetToolTip(btn_OK, Utils.resourcemanager.GetString("btn_OK"));
                tooltip.SetToolTip(btn_cancel, Utils.resourcemanager.GetString("btn_cancel"));
                tooltip.SetToolTip(btn_AddSeriale, Utils.resourcemanager.GetString("btn_AddSeriale"));
                tooltip.SetToolTip(btn_AddManySerials, Utils.resourcemanager.GetString("btn_AddManySerials"));
                #endregion
                #region Label
                lbl_SN.Text = Utils.resourcemanager.GetString("lbl_SN");
                lbl_fromSN.Text = Utils.resourcemanager.GetString("lbl_fromSN");
                lbl_toSN.Text = Utils.resourcemanager.GetString("lbl_toSN");
                #endregion
                #region DataGridViewColumns
                dgv_listaSeriali.Columns["numSer"].HeaderText = Utils.resourcemanager.GetString("dgv_listaSeriali_numSer");
                #endregion
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_AddSerial_Click(object sender, EventArgs e)
        {
            try
            {
                addSerial(0);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_AddManySerials_Click(object sender, EventArgs e)
        {
            try
            {
                addSerial(1);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void addSerial(int main)
        {
            try
            {
                switch (main)
                {
                    case 0:
                        {
                            if (tb_Seriale.Text.Equals(""))
                                MessageBox.Show("Inserisci il numero seriale da aggiungere.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            else
                                seriali.Add(tb_Seriale.Text.Trim());
                        }
                        break;
                    case 1:
                        {
                            if (tb_dalSeriale.Text.Equals(""))
                                MessageBox.Show("Inserisci il primo seriale della sequenza.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            else if (tb_alSeriale.Text.Equals(""))
                                MessageBox.Show("Inserisci l'ultimo seriale della sequenza.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            else if (String.Compare(tb_dalSeriale.Text, tb_alSeriale.Text) > 0)
                                MessageBox.Show("Il seriale iniziale dev'essere inferiore a quello finale.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            else
                            {
                                Int64 dalSerInt = Convert.ToInt64(tb_dalSeriale.Text.Trim());
                                Int64 alSerInt = Convert.ToInt64(tb_alSeriale.Text.Trim());
                                for (Int64 i = dalSerInt; i <= alSerInt; i++)
                                    seriali.Add(i.ToString());
                            }
                        }
                        break;
                }
                refreshDataGridView();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void refreshDataGridView()
        {
            try
            {
                dgv_listaSeriali.Rows.Clear();
                foreach (String s in seriali)
                    dgv_listaSeriali.Rows.Insert(0, true, s);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            try
            {
                this.DialogResult = DialogResult.OK;
                this.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_cancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.DialogResult = DialogResult.Cancel;
                this.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
