﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Ralco_ServiceManager
{
    public partial class Form_Responsabile : Form
    {
        public String nomeResponsabile;
        private List<String> responsabili = new List<String>();
        private SQLConnector conn;
        public Form_Responsabile()
        {
            try
            {
                InitializeComponent();
                setWidgetDescriptions();
                conn = new SQLConnector();
                initializeCombobox();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void setWidgetDescriptions()
        {
            try
            {
                #region Form
                this.Text = Utils.resourcemanager.GetString("Form_Responsabile");
                #endregion
                #region TooltipsPerBottoni
                ToolTip tooltip = new System.Windows.Forms.ToolTip();
                tooltip.SetToolTip(btn_AddResponsabile, Utils.resourcemanager.GetString("btn_AddResponsabile"));
                #endregion
                #region Label
                lbl_nomeRespo.Text = Utils.resourcemanager.GetString("lbl_nomeRespo");
                #endregion
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void initializeCombobox()
        {
            try
            {
                cmb_NomeResponsabile.Items.Clear();
                responsabili = (List<String>)conn.CreateCommand("SELECT [Descrizione] FROM [archivi].[dbo].[Operatori] ORDER BY [Descrizione] ASC", responsabili);
                foreach (var r in responsabili)
                    cmb_NomeResponsabile.Items.Add(r);
                List<String> altriResponsabili = new List<String>();
                altriResponsabili = Utils.getAddedComboboxValues(cmb_NomeResponsabile);
                foreach (string r in altriResponsabili)
                    cmb_NomeResponsabile.Items.Add(r);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_AddResponsabile_Click(object sender, EventArgs e)
        {
            try
            {
                if (cmb_NomeResponsabile.SelectedItem.Equals(""))
                    MessageBox.Show("Seleziona un utente", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                {
                    nomeResponsabile = cmb_NomeResponsabile.SelectedItem.ToString();
                    this.DialogResult = DialogResult.OK;
                    this.Dispose();
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
