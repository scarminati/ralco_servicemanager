﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ralco_ServiceManager
{
    public class RMA_Collimatore
    {
        public string idRMA { get; set; }
        public string seriale { get; set; }
        public string modelloColl { get; set; }
        public DateTime dataProduzione { get; set; }
        public RMA_ClientiRalco cliente { get; set; }
        public string codCollCliente { get; set; }//è il modo in cui il cliente chiama il collimatore, che può essere differente dalla nomenclatura RALCO
        public string numFlowChart { get; set; }
        public Int64 da_N { get; set; }
        public Int64 a_N { get; set; }
        public Int16 qta { get; set; }
    }
}
