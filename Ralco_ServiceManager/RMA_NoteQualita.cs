﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ralco_ServiceManager
{
    public class RMA_NoteQualita
    {
        public int idNoteQualita { get; set; }
        public string idRMA { get; set; }
        public string sicurezzaProdotto { get; set; }
        public string segnalazIncidente { get; set; }
        public string invioNotaInf { get; set; }
        public string revDocValutazRischi { get; set; }
        public string statoAzCorrett { get; set; }
        public string rifAzCorrett { get; set; }
        public string descAzCorrett { get; set; }
        public string note { get; set; }
    }
}
