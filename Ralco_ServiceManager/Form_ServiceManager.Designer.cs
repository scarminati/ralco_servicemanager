﻿namespace Ralco_ServiceManager
{
    partial class Form_ServiceManager
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_ServiceManager));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.toolStrip_Menu = new System.Windows.Forms.ToolStrip();
            this.lbl_Utente = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_updates = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_collInfo = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_Refresh = new System.Windows.Forms.ToolStripButton();
            this.cmb_RMASelection = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.cmb_linesPerPage = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_AddCNC = new System.Windows.Forms.ToolStripButton();
            this.btn_CloneCNC = new System.Windows.Forms.ToolStripButton();
            this.btn_EditCNC = new System.Windows.Forms.ToolStripButton();
            this.btn_EditMultipleCNC = new System.Windows.Forms.ToolStripButton();
            this.btn_DeleteCNC = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this.cmb_criterioRicerca = new System.Windows.Forms.ToolStripComboBox();
            this.tb_ricerca = new System.Windows.Forms.ToolStripTextBox();
            this.btn_DatabaseSearch = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator12 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_StampaEtichetteRMA = new System.Windows.Forms.ToolStripButton();
            this.btn_PrintCNC = new System.Windows.Forms.ToolStripButton();
            this.btn_ExportCNC = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_Logout = new System.Windows.Forms.ToolStripButton();
            this.dgv_ListaNonConf = new Ralco_ServiceManager.CustomDataGridView();
            this.checks = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.RMA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nomeCli = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.modCollPart = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numCollimatori = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DataCreazione = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DataUpdate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.aggiornDa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataScad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Priorita = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Stato = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numSCAR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.docReso = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ordRip = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.noteCliente = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.garanzia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.causeProb = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.difettiRisc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.provenienza = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolStrip_Menu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ListaNonConf)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStrip_Menu
            // 
            this.toolStrip_Menu.AutoSize = false;
            this.toolStrip_Menu.BackColor = System.Drawing.SystemColors.Control;
            this.toolStrip_Menu.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip_Menu.ImageScalingSize = new System.Drawing.Size(47, 47);
            this.toolStrip_Menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lbl_Utente,
            this.toolStripSeparator1,
            this.btn_updates,
            this.toolStripSeparator4,
            this.btn_collInfo,
            this.toolStripSeparator10,
            this.btn_Refresh,
            this.cmb_RMASelection,
            this.toolStripSeparator5,
            this.cmb_linesPerPage,
            this.toolStripSeparator3,
            this.btn_AddCNC,
            this.btn_CloneCNC,
            this.btn_EditCNC,
            this.btn_EditMultipleCNC,
            this.btn_DeleteCNC,
            this.toolStripSeparator11,
            this.cmb_criterioRicerca,
            this.tb_ricerca,
            this.btn_DatabaseSearch,
            this.toolStripSeparator12,
            this.btn_StampaEtichetteRMA,
            this.btn_PrintCNC,
            this.btn_ExportCNC,
            this.toolStripSeparator6,
            this.btn_Logout});
            this.toolStrip_Menu.Location = new System.Drawing.Point(0, 0);
            this.toolStrip_Menu.Name = "toolStrip_Menu";
            this.toolStrip_Menu.Size = new System.Drawing.Size(1332, 50);
            this.toolStrip_Menu.TabIndex = 6;
            this.toolStrip_Menu.Text = "toolStrip1";
            // 
            // lbl_Utente
            // 
            this.lbl_Utente.Name = "lbl_Utente";
            this.lbl_Utente.Size = new System.Drawing.Size(78, 47);
            this.lbl_Utente.Text = "Nome Utente";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 50);
            // 
            // btn_updates
            // 
            this.btn_updates.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_updates.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_updates.Image = ((System.Drawing.Image)(resources.GetObject("btn_updates.Image")));
            this.btn_updates.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_updates.Name = "btn_updates";
            this.btn_updates.Size = new System.Drawing.Size(51, 47);
            this.btn_updates.ToolTipText = "Aggiungi A Magazzino";
            this.btn_updates.Click += new System.EventHandler(this.btn_updates_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 50);
            // 
            // btn_collInfo
            // 
            this.btn_collInfo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_collInfo.Image = ((System.Drawing.Image)(resources.GetObject("btn_collInfo.Image")));
            this.btn_collInfo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_collInfo.Name = "btn_collInfo";
            this.btn_collInfo.Size = new System.Drawing.Size(51, 47);
            this.btn_collInfo.Text = "Clona Record";
            this.btn_collInfo.Click += new System.EventHandler(this.btn_collInfo_Click);
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(6, 50);
            // 
            // btn_Refresh
            // 
            this.btn_Refresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_Refresh.Image = ((System.Drawing.Image)(resources.GetObject("btn_Refresh.Image")));
            this.btn_Refresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_Refresh.Name = "btn_Refresh";
            this.btn_Refresh.Size = new System.Drawing.Size(51, 47);
            this.btn_Refresh.Text = "Evidenzia";
            this.btn_Refresh.ToolTipText = "Evidenzia";
            this.btn_Refresh.Click += new System.EventHandler(this.Btn_Refresh_Click);
            // 
            // cmb_RMASelection
            // 
            this.cmb_RMASelection.AutoCompleteCustomSource.AddRange(new string[] {
            "Seleziona Tutti",
            "Deseleziona Tutti"});
            this.cmb_RMASelection.Items.AddRange(new object[] {
            "Seleziona Tutti",
            "Deseleziona Tutti"});
            this.cmb_RMASelection.Name = "cmb_RMASelection";
            this.cmb_RMASelection.Size = new System.Drawing.Size(121, 50);
            this.cmb_RMASelection.TextChanged += new System.EventHandler(this.Cmb_RMASelection_TextChanged);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 50);
            // 
            // cmb_linesPerPage
            // 
            this.cmb_linesPerPage.AutoCompleteCustomSource.AddRange(new string[] {
            "100 lines/page",
            "200 lines/page",
            "500 lines/page",
            "750 lines/page",
            "1000 lines/page",
            "All lines"});
            this.cmb_linesPerPage.Items.AddRange(new object[] {
            "100 linee/pagina",
            "200 linee/pagina",
            "300 linee/pagina",
            "500 linee/pagina",
            "1000 linee/pagina",
            "Tutto il Database"});
            this.cmb_linesPerPage.Name = "cmb_linesPerPage";
            this.cmb_linesPerPage.Size = new System.Drawing.Size(121, 50);
            this.cmb_linesPerPage.TextChanged += new System.EventHandler(this.Cmb_linesPerPage_TextChanged);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 50);
            // 
            // btn_AddCNC
            // 
            this.btn_AddCNC.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_AddCNC.Image = ((System.Drawing.Image)(resources.GetObject("btn_AddCNC.Image")));
            this.btn_AddCNC.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_AddCNC.Name = "btn_AddCNC";
            this.btn_AddCNC.Size = new System.Drawing.Size(51, 47);
            this.btn_AddCNC.Text = "toolStripButton4";
            this.btn_AddCNC.ToolTipText = "Esporta Magazzino";
            this.btn_AddCNC.Click += new System.EventHandler(this.btn_AddCNC_Click);
            // 
            // btn_CloneCNC
            // 
            this.btn_CloneCNC.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_CloneCNC.Enabled = false;
            this.btn_CloneCNC.Image = ((System.Drawing.Image)(resources.GetObject("btn_CloneCNC.Image")));
            this.btn_CloneCNC.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_CloneCNC.Name = "btn_CloneCNC";
            this.btn_CloneCNC.Size = new System.Drawing.Size(51, 47);
            this.btn_CloneCNC.ToolTipText = "Stampa Test";
            this.btn_CloneCNC.Click += new System.EventHandler(this.btn_CloneCNC_Click);
            // 
            // btn_EditCNC
            // 
            this.btn_EditCNC.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_EditCNC.Image = ((System.Drawing.Image)(resources.GetObject("btn_EditCNC.Image")));
            this.btn_EditCNC.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_EditCNC.Name = "btn_EditCNC";
            this.btn_EditCNC.Size = new System.Drawing.Size(51, 47);
            this.btn_EditCNC.Text = "toolStripButton1";
            this.btn_EditCNC.Click += new System.EventHandler(this.btn_EditCNC_Click);
            // 
            // btn_EditMultipleCNC
            // 
            this.btn_EditMultipleCNC.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_EditMultipleCNC.Enabled = false;
            this.btn_EditMultipleCNC.Image = ((System.Drawing.Image)(resources.GetObject("btn_EditMultipleCNC.Image")));
            this.btn_EditMultipleCNC.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_EditMultipleCNC.Name = "btn_EditMultipleCNC";
            this.btn_EditMultipleCNC.Size = new System.Drawing.Size(51, 47);
            this.btn_EditMultipleCNC.Text = "toolStripButton2";
            this.btn_EditMultipleCNC.Click += new System.EventHandler(this.btn_EditMultipleCNC_Click);
            // 
            // btn_DeleteCNC
            // 
            this.btn_DeleteCNC.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_DeleteCNC.Enabled = false;
            this.btn_DeleteCNC.Image = ((System.Drawing.Image)(resources.GetObject("btn_DeleteCNC.Image")));
            this.btn_DeleteCNC.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_DeleteCNC.Name = "btn_DeleteCNC";
            this.btn_DeleteCNC.Size = new System.Drawing.Size(51, 47);
            this.btn_DeleteCNC.Text = "toolStripButton2";
            this.btn_DeleteCNC.ToolTipText = "Rimuovi Da Magazzino";
            this.btn_DeleteCNC.Click += new System.EventHandler(this.btn_DeleteCNC_Click);
            // 
            // toolStripSeparator11
            // 
            this.toolStripSeparator11.Name = "toolStripSeparator11";
            this.toolStripSeparator11.Size = new System.Drawing.Size(6, 50);
            // 
            // cmb_criterioRicerca
            // 
            this.cmb_criterioRicerca.Name = "cmb_criterioRicerca";
            this.cmb_criterioRicerca.Size = new System.Drawing.Size(121, 50);
            // 
            // tb_ricerca
            // 
            this.tb_ricerca.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.tb_ricerca.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.tb_ricerca.Name = "tb_ricerca";
            this.tb_ricerca.Size = new System.Drawing.Size(100, 50);
            this.tb_ricerca.ToolTipText = "Cerca In Magazzino";
            // 
            // btn_DatabaseSearch
            // 
            this.btn_DatabaseSearch.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_DatabaseSearch.Image = ((System.Drawing.Image)(resources.GetObject("btn_DatabaseSearch.Image")));
            this.btn_DatabaseSearch.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_DatabaseSearch.Name = "btn_DatabaseSearch";
            this.btn_DatabaseSearch.Size = new System.Drawing.Size(51, 47);
            this.btn_DatabaseSearch.Text = "toolStripButton3";
            this.btn_DatabaseSearch.Click += new System.EventHandler(this.Btn_DatabaseSearch_Click);
            // 
            // toolStripSeparator12
            // 
            this.toolStripSeparator12.Name = "toolStripSeparator12";
            this.toolStripSeparator12.Size = new System.Drawing.Size(6, 50);
            // 
            // btn_StampaEtichetteRMA
            // 
            this.btn_StampaEtichetteRMA.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_StampaEtichetteRMA.Image = ((System.Drawing.Image)(resources.GetObject("btn_StampaEtichetteRMA.Image")));
            this.btn_StampaEtichetteRMA.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_StampaEtichetteRMA.Name = "btn_StampaEtichetteRMA";
            this.btn_StampaEtichetteRMA.Size = new System.Drawing.Size(51, 47);
            this.btn_StampaEtichetteRMA.Text = "toolStripButton4";
            this.btn_StampaEtichetteRMA.Click += new System.EventHandler(this.btn_StampaEtichetteRMA_Click);
            // 
            // btn_PrintCNC
            // 
            this.btn_PrintCNC.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_PrintCNC.Image = ((System.Drawing.Image)(resources.GetObject("btn_PrintCNC.Image")));
            this.btn_PrintCNC.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_PrintCNC.Name = "btn_PrintCNC";
            this.btn_PrintCNC.Size = new System.Drawing.Size(51, 47);
            this.btn_PrintCNC.Text = "toolStripButton5";
            this.btn_PrintCNC.Click += new System.EventHandler(this.btn_PrintCNC_Click);
            // 
            // btn_ExportCNC
            // 
            this.btn_ExportCNC.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_ExportCNC.Image = ((System.Drawing.Image)(resources.GetObject("btn_ExportCNC.Image")));
            this.btn_ExportCNC.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_ExportCNC.Name = "btn_ExportCNC";
            this.btn_ExportCNC.Size = new System.Drawing.Size(51, 47);
            this.btn_ExportCNC.Text = "toolStripButton6";
            this.btn_ExportCNC.Click += new System.EventHandler(this.btn_ExportCNC_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 50);
            // 
            // btn_Logout
            // 
            this.btn_Logout.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_Logout.Image = ((System.Drawing.Image)(resources.GetObject("btn_Logout.Image")));
            this.btn_Logout.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_Logout.Name = "btn_Logout";
            this.btn_Logout.Size = new System.Drawing.Size(51, 47);
            this.btn_Logout.Text = "toolStripButton7";
            this.btn_Logout.Click += new System.EventHandler(this.btn_Logout_Click);
            // 
            // dgv_ListaNonConf
            // 
            this.dgv_ListaNonConf.AllowUserToAddRows = false;
            this.dgv_ListaNonConf.AllowUserToOrderColumns = true;
            this.dgv_ListaNonConf.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv_ListaNonConf.BackgroundColor = System.Drawing.SystemColors.ActiveBorder;
            this.dgv_ListaNonConf.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_ListaNonConf.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.checks,
            this.RMA,
            this.nomeCli,
            this.modCollPart,
            this.numCollimatori,
            this.SN,
            this.FC,
            this.DataCreazione,
            this.DataUpdate,
            this.aggiornDa,
            this.dataScad,
            this.Priorita,
            this.Stato,
            this.numSCAR,
            this.docReso,
            this.ordRip,
            this.noteCliente,
            this.garanzia,
            this.causeProb,
            this.difettiRisc,
            this.provenienza});
            this.dgv_ListaNonConf.Cursor = System.Windows.Forms.Cursors.Default;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_ListaNonConf.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgv_ListaNonConf.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_ListaNonConf.Location = new System.Drawing.Point(0, 50);
            this.dgv_ListaNonConf.Name = "dgv_ListaNonConf";
            this.dgv_ListaNonConf.ReadOnly = true;
            this.dgv_ListaNonConf.RowHeadersVisible = false;
            this.dgv_ListaNonConf.Size = new System.Drawing.Size(1332, 589);
            this.dgv_ListaNonConf.TabIndex = 14;
            this.dgv_ListaNonConf.Tag = "";
            this.dgv_ListaNonConf.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_ListaNonConf_CellContentClick);
            // 
            // checks
            // 
            this.checks.HeaderText = "";
            this.checks.Name = "checks";
            this.checks.ReadOnly = true;
            this.checks.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.checks.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.checks.Width = 30;
            // 
            // RMA
            // 
            this.RMA.HeaderText = "ID RMA";
            this.RMA.Name = "RMA";
            this.RMA.ReadOnly = true;
            this.RMA.Width = 90;
            // 
            // nomeCli
            // 
            this.nomeCli.HeaderText = "Cliente";
            this.nomeCli.Name = "nomeCli";
            this.nomeCli.ReadOnly = true;
            this.nomeCli.Width = 200;
            // 
            // modCollPart
            // 
            this.modCollPart.HeaderText = "Modello Collimatore / Parte";
            this.modCollPart.Name = "modCollPart";
            this.modCollPart.ReadOnly = true;
            this.modCollPart.Width = 150;
            // 
            // numCollimatori
            // 
            this.numCollimatori.HeaderText = "Tot";
            this.numCollimatori.Name = "numCollimatori";
            this.numCollimatori.ReadOnly = true;
            this.numCollimatori.Width = 30;
            // 
            // SN
            // 
            this.SN.FillWeight = 70F;
            this.SN.HeaderText = "Numero Seriale";
            this.SN.Name = "SN";
            this.SN.ReadOnly = true;
            this.SN.Width = 60;
            // 
            // FC
            // 
            this.FC.FillWeight = 70F;
            this.FC.HeaderText = "Flow Chart";
            this.FC.Name = "FC";
            this.FC.ReadOnly = true;
            this.FC.Width = 70;
            // 
            // DataCreazione
            // 
            this.DataCreazione.HeaderText = "Data Creazione";
            this.DataCreazione.Name = "DataCreazione";
            this.DataCreazione.ReadOnly = true;
            this.DataCreazione.Width = 120;
            // 
            // DataUpdate
            // 
            this.DataUpdate.HeaderText = "Ultima Modifica";
            this.DataUpdate.Name = "DataUpdate";
            this.DataUpdate.ReadOnly = true;
            this.DataUpdate.Width = 120;
            // 
            // aggiornDa
            // 
            this.aggiornDa.HeaderText = "Aggiornato Da";
            this.aggiornDa.Name = "aggiornDa";
            this.aggiornDa.ReadOnly = true;
            // 
            // dataScad
            // 
            this.dataScad.HeaderText = "Data Scadenza";
            this.dataScad.Name = "dataScad";
            this.dataScad.ReadOnly = true;
            this.dataScad.Width = 60;
            // 
            // Priorita
            // 
            this.Priorita.HeaderText = "Priorità";
            this.Priorita.Name = "Priorita";
            this.Priorita.ReadOnly = true;
            this.Priorita.Width = 70;
            // 
            // Stato
            // 
            this.Stato.HeaderText = "Stato";
            this.Stato.Name = "Stato";
            this.Stato.ReadOnly = true;
            this.Stato.Width = 150;
            // 
            // numSCAR
            // 
            this.numSCAR.HeaderText = "Numero SCAR";
            this.numSCAR.Name = "numSCAR";
            this.numSCAR.ReadOnly = true;
            // 
            // docReso
            // 
            this.docReso.HeaderText = "Doc. di Reso";
            this.docReso.Name = "docReso";
            this.docReso.ReadOnly = true;
            // 
            // ordRip
            // 
            this.ordRip.HeaderText = "Ordine di Riparazione";
            this.ordRip.Name = "ordRip";
            this.ordRip.ReadOnly = true;
            // 
            // noteCliente
            // 
            this.noteCliente.HeaderText = "Note Cliente";
            this.noteCliente.Name = "noteCliente";
            this.noteCliente.ReadOnly = true;
            this.noteCliente.Width = 1000;
            // 
            // garanzia
            // 
            this.garanzia.HeaderText = "Garanzia";
            this.garanzia.Name = "garanzia";
            this.garanzia.ReadOnly = true;
            // 
            // causeProb
            // 
            this.causeProb.HeaderText = "Cause Problema";
            this.causeProb.Name = "causeProb";
            this.causeProb.ReadOnly = true;
            // 
            // difettiRisc
            // 
            this.difettiRisc.HeaderText = "Difetti Riscontrati";
            this.difettiRisc.Name = "difettiRisc";
            this.difettiRisc.ReadOnly = true;
            // 
            // provenienza
            // 
            this.provenienza.HeaderText = "Provenienza";
            this.provenienza.Name = "provenienza";
            this.provenienza.ReadOnly = true;
            // 
            // Form_ServiceManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1332, 639);
            this.Controls.Add(this.dgv_ListaNonConf);
            this.Controls.Add(this.toolStrip_Menu);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(1348, 677);
            this.MinimumSize = new System.Drawing.Size(1348, 677);
            this.Name = "Form_ServiceManager";
            this.Tag = "";
            this.Text = "Ralco Service Manager";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form_ServiceManager_FormClosing);
            this.toolStrip_Menu.ResumeLayout(false);
            this.toolStrip_Menu.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ListaNonConf)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ToolStrip toolStrip_Menu;
        private System.Windows.Forms.ToolStripLabel lbl_Utente;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton btn_updates;
        private System.Windows.Forms.ToolStripButton btn_collInfo;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripButton btn_Refresh;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton btn_DeleteCNC;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;
        private System.Windows.Forms.ToolStripTextBox tb_ricerca;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator12;
        private System.Windows.Forms.ToolStripButton btn_AddCNC;
        private System.Windows.Forms.ToolStripButton btn_CloneCNC;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripComboBox cmb_RMASelection;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripComboBox cmb_linesPerPage;
        private System.Windows.Forms.ToolStripButton btn_EditCNC;
        private System.Windows.Forms.ToolStripButton btn_EditMultipleCNC;
        private System.Windows.Forms.ToolStripComboBox cmb_criterioRicerca;
        private System.Windows.Forms.ToolStripButton btn_DatabaseSearch;
        private System.Windows.Forms.ToolStripButton btn_StampaEtichetteRMA;
        private System.Windows.Forms.ToolStripButton btn_PrintCNC;
        private System.Windows.Forms.ToolStripButton btn_ExportCNC;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripButton btn_Logout;
        private CustomDataGridView dgv_ListaNonConf;
        private System.Windows.Forms.DataGridViewCheckBoxColumn checks;
        private System.Windows.Forms.DataGridViewTextBoxColumn RMA;
        private System.Windows.Forms.DataGridViewTextBoxColumn nomeCli;
        private System.Windows.Forms.DataGridViewTextBoxColumn modCollPart;
        private System.Windows.Forms.DataGridViewTextBoxColumn numCollimatori;
        private System.Windows.Forms.DataGridViewTextBoxColumn SN;
        private System.Windows.Forms.DataGridViewTextBoxColumn FC;
        private System.Windows.Forms.DataGridViewTextBoxColumn DataCreazione;
        private System.Windows.Forms.DataGridViewTextBoxColumn DataUpdate;
        private System.Windows.Forms.DataGridViewTextBoxColumn aggiornDa;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataScad;
        private System.Windows.Forms.DataGridViewTextBoxColumn Priorita;
        private System.Windows.Forms.DataGridViewTextBoxColumn Stato;
        private System.Windows.Forms.DataGridViewTextBoxColumn numSCAR;
        private System.Windows.Forms.DataGridViewTextBoxColumn docReso;
        private System.Windows.Forms.DataGridViewTextBoxColumn ordRip;
        private System.Windows.Forms.DataGridViewTextBoxColumn noteCliente;
        private System.Windows.Forms.DataGridViewTextBoxColumn garanzia;
        private System.Windows.Forms.DataGridViewTextBoxColumn causeProb;
        private System.Windows.Forms.DataGridViewTextBoxColumn difettiRisc;
        private System.Windows.Forms.DataGridViewTextBoxColumn provenienza;
    }
}

