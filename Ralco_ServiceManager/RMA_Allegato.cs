﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ralco_ServiceManager
{
    public class RMA_Allegato
    {
        public int idProgressivo { get; set; }
        public string idRMA { get; set; }
        public string pathAllegato { get; set; }

        //costruttore di default
        public RMA_Allegato()
        {
            //do nothing
        }

        //overload costruttore
        public RMA_Allegato(string idRMA, string pathAllegato)
        {
            this.idRMA = idRMA;
            this.pathAllegato = pathAllegato;
        }

        //overload costruttore
        public RMA_Allegato(RMA_Allegato allegato)
        {
            this.idProgressivo = allegato.idProgressivo;
            this.idRMA = allegato.idRMA;
            this.pathAllegato = allegato.pathAllegato;
        }
    }
}
