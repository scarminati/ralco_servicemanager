﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ralco_ServiceManager
{
    public class RMA_Articolo :IEquatable <RMA_Articolo>
    {
        public string idRMA { get; set; }
        public int numPreventivo { get; set; }
        public string revisionePrev { get; set; }
        public string idArticolo { get; set; }
        public int quantita { get; set; }
        public string descrizione { get; set; }
        public Decimal prezzoUnitario { get; set; }
        private SQLConnector conn;

        //costruttore di default
        public RMA_Articolo()
        {
            //do nothing...
        }

        //override del costruttore di default
        public RMA_Articolo(string idArticolo, string descrizione, decimal prezzo, int quantita)
        {
            this.idArticolo = idArticolo;
            this.descrizione = descrizione;
            this.prezzoUnitario = prezzo;
            this.quantita = quantita;
        }

        public void addArticleEnglishDescription()
        {
            conn = new SQLConnector();
            try
            {
                if (!this.descrizione.Contains("*/*")) //bookmark (scelto da me) per separare le descrizioni it/en
                {
                    string descrizione = "";
                    descrizione = ((String)conn.CreateCommand("SELECT [descArt] FROM [Autotest].[dbo].[SM_VISTA_ARTPANTHLIN] WHERE [Autotest].[dbo].[SM_VISTA_ARTPANTHLIN].[ID_LINGUA] = 'en' AND [Autotest].[dbo].[SM_VISTA_ARTPANTHLIN].[ID_ARTICOLO] = '" + this.idArticolo + "'", descrizione));
                    if (descrizione != null)
                        this.descrizione = this.descrizione + " */* " + descrizione;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void calculatePrice(RMA_Articolo articolo)
        {
            try
            {
                conn = new SQLConnector();
                decimal costo = 0;
                costo = (decimal)conn.CreateCommand("SELECT COSTO FROM [archivi].[dbo].[vistalistaUltimiPrezziPanth] WHERE ID_ARTICOLO LIKE '%" + articolo.idArticolo + "%'", costo);
                if (costo == 0)
                {
                    List<RALCO_ModelliProduttivi> modProdList = new List<RALCO_ModelliProduttivi>();
                    modProdList = (List<RALCO_ModelliProduttivi>)conn.CreateCommand("SELECT PADRE, DSC_PADRE, FIGLIO, DSC_FIGLIO, COSTO FROM [archivi].dbo.VISTA_SM_MODPRO_PANTH WHERE PADRE LIKE '%" + articolo.idArticolo + "%'", modProdList);
                    if (modProdList != null)
                    {
                        foreach (RALCO_ModelliProduttivi m in modProdList)
                            if (!m.costo.Equals(0))
                            {
                                costo = costo + m.costo;
                            }
                            else
                            {
                                //articolo figlio non prezzato -> da rianalizzare!
                                RMA_Articolo s = new RMA_Articolo(m.figlio, m.descFiglio, m.costo,1);
                                calculatePrice(s);
                            }
                    }
                }
                this.prezzoUnitario = costo;
                return;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public bool Equals(RMA_Articolo obj)
        {
            if (obj == null)
                return false;
            else if (obj.idArticolo.Equals(this.idArticolo))
                return true;
            else
                return false;
        }
    }
}
