﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ralco_ServiceManager
{
    public class RMA_InformativaCliente
    {
        public int idInfoCli { get; set; }
        public string idRMA { get; set; }
        public string decisioneCliente { get; set; }
        public string numScarCli { get; set; }
        public string SCARCliente { get; set; }
        public string ordRiparazione { get; set; }
        public DateTime dataOrdineRip { get; set; }
        public string docReso { get; set; }
        public DateTime dataSpedizione { get; set; }
        public string speseSpedizione { get; set; }
        public string noteCliente { get; set; }
        public RMA_ClientiRalco cliente { get; set; }
        public DateTime dataArrivoCollim { get; set; }
        public DateTime dataScadenzaRMA { get; set; }
        public string numSCARCliente { get; set; }
        public DateTime dataDocReso { get; set; }
        public string tipoTrasporto { get; set; }
        public string codCollCli { get; set; }
        public string tipoImballaggio { get; set; }
        public string noteImballaggio { get; set; }
    }
}
