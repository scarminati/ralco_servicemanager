﻿namespace Ralco_ServiceManager
{
    partial class Form_Collimatore
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Collimatore));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btn_cercaColl = new System.Windows.Forms.Button();
            this.tb_numSeriale = new System.Windows.Forms.MaskedTextBox();
            this.lbl_SN = new System.Windows.Forms.Label();
            this.btn_OK = new System.Windows.Forms.Button();
            this.dgv_ListaCollimatoriRMA = new System.Windows.Forms.DataGridView();
            this.chk_coll = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.serialNr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.modello = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataProd = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cliente = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codColl = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numFC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.da_SN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.al_SN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.qta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rmaAttivi = new System.Windows.Forms.DataGridViewLinkColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.gb_cercaColl = new System.Windows.Forms.GroupBox();
            this.gb_addColl = new System.Windows.Forms.GroupBox();
            this.tb_flowChart = new System.Windows.Forms.MaskedTextBox();
            this.lbl_FC = new System.Windows.Forms.Label();
            this.lbl_toSN = new System.Windows.Forms.Label();
            this.tb_alNumSeriale = new System.Windows.Forms.MaskedTextBox();
            this.lbl_fromSN = new System.Windows.Forms.Label();
            this.tb_dalNumSeriale = new System.Windows.Forms.MaskedTextBox();
            this.btn_AddCollimators = new System.Windows.Forms.Button();
            this.btn_cancel = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ListaCollimatoriRMA)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.gb_cercaColl.SuspendLayout();
            this.gb_addColl.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_cercaColl
            // 
            this.btn_cercaColl.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_cercaColl.BackgroundImage")));
            this.btn_cercaColl.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_cercaColl.Location = new System.Drawing.Point(231, 24);
            this.btn_cercaColl.Name = "btn_cercaColl";
            this.btn_cercaColl.Size = new System.Drawing.Size(25, 25);
            this.btn_cercaColl.TabIndex = 2;
            this.btn_cercaColl.UseVisualStyleBackColor = true;
            this.btn_cercaColl.Click += new System.EventHandler(this.btn_cercaColl_Click);
            // 
            // tb_numSeriale
            // 
            this.tb_numSeriale.Location = new System.Drawing.Point(108, 27);
            this.tb_numSeriale.Mask = "9999999999";
            this.tb_numSeriale.MaximumSize = new System.Drawing.Size(100, 20);
            this.tb_numSeriale.MinimumSize = new System.Drawing.Size(100, 20);
            this.tb_numSeriale.Name = "tb_numSeriale";
            this.tb_numSeriale.Size = new System.Drawing.Size(100, 20);
            this.tb_numSeriale.TabIndex = 1;
            // 
            // lbl_SN
            // 
            this.lbl_SN.AutoSize = true;
            this.lbl_SN.Location = new System.Drawing.Point(12, 30);
            this.lbl_SN.Name = "lbl_SN";
            this.lbl_SN.Size = new System.Drawing.Size(79, 13);
            this.lbl_SN.TabIndex = 2;
            this.lbl_SN.Text = "Numero Seriale";
            // 
            // btn_OK
            // 
            this.btn_OK.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_OK.BackgroundImage")));
            this.btn_OK.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_OK.Location = new System.Drawing.Point(397, 298);
            this.btn_OK.Name = "btn_OK";
            this.btn_OK.Size = new System.Drawing.Size(50, 50);
            this.btn_OK.TabIndex = 102;
            this.btn_OK.UseVisualStyleBackColor = true;
            this.btn_OK.Click += new System.EventHandler(this.btn_okColl_Click);
            // 
            // dgv_ListaCollimatoriRMA
            // 
            this.dgv_ListaCollimatoriRMA.AllowUserToAddRows = false;
            this.dgv_ListaCollimatoriRMA.AllowUserToOrderColumns = true;
            this.dgv_ListaCollimatoriRMA.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv_ListaCollimatoriRMA.BackgroundColor = System.Drawing.SystemColors.ActiveBorder;
            this.dgv_ListaCollimatoriRMA.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_ListaCollimatoriRMA.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.chk_coll,
            this.serialNr,
            this.modello,
            this.dataProd,
            this.cliente,
            this.codColl,
            this.numFC,
            this.da_SN,
            this.al_SN,
            this.qta,
            this.rmaAttivi});
            this.dgv_ListaCollimatoriRMA.Cursor = System.Windows.Forms.Cursors.Default;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_ListaCollimatoriRMA.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_ListaCollimatoriRMA.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_ListaCollimatoriRMA.Location = new System.Drawing.Point(3, 16);
            this.dgv_ListaCollimatoriRMA.Name = "dgv_ListaCollimatoriRMA";
            this.dgv_ListaCollimatoriRMA.ReadOnly = true;
            this.dgv_ListaCollimatoriRMA.RowHeadersVisible = false;
            this.dgv_ListaCollimatoriRMA.Size = new System.Drawing.Size(1029, 179);
            this.dgv_ListaCollimatoriRMA.TabIndex = 103;
            this.dgv_ListaCollimatoriRMA.Click += new System.EventHandler(this.dgv_ListaCollimatoriRMA_Click);
            // 
            // chk_coll
            // 
            this.chk_coll.HeaderText = "";
            this.chk_coll.Name = "chk_coll";
            this.chk_coll.ReadOnly = true;
            this.chk_coll.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.chk_coll.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.chk_coll.Width = 30;
            // 
            // serialNr
            // 
            this.serialNr.HeaderText = "Seriale";
            this.serialNr.Name = "serialNr";
            this.serialNr.ReadOnly = true;
            this.serialNr.Width = 60;
            // 
            // modello
            // 
            this.modello.HeaderText = "Modello";
            this.modello.Name = "modello";
            this.modello.ReadOnly = true;
            this.modello.Width = 165;
            // 
            // dataProd
            // 
            this.dataProd.HeaderText = "Data Produzione";
            this.dataProd.Name = "dataProd";
            this.dataProd.ReadOnly = true;
            this.dataProd.Width = 110;
            // 
            // cliente
            // 
            this.cliente.HeaderText = "Cliente";
            this.cliente.Name = "cliente";
            this.cliente.ReadOnly = true;
            this.cliente.Width = 165;
            // 
            // codColl
            // 
            this.codColl.HeaderText = "Codice Collimatore";
            this.codColl.Name = "codColl";
            this.codColl.ReadOnly = true;
            this.codColl.Width = 120;
            // 
            // numFC
            // 
            this.numFC.HeaderText = "Flow Chart";
            this.numFC.Name = "numFC";
            this.numFC.ReadOnly = true;
            this.numFC.Width = 80;
            // 
            // da_SN
            // 
            this.da_SN.HeaderText = "Dal SN";
            this.da_SN.Name = "da_SN";
            this.da_SN.ReadOnly = true;
            this.da_SN.Width = 65;
            // 
            // al_SN
            // 
            this.al_SN.HeaderText = "Al SN";
            this.al_SN.Name = "al_SN";
            this.al_SN.ReadOnly = true;
            this.al_SN.Width = 60;
            // 
            // qta
            // 
            this.qta.HeaderText = "Quantità";
            this.qta.Name = "qta";
            this.qta.ReadOnly = true;
            this.qta.Width = 65;
            // 
            // rmaAttivi
            // 
            this.rmaAttivi.HeaderText = "RMA Attivi";
            this.rmaAttivi.Name = "rmaAttivi";
            this.rmaAttivi.ReadOnly = true;
            this.rmaAttivi.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.rmaAttivi.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.rmaAttivi.Width = 90;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dgv_ListaCollimatoriRMA);
            this.groupBox1.Location = new System.Drawing.Point(10, 77);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1035, 198);
            this.groupBox1.TabIndex = 104;
            this.groupBox1.TabStop = false;
            // 
            // gb_cercaColl
            // 
            this.gb_cercaColl.Controls.Add(this.lbl_SN);
            this.gb_cercaColl.Controls.Add(this.tb_numSeriale);
            this.gb_cercaColl.Controls.Add(this.btn_cercaColl);
            this.gb_cercaColl.Location = new System.Drawing.Point(10, 12);
            this.gb_cercaColl.Name = "gb_cercaColl";
            this.gb_cercaColl.Size = new System.Drawing.Size(270, 59);
            this.gb_cercaColl.TabIndex = 106;
            this.gb_cercaColl.TabStop = false;
            this.gb_cercaColl.Text = "Cerca Collimatore";
            // 
            // gb_addColl
            // 
            this.gb_addColl.Controls.Add(this.tb_flowChart);
            this.gb_addColl.Controls.Add(this.lbl_FC);
            this.gb_addColl.Controls.Add(this.lbl_toSN);
            this.gb_addColl.Controls.Add(this.tb_alNumSeriale);
            this.gb_addColl.Controls.Add(this.lbl_fromSN);
            this.gb_addColl.Controls.Add(this.tb_dalNumSeriale);
            this.gb_addColl.Controls.Add(this.btn_AddCollimators);
            this.gb_addColl.Location = new System.Drawing.Point(286, 12);
            this.gb_addColl.Name = "gb_addColl";
            this.gb_addColl.Size = new System.Drawing.Size(756, 59);
            this.gb_addColl.TabIndex = 107;
            this.gb_addColl.TabStop = false;
            this.gb_addColl.Text = "Aggiungi Collimatori";
            // 
            // tb_flowChart
            // 
            this.tb_flowChart.Location = new System.Drawing.Point(98, 27);
            this.tb_flowChart.Mask = "9999/9999";
            this.tb_flowChart.MaximumSize = new System.Drawing.Size(100, 20);
            this.tb_flowChart.MinimumSize = new System.Drawing.Size(100, 20);
            this.tb_flowChart.Name = "tb_flowChart";
            this.tb_flowChart.Size = new System.Drawing.Size(100, 20);
            this.tb_flowChart.TabIndex = 3;
            // 
            // lbl_FC
            // 
            this.lbl_FC.AutoSize = true;
            this.lbl_FC.Location = new System.Drawing.Point(12, 30);
            this.lbl_FC.Name = "lbl_FC";
            this.lbl_FC.Size = new System.Drawing.Size(57, 13);
            this.lbl_FC.TabIndex = 7;
            this.lbl_FC.Text = "Flow Chart";
            // 
            // lbl_toSN
            // 
            this.lbl_toSN.AutoSize = true;
            this.lbl_toSN.Location = new System.Drawing.Point(467, 30);
            this.lbl_toSN.Name = "lbl_toSN";
            this.lbl_toSN.Size = new System.Drawing.Size(89, 13);
            this.lbl_toSN.TabIndex = 5;
            this.lbl_toSN.Text = "Al numero Seriale";
            // 
            // tb_alNumSeriale
            // 
            this.tb_alNumSeriale.Location = new System.Drawing.Point(580, 27);
            this.tb_alNumSeriale.Mask = "9999999";
            this.tb_alNumSeriale.MaximumSize = new System.Drawing.Size(100, 20);
            this.tb_alNumSeriale.MinimumSize = new System.Drawing.Size(100, 20);
            this.tb_alNumSeriale.Name = "tb_alNumSeriale";
            this.tb_alNumSeriale.Size = new System.Drawing.Size(100, 20);
            this.tb_alNumSeriale.TabIndex = 5;
            // 
            // lbl_fromSN
            // 
            this.lbl_fromSN.AutoSize = true;
            this.lbl_fromSN.Location = new System.Drawing.Point(216, 30);
            this.lbl_fromSN.Name = "lbl_fromSN";
            this.lbl_fromSN.Size = new System.Drawing.Size(96, 13);
            this.lbl_fromSN.TabIndex = 2;
            this.lbl_fromSN.Text = "Dal numero Seriale";
            // 
            // tb_dalNumSeriale
            // 
            this.tb_dalNumSeriale.Location = new System.Drawing.Point(343, 27);
            this.tb_dalNumSeriale.Mask = "9999999";
            this.tb_dalNumSeriale.MaximumSize = new System.Drawing.Size(100, 20);
            this.tb_dalNumSeriale.MinimumSize = new System.Drawing.Size(100, 20);
            this.tb_dalNumSeriale.Name = "tb_dalNumSeriale";
            this.tb_dalNumSeriale.Size = new System.Drawing.Size(100, 20);
            this.tb_dalNumSeriale.TabIndex = 4;
            // 
            // btn_AddCollimators
            // 
            this.btn_AddCollimators.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_AddCollimators.BackgroundImage")));
            this.btn_AddCollimators.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_AddCollimators.Location = new System.Drawing.Point(714, 24);
            this.btn_AddCollimators.Name = "btn_AddCollimators";
            this.btn_AddCollimators.Size = new System.Drawing.Size(25, 25);
            this.btn_AddCollimators.TabIndex = 6;
            this.btn_AddCollimators.UseVisualStyleBackColor = true;
            this.btn_AddCollimators.Click += new System.EventHandler(this.btn_AddMultipleCollimators_Click);
            // 
            // btn_cancel
            // 
            this.btn_cancel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_cancel.BackgroundImage")));
            this.btn_cancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_cancel.Location = new System.Drawing.Point(502, 298);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(50, 50);
            this.btn_cancel.TabIndex = 112;
            this.btn_cancel.UseVisualStyleBackColor = true;
            this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
            // 
            // Form_Collimatore
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1054, 363);
            this.Controls.Add(this.btn_cancel);
            this.Controls.Add(this.gb_addColl);
            this.Controls.Add(this.gb_cercaColl);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btn_OK);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(1070, 401);
            this.MinimumSize = new System.Drawing.Size(1070, 401);
            this.Name = "Form_Collimatore";
            this.Text = "Cerca Collimatore";
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ListaCollimatoriRMA)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.gb_cercaColl.ResumeLayout(false);
            this.gb_cercaColl.PerformLayout();
            this.gb_addColl.ResumeLayout(false);
            this.gb_addColl.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btn_cercaColl;
        private System.Windows.Forms.MaskedTextBox tb_numSeriale;
        private System.Windows.Forms.Label lbl_SN;
        private System.Windows.Forms.Button btn_OK;
        private System.Windows.Forms.DataGridView dgv_ListaCollimatoriRMA;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox gb_cercaColl;
        private System.Windows.Forms.GroupBox gb_addColl;
        private System.Windows.Forms.Label lbl_toSN;
        private System.Windows.Forms.MaskedTextBox tb_alNumSeriale;
        private System.Windows.Forms.Label lbl_fromSN;
        private System.Windows.Forms.MaskedTextBox tb_dalNumSeriale;
        private System.Windows.Forms.Button btn_AddCollimators;
        private System.Windows.Forms.MaskedTextBox tb_flowChart;
        private System.Windows.Forms.Label lbl_FC;
        private System.Windows.Forms.DataGridViewCheckBoxColumn chk_coll;
        private System.Windows.Forms.DataGridViewTextBoxColumn serialNr;
        private System.Windows.Forms.DataGridViewTextBoxColumn modello;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataProd;
        private System.Windows.Forms.DataGridViewTextBoxColumn cliente;
        private System.Windows.Forms.DataGridViewTextBoxColumn codColl;
        private System.Windows.Forms.DataGridViewTextBoxColumn numFC;
        private System.Windows.Forms.DataGridViewTextBoxColumn da_SN;
        private System.Windows.Forms.DataGridViewTextBoxColumn al_SN;
        private System.Windows.Forms.DataGridViewTextBoxColumn qta;
        private System.Windows.Forms.DataGridViewLinkColumn rmaAttivi;
        private System.Windows.Forms.Button btn_cancel;
    }
}