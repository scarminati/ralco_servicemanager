﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ralco_ServiceManager
{
    public class RMA_DifettoRiscontrato
    {
        public string fase { get; set; }
        public string descDifetto { get; set; }
        public string codParte { get; set; }

        public RMA_DifettoRiscontrato()
        {
            //do nothing...
        }

        public RMA_DifettoRiscontrato(RMA_DifettoRiscontrato dif)
        {
            this.fase = dif.fase;
            this.descDifetto = dif.descDifetto;
            this.codParte = dif.codParte;
        }
    }
}
