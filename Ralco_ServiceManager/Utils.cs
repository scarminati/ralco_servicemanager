﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Xml;
using System.Resources;
using System.Reflection;

namespace Ralco_ServiceManager
{
    public static class Utils
    {
        public static bool isSortColumnPressed = false;
        public static ResourceManager resourcemanager;
        #region utility

        public static void setResourceManager()
        {
            try
            {
                if (Globals.user.language.Equals("Italiano"))
                    resourcemanager = new ResourceManager("Ralco_ServiceManager.lang_it", Assembly.GetExecutingAssembly());
                else if (Globals.user.language.Equals("English"))
                    resourcemanager = new ResourceManager("Ralco_ServiceManager.lang_en", Assembly.GetExecutingAssembly());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public static string parseUselessChars(string toParse)
        {
            string parsed = toParse;
            List<string> patterns = new List<string>() { "'" };
            foreach (string pattern in patterns)
            {
                Regex rgx = new Regex(pattern);
                if (pattern.Equals("'"))
                    parsed = rgx.Replace(parsed, "''");
            }
            return parsed;
        }
        #endregion

        //Metodi per oggetti DataGridView

        public static void DataGridView_CellContentClick(DataGridView dgv, int main)
        {
            try
            {
                if (isSortColumnPressed.Equals(false))
                {
                    switch (main)
                    {
                        case 1:
                            {
                                if (dgv.CurrentCell.ColumnIndex.Equals(0))
                                {
                                    if (dgv.CurrentCell.Value == (null) || dgv.CurrentCell.Value.Equals(false))
                                        dgv.CurrentCell.Value = true;
                                    else
                                        dgv.CurrentCell.Value = false;
                                }
                            }
                            break;
                        case 2://dgv_FasiDaRieseguire
                            {
                                if (dgv.CurrentCell.ColumnIndex.Equals(0))
                                {
                                    if (dgv.CurrentCell.Value == (null) || dgv.CurrentCell.Value.Equals(false))
                                    {
                                        dgv.CurrentRow.Cells[0].Value = true;
                                        dgv.CurrentRow.Cells[2].Value = "15";
                                    }
                                    else
                                    {
                                        dgv.CurrentRow.Cells[0].Value = false;
                                        dgv.CurrentRow.Cells[2].Value = "0";
                                    }
                                }
                            }
                            break;
                    }
                }
                else
                {
                    isSortColumnPressed = false;
                }
                    

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public static object GetCellValueFromColumnHeader(this DataGridViewCellCollection CellCollection, string HeaderText)
        {
            return CellCollection.Cast<DataGridViewCell>().First(c => c.OwningColumn.HeaderText == HeaderText).Value;
        }

        #region MetodiAggiunteCombobox

        public static List<String> getAddedComboboxValues(ComboBox c)
        {
            List<String> items = new List<String>();
            SQLConnector  conn = new SQLConnector();
            items = (List<String>)conn.CreateCommand("SELECT [AutoTest].[dbo].[SM_AggiunteCombobox].[descrizione] FROM [AutoTest].[dbo].[SM_AggiunteCombobox] WHERE [AutoTest].[dbo].[SM_AggiunteCombobox].[nomeCombobox] = '" + c.Name.ToString() + "'", items);
            if (items == null)
                items = new List<String>();
            return (items);
        }
        #endregion

        #region Xml

        //restituisce tutti gli elementi del file XML il cui tag è uguale ad elementName
        public static XmlDocument readXmlDocument(string fileName)
        {
            try
            {
                XmlDocument xml = new XmlDocument();
                xml.Load(fileName);
                return (xml);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return (null);
            }
        }


        #endregion

    }
}
