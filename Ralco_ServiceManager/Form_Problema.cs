﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ralco_ServiceManager
{
    public partial class Form_Problema : Form
    {
        public RMA_Problema problema { get; set; }
        private SQLConnector conn;
        private List<string> numFlowChart;
        private string idRMA;
        private int idProblema;
        private List<RMA_DifettoRiscontrato> difList;
        private List<String> respoAdded = new List<String>();
        private List<RMA_Articolo> artListFromDB { get; set; }
        private List<RMA_EsitiTestPreAnalisi> esitiTestListFromDB { get; set; }
        private List<RMA_EsitiTestPreAnalisi> esitiTestListAdded { get; set; }
        private bool isTemplateDBProblem = false;
        public Form_Problema(string idRMA, List<string> numFlowChart, List<RMA_DifettoRiscontrato> difList, bool isTemplateDBProblem, params RMA_Problema[] prob_FromForm)
        {
            try
            {
                InitializeComponent();
                conn = new SQLConnector();
                this.isTemplateDBProblem = isTemplateDBProblem;
                setWidgetDescriptions();
                List<ComboBox> cmbListToSet = new List<ComboBox>() { cmb_gravitaProblema, cmb_causaProblema, cmb_rifFlowChart };
                foreach (ComboBox c in cmbListToSet)
                    setComboboxValues(c);
                if (!isTemplateDBProblem)
                {
                    this.numFlowChart = numFlowChart;
                    this.idRMA = idRMA;
                }
                this.difList = difList;
                setCmbFasiProduzione();
                setCmbCodParteControllo();
                setDgvTestDaEseguire();
                esitiTestListFromDB = new List<RMA_EsitiTestPreAnalisi>();
                esitiTestListAdded = new List<RMA_EsitiTestPreAnalisi>();
                if (prob_FromForm != null)
                {
                    problema = new RMA_Problema(prob_FromForm[0]);
                    idProblema = prob_FromForm[0].idProblema;
                    if (!isTemplateDBProblem)
                        esitiTestListFromDB = (List<RMA_EsitiTestPreAnalisi>)conn.CreateCommand("SELECT * FROM [Autotest].[dbo].[RMA_EsitiTestPreAnalisi] WHERE [Autotest].[dbo].[RMA_EsitiTestPreAnalisi].[idRMA] = '" + idRMA + "' AND [Autotest].[dbo].[RMA_EsitiTestPreAnalisi].[idProblema] = '" + idProblema + "'", esitiTestListFromDB);
                    else
                        esitiTestListFromDB = null;
                    fillForm();
                }
                else
                {
                    problema = new RMA_Problema();
                    idProblema = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public Form_Problema(string idRMA, List<RMA_Problema> problems)
        {
            try
            {
                this.idRMA = idRMA;
                conn = new SQLConnector();
                foreach (RMA_Problema p in problems)
                {
                    problema = new RMA_Problema(p);
                    problema.idRMA = null;
                    commitDB();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void setWidgetDescriptions()
        {
            try
            {
                #region Form
                this.Text = Utils.resourcemanager.GetString("Form_Problema");
                #endregion
                #region TooltipsPerBottoni
                ToolTip tooltip = new System.Windows.Forms.ToolTip();
                tooltip.SetToolTip(btn_OK, Utils.resourcemanager.GetString("btn_OK"));
                tooltip.SetToolTip(btn_cancel, Utils.resourcemanager.GetString("btn_cancel"));
                tooltip.SetToolTip(btn_AddRespo, Utils.resourcemanager.GetString("btn_AddRespo"));
                tooltip.SetToolTip(btn_CancelRespo, Utils.resourcemanager.GetString("btn_CancelRespo"));
                #endregion
                #region Label
                lbl_difRiscRalco.Text = Utils.resourcemanager.GetString("lbl_difRiscRalco");
                lbl_causaProb.Text = Utils.resourcemanager.GetString("lbl_causaProb");
                lbl_faseProd.Text = Utils.resourcemanager.GetString("lbl_faseProd");
                lbl_rifFlowChart.Text = Utils.resourcemanager.GetString("lbl_rifFlowChart");
                lbl_DescrizParte.Text = Utils.resourcemanager.GetString("lbl_DescrizParte");
                lbl_gravita.Text = Utils.resourcemanager.GetString("lbl_gravita");
                #endregion
                #region Groupbox
                grp_dettProb.Text = Utils.resourcemanager.GetString("grp_dettProb");
                grp_testEseguiti.Text = Utils.resourcemanager.GetString("grp_testEseguiti");
                #endregion
                #region Radio Button
                rb_codiceParte.Text = Utils.resourcemanager.GetString("rb_codiceParte");
                #endregion
                #region DataGridViewColumns
                dgv_testDaEseguire.Columns["descTest"].HeaderText = Utils.resourcemanager.GetString("dgv_testDaEseguire_descTest");
                dgv_testDaEseguire.Columns["esitoTest"].HeaderText = Utils.resourcemanager.GetString("dgv_testDaEseguire_esitoTest");
                dgv_nomeResponsabile.Columns["nomeResponsabile"].HeaderText = Utils.resourcemanager.GetString("dgv_nomeResponsabile_nomeResponsabile");
                #endregion
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgv_nomeResponsabili_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                Utils.DataGridView_CellContentClick(dgv_nomeResponsabile, 1);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void setComboboxValues(ComboBox combobox)
        {
            try
            {
                List<String> items = new List<String>();
                items = Utils.getAddedComboboxValues(combobox);
                if (!combobox.Name.Equals("cmb_rifFlowChart") && !combobox.Name.Equals("cmb_faseProduzione"))
                    combobox.Items.Clear();
                foreach (String s in items)
                    combobox.Items.Add(s);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void setCmbDifettoRisc()
        {
            try
            {
                cmb_rifFlowChart.SelectedIndex = -1;
                cmb_rifFlowChart.Items.Clear();
                cmb_codParteControllo.Text = "";
                foreach (RMA_DifettoRiscontrato d in difList)
                    Console.WriteLine("Codice = " + d.codParte + "Difetto = " + d.descDifetto + "Fase = " + d.fase); ;
                foreach (RMA_DifettoRiscontrato d in difList)
                    if (d.fase.Equals(cmb_faseProduzione.SelectedItem.ToString()))
                        cmb_rifFlowChart.Items.Add(d.descDifetto);
                setComboboxValues(cmb_rifFlowChart);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void setCmbFasiProduzione()
        {
            try
            {
                cmb_faseProduzione.Items.Clear();
                List<string> listFasi = new List<string>();
                listFasi = getListValues("SELECT [Autotest].[dbo].[RMA_FasiProduzione].[descrizFaseProduzione] FROM [Autotest].[dbo].[RMA_FasiProduzione]");
                if (listFasi != null)
                    foreach (string s in listFasi)
                        cmb_faseProduzione.Items.Add(s);
                setComboboxValues(cmb_faseProduzione);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void setCmbCodParteControllo()
        {
            try
            {
                artListFromDB = new List<RMA_Articolo>();
                artListFromDB = getIdArticoli();
                foreach (RMA_Articolo a in artListFromDB)
                    cmb_codParteControllo.Items.Add(a.idArticolo.Trim());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void setDgvTestDaEseguire()
        {
            try
            {
                dgv_testDaEseguire.Rows.Clear();
                List<string> listTest = new List<string>();
                listTest = getListValues("SELECT descrizioneTest FROM [Autotest].[dbo].[SM_VISTA_RMA_CLI_MODCOLL_DESCTEST] WHERE idRMA = '" + idRMA + "'");
                listTest.Reverse();
                if (listTest != null)
                    foreach (string s in listTest)
                        dgv_testDaEseguire.Rows.Insert(0, false, s, "");
                dgv_testDaEseguire.Refresh();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private List<RMA_Articolo> getIdArticoli()
        {
            try
            {
                List<RMA_Articolo> artListFromDB = new List<RMA_Articolo>();
                artListFromDB = (List<RMA_Articolo>)conn.CreateCommand("SELECT DISTINCT ID_AZIENDA, ID_ARTICOLO, UPPER (DESCRIZIONE), ID_SCELTA, ID_SCELTA FROM [archivi].[dbo].[vistaListaArticoliPanth] WHERE ID_LINGUA = 'it' ORDER BY ID_ARTICOLO", artListFromDB);
                return (artListFromDB);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }

        private void cmb_faseIndProblema_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                setCmbDifettoRisc();
                setNomeResponsabile();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cmb_difettoRiscontrato_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (!cmb_rifFlowChart.SelectedIndex.Equals(-1) && !cmb_rifFlowChart.SelectedItem.ToString().Contains("GENERICO : "))
                {
                    string codParteDif = difList.SingleOrDefault(r => r.descDifetto.Equals(cmb_rifFlowChart.SelectedItem.ToString()) && r.fase.Equals(cmb_faseProduzione.SelectedItem.ToString())).codParte.Replace("/", "_");
                    if (cmb_codParteControllo.Items.Contains(codParteDif))
                        cmb_codParteControllo.SelectedIndex = cmb_codParteControllo.FindString(codParteDif);
                    else
                        cmb_codParteControllo.SelectedIndex = -1;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private List<string> getListValues(string SQLquery)
        {
            try
            {
                List<string> listValues = new List<string>();
                listValues.Clear();
                listValues = (List<string>)conn.CreateCommand(SQLquery, listValues);
                return listValues;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }

        //determina a chi assegnare la non conformità in base al reparto selezionato.
        private void setNomeResponsabile()
        {
            try
            {
                if (numFlowChart != null)
                {
                    foreach (string f in numFlowChart)
                    {
                        string[] flowChartSplitted = f.Split('/');
                        string fc = flowChartSplitted[1] + flowChartSplitted[0];
                        int fcInt = Convert.ToInt32(fc);
                        respoAdded.Clear();
                        respoAdded = getListValues("SELECT [archivi].[dbo].[VISTA_SM_AVLAV].[Operatore] FROM [archivi].[dbo].[VISTA_SM_AVLAV] WHERE [archivi].[dbo].[VISTA_SM_AVLAV].[IDFlow] = " + fcInt + "AND [archivi].[dbo].[VISTA_SM_AVLAV].[Desc] = '" + cmb_faseProduzione.SelectedItem.ToString() + "'");
                        dgv_nomeResponsabile.Rows.Clear();
                        refreshTableNomeResponsabili();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void fillForm()
        {
            try
            {
                cmb_faseProduzione.SelectedIndex = cmb_faseProduzione.FindString(problema.faseProduzione);
                cmb_rifFlowChart.SelectedIndex = cmb_rifFlowChart.FindString(problema.rifFlowChart);
                cmb_codParteControllo.SelectedIndex = cmb_codParteControllo.FindString(problema.codParteControllo);
                cmb_causaProblema.SelectedIndex = cmb_causaProblema.FindString(problema.causaProblema);
                cmb_gravitaProblema.SelectedIndex = cmb_gravitaProblema.FindString(problema.gravitaProblema);
                if (!problema.idRMA.Equals(""))
                {
                    respoAdded.Clear();
                    foreach (string s in problema.nomeResponsabile)
                        if (!s.Equals(""))
                            respoAdded.Add(s);
                    refreshTableNomeResponsabili();
                }
                tb_difRiscDaRalco.Text = problema.difRiscDaRalco;
                if (esitiTestListFromDB != null)
                    foreach (DataGridViewRow r in dgv_testDaEseguire.Rows)
                    {
                        RMA_EsitiTestPreAnalisi tmp = new RMA_EsitiTestPreAnalisi(idRMA, r.Cells[1].Value.ToString(), r.Cells[2].Value.ToString());
                        foreach (RMA_EsitiTestPreAnalisi test in esitiTestListFromDB)
                        {
                            if (test.descTestDaEseguire.Equals(tmp.descTestDaEseguire))
                            {
                                r.Cells[0].Value = true;
                                r.Cells[2].Value = test.esitoTest;
                                break;
                            }
                        }
                    }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void refreshTableNomeResponsabili()
        {
            try
            {
                dgv_nomeResponsabile.Rows.Clear();
                foreach (string s in respoAdded)
                    if (!s.Equals(""))
                        dgv_nomeResponsabile.Rows.Insert(0, false, s);
                dgv_nomeResponsabile.Refresh();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_AddRespo_Click(object sender, EventArgs e)
        {
            try
            {
                if (respoAdded.Count < 5)
                {
                    using (var form = new Form_Responsabile())
                    {
                        var result = form.ShowDialog();
                        if (result == DialogResult.OK)
                        {
                            respoAdded.Add(form.nomeResponsabile);
                        }
                    }
                    refreshTableNomeResponsabili();
                }
                else
                    MessageBox.Show("Non è possibile inserire più di 5 responsabili per un singolo problema", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_CancelRespo_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow row in dgv_nomeResponsabile.Rows)
                {
                    if (row.Cells["chk_Respo"].Value != null && row.Cells["chk_Respo"].Value.Equals(true))
                    {
                        var itemToRemove = row.Cells["nomeResponsabile"].Value;
                        if (itemToRemove != null)
                        {
                            try
                            {
                                respoAdded.Remove(itemToRemove.ToString());
                            }
                            catch (Exception ex)
                            {
                                //non esiste alcun elemento della lista corrispondente ad itemToRemove -> do nothing...
                            }

                        }
                    }
                }
                refreshTableNomeResponsabili();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //assegna i valori dei vari campi del form agli attributi del problema
        private void updateVariabilePubblica()
        {
            try
            {
                problema.dataAggProb = DateTime.Now;
                problema.difRiscDaRalco = Utils.parseUselessChars(tb_difRiscDaRalco.Text);
                if (cmb_causaProblema.SelectedItem != null)
                    problema.causaProblema = cmb_causaProblema.SelectedItem.ToString();
                else
                    problema.causaProblema = "";
                if (cmb_faseProduzione.SelectedItem != null)
                    problema.faseProduzione = cmb_faseProduzione.SelectedItem.ToString();
                else
                    problema.faseProduzione = "";
                if (cmb_rifFlowChart.SelectedItem != null)
                    problema.rifFlowChart = Utils.parseUselessChars(cmb_rifFlowChart.SelectedItem.ToString());
                else
                    problema.rifFlowChart = "";
                if (cmb_codParteControllo.SelectedItem != null)
                    problema.codParteControllo = cmb_codParteControllo.Text;
                else
                    problema.codParteControllo = "";
                problema.descParte = lbl_descParte.Text;
                if (cmb_gravitaProblema.SelectedItem != null)
                    problema.gravitaProblema = cmb_gravitaProblema.SelectedItem.ToString();
                else
                    problema.gravitaProblema = "";
                if (problema.nomeResponsabile == null)
                    problema.nomeResponsabile = new List<string>();
                problema.nomeResponsabile.Clear();
                foreach (String s in respoAdded)
                    problema.nomeResponsabile.Add(s);
                //restituisco sempre 5 nomi responsabili.(nel caso non ce ne siano, restituisco vuoto). Condizione necessaria per permettere la insert nel DB
                if (problema.nomeResponsabile.Count < 5)
                    while (!problema.nomeResponsabile.Count.Equals(5))
                        problema.nomeResponsabile.Add("");
                problema.nomeTecnicoRalco = Globals.user.getFullUserName();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void commitDB()
        {
            try
            {
                if (!isTemplateDBProblem)
                {
                    //ho aggiunto un nuovo problema tra quelli degli RMA
                    if (problema.idRMA == null || idProblema == 0 || problema.idRMA == "")
                    {
                        problema.idRMA = idRMA;
                        conn.CreateCommand("INSERT INTO [Autotest].[dbo].[RMA_Problema] " +
                        "([idRMA],[dataAggProb],[faseProduzione],[codParteControllo],[descParte],[difettoRiscontrato],[causaProblema],[gravitaProblema],[nomeResponsabile],[nomeTecnicoRalco],[faseIndividuazioneProblema],[noteProblema],[nomeResponsabile2],[nomeResponsabile3],[nomeResponsabile4],[nomeResponsabile5]) " +
                        "VALUES ('" + problema.idRMA + "','" + problema.dataAggProb.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "','" + problema.faseProduzione + "','" + problema.codParteControllo + "','" + problema.descParte + "','" + problema.rifFlowChart + "','" + problema.causaProblema + "','" + problema.gravitaProblema + "','" + problema.nomeResponsabile[0] + "','" + problema.nomeTecnicoRalco + "','" + problema.faseIndividuazioneProblema + "','" + problema.difRiscDaRalco + "','" + problema.nomeResponsabile[1] + "','" + problema.nomeResponsabile[2] + "','" + problema.nomeResponsabile[3] + "','" + problema.nomeResponsabile[4] + "')", null);
                    }
                    else
                    {
                        //aggiorno i dati del problema
                        conn.CreateCommand("UPDATE [Autotest].[dbo].[RMA_Problema] SET" +
                        "[idRMA] = '" + problema.idRMA + "'" +
                        ",[dataAggProb] = '" + problema.dataAggProb.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "'" +
                        ",[faseProduzione] = '" + problema.faseProduzione + "'" +
                        ",[codParteControllo] = '" + problema.codParteControllo + "'" +
                        ",[descParte] = '" + problema.descParte + "'" +
                        ",[difettoRiscontrato] = '" + problema.rifFlowChart + "'" +
                        ",[causaProblema] = '" + problema.causaProblema + "'" +
                        ",[gravitaProblema] = '" + problema.gravitaProblema + "'" +
                        ",[nomeResponsabile] = '" + problema.nomeResponsabile[0] + "'" +
                        ",[nomeTecnicoRalco] = '" + problema.nomeTecnicoRalco + "'" +
                        ",[faseIndividuazioneProblema] = '" + problema.faseIndividuazioneProblema + "'" +
                        ",[noteProblema] = '" + problema.difRiscDaRalco + "'" +
                        ",[nomeResponsabile2] = '" + problema.nomeResponsabile[1] + "'" +
                        ",[nomeResponsabile3] = '" + problema.nomeResponsabile[2] + "'" +
                        ",[nomeResponsabile4] = '" + problema.nomeResponsabile[3] + "'" +
                        ",[nomeResponsabile5] = '" + problema.nomeResponsabile[4] + "'" +
                        " WHERE ([Autotest].[dbo].[RMA_Problema].[idRMA] = '" + problema.idRMA + "' AND [Autotest].[dbo].[RMA_Problema].[idProblema] = '" + problema.idProblema + "')", null);
                    }
                    if (idProblema == 0)
                    {
                        idProblema = (int)conn.CreateCommand("SELECT TOP(1)[idProblema] FROM[AutoTest].[dbo].[RMA_Problema] ORDER BY idProblema DESC", idProblema);
                        problema.idProblema = idProblema;
                    }
                    if (esitiTestListAdded != null)
                    {
                        foreach (RMA_EsitiTestPreAnalisi test in esitiTestListAdded)
                        {
                            conn.CreateCommand("INSERT INTO [Autotest].[dbo].[RMA_EsitiTestPreAnalisi] " +
                            "([idRMA],[idProblema],[descTestDaEseguire],[esitoTest]) " +
                            "VALUES ('" + test.idRMA + "','" + idProblema + "','" + test.descTestDaEseguire + "','" + test.esitoTest + "')", null);
                        }
                    }
                    esitiTestListFromDB = new List<RMA_EsitiTestPreAnalisi>();
                    esitiTestListFromDB = (List<RMA_EsitiTestPreAnalisi>)conn.CreateCommand("SELECT * FROM [Autotest].[dbo].[RMA_EsitiTestPreAnalisi] WHERE [Autotest].[dbo].[RMA_EsitiTestPreAnalisi].[idRMA] = '" + idRMA + "' AND [Autotest].[dbo].[RMA_EsitiTestPreAnalisi].[idProblema] = '" + idProblema + "'", esitiTestListFromDB);
                    if (esitiTestListFromDB == null)
                        esitiTestListFromDB = new List<RMA_EsitiTestPreAnalisi>();
                    if (esitiTestListAdded != null)
                        esitiTestListFromDB.AddRange(esitiTestListAdded);
                    if (dgv_testDaEseguire != null)
                    {
                        foreach (DataGridViewRow r in dgv_testDaEseguire.Rows)
                        {
                            RMA_EsitiTestPreAnalisi tmp = new RMA_EsitiTestPreAnalisi(idRMA, r.Cells[1].Value.ToString(), r.Cells[2].Value.ToString());
                            foreach (RMA_EsitiTestPreAnalisi test in esitiTestListFromDB)
                            {
                                if (test.Equals(tmp))
                                {
                                    if (r.Cells[0].Value.Equals(true))
                                    {
                                        conn.CreateCommand("UPDATE [Autotest].[dbo].[RMA_EsitiTestPreAnalisi] SET" +
                                        "[esitoTest] = '" + r.Cells[2].Value.ToString() + "'" +
                                        " WHERE ([Autotest].[dbo].[RMA_EsitiTestPreAnalisi].[idRMA] = '" + test.idRMA + "' AND [Autotest].[dbo].[RMA_EsitiTestPreAnalisi].[idProblema] = '" + idProblema + "' AND [Autotest].[dbo].[RMA_EsitiTestPreAnalisi].[idProgressivo] = '" + test.idProgressivo + "')", null);
                                    }
                                    else
                                    {
                                        conn.CreateCommand("DELETE FROM [Autotest].[dbo].[RMA_EsitiTestPreAnalisi] " +
                                        " WHERE ([Autotest].[dbo].[RMA_EsitiTestPreAnalisi].[idRMA] = '" + test.idRMA + "' AND [Autotest].[dbo].[RMA_EsitiTestPreAnalisi].[idProblema] = '" + idProblema + "' AND [Autotest].[dbo].[RMA_EsitiTestPreAnalisi].[idProgressivo] = '" + test.idProgressivo + "')", null);
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }
                else
                {
                    //ho aggiunto un nuovo problema tra i template del DB.
                    if (problema.idRMA == null || idProblema == 0)
                    {
                        problema.idRMA = "";
                        conn.CreateCommand("INSERT INTO [Autotest].[dbo].[RMA_ProblemaTemplateDB] " +
                        "([idRMA],[dataAggProb],[faseProduzione],[codParteControllo],[descParte],[difettoRiscontrato],[causaProblema],[gravitaProblema],[nomeResponsabile],[nomeTecnicoRalco],[faseIndividuazioneProblema],[noteProblema],[nomeResponsabile2],[nomeResponsabile3],[nomeResponsabile4],[nomeResponsabile5]) " +
                        "VALUES ('" + problema.idRMA + "','" + problema.dataAggProb.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "','" + problema.faseProduzione + "','" + problema.codParteControllo + "','" + problema.descParte + "','" + problema.rifFlowChart + "','" + problema.causaProblema + "','" + problema.gravitaProblema + "','" + problema.nomeResponsabile[0] + "','" + problema.nomeTecnicoRalco + "','" + problema.faseIndividuazioneProblema + "','" + problema.difRiscDaRalco + "','" + problema.nomeResponsabile[1] + "','" + problema.nomeResponsabile[2] + "','" + problema.nomeResponsabile[3] + "','" + problema.nomeResponsabile[4] + "')", null);
                    }
                    else
                    {
                        //aggiorno i dati del problema
                        conn.CreateCommand("UPDATE [Autotest].[dbo].[RMA_ProblemaTemplateDB] SET" +
                        "[idRMA] = '" + problema.idRMA + "'" +
                        ",[dataAggProb] = '" + problema.dataAggProb.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "'" +
                        ",[faseProduzione] = '" + problema.faseProduzione + "'" +
                        ",[codParteControllo] = '" + problema.codParteControllo + "'" +
                        ",[descParte] = '" + problema.descParte + "'" +
                        ",[difettoRiscontrato] = '" + problema.rifFlowChart + "'" +
                        ",[causaProblema] = '" + problema.causaProblema + "'" +
                        ",[gravitaProblema] = '" + problema.gravitaProblema + "'" +
                        ",[nomeResponsabile] = '" + problema.nomeResponsabile[0] + "'" +
                        ",[nomeTecnicoRalco] = '" + problema.nomeTecnicoRalco + "'" +
                        ",[faseIndividuazioneProblema] = '" + problema.faseIndividuazioneProblema + "'" +
                        ",[noteProblema] = '" + problema.difRiscDaRalco + "'" +
                        ",[nomeResponsabile2] = '" + problema.nomeResponsabile[1] + "'" +
                        ",[nomeResponsabile3] = '" + problema.nomeResponsabile[2] + "'" +
                        ",[nomeResponsabile4] = '" + problema.nomeResponsabile[3] + "'" +
                        ",[nomeResponsabile5] = '" + problema.nomeResponsabile[4] + "'" +
                        " WHERE ([Autotest].[dbo].[RMA_ProblemaTemplateDB].[idRMA] = '" + problema.idRMA + "' AND [Autotest].[dbo].[RMA_ProblemaTemplateDB].[idProblema] = '" + problema.idProblema + "')", null);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_okProblema_Click(object sender, EventArgs e)
        {
            try
            {
                updateVariabilePubblica();
                commitDB();
                if (isTemplateDBProblem)
                {
                    problema = null;//annullo la variabile pubblica perchè non devo aggiornare la lista dei problemi da usare nell'RMA nel caso di aggiunta al DB template
                }
                this.DialogResult = DialogResult.OK;
                this.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_cancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.DialogResult = DialogResult.Cancel;
                this.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void rb_codiceParte_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                cmb_codParteControllo.Enabled = rb_codiceParte.Checked;
                cmb_codParteControllo.Text = "";
                cmb_codParteControllo.DropDownStyle = ComboBoxStyle.DropDown;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cmb_codParteControllo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                cmb_codParteControllo_TextChanged(null, null);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cmb_codParteControllo_TextChanged(object sender, EventArgs e)
        {
            try
            {
                lbl_descParte.Text = "";
                lbl_descParte.Text = artListFromDB.Single(r => r.idArticolo.Trim().Equals(cmb_codParteControllo.Text.ToString().Trim())).descrizione;
            }
            catch (Exception ex)
            {
                //do nothing...
            }
        }

        private void dgv_testDaEseguire_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                Utils.DataGridView_CellContentClick(dgv_testDaEseguire, 1);
                if (dgv_testDaEseguire.CurrentCell.Value.Equals(true))
                    esitiTestListAdded.Add(new RMA_EsitiTestPreAnalisi(idRMA, dgv_testDaEseguire.CurrentRow.Cells[1].Value.ToString(), dgv_testDaEseguire.CurrentRow.Cells[2].Value.ToString()));
                else
                {
                    RMA_EsitiTestPreAnalisi tmp = new RMA_EsitiTestPreAnalisi(idRMA, dgv_testDaEseguire.CurrentRow.Cells[1].Value.ToString(), dgv_testDaEseguire.CurrentRow.Cells[2].Value.ToString());
                    bool found = false;
                    foreach (RMA_EsitiTestPreAnalisi t in esitiTestListAdded)
                        if (t.Equals(tmp))
                        {
                            found = true;
                            break;
                        }
                    if (found)
                        esitiTestListAdded.Remove(tmp);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
