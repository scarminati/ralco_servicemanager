﻿namespace Ralco_ServiceManager
{
    partial class Form_Responsabile
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Responsabile));
            this.cmb_NomeResponsabile = new System.Windows.Forms.ComboBox();
            this.lbl_nomeRespo = new System.Windows.Forms.Label();
            this.btn_AddResponsabile = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // cmb_NomeResponsabile
            // 
            this.cmb_NomeResponsabile.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmb_NomeResponsabile.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_NomeResponsabile.FormattingEnabled = true;
            this.cmb_NomeResponsabile.Location = new System.Drawing.Point(138, 19);
            this.cmb_NomeResponsabile.Name = "cmb_NomeResponsabile";
            this.cmb_NomeResponsabile.Size = new System.Drawing.Size(255, 21);
            this.cmb_NomeResponsabile.Sorted = true;
            this.cmb_NomeResponsabile.TabIndex = 2;
            this.cmb_NomeResponsabile.Tag = "";
            // 
            // lbl_nomeRespo
            // 
            this.lbl_nomeRespo.AutoSize = true;
            this.lbl_nomeRespo.Location = new System.Drawing.Point(11, 22);
            this.lbl_nomeRespo.Name = "lbl_nomeRespo";
            this.lbl_nomeRespo.Size = new System.Drawing.Size(102, 13);
            this.lbl_nomeRespo.TabIndex = 3;
            this.lbl_nomeRespo.Text = "Nome Responsabile";
            // 
            // btn_AddResponsabile
            // 
            this.btn_AddResponsabile.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_AddResponsabile.BackgroundImage")));
            this.btn_AddResponsabile.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_AddResponsabile.Location = new System.Drawing.Point(423, 19);
            this.btn_AddResponsabile.Name = "btn_AddResponsabile";
            this.btn_AddResponsabile.Size = new System.Drawing.Size(25, 25);
            this.btn_AddResponsabile.TabIndex = 7;
            this.btn_AddResponsabile.UseVisualStyleBackColor = true;
            this.btn_AddResponsabile.Click += new System.EventHandler(this.btn_AddResponsabile_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cmb_NomeResponsabile);
            this.groupBox1.Controls.Add(this.btn_AddResponsabile);
            this.groupBox1.Controls.Add(this.lbl_nomeRespo);
            this.groupBox1.Location = new System.Drawing.Point(11, 7);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(469, 56);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            // 
            // Form_Responsabile
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(488, 75);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(504, 113);
            this.MinimumSize = new System.Drawing.Size(504, 113);
            this.Name = "Form_Responsabile";
            this.Text = "Aggiungi Responsabile";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox cmb_NomeResponsabile;
        private System.Windows.Forms.Label lbl_nomeRespo;
        private System.Windows.Forms.Button btn_AddResponsabile;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}