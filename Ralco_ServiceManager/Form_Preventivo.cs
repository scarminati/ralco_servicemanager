﻿#define USEPANTHERA
//#undef USEPANTHERA
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;

namespace Ralco_ServiceManager
{
    public partial class Form_Preventivo : Form
    {
        public RMA_Preventivo preventivo;
        private SQLConnector conn;
        private List<RMA_Collimatore> collList;
        private List<RMA_Parte> partList;
        private RMA_FaseProduzione faseProd;
        private List<RMA_Articolo> artList;
        private List<RMA_Articolo> artRemovedFromList;
        private List<RMA_Riparazione> ripList;
        private RMA_ClientiRalco cliente;
        private bool isNewPrev = true;
        private string idRMA;
        private List<RMA_Articolo> artListDB;
        private const decimal COEFF_MOLT = 2.5m;
        private RMA_Articolo costoPratica = new RMA_Articolo("CP","Costo Pratica",60.0m,1);
        /*Costo pratica : 60 euro fissi per una pratica contenente fino a 6 collimatori, aggiungo poi 20 euro ad ogni collimatore in più
         ES : costo pratica 1 collimatore = 60 euro
              costo pratica 3 collimatori = 60 euro
              costo pratica 6 collimatori = 60 euro
              costo pratica 7 collimatori = 60 euro + 20 euro = 80 euro
              costo pratica 8 collimatori = 60 euro + 20 euro + 20 euro = 100 euro*/
        private RMA_Articolo costoManodoperaCalibrazione = new RMA_Articolo("MC", "Manodopera e Calibrazione", 0.0m,1);//Il costo della manodopera è la sommatoria dei minuti spesi per la riparazione
        private RMA_GaranziaRalco garanziaRalco;
        /*
         prevConditions = [true, false, false] => ADD
         prevConditions = [false, true, false] => EDIT
         prevConditions = [false, false, true] => CLONE*/
        public Form_Preventivo(string idRMA, RMA_Preventivo prev_FromForm, RMA_ClientiRalco cliente, List<RMA_Collimatore> collList, List<RMA_Riparazione> ripList, RMA_FaseProduzione faseProd,  RMA_GaranziaRalco garanziaRalco, params bool[] prevConditions)
        {
            try
            {
                InitializeComponent();
                conn = new SQLConnector();
                setWidgetDescriptions();
                artList = new List<RMA_Articolo>();
                this.garanziaRalco = garanziaRalco;
                artRemovedFromList = new List<RMA_Articolo>();
                artListDB = new List<RMA_Articolo>();
                artListDB = getIdArticoli();
                this.idRMA = idRMA;
                this.collList = collList;
                this.faseProd = faseProd;
                this.ripList = ripList;
                if (cliente != null)
                    this.cliente = new RMA_ClientiRalco(cliente);
                else
                    this.cliente = new RMA_ClientiRalco();
                if (prev_FromForm != null)
                {
                    this.preventivo = new RMA_Preventivo(prev_FromForm);
                }
                else
                {
                    this.preventivo = new RMA_Preventivo();
                    addArticoliDefault();
                }
                cmb_statoPrev.SelectedIndex = 0;
                //nuovo preventivo
                if (prevConditions[0].Equals(true))
                    calculateIDPreventivo(0);
                else
                {
                    //edit preventivo
                    if (prevConditions[1].Equals(true))
                        calculateIDPreventivo(1);
                    //clone preventivo
                    else if (prevConditions[2].Equals(true))
                        calculateIDPreventivo(2);
                }
                btn_RefreshPreventivo_Click(null, null);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /*
         prevConditions = [true, false, false] => ADD
         prevConditions = [false, true, false] => EDIT
         prevConditions = [false, false, true] => CLONE*/
        public Form_Preventivo(string idRMA, RMA_Preventivo prev_FromForm, RMA_ClientiRalco cliente, List<RMA_Parte> partList, List<RMA_Riparazione> ripList, RMA_FaseProduzione faseProd, RMA_GaranziaRalco garanziaRalco, params bool[] prevConditions)
        {
            try
            {
                InitializeComponent();
                conn = new SQLConnector();
                setWidgetDescriptions();
                artList = new List<RMA_Articolo>();
                this.garanziaRalco = garanziaRalco;
                artRemovedFromList = new List<RMA_Articolo>();
                artListDB = new List<RMA_Articolo>();
                artListDB = getIdArticoli();
                this.idRMA = idRMA;
                this.partList = partList;
                this.faseProd = faseProd;
                this.ripList = ripList;
                if (cliente != null)
                    this.cliente = new RMA_ClientiRalco(cliente);
                else
                    this.cliente = new RMA_ClientiRalco();
                if (prev_FromForm != null)
                {
                    this.preventivo = new RMA_Preventivo(prev_FromForm);
                }
                else
                {
                    this.preventivo = new RMA_Preventivo();
                    addArticoliDefault();
                }
                cmb_statoPrev.SelectedIndex = 0;
                //nuovo preventivo
                if (prevConditions[0].Equals(true))
                    calculateIDPreventivo(0);
                else
                {
                    //edit preventivo
                    if (prevConditions[1].Equals(true))
                        calculateIDPreventivo(1);
                    //clone preventivo
                    else if (prevConditions[2].Equals(true))
                        calculateIDPreventivo(2);
                }
                btn_RefreshPreventivo_Click(null, null);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public Form_Preventivo(string idRMA, List<RMA_Preventivo> estimates)
        {
            try
            {
                this.idRMA = idRMA;
                isNewPrev = true;
                conn = new SQLConnector();
                foreach (RMA_Preventivo p in estimates)
                {
                    preventivo = new RMA_Preventivo(p);
                    artList = new List<RMA_Articolo>();
                    artList = (List<RMA_Articolo>)conn.CreateCommand("SELECT * FROM [Autotest].[dbo].[RMA_Articoli_Preventivo] WHERE [Autotest].[dbo].[RMA_Articoli_Preventivo].[idRMA] = '" + preventivo.idRMA + "' AND [Autotest].[dbo].[RMA_Articoli_Preventivo].[numPreventivo] = " + preventivo.numPreventivo + " AND [Autotest].[dbo].[RMA_Articoli_Preventivo].[revPreventivo] = '" + preventivo.revisionePrev + "'", artList);
                    foreach (RMA_Articolo a in artList)
                        a.idRMA = null;
                    preventivo.idRMA = null;
                    commitDB();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void setWidgetDescriptions()
        {
            try
            {
                #region Form
                this.Text = Utils.resourcemanager.GetString("Form_Preventivo");
                #endregion
                #region TooltipsPerBottoni
                ToolTip tooltip = new System.Windows.Forms.ToolTip();
                tooltip.SetToolTip(btn_OK, Utils.resourcemanager.GetString("btn_OK"));
                tooltip.SetToolTip(btn_cancel, Utils.resourcemanager.GetString("btn_cancel"));
                btn_RefreshPreventivo.ToolTipText = Utils.resourcemanager.GetString("btn_RefreshPreventivo");
                btn_importRiparazioni.ToolTipText = Utils.resourcemanager.GetString("btn_importRiparazioni");
                btn_AddArticolo.ToolTipText = Utils.resourcemanager.GetString("btn_AddArticolo");
                btn_DeleteArticolo.ToolTipText = Utils.resourcemanager.GetString("btn_DeleteArticolo");
                btn_AddExtraCosts.ToolTipText = Utils.resourcemanager.GetString("btn_AddExtraCosts");
                #endregion
                #region Label
                lbl_IDPreventivo.Text = Utils.resourcemanager.GetString("lbl_IDPreventivo");
                lbl_revisione.Text = Utils.resourcemanager.GetString("lbl_revisione");
                lbl_stato.Text = Utils.resourcemanager.GetString("lbl_stato");
                lbl_totConsuntivo.Text = Utils.resourcemanager.GetString("lbl_totConsuntivo");
                #endregion
                #region Groupbox
                gb_InfoGenerali.Text = Utils.resourcemanager.GetString("gb_InfoGenerali");
                #endregion
                #region Radio Button
                rb_totPreventivo.Text = Utils.resourcemanager.GetString("rb_totPreventivo");
                #endregion
                #region DataGridViewColumns
                dgv_ListaArticoliPreventivo.Columns["idArticolo"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaArticoliPreventivo_idArticolo");
                dgv_ListaArticoliPreventivo.Columns["qta"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaArticoliPreventivo_qta");
                dgv_ListaArticoliPreventivo.Columns["descArticolo"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaArticoliPreventivo_descArticolo");
                dgv_ListaArticoliPreventivo.Columns["prezzo"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaArticoliPreventivo_prezzo");
                #endregion
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private List<RMA_Articolo> getIdArticoli()
        {
            try
            {
                List<RMA_Articolo> artListFromDB = new List<RMA_Articolo>();
                artListFromDB = (List<RMA_Articolo>)conn.CreateCommand("SELECT DISTINCT ID_AZIENDA, ID_ARTICOLO, UPPER (DESCRIZIONE), ID_SCELTA, ID_SCELTA FROM [archivi].[dbo].[vistaListaArticoliPanth] WHERE ID_LINGUA = 'it' ORDER BY ID_ARTICOLO", artListDB);
                return (artListFromDB);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }

        private void dgv_ListaArticoliPreventivo_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgv_ListaArticoliPreventivo.CurrentCell.ColumnIndex.Equals(0))
                {
                    if (dgv_ListaArticoliPreventivo.CurrentCell.Value == (null) || dgv_ListaArticoliPreventivo.CurrentCell.Value.Equals(false))
                        dgv_ListaArticoliPreventivo.CurrentCell.Value = true;
                    else
                        dgv_ListaArticoliPreventivo.CurrentCell.Value = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void calculateIDPreventivo(int main)
        {
            try
            {
                //preventivo = new RMA_Preventivo();
                switch (main)
                {
                    case 0://add preventivo
                        {
                            int numPreventivo = 0;
                            numPreventivo = (int)conn.CreateCommand("SELECT TOP 1 numPreventivo FROM [Autotest].[dbo].[RMA_Preventivo] WHERE [Autotest].[dbo].[RMA_Preventivo].[idRMA] = '" + this.idRMA + "' ORDER BY numPreventivo DESC", numPreventivo);
                            if (numPreventivo == 0)
                                lbl_IDPrev.Text = "001";
                            else
                                lbl_IDPrev.Text = (numPreventivo + 1).ToString();                                
                            lbl_RevPrev.Text = "0";
                        }
                        break;
                    case 1://edit preventivo
                        {
                            refreshFormPreventivo();
                            if (!preventivo.stato.Equals("Approvato") && !preventivo.stato.Equals("Respinto"))
                                isNewPrev = false;
                            else
                                isNewPrev = true;
                            if (isNewPrev.Equals(true))
                            {
                                if (preventivo.revisionePrev.Equals("0"))
                                    preventivo.revisionePrev = "A";
                                else
                                {
                                    char c = Convert.ToChar(preventivo.revisionePrev);
                                    // convert char to ascii
                                    int ascii = (int)c;
                                    // get the next ascii
                                    int nextAscii = ascii + 1;
                                    // convert ascii to char
                                    char nextChar = (char)nextAscii;
                                    preventivo.revisionePrev = nextChar.ToString();
                                }
                                lbl_RevPrev.Text = preventivo.revisionePrev;
                                if (artList != null)
                                {
                                    foreach (RMA_Articolo a in artList)
                                        a.idRMA = null;
                                }
                            }
                        }
                        break;
                    case 2://clone preventivo
                        {
                            refreshFormPreventivo();
                            preventivo.numPreventivo++;
                            lbl_IDPrev.Text = (preventivo.numPreventivo).ToString();
                            lbl_RevPrev.Text = "0";
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void refreshFormPreventivo()
        {
            try
            {
                lbl_IDPrev.Text = preventivo.numPreventivo.ToString();
                lbl_RevPrev.Text = preventivo.revisionePrev;
                cmb_statoPrev.SelectedItem = preventivo.stato;
                tb_TotalePrev.Text = preventivo.totale.ToString();
                lbl_TotaleCostoRalco.Text = preventivo.totaleCostoRalco.ToString();
                //Tab Analisi del Problema
                artList.Clear();
                artList = (List<RMA_Articolo>)conn.CreateCommand("SELECT * FROM [Autotest].[dbo].[RMA_Articoli_Preventivo] WHERE [Autotest].[dbo].[RMA_Articoli_Preventivo].[idRMA] = '" + preventivo.idRMA + "' AND [Autotest].[dbo].[RMA_Articoli_Preventivo].[numPreventivo] = " + preventivo.numPreventivo + " AND [Autotest].[dbo].[RMA_Articoli_Preventivo].[revPreventivo] = '" + preventivo.revisionePrev + "'", artList);
                //potrei ottenere null dalla query nel caso in cui un RMA non abbia associato alcun problema. Ma in questo caso devo re-inizializzare la lista dei problemi.
                if (preventivo.idArticolo == null)
                    preventivo.idArticolo = new List<string>();
                if (artList == null)
                    addArticoliDefault();
                //dato che uso List<RMA_Articolo> per la selezione degli articoli e degli articoli nel preventivo, devo valorizzare i campi esclusi dalla query
                else
                {
                    foreach (RMA_Articolo a in artList)
                    {
                        a.idRMA = preventivo.idRMA;
                        a.numPreventivo = preventivo.numPreventivo;
                        a.revisionePrev = preventivo.revisionePrev;
                        preventivo.idArticolo.Add(a.idArticolo);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void addArticoliDefault()
        {
            try
            {
                artList = new List<RMA_Articolo>();
                if (collList != null && collList.Count >= 6)//se ho una pratica da più di 6 collimatori, ricalcolo il costo unitario della pratica (di default settato a 60 euro)
                {
                    int delta = collList.Count - 6;
                    costoPratica.prezzoUnitario = costoPratica.prezzoUnitario + (20.0m * delta);
                }
                else if (partList != null && partList.Count >= 6)//se ho una pratica da più di 6 collimatori, ricalcolo il costo unitario della pratica (di default settato a 60 euro)
                {
                    int delta = partList.Count - 6;
                    costoPratica.prezzoUnitario = costoPratica.prezzoUnitario + (20.0m * delta);
                }
                costoPratica.prezzoUnitario = costoPratica.prezzoUnitario / COEFF_MOLT;
                artList.Add(costoPratica);
                foreach (int m in faseProd.minFaseProd)
                    costoManodoperaCalibrazione.prezzoUnitario = (costoManodoperaCalibrazione.prezzoUnitario + m) * 1.0m;
                costoManodoperaCalibrazione.prezzoUnitario = costoManodoperaCalibrazione.prezzoUnitario / COEFF_MOLT; //perchè poi moltiplico per COEFF_MOLT, ma le ore devono essere conteggiate come 1 euro al minuto
                artList.Add(costoManodoperaCalibrazione);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_RefreshPreventivo_Click(object sender, EventArgs e)
        {
            try
            {
                dgv_ListaArticoliPreventivo.Rows.Clear();
                tb_TotalePrev.Text = "";
                lbl_TotaleCostoRalco.Text = "";
                decimal prezzoTotalePreventivo = 0;
                decimal prezzoTotaleCostoRalco = 0;
                if (artList != null)
                {
                    foreach (RMA_Articolo a in artList)
                    {
                        if (a.descrizione.Equals("Costo Pratica"))
                            a.descrizione = a.descrizione + " */* Dossier Cost";
                        else if (a.descrizione.Equals("Manodopera e Calibrazione"))
                            a.descrizione = a.descrizione + " */* Labor and Calibration";
                        else
                            a.addArticleEnglishDescription();
                        //rifaccio il calcolo solo se necessario...
                        if (a.prezzoUnitario.Equals(0))
                        {
                            a.calculatePrice(a);
                        }
                        dgv_ListaArticoliPreventivo.Rows.Insert(0, false, a.idArticolo, a.quantita, a.descrizione, a.prezzoUnitario * a.quantita * COEFF_MOLT);
                        prezzoTotaleCostoRalco = prezzoTotaleCostoRalco + (a.prezzoUnitario * a.quantita);
                        prezzoTotalePreventivo = prezzoTotalePreventivo + (a.prezzoUnitario * a.quantita * COEFF_MOLT);
                    }
                    if (garanziaRalco.statoGaranzia.Equals("In Garanzia"))
                        prezzoTotalePreventivo = 0;
                    dgv_ListaArticoliPreventivo.Refresh();
                    lbl_TotaleCostoRalco.Text = prezzoTotaleCostoRalco.ToString();
                    //se ho aggiornato il totale del preventivo manualmente devo far comparire quel valore; altrimenti quello calcolato automaticamente
                    //cerco la revisione del preventivo nel DB; se non la trovo, significa che è uova, e quindi il prezzo va ricalcolato automaticamente
                    string revPreventivoInDB = "";
                    revPreventivoInDB = (string)conn.CreateCommand("SELECT TOP 1 revPreventivo FROM [Autotest].[dbo].[RMA_Preventivo] WHERE [Autotest].[dbo].[RMA_Preventivo].[idRMA] = '" + this.idRMA + "' AND [Autotest].[dbo].[RMA_Preventivo].[numPreventivo] = '" + lbl_IDPrev.Text + "' AND [Autotest].[dbo].[RMA_Preventivo].[revPreventivo] = '" + lbl_RevPrev.Text + "'", revPreventivoInDB);
                    if (preventivo.idRMA != null && revPreventivoInDB!="")
                        if (!prezzoTotalePreventivo.ToString().Equals(preventivo.totale))
                            tb_TotalePrev.Text = preventivo.totale.ToString();
                        else
                            tb_TotalePrev.Text = prezzoTotalePreventivo.ToString();
                    else
                        tb_TotalePrev.Text = prezzoTotalePreventivo.ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_importRiparazioni_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (RMA_Riparazione r in ripList)
                {
                    artList.Add(artListDB.Single(a => a.idArticolo.Equals(r.idParte.ToString())));
                    artList[artList.Count - 1].quantita = r.quantita;
                    if (artList[artList.Count - 1].prezzoUnitario.Equals(0))
                        artList[artList.Count - 1].calculatePrice(artList[artList.Count - 1]);
                }
                btn_RefreshPreventivo_Click(sender, e);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_AddArticolo_Click(object sender, EventArgs e)
        {
            try
            {
                using (var form = new Form_Articolo(artListDB))
                {
                    var result = form.ShowDialog();
                    if (result == DialogResult.OK)
                    {
                        artList.Add(form.articolo);
                    }
                }
                btn_RefreshPreventivo_Click(sender, e);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void btn_DeleteArticolo_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow row in dgv_ListaArticoliPreventivo.Rows)
                {
                    if (row.Cells["chk_Art"].Value != null && row.Cells["chk_Art"].Value.Equals(true))
                    {
                        var itemToRemove = artList.Single(r => r.idArticolo.Equals(row.Cells.GetCellValueFromColumnHeader("ID Articolo")));
                        if (itemToRemove != null)
                        {
                            var result = MessageBox.Show("L'elemento selezionato verrà rimosso definitivamente. Sei sicuro di voler procedere?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (result == DialogResult.Yes)
                            {
                                artRemovedFromList.Add(itemToRemove);
                                artList.Remove(itemToRemove);
                                //se sono nel caso di modifica dell'RMA, rimuovo l'elemento anche dal DB;
                                if (preventivo != null)
                                {
                                    try
                                    {
                                        conn.CreateCommand("DELETE FROM [Autotest].[dbo].[RMA_Articoli_Preventivo] " +
                                        "WHERE [Autotest].[dbo].[RMA_Articoli_Preventivo].[idRMA] = '" + preventivo.idRMA + "' AND [Autotest].[dbo].[RMA_Articoli_Preventivo].[numPreventivo] = '" + preventivo.numPreventivo + "' AND [Autotest].[dbo].[RMA_Articoli_Preventivo].[revPreventivo] = '" + preventivo.revisionePrev + "' AND [Autotest].[dbo].[RMA_Articoli_Preventivo].[idArticolo] = '" + itemToRemove.idArticolo + "'", null);
                                    }
                                    catch (Exception ex)
                                    {
                                        MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    }
                                }
                            }
                        }
                    }
                }
                btn_RefreshPreventivo_Click(sender, e);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_Extra_Click(object sender, EventArgs e)
        {
            try
            {
                using (var form = new Form_CostiExtra())
                {
                    var result = form.ShowDialog();
                    if (result == DialogResult.OK)
                    {
                        if (!form.costiExtraList.Count.Equals(0))
                        {
                            RMA_Articolo articolo;
                            foreach (RMA_CostiExtra c in form.costiExtraList)
                            {
                                articolo = new RMA_Articolo("EXTRA" + c.idExtra, c.descrizione, c.prezzoUnitario, c.quantita);
                                preventivo.totale = preventivo.totale + articolo.prezzoUnitario;
                                articolo.prezzoUnitario = articolo.prezzoUnitario / COEFF_MOLT;
                                artList.Add(articolo);
                            }
                        }
                    }
                }
                btn_RefreshPreventivo_Click(sender, e);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void rb_TotPreventivo_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                tb_TotalePrev.ReadOnly = !rb_totPreventivo.Checked;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        ////refresh dei campi dell'istanza pubblica preventivo
        private void updateVariabilePubblica()
        {
            try
            {

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            preventivo.nomeTecnicoRalco = Globals.user.getFullUserName();
            preventivo.idRMA = this.idRMA;
            preventivo.numPreventivo = Convert.ToInt32(lbl_IDPrev.Text);
            preventivo.revisionePrev = lbl_RevPrev.Text;
            preventivo.dataAggPrev = DateTime.Now;
            preventivo.stato = cmb_statoPrev.SelectedItem.ToString();
            preventivo.totale = Convert.ToDecimal(tb_TotalePrev.Text);
            preventivo.totaleCostoRalco = Convert.ToDecimal(lbl_TotaleCostoRalco.Text);
            preventivo.idArticolo = new List<string>();
            foreach (RMA_Articolo a in artList)
            {
                preventivo.idArticolo.Add(a.idArticolo);
            }
        }

        private void commitDB()
        {
            try
            {
                foreach (RMA_Articolo a in artList)
                {
                    //ho aggiunto un articolo al preventivo
                    if (a.idRMA == null)
                    {
                        a.idRMA = this.idRMA;
                        a.numPreventivo = preventivo.numPreventivo;
                        a.revisionePrev = preventivo.revisionePrev;
                        conn.CreateCommand("INSERT INTO [Autotest].[dbo].[RMA_Articoli_Preventivo] " +
                        "([idRMA],[numPreventivo],[revPreventivo],[idArticolo],[quantita],[descArticolo],[prezzoUnitario]) " +
                        "VALUES ('" + a.idRMA + "','" + a.numPreventivo + "','" + a.revisionePrev + "','" + a.idArticolo + "'," + a.quantita + ",'" + a.descrizione + "'," + a.prezzoUnitario.ToString(System.Globalization.CultureInfo.InvariantCulture) + ")", null);
                    }
                    else
                    {
                        conn.CreateCommand("UPDATE [Autotest].[dbo].[RMA_Articoli_Preventivo] SET " +
                        "[idRMA] = '" + this.idRMA + "'" +
                        ",[numPreventivo] = '" + a.numPreventivo + "'" +
                        ",[revPreventivo] = '" + a.revisionePrev + "'" +
                        ",[idArticolo] = '" + a.idArticolo + "'" +
                        ",[quantita] = " + a.quantita + "" +
                        ",[descArticolo] = '" + a.descrizione + "'" +
                        ",[prezzoUnitario]=  " + a.prezzoUnitario.ToString(System.Globalization.CultureInfo.InvariantCulture) + " " +
                        "WHERE ([Autotest].[dbo].[RMA_Articoli_Preventivo].[idRMA] = '" + this.idRMA + "' AND [Autotest].[dbo].[RMA_Articoli_Preventivo].[numPreventivo] = '" + a.numPreventivo + "'  AND [Autotest].[dbo].[RMA_Articoli_Preventivo].[revPreventivo] = '" + a.revisionePrev + "' AND [Autotest].[dbo].[RMA_Articoli_Preventivo].[idArticolo] = '" + a.idArticolo + "')", null);
                    }
#if USEPANTHERA
                    if (preventivo.stato.Equals("Approvato"))
                    {
                        if (!a.idArticolo.Equals("CP") && !a.idArticolo.Equals("MC") && !a.idArticolo.Contains("Extra"))
                        {
                            //inserimento dei dati nella vista RMA_RIG_PREV di Panthera solo dopo che il preventivo è stato approvato
                            conn.CreateCommand("INSERT INTO [Autotest].[dbo].[RMA_RIG_PREV] " +
                            "([ID_AZIENDA],[ID_RMA],[ID_MATERIALE],[QUANTITA],[NOTE],[PREZZO]) " +
                            "VALUES ('001','" + a.idRMA.Replace('/', '_') + "','" + a.idArticolo + "','" + a.quantita + "','" + a.descrizione + "'," + a.prezzoUnitario.ToString(System.Globalization.CultureInfo.InvariantCulture) + ")", null);
                        }
                    }
#endif
                }
                //sono nel caso di nuovo preventivo o di nuova revisione
                if (isNewPrev.Equals(true))
                {
                    //Console.WriteLine(preventivo.idRMA);
                    conn.CreateCommand("INSERT INTO [Autotest].[dbo].[RMA_Preventivo] " +
                    "([idRMA],[numPreventivo],[revPreventivo],[dataAggPrev],[stato],[totale],[totaleCostoRalco],[nomeTecnicoRalco]) " +
                    "VALUES ('" + this.idRMA + "','" + preventivo.numPreventivo + "','" + preventivo.revisionePrev + "','" + preventivo.dataAggPrev.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "','" + preventivo.stato + "'," + preventivo.totale.ToString(System.Globalization.CultureInfo.InvariantCulture) + "," + preventivo.totaleCostoRalco.ToString(System.Globalization.CultureInfo.InvariantCulture) + ",'" + Globals.user.getFullUserName() + "')", null);
                }
                else
                {
                    //Console.WriteLine(preventivo.idRMA);
                    conn.CreateCommand("UPDATE [Autotest].[dbo].[RMA_Preventivo] SET " +
                    "[idRMA] = '" + this.idRMA + "'" +
                    ",[numPreventivo] = '" + preventivo.numPreventivo + "'" +
                    ",[revPreventivo] = '" + preventivo.revisionePrev + "'" +
                    ",[dataAggPrev] = '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "'" +
                    ",[stato] = '" + cmb_statoPrev.SelectedItem + "'" +
                    ",[totale] = " + Convert.ToDecimal(tb_TotalePrev.Text).ToString(System.Globalization.CultureInfo.InvariantCulture) + "" +
                    ",[totaleCostoRalco] = " + preventivo.totaleCostoRalco.ToString(System.Globalization.CultureInfo.InvariantCulture) + "" +
                    ",[nomeTecnicoRalco] = '" + Globals.user.getFullUserName() + "' " +
                    "WHERE ([Autotest].[dbo].[RMA_Preventivo].[idRMA] = '" + this.idRMA + "' AND [Autotest].[dbo].[RMA_Preventivo].[numPreventivo] = '" + preventivo.numPreventivo + "'  AND [Autotest].[dbo].[RMA_Preventivo].[revPreventivo] = '" + preventivo.revisionePrev + "')", null);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_okPreventivo_Click(object sender, EventArgs e)
        {
            try
            {
                updateVariabilePubblica();
                commitDB();
                this.DialogResult = DialogResult.OK;
                this.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_cancelPreventivo_Click(object sender, EventArgs e)
        {
            try
            {
                this.DialogResult = DialogResult.Cancel;
                this.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

    }
}
