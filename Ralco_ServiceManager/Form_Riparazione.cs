﻿#define USEPANTHERA
//#undef USEPANTHERA
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ralco_ServiceManager
{
    public partial class Form_Riparazione : Form
    {
        public RMA_Riparazione riparazione { get; set; }
        private SQLConnector conn;
        private List<RMA_Articolo> artListDB;
        private string idRMA;
        private bool isTemplateDB;
        public Form_Riparazione(string idRMA, bool isTemplateDB, params RMA_Riparazione[] rip_FromForm)
        {
            try
            {
                InitializeComponent();
                conn = new SQLConnector();
                this.isTemplateDB = isTemplateDB;
                setWidgetDescriptions();
                if (!isTemplateDB)
                    this.idRMA = idRMA;
                artListDB = getIdArticoli();
                setValoriCombobox();
                setCmbArticoli(artListDB);
                if (Globals.user.credenziale.Equals("NonConfExt"))
                    cmb_statoRiparazione.SelectedIndex = 1;
                else
                    cmb_statoRiparazione.SelectedIndex = 0;
                if (rip_FromForm[0] != null)
                {
                    riparazione = new RMA_Riparazione(rip_FromForm[0]);
                    fillForm();
                }
                else
                {
                    riparazione = new RMA_Riparazione();
                    cmb_parteSostituita.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public Form_Riparazione(string idRMA, List<RMA_Riparazione> repairments)
        {
            try
            {
                InitializeComponent();
                this.idRMA = idRMA;
                conn = new SQLConnector();
                foreach (RMA_Riparazione r in repairments)
                {
                    riparazione = new RMA_Riparazione(r);
                    riparazione.idRMA = null;
                    commitDB();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void setCmbArticoli(List<RMA_Articolo> artList)
        {
            try
            {

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            cmb_parteSostituita.Items.Clear();
            foreach (RMA_Articolo a in artList)
                cmb_parteSostituita.Items.Add(a.idArticolo);
        }

        private void setValoriCombobox()
        {
            try
            {
                //coombobox cmb_descRiparazione
                cmb_descRiparazione.Items.Clear();
                List<String> descRipList = new List<String>();
                descRipList = Utils.getAddedComboboxValues(cmb_descRiparazione);
                foreach (String s in descRipList)
                    cmb_descRiparazione.Items.Add(s);
                //coombobox cmb_statoRiparazione
                cmb_statoRiparazione.Items.Clear();
                List<String> descStatoRipList = new List<String>();
                descStatoRipList = Utils.getAddedComboboxValues(cmb_statoRiparazione);
                foreach (String s in descStatoRipList)
                    cmb_statoRiparazione.Items.Add(s);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cmb_parteSostituita_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                tb_descParte.Text = "";
                tb_descParte.Text = artListDB.Single(r => r.idArticolo.Equals(cmb_parteSostituita.SelectedItem.ToString())).descrizione.Replace("'", "");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private List<RMA_Articolo> getIdArticoli()
        {
            try
            {
                List<RMA_Articolo> artListFromDB = new List<RMA_Articolo>();
                artListFromDB = (List<RMA_Articolo>)conn.CreateCommand("SELECT DISTINCT ID_AZIENDA, ID_ARTICOLO, UPPER (DESCRIZIONE), ID_SCELTA, ID_SCELTA FROM [archivi].[dbo].[vistaListaArticoliPanth] WHERE ID_LINGUA = 'it' ORDER BY ID_ARTICOLO", artListFromDB);
                return (artListFromDB);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }

        private void setWidgetDescriptions()
        {
            try
            {
                #region Form
                this.Text = Utils.resourcemanager.GetString("Form_Riparazione");
                #endregion
                #region TooltipsPerBottoni
                ToolTip tooltip = new System.Windows.Forms.ToolTip();
                tooltip.SetToolTip(btn_OK, Utils.resourcemanager.GetString("btn_OK"));
                tooltip.SetToolTip(btn_cancel, Utils.resourcemanager.GetString("btn_cancel"));
                #endregion
                #region Label
                lbl_descRip.Text = Utils.resourcemanager.GetString("lbl_descRip");
                lbl_parteSost.Text = Utils.resourcemanager.GetString("lbl_parteSost");
                lbl_qta.Text = Utils.resourcemanager.GetString("lbl_qta");
                lbl_statoRip.Text = Utils.resourcemanager.GetString("lbl_statoRip");
                lbl_note.Text = Utils.resourcemanager.GetString("lbl_note");
                #endregion
                #region Groupbox
                gb_dettRip.Text = Utils.resourcemanager.GetString("gb_dettRip");
                #endregion
                #region Radio Button
                rb_descrizioneParte.Text = Utils.resourcemanager.GetString("rb_descrizioneParte");
                #endregion
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void fillForm()
        {
            try
            {
                cmb_descRiparazione.SelectedIndex = cmb_descRiparazione.FindString(riparazione.descRip);
                if (cmb_parteSostituita.Items.Contains(riparazione.idParte))
                    cmb_parteSostituita.SelectedItem = riparazione.idParte;
                nud_Quantita.Value = riparazione.quantita;
                tb_descParte.Text = riparazione.descParte;
                cmb_statoRiparazione.SelectedIndex = cmb_statoRiparazione.FindString(riparazione.statoRip);
                tb_noteRiparazione.Text = riparazione.noteRip;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //assegna i valori dei vari campi del form agli attributi del problema
        private void updateVariabilePubblica()
        {
            try
            {
                riparazione.nomeTecnicoRalco = Globals.user.getFullUserName();
                riparazione.dataAggRip = DateTime.Now;
                if (cmb_descRiparazione.SelectedItem != null)
                    riparazione.descRip = cmb_descRiparazione.SelectedItem.ToString();
                riparazione.quantita = Convert.ToInt32(nud_Quantita.Value);
                if (cmb_parteSostituita.SelectedItem != null)
                    riparazione.idParte = cmb_parteSostituita.SelectedItem.ToString().ToUpper();
                riparazione.descParte = tb_descParte.Text;
                if (cmb_statoRiparazione.SelectedItem != null)
                    riparazione.statoRip = cmb_statoRiparazione.SelectedItem.ToString();
                riparazione.noteRip = Utils.parseUselessChars(tb_noteRiparazione.Text);
                RMA_Articolo art = artListDB.Single(r => r.idArticolo.Equals(cmb_parteSostituita.SelectedItem.ToString()));
                art.calculatePrice(art);
                riparazione.prezzoUnitario = art.prezzoUnitario;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void commitDB()
        {
            try
            {
                if (!isTemplateDB)
                {
                    //ho aggiunto una nuova riparazione
                    if (riparazione.idRMA == null || riparazione.idRiparazione == 0 || riparazione.idRMA == "")
                    {
                        riparazione.idRMA = idRMA;
                        conn.CreateCommand("INSERT INTO [Autotest].[dbo].[RMA_Riparazione] " +
                        "([idRMA],[dataAggRip],[descRip],[idParte],[descParte],[quantita],[statoRip],[noteRip],[nomeTecnicoRalco],[prezzoUnitario]) " +
                        "VALUES ('" + riparazione.idRMA + "','" + riparazione.dataAggRip.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "','" + riparazione.descRip + "','" + riparazione.idParte + "','" + riparazione.descParte + "','" + riparazione.quantita + "','" + riparazione.statoRip + "','" + riparazione.noteRip + "','" + riparazione.nomeTecnicoRalco + "'," + riparazione.prezzoUnitario.ToString(System.Globalization.CultureInfo.InvariantCulture) + ")", null);
                        int idRiparazione = 0;
                        idRiparazione = (int)conn.CreateCommand("SELECT TOP(1)[idRiparazione] FROM[AutoTest].[dbo].[RMA_Riparazione] ORDER BY idRiparazione DESC", idRiparazione);
                        riparazione.idRiparazione = idRiparazione;
                        #if USEPANTHERA
                        //inserimento dei dati nella vista RMA_RIG_MAT di Panthera
                        conn.CreateCommand("INSERT INTO [Autotest].[dbo].[RMA_RIG_MAT] " +
                        "([ID_AZIENDA],[ID_RMA],[ID_MATERIALE],[QUANTITA],[NOTE],[PREZZO]) " +
                        "VALUES ('001','" + riparazione.idRMA.Replace('/', '_') + "','" + riparazione.idParte + "','" + riparazione.quantita + "','" + riparazione.noteRip + "'," + riparazione.prezzoUnitario.ToString(System.Globalization.CultureInfo.InvariantCulture) + ")", null);
                        #endif
                    }
                    else
                    {
                        //aggiorno i dati della riparazione
                        conn.CreateCommand("UPDATE [Autotest].[dbo].[RMA_Riparazione] SET" +
                        "[idRMA] = '" + riparazione.idRMA + "'" +
                        ",[dataAggRip] = '" + riparazione.dataAggRip.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "'" +
                        ",[descRip] = '" + riparazione.descRip + "'" +
                        ",[idParte] = '" + riparazione.idParte + "'" +
                        ",[descParte] = '" + riparazione.descParte + "'" +
                        ",[quantita] = '" + riparazione.quantita + "'" +
                        ",[statoRip] = '" + riparazione.statoRip + "'" +
                        ",[noteRip] = '" + riparazione.noteRip + "'" +
                        ",[nomeTecnicoRalco] = '" + riparazione.nomeTecnicoRalco + "'" +
                        ",[prezzoUnitario] = " + riparazione.prezzoUnitario.ToString(System.Globalization.CultureInfo.InvariantCulture) + "" +
                        " WHERE ([Autotest].[dbo].[RMA_Riparazione].[idRMA] = '" + riparazione.idRMA + "' AND [Autotest].[dbo].[RMA_Riparazione].[idRiparazione] = '" + riparazione.idRiparazione + "')", null);
                        #if USEPANTHERA
                        //aggiorno i dati nella vista RMA_RIG_MAT di Panthera
                        conn.CreateCommand("UPDATE [Autotest].[dbo].[RMA_RIG_MAT] SET" +
                        "[QUANTITA] = " + riparazione.quantita + "'" +
                        ",[NOTE] " + riparazione.noteRip + "'" +
                        " WHERE ([Autotest].[dbo].[RMA_RIG_MAT].[ID_AZIENDA] = '001' AND [Autotest].[dbo].[RMA_RIG_MAT].[ID_RMA] = '" + riparazione.idRMA + "' AND [Autotest].[dbo].[RMA_RIG_MAT].[ID_MATERIALE] = '" + riparazione.idParte + "')", null);
                        #endif
                    }
                }
                else
                {
                    //ho aggiunto un nuovo problema tra i template del DB.
                    if (riparazione.idRMA == null)
                    {
                        riparazione.idRMA = "";
                        conn.CreateCommand("INSERT INTO [Autotest].[dbo].[RMA_RiparazioneTemplateDB] " +
                        "([idRMA],[dataAggRip],[descRip],[idParte],[descParte],[quantita],[statoRip],[noteRip],[nomeTecnicoRalco],[prezzoUnitario]) " +
                        "VALUES ('" + riparazione.idRMA + "','" + riparazione.dataAggRip.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "','" + riparazione.descRip + "','" + riparazione.idParte + "','" + riparazione.descParte + "','" + riparazione.quantita.ToString() + "','" + riparazione.statoRip + "','" + riparazione.noteRip + "','" + riparazione.nomeTecnicoRalco + "','" + riparazione.prezzoUnitario.ToString(System.Globalization.CultureInfo.InvariantCulture) + "')", null);
                    }
                    else
                    {
                        //aggiorno i dati della riparazione
                        conn.CreateCommand("UPDATE [Autotest].[dbo].[RMA_RiparazioneTemplateDB] SET" +
                        "[idRMA] = '" + riparazione.idRMA + "'" +
                        ",[dataAggRip] = '" + riparazione.dataAggRip.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "'" +
                        ",[descRip] = '" + riparazione.descRip + "'" +
                        ",[idParte] = '" + riparazione.idParte + "'" +
                        ",[descParte] = '" + riparazione.descParte + "'" +
                        ",[quantita] = '" + riparazione.quantita + "'" +
                        ",[statoRip] = '" + riparazione.statoRip + "'" +
                        ",[noteRip] = '" + riparazione.noteRip + "'" +
                        ",[nomeTecnicoRalco] = '" + riparazione.nomeTecnicoRalco + "'" +
                        ",[prezzoUnitario] = " + riparazione.prezzoUnitario.ToString(System.Globalization.CultureInfo.InvariantCulture) + "" +
                        " WHERE ([Autotest].[dbo].[RMA_RiparazioneTemplateDB].[idRMA] = '" + riparazione.idRMA + "' AND [Autotest].[dbo].[RMA_RiparazioneTemplateDB].[idRiparazione] = '" + riparazione.idRiparazione + "')", null);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_okRiparazione_Click(object sender, EventArgs e)
        {
            try
            {
                updateVariabilePubblica();
                commitDB();
                if (isTemplateDB)
                {
                    riparazione = null;//annullo la variabile pubblica perchè non devo aggiornare la lista delle riparazioni da usare nell'RMA nel caso di aggiunta al DB template
                }
                this.DialogResult = DialogResult.OK;
                this.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_cancelRiparazione_Click(object sender, EventArgs e)
        {
            try
            {
                this.DialogResult = DialogResult.Cancel;
                this.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void rb_descrizioneParte_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                tb_descParte.Enabled = rb_descrizioneParte.Checked;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
