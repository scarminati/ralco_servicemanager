﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ralco_ServiceManager
{
    public class RMA_Generali
    {
        public Int64 idGenerali { get; set; }
        public string idRMA { get; set; }
        public string tipoRMA { get; set; }
        public DateTime dataCreazione { get; set; }
        public string creatoDa { get; set; }
        public DateTime dataAggiornamento { get; set; }
        public string aggiornatoDa { get; set; }
        public string location { get; set; }//RalcoIT, RalcoUSA,...
        public string priorita { get; set; }
        public string note { get; set; }
        public string stato { get; set; }
        public List<String> seriali { get; set; }
        public List<int> progrParte { get; set; }
    }
}
