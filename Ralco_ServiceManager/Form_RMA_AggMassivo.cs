﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Reflection;
using Xceed.Words.NET;
using System.Net.Mail;
using System.Diagnostics;
using System.Drawing.Printing;
using System.Xml;

namespace Ralco_ServiceManager
{
    public partial class Form_RMA_AggMassivo : Form
    {
        //variabili usate per il tab Generali
        private List<RMA_Generali> rmaGenList { get; set; }
        //variabili usate per il tab Informativa Cliente
        private List<RMA_InformativaCliente> infoCliList { get; set; }
        //variabili usate per il tab Garanzia
        private List<RMA_GaranziaRalco> garanziaRalcoList { get; set; }
        //variabili usate per il tab Note Qualita
        private List<RMA_NoteQualita> noteQualitaList { get; set; }
        //variabili usate per il tab Note RMA
        private List<RMA_Log> noteRMAList { get; set; }
        //variabili usate per il form
        private List<RMA_ClientiRalco> cliList;
        private List<RMA_View> rmaFromFormList;
        private SQLConnector conn;
        List<string> listAzioniSuppl { get; set; }
        public Form_RMA_AggMassivo(List<RMA_View> rmaList, params bool[] isClonedRMA)
        {
            try
            {
                InitializeComponent();
                conn = new SQLConnector();
                //inizializzazione variabili delle varie list usate in fase di insert nel database alla pressione del tasto OK
                this.rmaFromFormList = rmaList;
                setCmbListaClienti();
                setWidgetDescriptions();//da completare
                cleanFormRMA();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Setta le tooltips agli widget del form
        /// </summary>
        private void setWidgetDescriptions()
        {
            try
            {
                #region TooltipsPerBottoni
                ToolTip tooltip = new System.Windows.Forms.ToolTip();
                tooltip.SetToolTip(btn_saveRMA, Utils.resourcemanager.GetString("btn_saveRMA"));
                tooltip.SetToolTip(btn_SaveAndClose, Utils.resourcemanager.GetString("btn_SaveAndCloseRMA"));
                tooltip.SetToolTip(btn_cancel, Utils.resourcemanager.GetString("btn_cancel"));
                #endregion
                #region Label
                lbl_priorita.Text = Utils.resourcemanager.GetString("lbl_priorita");
                lbl_stato.Text = Utils.resourcemanager.GetString("lbl_stato");
                lbl_nomeCliente.Text = Utils.resourcemanager.GetString("lbl_nomeCliente");
                lbl_idCli.Text = Utils.resourcemanager.GetString("lbl_idCli");
                lbl_codCollCli.Text = Utils.resourcemanager.GetString("lbl_codCollCli");
                lbl_decisCli.Text = Utils.resourcemanager.GetString("lbl_decisCli");
                lbl_dataArrColl.Text = Utils.resourcemanager.GetString("lbl_dataArrColl");
                lbl_dataScadRMA.Text = Utils.resourcemanager.GetString("lbl_dataScadRMA");
                lbl_SCARcli.Text = Utils.resourcemanager.GetString("lbl_SCARcli");
                lbl_numSCARcli.Text = Utils.resourcemanager.GetString("lbl_numSCARcli");
                lbl_ordRip.Text = Utils.resourcemanager.GetString("lbl_ordRip");
                lbl_dataOrdRip.Text = Utils.resourcemanager.GetString("lbl_dataOrdRip");
                lbl_docReso.Text = Utils.resourcemanager.GetString("lbl_docReso");
                lbl_dataDocReso.Text = Utils.resourcemanager.GetString("lbl_dataDocReso");
                lbl_provColl.Text = Utils.resourcemanager.GetString("lbl_provColl");
                lbl_anniVitaColl.Text = Utils.resourcemanager.GetString("lbl_anniVitaColl");
                lbl_respDannoColl.Text = Utils.resourcemanager.GetString("lbl_respDannoColl");
                lbl_azSupplRalco.Text = Utils.resourcemanager.GetString("lbl_azSupplRalco");
                lbl_dataSped.Text = Utils.resourcemanager.GetString("lbl_dataSped");
                lbl_speseSped.Text = Utils.resourcemanager.GetString("lbl_speseSped");
                lbl_sicProd.Text = Utils.resourcemanager.GetString("lbl_sicProd");
                lbl_segnIncid.Text = Utils.resourcemanager.GetString("lbl_segnIncid");
                lbl_invioNotaInf.Text = Utils.resourcemanager.GetString("lbl_invioNotaInf");
                lbl_revDocValutazRischi.Text = Utils.resourcemanager.GetString("lbl_revDocValutazRischi");
                lbl_statoAzCorr.Text = Utils.resourcemanager.GetString("lbl_stato");
                lbl_rif.Text = Utils.resourcemanager.GetString("lbl_rif");
                lbl_descAzCorr.Text = Utils.resourcemanager.GetString("lbl_Desc");
                #endregion
                #region Groupbox
                gb_InfoGenerali.Text = Utils.resourcemanager.GetString("gb_InfoGenerali");
                gb_dettagli.Text = Utils.resourcemanager.GetString("gb_dettagli");
                gb_impattoSCAR.Text = Utils.resourcemanager.GetString("gb_impattoSCAR");
                gb_azCorrPrev.Text = Utils.resourcemanager.GetString("gb_azCorrPrev");
                #endregion
                #region Radio Button
                rb_statoGaranzia.Text = Utils.resourcemanager.GetString("rb_statoGaranzia");
                #endregion
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Setta i valori iniziali di ogni componente del form
        /// </summary>
        private void cleanFormRMA()
        {
            try
            {
                #region principale
                cmb_prioritaRMA.SelectedIndex = -1;
                cmb_statoRMA.SelectedIndex = -1;
                #endregion
                #region InformativaCliente
                cmb_nomeCliente.SelectedIndex = -1;
                cmb_decisCliente.SelectedIndex = -1;
                //imposto il valore iniziale delle datetimepicker alla data odierna.
                dtp_DataArrivoCollimatore.Value = DateTime.Now;
                dtp_dataScadenzaRMA.Value = DateTime.Now;
                dtp_dataOrdRip.Value = DateTime.Now;
                dtp_dataDocReso.Value = DateTime.Now;
                //e setto i valori delle textbox corrispondenti a ""
                tb_DataArrivoCollimatore.Text = "  /  /";
                tb_DataScadenzaRMA.Text = "  /  /";
                tb_DataOrdineRiparazione.Text = "  /  /";
                tb_DataDocumentoDiReso.Text = "  /  /";
                #endregion
                #region Garanzia
                cmb_provCollimatore.SelectedIndex = -1;
                cmb_anniVitaCollimatore.SelectedIndex = -1;
                cmb_respDannoCollimatore.SelectedIndex = -1;
                cmb_azioniSupplRalco.SelectedIndex = -1;
                #endregion
                #region Trasporto
                dtp_dataSpedizione.Value = DateTime.Now;
                tb_DataSpedizione.Text = "  /  /";
                cmb_speseSpediz.SelectedIndex = -1;
                cmb_tipoImballaggio.SelectedIndex = -1;
                #endregion
                #region NoteQualità
                cmb_sicProdotto.SelectedIndex = -1;
                cmb_segIncidente.SelectedIndex = -1;
                cmb_invioNotaInf.SelectedIndex = -1;
                cmb_revDocValutRischi.SelectedIndex = -1;
                cmb_statoAzCorr.SelectedIndex = -1;
                cmb_rifAzCorr.SelectedIndex = -1;
                #endregion
                #region NoteRMA
                dtp_dataAggiornamento.Value = DateTime.Now;
                tb_dataAggiornamento.Text = "  /  /";
                cmb_statoNotaRMA.SelectedIndex = -1;
                #endregion
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dtp_DataArrivoCollimatore_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                tb_DataArrivoCollimatore.Text = dtp_DataArrivoCollimatore.Value.ToShortDateString();
                if (tb_DataScadenzaRMA.Text.Equals("  /  /"))
                {
                    tb_DataScadenzaRMA.Text = dtp_DataArrivoCollimatore.Value.AddDays(38).ToShortDateString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dtp_dataScadenzaRMA_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                tb_DataScadenzaRMA.Text = dtp_dataScadenzaRMA.Value.ToShortDateString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dtp_dataOrdRip_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                tb_DataOrdineRiparazione.Text = dtp_dataOrdRip.Value.ToShortDateString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dtp_dataDocReso_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                tb_DataDocumentoDiReso.Text = dtp_dataDocReso.Value.ToShortDateString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dtp_dataSpedizione_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                tb_DataSpedizione.Text = dtp_dataSpedizione.Value.ToShortDateString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dtp_dataAggiornamento_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                tb_dataAggiornamento.Text = dtp_dataAggiornamento.Value.ToShortDateString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cmb_statoRMA_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cmb_statoRMA.SelectedItem.Equals("Chiuso"))
                {
                    cmb_prioritaRMA.SelectedItem = "3 - Bassa";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void setCmbListaClienti()
        {
            try
            {
                cmb_nomeCliente.Items.Clear();
                cliList = new List<RMA_ClientiRalco>();
                cliList = (List<RMA_ClientiRalco>)conn.CreateCommand("SELECT * FROM [archivi].[dbo].[VISTA_SM_CLIENTI] order by ID", cliList);
                foreach (RMA_ClientiRalco cli in cliList)
                    cmb_nomeCliente.Items.Add(cli.nomeCliente.TrimStart().TrimEnd());
                cmb_nomeCliente.Sorted = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //attivata ogni volta che modifico il valore della combobox
        private void cmb_nomeCliente_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                lbl_idCliente.Text = "";
                lbl_idCliente.Text = cliList.First(r => r.nomeCliente.TrimEnd().Equals(cmb_nomeCliente.SelectedItem.ToString())).IdCliente.ToString();
            }
            catch (Exception ex)
            {
                lbl_idCliente.Text = "N/A";
            }
        }


        private void cmb_anniVitaCollimatore_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                cmb_statoGaranzia.Text = "";
                logicaDeterminazioneGaranzia();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cmb_respDannoCollimatore_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                logicaDeterminazioneGaranzia();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void logicaDeterminazioneGaranzia()
        {
            try
            {
                switch (cmb_anniVitaCollimatore.SelectedIndex)
                {
                    case 0://- di 2
                        switch (cmb_respDannoCollimatore.SelectedIndex)
                        {
                            case 0://cliente
                                cmb_statoGaranzia.SelectedIndex = 1; //Fuori Garanzia
                                break;
                            case 1://Ralco
                                cmb_statoGaranzia.SelectedIndex = 0; //In Garanzia
                                break;
                        }
                        break;
                    case 1://+ di 2
                        cmb_statoGaranzia.SelectedIndex = 1;//Fuori Garanzia
                        break;
                }
                if (cmb_statoGaranzia.SelectedIndex.Equals(1))
                    cmb_statoAzCorr.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void rb_appendNoteGenerali_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                rb_replaceNoteGenerali.Checked = !rb_appendNoteGenerali.Checked;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void rb_replaceNoteGenerali_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                rb_appendNoteGenerali.Checked = !rb_replaceNoteGenerali.Checked;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void rb_appendDifRiscCliente_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                rb_replaceDifRiscCli.Checked = !rb_appendDifRiscCliente.Checked;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void rb_replaceDifRiscCli_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                rb_appendDifRiscCliente.Checked = !rb_replaceDifRiscCli.Checked;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void rb_statoGaranzia_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                cmb_statoGaranzia.Enabled = rb_statoGaranzia.Checked;
                cmb_statoGaranzia.DropDownStyle = ComboBoxStyle.DropDown;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void rb_appendNoteGaranzia_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                rb_replaceNoteGaranzia.Checked = !rb_appendNoteGaranzia.Checked;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void rb_replaceNoteGaranzia_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                rb_appendNoteGaranzia.Checked = !rb_replaceNoteGaranzia.Checked;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //private void rb_appendNoteImballaggio_CheckedChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        rb_replaceNoteImballaggio.Checked = !rb_appendNoteImballaggio.Checked;
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //    }
        //}

        //private void rb_replaceNoteImballaggio_CheckedChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        rb_appendNoteImballaggio.Checked = !rb_replaceNoteImballaggio.Checked;
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //    }
        //}

        //private void rb_appendNoteTrasporto_CheckedChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        rb_replaceNoteTrasporto.Checked = !rb_appendNoteTrasporto.Checked;
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //    }
        //}

        //private void rb_replaceNoteTrasporto_CheckedChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        rb_appendNoteTrasporto.Checked = !rb_replaceNoteTrasporto.Checked;
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //    }
        //}

        private void rb_appendNoteQualita_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                rb_replaceNoteQualita.Checked = !rb_appendNoteQualita.Checked;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void rb_replaceNoteQualita_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                rb_appendNoteQualita.Checked = !rb_replaceNoteQualita.Checked;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Cerco in RMA_Generali l'idRMA. Se non lo trovo, significa che non ho mai premuto save -> rimuovo dalla sola tabella RMA_Main this.idRMA, valorizzato durante la calcolaIdRMA();
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_cancelRMA_Click(object sender, EventArgs e)
        {
            try
            {
                formCancel();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Form_RMA_AggMassivo_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                formCancel();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void formCancel()
        {
            try
            {
                this.DialogResult = DialogResult.Cancel;
                this.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_saveRMA_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult result = MessageBox.Show("Sei sicuro di voler eseguire il salvataggio sul Database degli RMA selezionati?", "Salvataggio", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == System.Windows.Forms.DialogResult.Yes)
                {
                    updateRMAFromFormValues();
                    commitDB(0);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_saveAndCloseRMA_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult result = MessageBox.Show("Sei sicuro di voler eseguire il salvataggio sul Database degli RMA selezionati?", "Salvataggio", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == System.Windows.Forms.DialogResult.Yes)
                {
                    updateRMAFromFormValues();
                    commitDB(1);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// aggiorno i campi opportuni degli RMA provenienti dal form principale prima di eseguire il commit sul DB
        /// </summary>
        private void updateRMAFromFormValues()
        {
            try
            {
                rmaGenList = new List<RMA_Generali>();
                infoCliList = new List<RMA_InformativaCliente>();
                garanziaRalcoList = new List<RMA_GaranziaRalco>();
                noteQualitaList = new List<RMA_NoteQualita>();
                noteRMAList = new List<RMA_Log>();
                foreach (RMA_View rma in rmaFromFormList)
                {
                    #region RMA_Generali
                    RMA_Generali rmaGen = new RMA_Generali();
                    rmaGen = (RMA_Generali)conn.CreateCommand("SELECT * FROM [Autotest].[dbo].[RMA_Generali] WHERE [Autotest].[dbo].[RMA_Generali].[idRMA] = '" + rma.idRMA + "'", rmaGen);
                    if (cmb_prioritaRMA.SelectedItem != null)
                        rmaGen.priorita = cmb_prioritaRMA.SelectedItem.ToString();
                    if (cmb_statoRMA.SelectedItem != null)
                        rmaGen.stato = cmb_statoRMA.SelectedItem.ToString();
                    if (!tb_noteGenerali.Text.Equals(""))
                    {
                        if (rb_replaceNoteGenerali.Checked)
                            rmaGen.note = Utils.parseUselessChars(tb_noteGenerali.Text);
                        else
                            rmaGen.note = rmaGen.note + "\n" + Utils.parseUselessChars(tb_noteGenerali.Text);
                    }
                    rmaGenList.Add(rmaGen);
                    #endregion
                    #region InformativaCliente + Trasporto
                    RMA_InformativaCliente infoCli = new RMA_InformativaCliente();
                    infoCli = (RMA_InformativaCliente)conn.CreateCommand("SELECT * FROM [Autotest].[dbo].[RMA_InformativaCliente] WHERE [Autotest].[dbo].[RMA_InformativaCliente].[idRMA] = '" + rma.idRMA + "'", infoCli);
                    if (cmb_nomeCliente.SelectedItem != null)
                    {
                        infoCli.cliente = cliList.First(r => r.nomeCliente.TrimEnd().Equals(cmb_nomeCliente.SelectedItem.ToString()));
                    }
                    if (cmb_decisCliente.SelectedItem != null)
                        infoCli.decisioneCliente = cmb_decisCliente.SelectedItem.ToString();
                    if (!tb_DataArrivoCollimatore.Text.Equals("  /  /"))
                        infoCli.dataArrivoCollim = Convert.ToDateTime(tb_DataArrivoCollimatore.Text);
                    if (!tb_DataScadenzaRMA.Text.Equals("  /  /"))
                        infoCli.dataScadenzaRMA = Convert.ToDateTime(tb_DataScadenzaRMA.Text);
                    if (!tb_scarCliente.Text.Equals(""))
                        infoCli.SCARCliente = Utils.parseUselessChars(tb_scarCliente.Text);
                    if (!tb_numScarCLI.Text.Equals(""))
                        infoCli.numScarCli = Utils.parseUselessChars(tb_numScarCLI.Text);
                    if (!tb_ordRiparaz.Text.Equals(""))
                        infoCli.ordRiparazione = Utils.parseUselessChars(tb_ordRiparaz.Text);
                    if (!tb_DataOrdineRiparazione.Text.Equals("  /  /"))
                        infoCli.dataOrdineRip = Convert.ToDateTime(tb_DataOrdineRiparazione.Text);
                    if (!tb_docReso.Text.Equals(""))
                        infoCli.docReso = Utils.parseUselessChars(tb_docReso.Text);
                    if (!tb_DataDocumentoDiReso.Text.Equals("  /  /"))
                        infoCli.dataDocReso = Convert.ToDateTime(tb_DataDocumentoDiReso.Text);
                    if (!tb_DataSpedizione.Text.Equals("  /  /"))
                        infoCli.dataSpedizione = Convert.ToDateTime(tb_DataSpedizione.Text);
                    if (cmb_speseSpediz.SelectedItem != null)
                        infoCli.speseSpedizione = cmb_speseSpediz.SelectedItem.ToString();
                    if (cmb_tipoImballaggio.SelectedItem != null)
                        infoCli.tipoImballaggio = cmb_tipoImballaggio.SelectedItem.ToString();
                    if (!tb_noteImballaggio.Text.Equals(""))
                    {
                        if (rb_replaceNoteImballaggio.Checked)
                            infoCli.noteImballaggio = Utils.parseUselessChars(tb_noteImballaggio.Text);
                        else
                            infoCli.noteImballaggio = infoCli.noteImballaggio + "\n" + Utils.parseUselessChars(tb_noteImballaggio.Text);
                    }
                    if (!tb_noteTrasporto.Text.Equals(""))
                    {
                        if (rb_replaceNoteTrasporto.Checked)
                            infoCli.tipoTrasporto = Utils.parseUselessChars(tb_noteTrasporto.Text);
                        else
                            infoCli.tipoTrasporto = infoCli.tipoTrasporto + "\n" + Utils.parseUselessChars(tb_noteTrasporto.Text);
                    }
                    if (!tb_noteCliente.Text.Equals(""))
                    {
                        if (rb_replaceDifRiscCli.Checked)
                            infoCli.noteCliente = Utils.parseUselessChars(tb_noteCliente.Text);
                        else
                            infoCli.noteCliente = infoCli.noteCliente + "\n" + Utils.parseUselessChars(tb_noteCliente.Text);
                    }
                    if (!lbl_codCollCliente.Text.Equals(""))
                        infoCli.codCollCli = lbl_codCollCliente.Text;
                    infoCliList.Add(infoCli);
                    #endregion
                    #region Garanzia
                    RMA_GaranziaRalco garanziaRalco = new RMA_GaranziaRalco();
                    garanziaRalco = (RMA_GaranziaRalco)conn.CreateCommand("SELECT * FROM [Autotest].[dbo].[RMA_GaranziaRalco] WHERE [Autotest].[dbo].[RMA_GaranziaRalco].[idRMA] = '" + rma.idRMA + "'", garanziaRalco);
                    if (cmb_provCollimatore.SelectedItem != null)
                        garanziaRalco.provCollimatore = cmb_provCollimatore.SelectedItem.ToString();
                    if (cmb_anniVitaCollimatore.SelectedItem != null)
                        garanziaRalco.anniVitaCollimatore = cmb_anniVitaCollimatore.SelectedItem.ToString();
                    if (cmb_respDannoCollimatore.SelectedItem != null)
                        garanziaRalco.respDannoCollimatore = cmb_respDannoCollimatore.SelectedItem.ToString();
                    if (!tb_noteGaranziaRalco.Text.Equals(""))
                    {
                        if (rb_replaceNoteGaranzia.Checked)
                            garanziaRalco.noteGaranziaRalco = Utils.parseUselessChars(tb_noteGaranziaRalco.Text);
                        else
                            garanziaRalco.noteGaranziaRalco = garanziaRalco.noteGaranziaRalco + "\n" + Utils.parseUselessChars(tb_noteGaranziaRalco.Text);
                    }
                    if (cmb_statoGaranzia.SelectedItem != null)
                        garanziaRalco.statoGaranzia = cmb_statoGaranzia.Text;
                    if (cmb_azioniSupplRalco.SelectedItem != null)
                        garanziaRalco.azioniSupplRalco = cmb_azioniSupplRalco.SelectedItem.ToString();
                    garanziaRalcoList.Add(garanziaRalco);
                    #endregion
                    #region NoteQualità
                    RMA_NoteQualita noteQualita = new RMA_NoteQualita();
                    noteQualita = (RMA_NoteQualita)conn.CreateCommand("SELECT * FROM [Autotest].[dbo].[RMA_NoteQualita] WHERE [Autotest].[dbo].[RMA_NoteQualita].[idRMA] = '" + rma.idRMA + "'", noteQualita);
                    if (cmb_sicProdotto.SelectedItem != null)
                        noteQualita.sicurezzaProdotto = cmb_sicProdotto.SelectedItem.ToString();
                    if (cmb_segIncidente.SelectedItem != null)
                        noteQualita.segnalazIncidente = cmb_segIncidente.SelectedItem.ToString();
                    if (cmb_invioNotaInf.SelectedItem != null)
                        noteQualita.invioNotaInf = cmb_invioNotaInf.SelectedItem.ToString();
                    if (cmb_revDocValutRischi.SelectedItem != null)
                        noteQualita.revDocValutazRischi = cmb_revDocValutRischi.SelectedItem.ToString();
                    if (cmb_statoAzCorr.SelectedItem != null)
                        noteQualita.statoAzCorrett = cmb_statoAzCorr.SelectedItem.ToString();
                    if (cmb_rifAzCorr.SelectedItem != null)
                        noteQualita.rifAzCorrett = cmb_rifAzCorr.SelectedItem.ToString();
                    if (!lbl_descAzCorrett.Text.Equals(""))
                        noteQualita.descAzCorrett = lbl_descAzCorrett.Text;
                    if (!tb_noteQualita.Text.Equals(""))
                    {
                        if (rb_replaceNoteQualita.Checked)
                            noteQualita.note = Utils.parseUselessChars(tb_noteQualita.Text);
                        else
                            noteQualita.note = noteQualita.note + "\n" + Utils.parseUselessChars(tb_noteQualita.Text);
                    }
                    noteQualitaList.Add(noteQualita);
                    #endregion
                    #region Note RMA
                    if (cmb_statoNotaRMA.SelectedItem != null)
                    {
                        //posso aggiungere una nuova nota RMA
                        RMA_Log notaRMA = new RMA_Log();
                        notaRMA.idRMA = rma.idRMA;
                        notaRMA.dataAggiornamento = Convert.ToDateTime(tb_dataAggiornamento.Text);
                        notaRMA.aggiornatoDa = Globals.user.getFullUserName();
                        notaRMA.stato = cmb_statoNotaRMA.SelectedItem.ToString();
                        notaRMA.note = Utils.parseUselessChars(tb_noteLog.Text);
                        noteRMAList.Add(notaRMA);
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Esegue il commit sul DB
        /// </summary>
        /// <param name="main">1 = chiude il form; 0 = form resta aperto</param>
        private void commitDB(int main)
        {
            try
            {
                foreach (RMA_Generali rmaGen in rmaGenList)
                {
                    conn.CreateCommand("UPDATE [Autotest].[dbo].[RMA_Generali] SET " +
                    "[priorita] = '" + rmaGen.priorita + "'" +
                    ",[stato] = '" + rmaGen.stato + "'" +
                    ",[note] = '" + rmaGen.note + "'" +
                    ",[dataAggiornamento] = '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "'" +
                    ",[aggiornatoDa] = '" + Globals.user.getFullUserName() + "'" + 
                    " WHERE ([Autotest].[dbo].[RMA_Generali].[idRMA] = '" + rmaGen.idRMA + "')", null);
                }
                foreach (RMA_InformativaCliente infoCli in infoCliList)
                {
                    conn.CreateCommand("UPDATE [Autotest].[dbo].[RMA_InformativaCliente] SET " +
                    "[nomeCliente] = '" + infoCli.cliente.nomeCliente + "'" +
                    ",[idCliente] = " + infoCli.cliente.IdCliente + "" +
                    ",[codCollCli] = '" + infoCli.codCollCli + "'" +
                    ",[decisioneCliente] = '" + infoCli.decisioneCliente + "'" +
                    ",[dataArrivoCollim] = '" + infoCli.dataArrivoCollim.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "'" +
                    ",[dataScadenzaRMA] = '" + infoCli.dataScadenzaRMA.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "'" +
                    ",[SCARCliente] = '" + infoCli.SCARCliente + "'" +
                    ",[numScarCli] = '" + infoCli.numScarCli + "'" +
                    ",[ordRiparazione] = '" + infoCli.ordRiparazione + "'" +
                    ",[dataOrdineRip] = '" + infoCli.dataOrdineRip.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "'" +
                    ",[docReso] = '" + infoCli.docReso + "'" +
                    ",[dataDocReso] = '" + infoCli.dataDocReso.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "'" +
                    ",[noteCliente] = '" + infoCli.noteCliente + "'" +
                    ",[dataSpedizione] = '" + infoCli.dataSpedizione.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "'" +
                    ",[speseSpedizione] = '" + infoCli.speseSpedizione + "'" +
                    ",[tipoTrasporto] = '" + infoCli.tipoTrasporto + "'" +
                    ",[tipoImballaggio] = '" + infoCli.tipoImballaggio + "'" +
                    ",[noteImballaggio] = '" + infoCli.noteImballaggio + "'" +
                    " WHERE ([Autotest].[dbo].[RMA_InformativaCliente].[idRMA] = '" + infoCli.idRMA + "')", null);
                }
                foreach (RMA_GaranziaRalco garanziaRalco in garanziaRalcoList)
                {
                    conn.CreateCommand("UPDATE [Autotest].[dbo].[RMA_GaranziaRalco] SET " +
                    "[provCollimatore] = '" + garanziaRalco.provCollimatore + "'" +
                    ",[anniVitaCollimatore] = '" + garanziaRalco.anniVitaCollimatore + "'" +
                    ",[respDannoCollimatore] = '" + garanziaRalco.respDannoCollimatore + "'" +
                    ",[noteGaranziaRalco] = '" + garanziaRalco.noteGaranziaRalco + "'" +
                    ",[statoGaranzia] = '" + garanziaRalco.statoGaranzia + "'" +
                    ",[azioniSupplRalco] = '" + garanziaRalco.azioniSupplRalco + "'" +
                    " WHERE ([Autotest].[dbo].[RMA_GaranziaRalco].[idRMA] = '" + garanziaRalco.idRMA + "')", null);
                }
                foreach (RMA_NoteQualita noteQualita in noteQualitaList)
                {
                    conn.CreateCommand("UPDATE [Autotest].[dbo].[RMA_NoteQualita] SET " +
                    "[sicurezzaProdotto] = '" + noteQualita.sicurezzaProdotto + "'" +
                    ",[segnalazIncidente] = '" + noteQualita.segnalazIncidente + "'" +
                    ",[invioNotaInf] = '" + noteQualita.invioNotaInf + "'" +
                    ",[revDocValutazRischi] = '" + noteQualita.revDocValutazRischi + "'" +
                    ",[statoAzCorrett] = '" + noteQualita.statoAzCorrett + "'" +
                    ",[rifAzCorrett] = '" + noteQualita.rifAzCorrett + "'" +
                    ",[descAzCorrett] = '" + noteQualita.descAzCorrett + "'" +
                    ",[note] = '" + noteQualita.note + "'" +
                    " WHERE ([Autotest].[dbo].[RMA_NoteQualita].[idRMA] = '" + noteQualita.idRMA + "')", null);
                }
                if (noteRMAList != null)
                {
                    foreach(RMA_Log log in noteRMAList)
                        conn.CreateCommand("INSERT INTO [Autotest].[dbo].[RMA_Log] " +
                        "([idRMA],[dataAggiornamento],[aggiornatoDa],[stato],[note]) " +
                        "VALUES ('" + log.idRMA + "','" + log.dataAggiornamento.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "','" + log.aggiornatoDa + "','" + log.stato + "','" + log.note.Replace("'", "") + "')", null);
                }
                switch (main)
                {
                    case 1:
                        this.DialogResult = DialogResult.OK;
                        this.Dispose();
                        break;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
