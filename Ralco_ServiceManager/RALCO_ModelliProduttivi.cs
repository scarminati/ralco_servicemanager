﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ralco_ServiceManager
{
    public class RALCO_ModelliProduttivi
    {
        public string padre { get; set; }
        public string descPadre { get; set; }
        public string figlio { get; set; }
        public string descFiglio { get; set; }
        public Decimal costo { get; set; }
    }
}
