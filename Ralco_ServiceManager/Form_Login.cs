﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ralco_ServiceManager
{
	public partial class Form_Login : Form
	{
        private List<Utente> users;
        private SQLConnector conn; 
        public Form_Login()
		{
            try
            {
                InitializeComponent();
                users = new List<Utente>();
                conn = new SQLConnector();
                users = (List<Utente>)conn.CreateCommand("SELECT * FROM [AutoTest].[dbo].[Utente] JOIN [AutoTest].[dbo].[Applicativo] ON [AutoTest].[dbo].[Utente].[idUtente] = [AutoTest].[dbo].[Applicativo].[idUtente] WHERE [Autotest].[dbo].[Applicativo].[nomeApplicativo] = 'Ralco Service Manager'", users);
                foreach (var u in users)
                    cmb_UserName.Items.Add(u.nome + " " + u.cognome);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_login_Click(object sender, EventArgs e)
		{
            try
            {
                if (cmb_UserName.SelectedItem != null)
                {
                    foreach (var usr in users)
                    {
                        if (cmb_UserName.SelectedItem.ToString().Equals(usr.nome + " " + usr.cognome) && usr.idApplicativo.Equals(1))//idApplicativo = 1 => Service Manager
                        {
                            if (txt_password.Text.Equals(usr.password))
                            {
                                Globals.setUser(usr);
                                Form_ServiceManager formServiceManager = new Form_ServiceManager();
                                formServiceManager.Show();
                                this.Hide();
                                break;
                            }
                            else
                                MessageBox.Show("Password errata", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
                else
                    MessageBox.Show("Inserisci un nome utente", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
