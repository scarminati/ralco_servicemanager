﻿namespace Ralco_ServiceManager
{
    partial class Form_Articolo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Articolo));
            this.lbl_IDArticolo = new System.Windows.Forms.Label();
            this.lbl_Desc = new System.Windows.Forms.Label();
            this.lbl_qta = new System.Windows.Forms.Label();
            this.lbl_prTot = new System.Windows.Forms.Label();
            this.lbl_descrizione = new System.Windows.Forms.Label();
            this.lbl_prezzoTot = new System.Windows.Forms.Label();
            this.btn_OK = new System.Windows.Forms.Button();
            this.nud_Quantita = new System.Windows.Forms.NumericUpDown();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cmb_IDArticoli = new System.Windows.Forms.ComboBox();
            this.lbl_prezzoUnit = new System.Windows.Forms.Label();
            this.lbl_prUnit = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dgv_ListaRifRSRO = new System.Windows.Forms.DataGridView();
            this.chk_Rip = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.codice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grp_RSRO_Rif = new System.Windows.Forms.GroupBox();
            this.btn_cancel = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.nud_Quantita)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ListaRifRSRO)).BeginInit();
            this.grp_RSRO_Rif.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbl_IDArticolo
            // 
            this.lbl_IDArticolo.AutoSize = true;
            this.lbl_IDArticolo.Location = new System.Drawing.Point(66, 18);
            this.lbl_IDArticolo.Name = "lbl_IDArticolo";
            this.lbl_IDArticolo.Size = new System.Drawing.Size(56, 13);
            this.lbl_IDArticolo.TabIndex = 0;
            this.lbl_IDArticolo.Text = "ID Articolo";
            // 
            // lbl_Desc
            // 
            this.lbl_Desc.AutoSize = true;
            this.lbl_Desc.Location = new System.Drawing.Point(66, 70);
            this.lbl_Desc.Name = "lbl_Desc";
            this.lbl_Desc.Size = new System.Drawing.Size(62, 13);
            this.lbl_Desc.TabIndex = 1;
            this.lbl_Desc.Text = "Descrizione";
            // 
            // lbl_qta
            // 
            this.lbl_qta.AutoSize = true;
            this.lbl_qta.Location = new System.Drawing.Point(66, 16);
            this.lbl_qta.Name = "lbl_qta";
            this.lbl_qta.Size = new System.Drawing.Size(47, 13);
            this.lbl_qta.TabIndex = 2;
            this.lbl_qta.Text = "Quantità";
            // 
            // lbl_prTot
            // 
            this.lbl_prTot.AutoSize = true;
            this.lbl_prTot.Location = new System.Drawing.Point(54, 71);
            this.lbl_prTot.Name = "lbl_prTot";
            this.lbl_prTot.Size = new System.Drawing.Size(87, 13);
            this.lbl_prTot.TabIndex = 3;
            this.lbl_prTot.Text = "Prezzo Totale (€)";
            // 
            // lbl_descrizione
            // 
            this.lbl_descrizione.AutoSize = true;
            this.lbl_descrizione.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_descrizione.Location = new System.Drawing.Point(15, 94);
            this.lbl_descrizione.MaximumSize = new System.Drawing.Size(172, 70);
            this.lbl_descrizione.MinimumSize = new System.Drawing.Size(172, 70);
            this.lbl_descrizione.Name = "lbl_descrizione";
            this.lbl_descrizione.Size = new System.Drawing.Size(172, 70);
            this.lbl_descrizione.TabIndex = 3;
            // 
            // lbl_prezzoTot
            // 
            this.lbl_prezzoTot.AutoSize = true;
            this.lbl_prezzoTot.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_prezzoTot.Location = new System.Drawing.Point(15, 97);
            this.lbl_prezzoTot.MaximumSize = new System.Drawing.Size(172, 20);
            this.lbl_prezzoTot.MinimumSize = new System.Drawing.Size(172, 20);
            this.lbl_prezzoTot.Name = "lbl_prezzoTot";
            this.lbl_prezzoTot.Size = new System.Drawing.Size(172, 20);
            this.lbl_prezzoTot.TabIndex = 6;
            // 
            // btn_OK
            // 
            this.btn_OK.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_OK.BackgroundImage")));
            this.btn_OK.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_OK.Location = new System.Drawing.Point(266, 396);
            this.btn_OK.Name = "btn_OK";
            this.btn_OK.Size = new System.Drawing.Size(50, 50);
            this.btn_OK.TabIndex = 7;
            this.btn_OK.UseVisualStyleBackColor = true;
            this.btn_OK.Click += new System.EventHandler(this.btn_ok_Click);
            // 
            // nud_Quantita
            // 
            this.nud_Quantita.Location = new System.Drawing.Point(15, 38);
            this.nud_Quantita.Name = "nud_Quantita";
            this.nud_Quantita.Size = new System.Drawing.Size(172, 20);
            this.nud_Quantita.TabIndex = 5;
            this.nud_Quantita.ValueChanged += new System.EventHandler(this.nud_Quantita_ValueChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cmb_IDArticoli);
            this.groupBox1.Controls.Add(this.lbl_prezzoUnit);
            this.groupBox1.Controls.Add(this.lbl_prUnit);
            this.groupBox1.Controls.Add(this.lbl_descrizione);
            this.groupBox1.Controls.Add(this.lbl_IDArticolo);
            this.groupBox1.Controls.Add(this.lbl_Desc);
            this.groupBox1.Location = new System.Drawing.Point(12, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(196, 245);
            this.groupBox1.TabIndex = 106;
            this.groupBox1.TabStop = false;
            // 
            // cmb_IDArticoli
            // 
            this.cmb_IDArticoli.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmb_IDArticoli.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_IDArticoli.FormattingEnabled = true;
            this.cmb_IDArticoli.Location = new System.Drawing.Point(15, 39);
            this.cmb_IDArticoli.Name = "cmb_IDArticoli";
            this.cmb_IDArticoli.Size = new System.Drawing.Size(172, 21);
            this.cmb_IDArticoli.TabIndex = 7;
            this.cmb_IDArticoli.SelectedIndexChanged += new System.EventHandler(this.cmb_IDArticoli_SelectedIndexChanged);
            // 
            // lbl_prezzoUnit
            // 
            this.lbl_prezzoUnit.AutoSize = true;
            this.lbl_prezzoUnit.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_prezzoUnit.Location = new System.Drawing.Point(15, 203);
            this.lbl_prezzoUnit.MaximumSize = new System.Drawing.Size(172, 20);
            this.lbl_prezzoUnit.MinimumSize = new System.Drawing.Size(172, 20);
            this.lbl_prezzoUnit.Name = "lbl_prezzoUnit";
            this.lbl_prezzoUnit.Size = new System.Drawing.Size(172, 20);
            this.lbl_prezzoUnit.TabIndex = 4;
            // 
            // lbl_prUnit
            // 
            this.lbl_prUnit.AutoSize = true;
            this.lbl_prUnit.Location = new System.Drawing.Point(55, 179);
            this.lbl_prUnit.Name = "lbl_prUnit";
            this.lbl_prUnit.Size = new System.Drawing.Size(93, 13);
            this.lbl_prUnit.TabIndex = 6;
            this.lbl_prUnit.Text = "Prezzo Unitario (€)";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lbl_prezzoTot);
            this.groupBox2.Controls.Add(this.lbl_qta);
            this.groupBox2.Controls.Add(this.nud_Quantita);
            this.groupBox2.Controls.Add(this.lbl_prTot);
            this.groupBox2.Location = new System.Drawing.Point(12, 253);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(196, 137);
            this.groupBox2.TabIndex = 107;
            this.groupBox2.TabStop = false;
            // 
            // dgv_ListaRifRSRO
            // 
            this.dgv_ListaRifRSRO.AllowUserToAddRows = false;
            this.dgv_ListaRifRSRO.AllowUserToOrderColumns = true;
            this.dgv_ListaRifRSRO.BackgroundColor = System.Drawing.SystemColors.ActiveBorder;
            this.dgv_ListaRifRSRO.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_ListaRifRSRO.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.chk_Rip,
            this.codice,
            this.descr});
            this.dgv_ListaRifRSRO.Cursor = System.Windows.Forms.Cursors.Default;
            this.dgv_ListaRifRSRO.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_ListaRifRSRO.Location = new System.Drawing.Point(3, 16);
            this.dgv_ListaRifRSRO.Name = "dgv_ListaRifRSRO";
            this.dgv_ListaRifRSRO.ReadOnly = true;
            this.dgv_ListaRifRSRO.RowHeadersVisible = false;
            this.dgv_ListaRifRSRO.Size = new System.Drawing.Size(450, 369);
            this.dgv_ListaRifRSRO.TabIndex = 7;
            // 
            // chk_Rip
            // 
            this.chk_Rip.HeaderText = "";
            this.chk_Rip.Name = "chk_Rip";
            this.chk_Rip.ReadOnly = true;
            this.chk_Rip.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.chk_Rip.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.chk_Rip.Width = 30;
            // 
            // codice
            // 
            this.codice.HeaderText = "Codice";
            this.codice.Name = "codice";
            this.codice.ReadOnly = true;
            this.codice.Width = 150;
            // 
            // descr
            // 
            this.descr.HeaderText = "Descrizione";
            this.descr.Name = "descr";
            this.descr.ReadOnly = true;
            this.descr.Width = 250;
            // 
            // grp_RSRO_Rif
            // 
            this.grp_RSRO_Rif.Controls.Add(this.dgv_ListaRifRSRO);
            this.grp_RSRO_Rif.Location = new System.Drawing.Point(214, 2);
            this.grp_RSRO_Rif.Name = "grp_RSRO_Rif";
            this.grp_RSRO_Rif.Size = new System.Drawing.Size(456, 388);
            this.grp_RSRO_Rif.TabIndex = 108;
            this.grp_RSRO_Rif.TabStop = false;
            this.grp_RSRO_Rif.Text = "RS / RO Di Riferimento";
            // 
            // btn_cancel
            // 
            this.btn_cancel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_cancel.BackgroundImage")));
            this.btn_cancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_cancel.Location = new System.Drawing.Point(373, 396);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(50, 50);
            this.btn_cancel.TabIndex = 112;
            this.btn_cancel.UseVisualStyleBackColor = true;
            this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
            // 
            // Form_Articolo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(683, 455);
            this.Controls.Add(this.btn_cancel);
            this.Controls.Add(this.grp_RSRO_Rif);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btn_OK);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form_Articolo";
            this.Text = "Articolo";
            ((System.ComponentModel.ISupportInitialize)(this.nud_Quantita)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ListaRifRSRO)).EndInit();
            this.grp_RSRO_Rif.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lbl_IDArticolo;
        private System.Windows.Forms.Label lbl_Desc;
        private System.Windows.Forms.Label lbl_qta;
        private System.Windows.Forms.Label lbl_prTot;
        private System.Windows.Forms.Label lbl_descrizione;
        private System.Windows.Forms.Label lbl_prezzoTot;
        private System.Windows.Forms.Button btn_OK;
        private System.Windows.Forms.NumericUpDown nud_Quantita;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lbl_prezzoUnit;
        private System.Windows.Forms.Label lbl_prUnit;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox cmb_IDArticoli;
        private System.Windows.Forms.DataGridView dgv_ListaRifRSRO;
        private System.Windows.Forms.GroupBox grp_RSRO_Rif;
        private System.Windows.Forms.Button btn_cancel;
        private System.Windows.Forms.DataGridViewCheckBoxColumn chk_Rip;
        private System.Windows.Forms.DataGridViewTextBoxColumn codice;
        private System.Windows.Forms.DataGridViewTextBoxColumn descr;
    }
}