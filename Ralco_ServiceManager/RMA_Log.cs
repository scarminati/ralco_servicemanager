﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ralco_ServiceManager
{
    public class RMA_Log
    {
        public Int64 idProgressivo { get; set; }
        public string idRMA { get; set; }
        public DateTime dataAggiornamento { get; set; }
        public string aggiornatoDa { get; set; }
        public string stato { get; set; }
        public string note { get; set; }

        //costruttore di default
        public RMA_Log()
        {
            //do nothing...
        }

        //overload del costruttore
        public RMA_Log (RMA_Log log)
        {
            this.idProgressivo = log.idProgressivo;
            this.idRMA = log.idRMA;
            this.dataAggiornamento = log.dataAggiornamento;
            this.aggiornatoDa = log.aggiornatoDa;
            this.stato = log.stato;
            this.note = log.note;
        }

        //overload2 del costruttore
        public RMA_Log(string idRMA)
        {
            this.idRMA = idRMA;
            this.dataAggiornamento = DateTime.Now;
            this.aggiornatoDa = Globals.user.getFullUserName();
            this.stato = "Aperto";
            this.note = "";
        }
    }
}
