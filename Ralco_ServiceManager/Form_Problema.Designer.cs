﻿namespace Ralco_ServiceManager
{
    partial class Form_Problema
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Problema));
            this.grp_dettProb = new System.Windows.Forms.GroupBox();
            this.lbl_DescrizParte = new System.Windows.Forms.Label();
            this.btn_CancelRespo = new System.Windows.Forms.Button();
            this.btn_AddRespo = new System.Windows.Forms.Button();
            this.lbl_descParte = new System.Windows.Forms.Label();
            this.dgv_nomeResponsabile = new System.Windows.Forms.DataGridView();
            this.chk_Respo = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.nomeResponsabile = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmb_gravitaProblema = new System.Windows.Forms.ComboBox();
            this.tb_difRiscDaRalco = new System.Windows.Forms.TextBox();
            this.lbl_gravita = new System.Windows.Forms.Label();
            this.lbl_causaProb = new System.Windows.Forms.Label();
            this.lbl_faseProd = new System.Windows.Forms.Label();
            this.cmb_codParteControllo = new System.Windows.Forms.ComboBox();
            this.cmb_faseProduzione = new System.Windows.Forms.ComboBox();
            this.rb_codiceParte = new System.Windows.Forms.RadioButton();
            this.lbl_rifFlowChart = new System.Windows.Forms.Label();
            this.cmb_rifFlowChart = new System.Windows.Forms.ComboBox();
            this.cmb_causaProblema = new System.Windows.Forms.ComboBox();
            this.lbl_difRiscRalco = new System.Windows.Forms.Label();
            this.btn_OK = new System.Windows.Forms.Button();
            this.btn_cancel = new System.Windows.Forms.Button();
            this.grp_testEseguiti = new System.Windows.Forms.GroupBox();
            this.dgv_testDaEseguire = new System.Windows.Forms.DataGridView();
            this.dataGridViewCheckBoxColumn2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.descTest = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.esitoTest = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.grp_dettProb.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_nomeResponsabile)).BeginInit();
            this.grp_testEseguiti.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_testDaEseguire)).BeginInit();
            this.SuspendLayout();
            // 
            // grp_dettProb
            // 
            this.grp_dettProb.Controls.Add(this.lbl_DescrizParte);
            this.grp_dettProb.Controls.Add(this.btn_CancelRespo);
            this.grp_dettProb.Controls.Add(this.btn_AddRespo);
            this.grp_dettProb.Controls.Add(this.lbl_descParte);
            this.grp_dettProb.Controls.Add(this.dgv_nomeResponsabile);
            this.grp_dettProb.Controls.Add(this.cmb_gravitaProblema);
            this.grp_dettProb.Controls.Add(this.tb_difRiscDaRalco);
            this.grp_dettProb.Controls.Add(this.lbl_gravita);
            this.grp_dettProb.Controls.Add(this.lbl_causaProb);
            this.grp_dettProb.Controls.Add(this.lbl_faseProd);
            this.grp_dettProb.Controls.Add(this.cmb_codParteControllo);
            this.grp_dettProb.Controls.Add(this.cmb_faseProduzione);
            this.grp_dettProb.Controls.Add(this.rb_codiceParte);
            this.grp_dettProb.Controls.Add(this.lbl_rifFlowChart);
            this.grp_dettProb.Controls.Add(this.cmb_rifFlowChart);
            this.grp_dettProb.Controls.Add(this.cmb_causaProblema);
            this.grp_dettProb.Controls.Add(this.lbl_difRiscRalco);
            this.grp_dettProb.Location = new System.Drawing.Point(12, 161);
            this.grp_dettProb.Name = "grp_dettProb";
            this.grp_dettProb.Size = new System.Drawing.Size(589, 443);
            this.grp_dettProb.TabIndex = 7;
            this.grp_dettProb.TabStop = false;
            this.grp_dettProb.Text = "Dettagli del problema";
            // 
            // lbl_DescrizParte
            // 
            this.lbl_DescrizParte.AutoSize = true;
            this.lbl_DescrizParte.Location = new System.Drawing.Point(15, 256);
            this.lbl_DescrizParte.Name = "lbl_DescrizParte";
            this.lbl_DescrizParte.Size = new System.Drawing.Size(90, 13);
            this.lbl_DescrizParte.TabIndex = 104;
            this.lbl_DescrizParte.Text = "Descrizione Parte";
            // 
            // btn_CancelRespo
            // 
            this.btn_CancelRespo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_CancelRespo.BackgroundImage")));
            this.btn_CancelRespo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_CancelRespo.Location = new System.Drawing.Point(85, 357);
            this.btn_CancelRespo.Name = "btn_CancelRespo";
            this.btn_CancelRespo.Size = new System.Drawing.Size(50, 50);
            this.btn_CancelRespo.TabIndex = 8;
            this.btn_CancelRespo.UseVisualStyleBackColor = true;
            this.btn_CancelRespo.Click += new System.EventHandler(this.btn_CancelRespo_Click);
            // 
            // btn_AddRespo
            // 
            this.btn_AddRespo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_AddRespo.BackgroundImage")));
            this.btn_AddRespo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_AddRespo.Location = new System.Drawing.Point(18, 357);
            this.btn_AddRespo.Name = "btn_AddRespo";
            this.btn_AddRespo.Size = new System.Drawing.Size(50, 50);
            this.btn_AddRespo.TabIndex = 7;
            this.btn_AddRespo.UseVisualStyleBackColor = true;
            this.btn_AddRespo.Click += new System.EventHandler(this.btn_AddRespo_Click);
            // 
            // lbl_descParte
            // 
            this.lbl_descParte.AutoSize = true;
            this.lbl_descParte.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_descParte.Location = new System.Drawing.Point(166, 256);
            this.lbl_descParte.MaximumSize = new System.Drawing.Size(405, 20);
            this.lbl_descParte.MinimumSize = new System.Drawing.Size(405, 20);
            this.lbl_descParte.Name = "lbl_descParte";
            this.lbl_descParte.Size = new System.Drawing.Size(405, 20);
            this.lbl_descParte.TabIndex = 103;
            this.lbl_descParte.Text = "N/A";
            // 
            // dgv_nomeResponsabile
            // 
            this.dgv_nomeResponsabile.AllowUserToAddRows = false;
            this.dgv_nomeResponsabile.AllowUserToOrderColumns = true;
            this.dgv_nomeResponsabile.BackgroundColor = System.Drawing.SystemColors.ActiveBorder;
            this.dgv_nomeResponsabile.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_nomeResponsabile.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.chk_Respo,
            this.nomeResponsabile});
            this.dgv_nomeResponsabile.Cursor = System.Windows.Forms.Cursors.Default;
            this.dgv_nomeResponsabile.Location = new System.Drawing.Point(166, 327);
            this.dgv_nomeResponsabile.MaximumSize = new System.Drawing.Size(405, 108);
            this.dgv_nomeResponsabile.MinimumSize = new System.Drawing.Size(405, 108);
            this.dgv_nomeResponsabile.Name = "dgv_nomeResponsabile";
            this.dgv_nomeResponsabile.ReadOnly = true;
            this.dgv_nomeResponsabile.RowHeadersVisible = false;
            this.dgv_nomeResponsabile.Size = new System.Drawing.Size(405, 108);
            this.dgv_nomeResponsabile.TabIndex = 6;
            this.dgv_nomeResponsabile.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_nomeResponsabili_CellContentClick);
            // 
            // chk_Respo
            // 
            this.chk_Respo.HeaderText = "";
            this.chk_Respo.Name = "chk_Respo";
            this.chk_Respo.ReadOnly = true;
            this.chk_Respo.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.chk_Respo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.chk_Respo.Width = 30;
            // 
            // nomeResponsabile
            // 
            this.nomeResponsabile.HeaderText = "Nome Responsabile";
            this.nomeResponsabile.Name = "nomeResponsabile";
            this.nomeResponsabile.ReadOnly = true;
            this.nomeResponsabile.Width = 350;
            // 
            // cmb_gravitaProblema
            // 
            this.cmb_gravitaProblema.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmb_gravitaProblema.FormattingEnabled = true;
            this.cmb_gravitaProblema.Items.AddRange(new object[] {
            "Estetico",
            "Funzionale",
            "Sicurezza",
            "Trascurabile"});
            this.cmb_gravitaProblema.Location = new System.Drawing.Point(166, 294);
            this.cmb_gravitaProblema.Name = "cmb_gravitaProblema";
            this.cmb_gravitaProblema.Size = new System.Drawing.Size(405, 21);
            this.cmb_gravitaProblema.Sorted = true;
            this.cmb_gravitaProblema.TabIndex = 5;
            // 
            // tb_difRiscDaRalco
            // 
            this.tb_difRiscDaRalco.Location = new System.Drawing.Point(166, 17);
            this.tb_difRiscDaRalco.Multiline = true;
            this.tb_difRiscDaRalco.Name = "tb_difRiscDaRalco";
            this.tb_difRiscDaRalco.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tb_difRiscDaRalco.Size = new System.Drawing.Size(405, 73);
            this.tb_difRiscDaRalco.TabIndex = 4;
            this.tb_difRiscDaRalco.Text = "N/A";
            // 
            // lbl_gravita
            // 
            this.lbl_gravita.AutoSize = true;
            this.lbl_gravita.Location = new System.Drawing.Point(15, 297);
            this.lbl_gravita.Name = "lbl_gravita";
            this.lbl_gravita.Size = new System.Drawing.Size(105, 13);
            this.lbl_gravita.TabIndex = 14;
            this.lbl_gravita.Text = "Gravità del Problema";
            // 
            // lbl_causaProb
            // 
            this.lbl_causaProb.AutoSize = true;
            this.lbl_causaProb.Location = new System.Drawing.Point(15, 108);
            this.lbl_causaProb.Name = "lbl_causaProb";
            this.lbl_causaProb.Size = new System.Drawing.Size(101, 13);
            this.lbl_causaProb.TabIndex = 9;
            this.lbl_causaProb.Text = "Causa del Problema";
            // 
            // lbl_faseProd
            // 
            this.lbl_faseProd.AutoSize = true;
            this.lbl_faseProd.Location = new System.Drawing.Point(15, 148);
            this.lbl_faseProd.Name = "lbl_faseProd";
            this.lbl_faseProd.Size = new System.Drawing.Size(86, 13);
            this.lbl_faseProd.TabIndex = 16;
            this.lbl_faseProd.Text = "Fase Produzione";
            // 
            // cmb_codParteControllo
            // 
            this.cmb_codParteControllo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmb_codParteControllo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_codParteControllo.Enabled = false;
            this.cmb_codParteControllo.FormattingEnabled = true;
            this.cmb_codParteControllo.Location = new System.Drawing.Point(166, 221);
            this.cmb_codParteControllo.Name = "cmb_codParteControllo";
            this.cmb_codParteControllo.Size = new System.Drawing.Size(405, 21);
            this.cmb_codParteControllo.Sorted = true;
            this.cmb_codParteControllo.TabIndex = 102;
            this.cmb_codParteControllo.SelectedIndexChanged += new System.EventHandler(this.cmb_codParteControllo_SelectedIndexChanged);
            this.cmb_codParteControllo.TextChanged += new System.EventHandler(this.cmb_codParteControllo_TextChanged);
            // 
            // cmb_faseProduzione
            // 
            this.cmb_faseProduzione.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmb_faseProduzione.FormattingEnabled = true;
            this.cmb_faseProduzione.Location = new System.Drawing.Point(166, 145);
            this.cmb_faseProduzione.Name = "cmb_faseProduzione";
            this.cmb_faseProduzione.Size = new System.Drawing.Size(405, 21);
            this.cmb_faseProduzione.Sorted = true;
            this.cmb_faseProduzione.TabIndex = 1;
            this.cmb_faseProduzione.SelectedIndexChanged += new System.EventHandler(this.cmb_faseIndProblema_SelectedIndexChanged);
            // 
            // rb_codiceParte
            // 
            this.rb_codiceParte.AutoSize = true;
            this.rb_codiceParte.Location = new System.Drawing.Point(18, 222);
            this.rb_codiceParte.Name = "rb_codiceParte";
            this.rb_codiceParte.Size = new System.Drawing.Size(138, 17);
            this.rb_codiceParte.TabIndex = 101;
            this.rb_codiceParte.TabStop = true;
            this.rb_codiceParte.Text = "Codice Parte / Controllo";
            this.rb_codiceParte.UseVisualStyleBackColor = true;
            this.rb_codiceParte.CheckedChanged += new System.EventHandler(this.rb_codiceParte_CheckedChanged);
            // 
            // lbl_rifFlowChart
            // 
            this.lbl_rifFlowChart.AutoSize = true;
            this.lbl_rifFlowChart.Location = new System.Drawing.Point(15, 186);
            this.lbl_rifFlowChart.Name = "lbl_rifFlowChart";
            this.lbl_rifFlowChart.Size = new System.Drawing.Size(113, 13);
            this.lbl_rifFlowChart.TabIndex = 7;
            this.lbl_rifFlowChart.Text = "Riferimento Flow Chart";
            // 
            // cmb_rifFlowChart
            // 
            this.cmb_rifFlowChart.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmb_rifFlowChart.FormattingEnabled = true;
            this.cmb_rifFlowChart.Location = new System.Drawing.Point(166, 183);
            this.cmb_rifFlowChart.Name = "cmb_rifFlowChart";
            this.cmb_rifFlowChart.Size = new System.Drawing.Size(405, 21);
            this.cmb_rifFlowChart.Sorted = true;
            this.cmb_rifFlowChart.TabIndex = 2;
            this.cmb_rifFlowChart.SelectedIndexChanged += new System.EventHandler(this.cmb_difettoRiscontrato_SelectedIndexChanged);
            // 
            // cmb_causaProblema
            // 
            this.cmb_causaProblema.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmb_causaProblema.FormattingEnabled = true;
            this.cmb_causaProblema.Location = new System.Drawing.Point(166, 107);
            this.cmb_causaProblema.Name = "cmb_causaProblema";
            this.cmb_causaProblema.Size = new System.Drawing.Size(405, 21);
            this.cmb_causaProblema.Sorted = true;
            this.cmb_causaProblema.TabIndex = 3;
            // 
            // lbl_difRiscRalco
            // 
            this.lbl_difRiscRalco.AutoSize = true;
            this.lbl_difRiscRalco.Location = new System.Drawing.Point(15, 47);
            this.lbl_difRiscRalco.Name = "lbl_difRiscRalco";
            this.lbl_difRiscRalco.Size = new System.Drawing.Size(143, 13);
            this.lbl_difRiscRalco.TabIndex = 19;
            this.lbl_difRiscRalco.Text = "Difetto Riscontrato Da Ralco";
            // 
            // btn_OK
            // 
            this.btn_OK.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_OK.BackgroundImage")));
            this.btn_OK.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_OK.Location = new System.Drawing.Point(211, 612);
            this.btn_OK.Name = "btn_OK";
            this.btn_OK.Size = new System.Drawing.Size(50, 50);
            this.btn_OK.TabIndex = 9;
            this.btn_OK.UseVisualStyleBackColor = true;
            this.btn_OK.Click += new System.EventHandler(this.btn_okProblema_Click);
            // 
            // btn_cancel
            // 
            this.btn_cancel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_cancel.BackgroundImage")));
            this.btn_cancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_cancel.Location = new System.Drawing.Point(316, 612);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(50, 50);
            this.btn_cancel.TabIndex = 112;
            this.btn_cancel.UseVisualStyleBackColor = true;
            this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
            // 
            // grp_testEseguiti
            // 
            this.grp_testEseguiti.Controls.Add(this.dgv_testDaEseguire);
            this.grp_testEseguiti.Location = new System.Drawing.Point(12, 1);
            this.grp_testEseguiti.Name = "grp_testEseguiti";
            this.grp_testEseguiti.Size = new System.Drawing.Size(589, 154);
            this.grp_testEseguiti.TabIndex = 113;
            this.grp_testEseguiti.TabStop = false;
            this.grp_testEseguiti.Text = "Test eseguiti in fase di analisi del problema";
            // 
            // dgv_testDaEseguire
            // 
            this.dgv_testDaEseguire.AllowUserToAddRows = false;
            this.dgv_testDaEseguire.AllowUserToOrderColumns = true;
            this.dgv_testDaEseguire.BackgroundColor = System.Drawing.SystemColors.ActiveBorder;
            this.dgv_testDaEseguire.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_testDaEseguire.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewCheckBoxColumn2,
            this.descTest,
            this.esitoTest});
            this.dgv_testDaEseguire.Cursor = System.Windows.Forms.Cursors.Default;
            this.dgv_testDaEseguire.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_testDaEseguire.Location = new System.Drawing.Point(3, 16);
            this.dgv_testDaEseguire.Name = "dgv_testDaEseguire";
            this.dgv_testDaEseguire.RowHeadersVisible = false;
            this.dgv_testDaEseguire.Size = new System.Drawing.Size(583, 135);
            this.dgv_testDaEseguire.TabIndex = 7;
            this.dgv_testDaEseguire.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_testDaEseguire_CellContentClick);
            // 
            // dataGridViewCheckBoxColumn2
            // 
            this.dataGridViewCheckBoxColumn2.HeaderText = "";
            this.dataGridViewCheckBoxColumn2.Name = "dataGridViewCheckBoxColumn2";
            this.dataGridViewCheckBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewCheckBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewCheckBoxColumn2.Width = 30;
            // 
            // descTest
            // 
            this.descTest.HeaderText = "Descrizione Test";
            this.descTest.Name = "descTest";
            this.descTest.ReadOnly = true;
            this.descTest.Width = 430;
            // 
            // esitoTest
            // 
            this.esitoTest.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.esitoTest.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.esitoTest.HeaderText = "Esito";
            this.esitoTest.Items.AddRange(new object[] {
            "Pass",
            "Fail"});
            this.esitoTest.Name = "esitoTest";
            this.esitoTest.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.esitoTest.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // Form_Problema
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(613, 671);
            this.Controls.Add(this.grp_testEseguiti);
            this.Controls.Add(this.btn_cancel);
            this.Controls.Add(this.grp_dettProb);
            this.Controls.Add(this.btn_OK);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(629, 709);
            this.MinimumSize = new System.Drawing.Size(629, 709);
            this.Name = "Form_Problema";
            this.Text = "Dettagli Problema";
            this.grp_dettProb.ResumeLayout(false);
            this.grp_dettProb.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_nomeResponsabile)).EndInit();
            this.grp_testEseguiti.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_testDaEseguire)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grp_dettProb;
        private System.Windows.Forms.ComboBox cmb_causaProblema;
        private System.Windows.Forms.Label lbl_causaProb;
        private System.Windows.Forms.ComboBox cmb_rifFlowChart;
        private System.Windows.Forms.Label lbl_rifFlowChart;
        private System.Windows.Forms.ComboBox cmb_faseProduzione;
        private System.Windows.Forms.Label lbl_faseProd;
        private System.Windows.Forms.ComboBox cmb_gravitaProblema;
        private System.Windows.Forms.Label lbl_gravita;
        private System.Windows.Forms.Button btn_OK;
        private System.Windows.Forms.Label lbl_difRiscRalco;
        private System.Windows.Forms.TextBox tb_difRiscDaRalco;
        private System.Windows.Forms.DataGridView dgv_nomeResponsabile;
        private System.Windows.Forms.Button btn_CancelRespo;
        private System.Windows.Forms.Button btn_AddRespo;
        private System.Windows.Forms.DataGridViewCheckBoxColumn chk_Respo;
        private System.Windows.Forms.DataGridViewTextBoxColumn nomeResponsabile;
        private System.Windows.Forms.Button btn_cancel;
        private System.Windows.Forms.RadioButton rb_codiceParte;
        private System.Windows.Forms.GroupBox grp_testEseguiti;
        private System.Windows.Forms.DataGridView dgv_testDaEseguire;
        private System.Windows.Forms.ComboBox cmb_codParteControllo;
        private System.Windows.Forms.Label lbl_descParte;
        private System.Windows.Forms.Label lbl_DescrizParte;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn descTest;
        private System.Windows.Forms.DataGridViewComboBoxColumn esitoTest;
    }
}