﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using Xceed.Words.NET;
using System.Diagnostics;
using System.IO;
using System.Configuration;
using System.Text.RegularExpressions;

namespace Ralco_ServiceManager
{
    public partial class Form_ServiceManager : Form
    {
        private List<RMA_View> rmaList;
        private SQLConnector conn;
        private String query = "";
        private String queryConditions = "";
        private String numberOfLines = "";
        private int dgvColumnToSort = -1;
        public Form_ServiceManager()
        {
            try
            {
                InitializeComponent();
                Utils.setResourceManager();
                checkForUpdates();
                lbl_Utente.Text = Globals.user.getFullUserName();
                this.Text = this.Text + String.Format(" - V{0}", System.Reflection.Assembly.GetExecutingAssembly().GetName().Version);
                rmaList = new List<RMA_View>();
                conn = new SQLConnector();
                enableComponentsByPrivilege();
                setWidgetDescriptions();
                cmb_RMASelection.SelectedIndex = 1;
                cmb_linesPerPage.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Abilita le componenti indicate solo ad utenti admin
        /// </summary>
        private void enableComponentsByPrivilege()
        {
            try
            {
                if (Globals.user.credenziale.Equals("admin"))
                {
                    btn_CloneCNC.Enabled = true;
                    btn_EditMultipleCNC.Enabled = true;
                    btn_DeleteCNC.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Imposta le descrizioni per i vari widget (bottoni, ...) a seconda della lingua dell'utente (impostata con il Utils.setResourceManager();)
        /// </summary>
        private void setWidgetDescriptions()
        {
            try
            {
                #region Tooltips Per Bottoni
                ToolTip tooltip = new System.Windows.Forms.ToolTip();
                btn_updates.ToolTipText = Utils.resourcemanager.GetString("btn_updates");
                btn_collInfo.ToolTipText = Utils.resourcemanager.GetString("btn_collInfo");
                btn_Refresh.ToolTipText = Utils.resourcemanager.GetString("btn_Refresh");
                btn_AddCNC.ToolTipText = Utils.resourcemanager.GetString("btn_AddCNC");
                btn_CloneCNC.ToolTipText = Utils.resourcemanager.GetString("btn_CloneCNC");
                btn_DeleteCNC.ToolTipText = Utils.resourcemanager.GetString("btn_DeleteCNC");
                btn_EditCNC.ToolTipText = Utils.resourcemanager.GetString("btn_EditCNC");
                btn_EditMultipleCNC.ToolTipText = Utils.resourcemanager.GetString("btn_EditMultipleCNC");
                btn_DatabaseSearch.ToolTipText = Utils.resourcemanager.GetString("btn_DatabaseSearch");
                btn_StampaEtichetteRMA.ToolTipText = Utils.resourcemanager.GetString("btn_StampaEtichetteRMA");
                btn_PrintCNC.ToolTipText = Utils.resourcemanager.GetString("btn_PrintCNC");
                btn_ExportCNC.ToolTipText = Utils.resourcemanager.GetString("btn_ExportCNC");
                btn_Logout.ToolTipText = Utils.resourcemanager.GetString("btn_Logout");
                #endregion
                #region Tooltips per Textbox
                tb_ricerca.ToolTipText = Utils.resourcemanager.GetString("tb_ricerca");
                #endregion
                //#region RadioButtons
                //rb_SelectAll.Text = Utils.resourcemanager.GetString("rb_SelectAll");
                //rb_DeselectAll.Text = Utils.resourcemanager.GetString("rb_DeselectAll");
                //#endregion
                #region Label
                //lbl_Utente.Text = Utils.resourcemanager.GetString("lbl_User");
                #endregion
                #region Combobox
                cmb_criterioRicerca.Items.Add(Utils.resourcemanager.GetString("dgv_ListaNonConf_IDRMA"));
                cmb_criterioRicerca.Items.Add(Utils.resourcemanager.GetString("dgv_ListaNonConf_Cliente"));
                cmb_criterioRicerca.Items.Add(Utils.resourcemanager.GetString("dgv_ListaNonConf_ModCollParte"));
                cmb_criterioRicerca.Items.Add(Utils.resourcemanager.GetString("dgv_ListaNonConf_SN"));
                cmb_criterioRicerca.Items.Add(Utils.resourcemanager.GetString("dgv_ListaNonConf_FC"));
                cmb_criterioRicerca.Items.Add(Utils.resourcemanager.GetString("dgv_ListaNonConf_AggiornatoDa"));
                cmb_criterioRicerca.Items.Add(Utils.resourcemanager.GetString("dgv_ListaNonConf_DataScadenza"));
                cmb_criterioRicerca.Items.Add(Utils.resourcemanager.GetString("dgv_ListaNonConf_Priorita"));
                cmb_criterioRicerca.Items.Add(Utils.resourcemanager.GetString("dgv_ListaNonConf_Stato"));
                cmb_criterioRicerca.Items.Add(Utils.resourcemanager.GetString("dgv_ListaNonConf_NumScar"));
                cmb_criterioRicerca.Items.Add(Utils.resourcemanager.GetString("dgv_ListaNonConf_DocReso"));
                cmb_criterioRicerca.Items.Add(Utils.resourcemanager.GetString("dgv_ListaNonConf_NumRiparaz"));
                cmb_criterioRicerca.Items.Add(Utils.resourcemanager.GetString("dgv_ListaNonConf_NoteCliente"));
                cmb_criterioRicerca.Items.Add(Utils.resourcemanager.GetString("dgv_ListaNonConf_Garanzia"));
                #endregion
                #region DataGridViewColumns
                dgv_ListaNonConf.Columns["RMA"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaNonConf_IDRMA");
                dgv_ListaNonConf.Columns["nomeCli"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaNonConf_Cliente");
                dgv_ListaNonConf.Columns["modCollPart"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaNonConf_ModCollParte");
                dgv_ListaNonConf.Columns["numCollimatori"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaNonConf_Tot");
                dgv_ListaNonConf.Columns["SN"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaNonConf_SN");
                dgv_ListaNonConf.Columns["FC"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaNonConf_FC");
                dgv_ListaNonConf.Columns["DataCreazione"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaNonConf_DataCreaz");
                dgv_ListaNonConf.Columns["DataUpdate"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaNonConf_UltimaModifica");
                dgv_ListaNonConf.Columns["aggiornDa"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaNonConf_AggiornatoDa");
                dgv_ListaNonConf.Columns["dataScad"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaNonConf_DataScadenza");
                dgv_ListaNonConf.Columns["Priorita"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaNonConf_Priorita");
                dgv_ListaNonConf.Columns["Stato"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaNonConf_Stato");
                dgv_ListaNonConf.Columns["numSCAR"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaNonConf_NumScar");
                dgv_ListaNonConf.Columns["docReso"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaNonConf_DocReso");
                dgv_ListaNonConf.Columns["ordRip"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaNonConf_NumRiparaz");
                dgv_ListaNonConf.Columns["noteCliente"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaNonConf_NoteCliente");
                dgv_ListaNonConf.Columns["garanzia"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaNonConf_Garanzia");
                #endregion
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void checkForUpdates()
        {
            try
            {
                removeBakFiles();
                string pathServer = ConfigurationManager.AppSettings["SERVER_UPDATE_APP_PATH"];
                if (pathServer != null)
                {
                    string[] dirs = Directory.GetFiles(pathServer, "Ralco_ServiceManager.exe");
                    if (dirs.Length.Equals(1))
                    {
                        FileInfo serverFileInfo = new FileInfo(dirs[0]);
                        FileInfo appFileInfo = new FileInfo(Environment.CurrentDirectory + @"\Ralco_ServiceManager.exe");
                        if (serverFileInfo.LastWriteTime > appFileInfo.LastWriteTime)
                        {
                            btn_updates.Enabled = true;
                            btn_updates.BackgroundImage = System.Drawing.Image.FromFile(Environment.CurrentDirectory + @"\img\updatesAvailable.png");
                        }
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //gestione funzionalità

        private void btn_updates_Click(object sender, EventArgs e)
        {
            try
            {
                string pathServer = ConfigurationManager.AppSettings["SERVER_UPDATE_APP_PATH"];
                FileInfo serverFileInfo;
                FileInfo localFileInfo;
                string[] pathFiles = Directory.GetFiles(pathServer);
                if (pathServer != null)
                {
                    for (int i = 0; i < pathFiles.Length; i++)
                    {
                        string fileName = Path.GetFileName(pathFiles[i]);
                        serverFileInfo = new FileInfo(pathFiles[i]);
                        localFileInfo = new FileInfo(Environment.CurrentDirectory + @"\" + fileName);
                        if (!fileName.Equals("Ralco_ServiceManager.exe"))
                        {
                            if (serverFileInfo.LastWriteTime > localFileInfo.LastWriteTime)
                                File.Copy(serverFileInfo.ToString(), localFileInfo.ToString(), true);
                        }
                        else
                        {
                            File.Move(Environment.CurrentDirectory + @"\Ralco_ServiceManager.exe", Environment.CurrentDirectory + @"\Ralco_ServiceManager.bak");
                            File.Copy(serverFileInfo.ToString(), localFileInfo.ToString(), true);
                        }
                    }
                    DialogResult result = MessageBox.Show("Il programma verrà riavviato per consentire l\'installazione degli aggiornamenti.\nPremere OK per continuare...", "Informazione", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    if (result.Equals(DialogResult.OK))
                    {
                        //File.Delete(Environment.CurrentDirectory + @"\Ralco_ServiceManager.bak");
                        this.Dispose();
                        Application.Restart();
                    }
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void removeBakFiles()
        {
            try
            {
                string[] dirs = Directory.GetFiles(Environment.CurrentDirectory, "Ralco_ServiceManager.bak");
                if (!dirs.Length.Equals(0))
                    File.Delete(dirs[0]);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_collInfo_Click(object sender, EventArgs e)
        {
            try
            {
                using (var form = new Form_Collimatore())
                {
                    var result = form.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Btn_Refresh_Click(object sender, EventArgs e)
        {
            try
            {
                //tb_ricerca.Text = "";
                //cmb_criterioRicerca.SelectedIndex = -1;
                this.queryConditions = "";
                Cmb_linesPerPage_TextChanged(null, null);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Cmb_linesPerPage_TextChanged(object sender, EventArgs e)
        {
            try
            {
                numberOfLines = cmb_linesPerPage.SelectedItem.ToString().Split(null)[0].Equals("Tutto") ? "" : "TOP (" + cmb_linesPerPage.SelectedItem.ToString().Split(null)[0] + ") ";
                refreshListData();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //refresh di tutte le non conformità attive
        private void refreshListData(params String[] query)
        {
            checkForUpdates();
            string modelli;
            string seriali;
            string numFlowChart;
            string causeProb;
            string difettiRiscRalco;
            if (query.Length.Equals(0))
                this.query = "";
            try
            {
                if (this.query.Equals(""))
                {
                    if (Globals.user.getUserCredential().Equals("NonConfInt"))
                        this.query = "SELECT " + numberOfLines + " * FROM [AutoTest].[dbo].[SM_MainView2] WHERE [AutoTest].[dbo].[SM_MainView2].[tipoRMA] = 'Interno'  AND [AutoTest].[dbo].[SM_MainView2].[location] LIKE '%" + Globals.user.getUserLocation() + "' ORDER BY [AutoTest].[dbo].[SM_MainView2].[dataCreazione] DESC";//, [AutoTest].[dbo].[SM_MainView2].[dataScadenza] ASC";
                    else if (Globals.user.getUserCredential().Equals("NonConfExt") || Globals.user.getUserCredential().Equals("admin") || Globals.user.getUserCredential().Equals("admin"))
                        this.query = "SELECT  " + numberOfLines + " * FROM [AutoTest].[dbo].[SM_MainView2] WHERE [AutoTest].[dbo].[SM_MainView2].[tipoRMA] = 'Esterno'  AND [AutoTest].[dbo].[SM_MainView2].[location] = '" + Globals.user.getUserLocation() + "' ORDER BY [AutoTest].[dbo].[SM_MainView2].[dataCreazione] DESC";//"' ORDER BY [AutoTest].[dbo].[SM_MainView2].[priorita] DESC,[AutoTest].[dbo].[SM_MainView2].[dataScadenza] ASC";
                }
                dgv_ListaNonConf.Rows.Clear();
                if (rmaList != null)
                    rmaList.Clear();
                else
                    rmaList = new List<RMA_View>();
                rmaList = (List<RMA_View>)conn.CreateCommand(this.query, rmaList);
                if (rmaList != null)
                {
                    rmaList.Reverse();
                    //if (dgvColumnToSort.Equals(10))
                    //{
                    //    List<RMA_View> rmaList_Part1 = rmaList.OrderBy(r => r.idRMA)
                    //         .Where(r => r.dataScadenza.ToString().Equals("01/01/1753 00:00:00"))
                    //         .Select(r => r)
                    //         .ToList();
                    //    List<RMA_View> rmaList_Part2 = rmaList.OrderByDescending(r => r.dataScadenza)
                    //             .Where(r => !r.dataScadenza.ToString().Equals("01/01/1753 00:00:00") && r.stato.Equals("Chiuso"))
                    //             .Select(r => r)
                    //             .ToList();
                    //    List<RMA_View> rmaList_Part3 = rmaList.OrderByDescending(r => r.dataScadenza)
                    //             .Where(r => !r.dataScadenza.ToString().Equals("01/01/1753 00:00:00") && !r.stato.Equals("Chiuso"))
                    //             .Select(r => r)
                    //             .ToList();
                    //    rmaList.Clear();
                    //    rmaList.AddRange(rmaList_Part1);
                    //    rmaList.AddRange(rmaList_Part2);
                    //    rmaList.AddRange(rmaList_Part3);
                    //    dgvColumnToSort = -1;
                    //}
                    foreach (RMA_View r in rmaList)
                    {
                        int numCollimatori = 0;
                        modelli = "";
                        seriali = "";
                        numFlowChart = "";
                        causeProb = "";
                        difettiRiscRalco = "";
                        foreach (string m in r.modelloCollim.Distinct())
                            if (modelli.Equals(""))
                                modelli = modelli + m;
                            else
                                modelli = modelli + "\n" + m;
                        foreach (string s in r.serialeCollim.Distinct())
                        {
                            if (seriali.Equals(""))
                                seriali = seriali + s;
                            else
                                seriali = seriali + "\n" + s;
                            numCollimatori++;
                        }
                        foreach (string f in r.numFlowChart.Distinct())
                            if (numFlowChart.Equals(""))
                                numFlowChart = numFlowChart + f;
                            else
                                numFlowChart = numFlowChart + "\n" + f;
                        foreach (string c in r.causaProblema.Distinct())
                            if (causeProb.Equals(""))
                                causeProb = causeProb + c;
                            else
                                causeProb = causeProb + "\n" + c;
                        foreach (string d in r.difRiscDaRalco.Distinct())
                            if (difettiRiscRalco.Equals(""))
                                difettiRiscRalco = difettiRiscRalco + d;
                            else
                                difettiRiscRalco = difettiRiscRalco + "\n" + d;
                        this.dgv_ListaNonConf.Rows.Insert(0, false, r.idRMA, r.nomeCliente, modelli, numCollimatori.ToString(), seriali, numFlowChart, r.dataCreazione, r.dataAggiornamento.ToShortDateString(), r.aggiornatoDa, r.dataScadenza.ToShortDateString(), r.priorita, r.stato, r.numSCAR, r.docReso, r.numOrdRip, r.noteCliente,r.statoGaranzia,causeProb,difettiRiscRalco,r.provCollimatore);
                        if (r.dataScadenza.ToString().Equals("01/01/1753 00:00:00") || r.dataScadenza.ToString().Equals("01/01/0001 00:00:00"))
                        {
                            //se la data è quella minima, non la faccio visualizzare (il valore deve essere sempre inserito come data, altrimenti non si può eseguire l'ordinamento)
                            this.dgv_ListaNonConf.Rows[0].Cells[10].Style.ForeColor = Color.Transparent;
                            this.dgv_ListaNonConf.Rows[0].Cells[10].Style.SelectionForeColor = Color.Transparent;
                        }
                            
                        if (r.idRMA.Contains("CNC"))
                            if (!r.dataScadenza.ToString().Equals("01/01/1753 00:00:00") && r.dataScadenza.Subtract(new TimeSpan(7, 0, 0, 0)) <= DateTime.Now && !r.stato.Equals("Chiuso") && !r.stato.Equals("Da Bonificare"))
                                this.dgv_ListaNonConf.Rows[0].DefaultCellStyle.BackColor = Color.Salmon;
                    }
                }
                //Dat creazione e Data Ultima modifica li metto invisibili. Rimuovere il commento per scopi futuri, se necessario
                dgv_ListaNonConf.Columns[7].Visible = true;
                dgv_ListaNonConf.Columns[8].Visible = true;
                //dgv_ListaNonConf.Sort(dgv_ListaNonConf.Columns[11], ListSortDirection.Ascending);
                //dgv_ListaNonConf.Refresh();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_AddCNC_Click(object sender, EventArgs e)
        {
            try
            {
                using (var form = new Form_RMA(null))
                {
                    var result = form.ShowDialog();
                    //if (result.Equals(DialogResult.Abort))
                    //    removetempRMA();
                    Btn_Refresh_Click(null, null);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// usata nel caso un utentr
        /// </summary>
        private void removetempRMA()
        {
            try
            {
                string idRMAToDelete = "", lastID_RMA = "";
                lastID_RMA = (string)conn.CreateCommand("SELECT TOP 1 idRMA FROM [Autotest].[dbo].[RMA_Main] ORDER BY [Autotest].[dbo].[RMA_Main].[idProgressivo] DESC", lastID_RMA);
                idRMAToDelete = (string)conn.CreateCommand("SELECT [idRMA] FROM [Autotest].[dbo].[RMA_Generali] WHERE [Autotest].[dbo].[RMA_Generali].[idRMA] = '" + lastID_RMA + "'", idRMAToDelete);
                if (idRMAToDelete.Equals(""))//non ho mai premuto Save -> idRMA è presente SOLO in Form_Main
                    conn.CreateCommand("DELETE FROM [Autotest].[dbo].[RMA_Main] " +
                    "WHERE [Autotest].[dbo].[RMA_Main].[idRMA] = '" + lastID_RMA + "'", null);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_CloneCNC_Click(object sender, EventArgs e)
        {
            try
            {
                List<RMA_View> itemToClone = new List<RMA_View>();
                foreach (DataGridViewRow row in dgv_ListaNonConf.Rows)
                {
                    if (row.Cells.GetCellValueFromColumnHeader("").Equals(true))
                    {
                        itemToClone.Add(rmaList.Single(r => r.idRMA.Equals(row.Cells.GetCellValueFromColumnHeader("ID RMA"))));
                    }
                }
                if (itemToClone != null && itemToClone.Count.Equals(1))
                {
                    RMA_View rmaCloned = new RMA_View(itemToClone[0]);
                    using (var form = new Form_RMA(rmaCloned, true))
                    {
                        var result = form.ShowDialog();
                        Btn_Refresh_Click(null, null);
                    }
                }
                else if (itemToClone.Count >= 2)
                    MessageBox.Show("Non è possibile clonare più di un RMA per volta", "Informazione", MessageBoxButtons.OK, MessageBoxIcon.Information);
                else
                    MessageBox.Show("Selezionare un RMA da clonare", "Informazione", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_DeleteCNC_Click(object sender, EventArgs e)
        {
            try
            {
                List<RMA_View> itemsToRemove = new List<RMA_View>();
                foreach (DataGridViewRow row in dgv_ListaNonConf.Rows)
                {
                    if (row.Cells.GetCellValueFromColumnHeader("").Equals(true))
                    {
                        DialogResult res = MessageBox.Show("Questa operazione rimuoverà definitivamente gli RMA selezionati dal sistema.\nSei sicuro di voler procedere?", "Error", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (res.Equals(DialogResult.Yes))
                        {
                            itemsToRemove.Add(rmaList.Single(r => r.idRMA.Equals(row.Cells.GetCellValueFromColumnHeader("ID RMA"))));
                            foreach (var v in itemsToRemove)
                                if (rmaList.Contains(v))
                                    rmaList.Remove(v);
                            Object retVal = null;
                            retVal = (Object)conn.CreateCommand("DELETE FROM [AutoTest].[dbo].[RMA_Articoli_Preventivo] WHERE [AutoTest].[dbo].[RMA_Articoli_Preventivo].[idRMA] = '" + row.Cells.GetCellValueFromColumnHeader("ID RMA") + "'", retVal);
                            retVal = (Object)conn.CreateCommand("DELETE FROM [AutoTest].[dbo].[RMA_Collimatore] WHERE [AutoTest].[dbo].[RMA_Collimatore].[idRMA] = '" + row.Cells.GetCellValueFromColumnHeader("ID RMA") + "'", retVal);
                            retVal = (Object)conn.CreateCommand("DELETE FROM [AutoTest].[dbo].[RMA_Parte] WHERE [AutoTest].[dbo].[RMA_Parte].[idRMA] = '" + row.Cells.GetCellValueFromColumnHeader("ID RMA") + "'", retVal);
                            retVal = (Object)conn.CreateCommand("DELETE FROM [AutoTest].[dbo].[RMA_FasiProduzioneDaRieseguire] WHERE [AutoTest].[dbo].[RMA_FasiProduzioneDaRieseguire].[idRMA] = '" + row.Cells.GetCellValueFromColumnHeader("ID RMA") + "'", retVal);
                            retVal = (Object)conn.CreateCommand("DELETE FROM [AutoTest].[dbo].[RMA_GaranziaRalco] WHERE [AutoTest].[dbo].[RMA_GaranziaRalco].[idRMA] = '" + row.Cells.GetCellValueFromColumnHeader("ID RMA") + "'", retVal);
                            retVal = (Object)conn.CreateCommand("DELETE FROM [AutoTest].[dbo].[RMA_Generali] WHERE [AutoTest].[dbo].[RMA_Generali].[idRMA] = '" + row.Cells.GetCellValueFromColumnHeader("ID RMA") + "'", retVal);
                            retVal = (Object)conn.CreateCommand("DELETE FROM [AutoTest].[dbo].[RMA_InformativaCliente] WHERE [AutoTest].[dbo].[RMA_InformativaCliente].[idRMA] = '" + row.Cells.GetCellValueFromColumnHeader("ID RMA") + "'", retVal);
                            retVal = (Object)conn.CreateCommand("DELETE FROM [AutoTest].[dbo].[RMA_Log] WHERE [AutoTest].[dbo].[RMA_Log].[idRMA] = '" + row.Cells.GetCellValueFromColumnHeader("ID RMA") + "'", retVal);
                            retVal = (Object)conn.CreateCommand("DELETE FROM [AutoTest].[dbo].[RMA_Main] WHERE [AutoTest].[dbo].[RMA_Main].[idRMA] = '" + row.Cells.GetCellValueFromColumnHeader("ID RMA") + "'", retVal);
                            retVal = (Object)conn.CreateCommand("DELETE FROM [AutoTest].[dbo].[RMA_NoteQualita] WHERE [AutoTest].[dbo].[RMA_NoteQualita].[idRMA] = '" + row.Cells.GetCellValueFromColumnHeader("ID RMA") + "'", retVal);
                            retVal = (Object)conn.CreateCommand("DELETE FROM [AutoTest].[dbo].[RMA_Preventivo] WHERE [AutoTest].[dbo].[RMA_Preventivo].[idRMA] = '" + row.Cells.GetCellValueFromColumnHeader("ID RMA") + "'", retVal);
                            retVal = (Object)conn.CreateCommand("DELETE FROM [AutoTest].[dbo].[RMA_Problema] WHERE [AutoTest].[dbo].[RMA_Problema].[idRMA] = '" + row.Cells.GetCellValueFromColumnHeader("ID RMA") + "'", retVal);
                            retVal = (Object)conn.CreateCommand("DELETE FROM [AutoTest].[dbo].[RMA_Riparazione] WHERE [AutoTest].[dbo].[RMA_Riparazione].[idRMA] = '" + row.Cells.GetCellValueFromColumnHeader("ID RMA") + "'", retVal);
                            retVal = (Object)conn.CreateCommand("DELETE FROM [AutoTest].[dbo].[RMA_TestDaEseguire] WHERE [AutoTest].[dbo].[RMA_TestDaEseguire].[idRMA] = '" + row.Cells.GetCellValueFromColumnHeader("ID RMA") + "'", retVal);
                            refreshListData();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_EditCNC_Click(object sender, EventArgs e)
        {
            try
            {
                int index = 0;//indice usato per determinare quale elemento della lista modificare
                rmaList.Reverse();//inverto l'ordine degli elementi della lista perchè list.Add aggiunge in Append, mentre nella datagridview aggiungo in testa...
                List<RMA_View> itemToEdit = new List<RMA_View>();
                foreach (DataGridViewRow row in dgv_ListaNonConf.Rows)
                {
                    if (row.Cells.GetCellValueFromColumnHeader("").Equals(true))
                    {
                        index = row.Index;
                        itemToEdit.Add(rmaList.Single(r => r.idRMA.Equals(row.Cells.GetCellValueFromColumnHeader("ID RMA"))));
                    }
                }
                if (itemToEdit != null && itemToEdit.Count.Equals(1))
                {
                    using (var form = new Form_RMA(itemToEdit[0]))
                    {
                        var result = form.ShowDialog();
                        Btn_Refresh_Click(null, null);
                    }
                    rmaList.Reverse();
                }
                else if (itemToEdit.Count >= 2)
                    MessageBox.Show("Non è possibile modificare più di un RMA per volta", "Informazione", MessageBoxButtons.OK, MessageBoxIcon.Information);
                else
                    MessageBox.Show("Selezionare un RMA da modificare", "Informazione", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_EditMultipleCNC_Click(object sender, EventArgs e)
        {
            try
            {
                try
                {
                    int index = 0;//indice usato per determinare quale elemento della lista modificare
                    rmaList.Reverse();//inverto l'ordine degli elementi della lista perchè list.Add aggiunge in Append, mentre nella datagridview aggiungo in testa...
                    List<RMA_View> itemToEdit = new List<RMA_View>();
                    foreach (DataGridViewRow row in dgv_ListaNonConf.Rows)
                    {
                        if (row.Cells.GetCellValueFromColumnHeader("").Equals(true))
                        {
                            index = row.Index;
                            itemToEdit.Add(rmaList.Single(r => r.idRMA.Equals(row.Cells.GetCellValueFromColumnHeader("ID RMA"))));
                        }
                    }
                    if (itemToEdit != null && !itemToEdit.Count.Equals(0))
                    {
                        using (var form = new Form_RMA_AggMassivo(itemToEdit))
                        {
                            var result = form.ShowDialog();
                            Btn_Refresh_Click(null, null);
                        }
                        rmaList.Reverse();
                    }
                    else
                        MessageBox.Show("Selezionare un RMA da modificare", "Informazione", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Btn_DatabaseSearch_Click(object sender, EventArgs e)
        {
            try
            {
                if (cmb_criterioRicerca.SelectedItem.Equals(""))
                    MessageBox.Show("Selezionare un criterio per la ricerca nel Database", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else if (tb_ricerca.Text.Equals(""))
                    MessageBox.Show("Inserire l'elemento da ricercare", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                {
                    String[] query = new String[1];
                    string nomeCampoDB = "";
                    if (cmb_criterioRicerca.SelectedItem.Equals(Utils.resourcemanager.GetString("dgv_ListaNonConf_IDRMA")))
                        nomeCampoDB = "idRMA";
                    else if (cmb_criterioRicerca.SelectedItem.Equals(Utils.resourcemanager.GetString("dgv_ListaNonConf_Cliente")))
                        nomeCampoDB = "nomeCliente";
                    else if (cmb_criterioRicerca.SelectedItem.Equals(Utils.resourcemanager.GetString("dgv_ListaNonConf_ModCollParte")))
                        nomeCampoDB = "modelloColl";
                    else if (cmb_criterioRicerca.SelectedItem.Equals(Utils.resourcemanager.GetString("dgv_ListaNonConf_SN")))
                        nomeCampoDB = "serialeColl";
                    else if (cmb_criterioRicerca.SelectedItem.Equals(Utils.resourcemanager.GetString("dgv_ListaNonConf_FC")))
                        nomeCampoDB = "numFlowChart";
                    else if (cmb_criterioRicerca.SelectedItem.Equals(Utils.resourcemanager.GetString("dgv_ListaNonConf_DataScadenza")))
                        nomeCampoDB = "dataScadenza";
                    else if (cmb_criterioRicerca.SelectedItem.Equals(Utils.resourcemanager.GetString("dgv_ListaNonConf_AggiornatoDa")))
                        nomeCampoDB = "aggiornatoDa";
                    else if (cmb_criterioRicerca.SelectedItem.Equals(Utils.resourcemanager.GetString("dgv_ListaNonConf_Priorita")))
                        nomeCampoDB = "priorita";
                    else if (cmb_criterioRicerca.SelectedItem.Equals(Utils.resourcemanager.GetString("dgv_ListaNonConf_Stato")))
                        nomeCampoDB = "stato";
                    else if (cmb_criterioRicerca.SelectedItem.Equals(Utils.resourcemanager.GetString("dgv_ListaNonConf_NumScar")))
                        nomeCampoDB = "numScarCli";
                    else if (cmb_criterioRicerca.SelectedItem.Equals(Utils.resourcemanager.GetString("dgv_ListaNonConf_DocReso")))
                        nomeCampoDB = "docReso";
                    else if (cmb_criterioRicerca.SelectedItem.Equals(Utils.resourcemanager.GetString("dgv_ListaNonConf_NumRiparaz")))
                        nomeCampoDB = "ordRiparazione";
                    else if (cmb_criterioRicerca.SelectedItem.Equals(Utils.resourcemanager.GetString("dgv_ListaNonConf_NoteCliente")))
                        nomeCampoDB = "noteCliente";
                    else if (cmb_criterioRicerca.SelectedItem.Equals(Utils.resourcemanager.GetString("dgv_ListaNonConf_Garanzia")))
                        nomeCampoDB = "statoGaranzia";
                    if (!nomeCampoDB.Equals(""))
                    {
                        string[] elements = tb_ricerca.Text.Split(';');
                        string condition = "";
                        for (int i = 0; i < elements.Length; i++)
                            if (condition.Equals(""))
                                condition = nomeCampoDB + " LIKE '%" + elements[i] + "%'";
                            else
                                condition = condition + " OR " + nomeCampoDB + " LIKE '%" + elements[i] + "%'";
                        if (this.queryConditions.Equals(""))
                            this.queryConditions = condition;
                        else
                            this.queryConditions = '(' + this.queryConditions + ") AND (" + condition + ')';
                        this.query = "SELECT  " + numberOfLines + " * FROM [AutoTest].[dbo].[SM_MainView2] WHERE " + this.queryConditions + " AND [AutoTest].[dbo].[SM_MainView2].[location] LIKE '%" + Globals.user.getUserLocation() + "%' ORDER BY [AutoTest].[dbo].[SM_MainView2].[dataAggiornamento]";
                        query[0] = this.query;
                        refreshListData(query);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_Logout_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult dr = MessageBox.Show("Vuoi davvero uscire da Ralco Service Manager?", "Uscita", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                switch (dr)
                {
                    case DialogResult.No:
                        break;
                    default:
                        {
                            removeBakFiles();
                            Application.Exit();
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgv_ListaNonConf_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                Utils.isSortColumnPressed = true;
                dgvColumnToSort = e.ColumnIndex;
                if (dgvColumnToSort.Equals(10))
                    refreshListData();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgv_ListaNonConf_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            Utils.DataGridView_CellContentClick(dgv_ListaNonConf, 1);
        }

        private void btn_StampaEtichetteRMA_Click(object sender, EventArgs e)
        {
            try
            {
                List<RMA_View> itemToPrintList = new List<RMA_View>();
                foreach (DataGridViewRow row in dgv_ListaNonConf.Rows)
                {
                    if (row.Cells.GetCellValueFromColumnHeader("").Equals(true))
                    {
                        itemToPrintList.Add(rmaList.Single(r => r.idRMA.Equals(row.Cells.GetCellValueFromColumnHeader("ID RMA"))));
                    }
                }
                if (itemToPrintList != null && itemToPrintList.Count >=1)
                {
                    string path = @"\\ralcoserver01\Testi\1 QUALITA' ITALIA-USA\MODULI_FORMS\MCCQ001-9 CONTROLLO QUALITA' PRODUZIONE FINALE\MCCQ001-9 Controllo Qualità Produzione Finale.docx";
                    var doc = DocX.Load(path);
                    var table = doc.Tables[0];
                    //conto il numero totale di etichette da stampare
                    int totEtichette = 0;
                    foreach (RMA_View r in itemToPrintList)
                    {
                        foreach (string ser in r.serialeCollim.Distinct())
                            totEtichette++;
                    }
                    //creo in ogni pagina il numero di etichette giusto
                    for (int i = 0; i < totEtichette-1; i++)
                    {
                        if (i!=0)
                            doc.InsertParagraph();
                        doc.InsertTable(table);
                    }
                    int numCollPerRMA = itemToPrintList[0].serialeCollim.Distinct().Count();
                    int idxRMA = 0;
                    for (int i = 0, j = 0; i < totEtichette; i++)
                    {
                        doc.Tables[i].Rows[1].Cells[2].ReplaceText("#DATE#", itemToPrintList[idxRMA].dataCreazione.ToShortDateString());
                        doc.Tables[i].Rows[3].Cells[0].ReplaceText("#CUSTOMER#", itemToPrintList[idxRMA].nomeCliente);
                        doc.Tables[i].Rows[3].Cells[1].ReplaceText("#COLLTYPE#", itemToPrintList[idxRMA].modelloCollim[j]);
                        doc.Tables[i].Rows[3].Cells[2].ReplaceText("#SER#", itemToPrintList[idxRMA].serialeCollim[j]);
                        doc.Tables[i].Rows[3].Cells[3].ReplaceText("#CNC#", itemToPrintList[idxRMA].idRMA);
                        doc.Tables[i].Rows[3].Cells[5].ReplaceText("#SER#", itemToPrintList[idxRMA].serialeCollim[j]);
                        numCollPerRMA = numCollPerRMA - 1;
                        j++;
                        if (numCollPerRMA.Equals(0))
                        {
                            idxRMA++;
                            if (idxRMA < itemToPrintList.Count)
                            {
                                numCollPerRMA = itemToPrintList[idxRMA].serialeCollim.Distinct().Count();
                                j = 0;
                            }
                                
                        }
                    }
                    //salvataggio del file nella cartella temporanea locale
                    if (!Directory.Exists(Environment.CurrentDirectory + @"\RMA\"))
                    Directory.CreateDirectory(Environment.CurrentDirectory + @"\RMA\");
                    doc.SaveAs(Environment.CurrentDirectory + @"\RMA\RMA_TMP.docx");
                    // Open in Word:
                    Process.Start("WINWORD.EXE", Environment.CurrentDirectory + @"\RMA\RMA_TMP.docx");
                }
                //else if (itemToPrintList.Count >= 2)
                //    MessageBox.Show("Non è possibile stampare le etichette di più di un RMA per volta", "Informazione", MessageBoxButtons.OK, MessageBoxIcon.Information);
                else
                    MessageBox.Show("Selezionare un RMA", "Informazione", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_PrintCNC_Click(object sender, EventArgs e)
        {
            try
            {
                rmaList.Reverse();//inverto l'ordine degli elementi della lista perchè list.Add aggiunge in Append, mentre nella datagridview aggiungo in testa...
                List<RMA_View> itemToEdit = new List<RMA_View>();
                foreach (DataGridViewRow row in dgv_ListaNonConf.Rows)
                {
                    if (row.Cells.GetCellValueFromColumnHeader("").Equals(true))
                    {
                        itemToEdit.Add(rmaList.Single(r => r.idRMA.Equals(row.Cells[1].Value)));
                    }
                }
                if (itemToEdit != null && !itemToEdit.Count.Equals(0))
                {
                    for (int k = 0; k < itemToEdit.Count; k++)
                    {
                        //reperimento info per non conformità INTERNE
                        RMA_Generali rmaGen = new RMA_Generali();
                        rmaGen = (RMA_Generali)conn.CreateCommand("SELECT * FROM [Autotest].[dbo].[RMA_Generali] WHERE [Autotest].[dbo].[RMA_Generali].[idRMA] = '" + itemToEdit[k].idRMA + "'", rmaGen);
                        RMA_InformativaCliente infoCli = new RMA_InformativaCliente();
                        infoCli = (RMA_InformativaCliente)conn.CreateCommand("SELECT * FROM [Autotest].[dbo].[RMA_InformativaCliente] WHERE [Autotest].[dbo].[RMA_InformativaCliente].[idRMA] = '" + itemToEdit[k].idRMA + "'", infoCli);
                        RMA_GaranziaRalco garanziaRalco = new RMA_GaranziaRalco();
                        garanziaRalco = (RMA_GaranziaRalco)conn.CreateCommand("SELECT * FROM [Autotest].[dbo].[RMA_GaranziaRalco] WHERE [Autotest].[dbo].[RMA_GaranziaRalco].[idRMA] = '" + itemToEdit[k].idRMA + "'", garanziaRalco);
                        List<RMA_Collimatore> collList = new List<RMA_Collimatore>();
                        collList = (List<RMA_Collimatore>)conn.CreateCommand("SELECT * FROM [Autotest].[dbo].[RMA_Collimatore] WHERE [Autotest].[dbo].[RMA_Collimatore].[idRMA] = '" + itemToEdit[k].idRMA + "'", collList);
                        List<RMA_Parte> partList = new List<RMA_Parte>();
                        partList = (List<RMA_Parte>)conn.CreateCommand("SELECT * FROM [Autotest].[dbo].[RMA_Parte] WHERE [Autotest].[dbo].[RMA_Parte].[idRMA] = '" + itemToEdit[k].idRMA + "'", partList);
                        List<RMA_Problema> probList = new List<RMA_Problema>();
                        probList = (List<RMA_Problema>)conn.CreateCommand("SELECT * FROM [Autotest].[dbo].[RMA_Problema] WHERE [Autotest].[dbo].[RMA_Problema].[idRMA] = '" + itemToEdit[k].idRMA + "'", probList);
                        List<String> nomeFaseDaRieseguire = new List<String>();
                        nomeFaseDaRieseguire = (List<String>)conn.CreateCommand("SELECT [descrizFaseProduzione] FROM [Autotest].[dbo].[RMA_FasiProduzione]", nomeFaseDaRieseguire);
                        RMA_FaseProduzione minFasiDaRies = new RMA_FaseProduzione();
                        minFasiDaRies = (RMA_FaseProduzione)conn.CreateCommand("SELECT * FROM [Autotest].[dbo].[RMA_FasiProduzioneDaRieseguire] WHERE [Autotest].[dbo].[RMA_FasiProduzioneDaRieseguire].[idRMA] = '" + itemToEdit[k].idRMA + "'", minFasiDaRies);
                        List<RMA_Riparazione> ripList = new List<RMA_Riparazione>();
                        ripList = (List<RMA_Riparazione>)conn.CreateCommand("SELECT * FROM [Autotest].[dbo].[RMA_Riparazione] WHERE [Autotest].[dbo].[RMA_Riparazione].[idRMA] = '" + itemToEdit[k].idRMA + "'", ripList);
                        RMA_NoteQualita noteQualita = new RMA_NoteQualita();
                        noteQualita = (RMA_NoteQualita)conn.CreateCommand("SELECT * FROM [Autotest].[dbo].[RMA_NoteQualita] WHERE [Autotest].[dbo].[RMA_NoteQualita].[idRMA] = '" + itemToEdit[k].idRMA + "'", noteQualita);
                        List<RMA_TestDaEseguire> testDaEseguireList = new List<RMA_TestDaEseguire>();
                        testDaEseguireList = (List<RMA_TestDaEseguire>)conn.CreateCommand("SELECT * FROM [Autotest].[dbo].[RMA_TestDaEseguire] WHERE [Autotest].[dbo].[RMA_TestDaEseguire].[idRMA] = '" + itemToEdit[k].idRMA + "'", testDaEseguireList);
                        //stampa NON CONFORMITA INTERNE
                        try
                        {
                            string path = "";
                            if (itemToEdit[k].idRMA.Contains("NCI"))
                                path = path + @"\\ralcoserver01\Testi\1 QUALITA' ITALIA-USA\MODULI_FORMS\MGTQ007-3 COLLIMATORE NON CONFORME PRODUZIONE\MGTQ007-3_NON_CONFORMITA_INTERNA.docx";
                            else
                                path = path + @"\\ralcoserver01\Testi\1 QUALITA' ITALIA-USA\MODULI_FORMS\MGTQ007 COLLIMATORE NON CONFORME\MGTQ007_NON_CONFORMITA_ESTERNA.docx";
                            var doc = DocX.Load(path);
                            var table = doc.Tables[0];
                            int actualRow = 0;
                            //Sezione informazioni generali
                            doc.ReplaceText("@IDRMA@", rmaGen.idRMA);
                            //doc.ReplaceText("@TYPE@", rmaGen.tipoRMA);
                            doc.ReplaceText("@OWNER@", Globals.user.getFullUserName());
                            doc.ReplaceText("@DATE@", rmaGen.dataCreazione.ToShortDateString());
                            //doc.ReplaceText("@STATUS@", rmaGen.stato);
                            if (itemToEdit[k].idRMA.Contains("CNC"))
                                doc.ReplaceText("@CUSTOMER@", infoCli.cliente.nomeCliente.ToString());//in una lista di collimatori ci sono solo collimatori dello stesso tipo
                            else
                                doc.ReplaceText("@CUSTOMER@", "");//in una lista di collimatori ci sono solo collimatori dello stesso tipo
                            if (collList != null)
                                doc.ReplaceText("@MODEL@", collList[0].modelloColl.ToString());//in una lista di collimatori ci sono solo collimatori dello stesso tipo
                            else if (partList != null)
                            {
                                List<String> idPartiList = partList.Select(o => o.idParte).Distinct().ToList();
                                string idParti = "";
                                if (idPartiList != null)
                                    foreach (string s in idPartiList)
                                        if (idParti == "")
                                            idParti = idParti + s;
                                        else
                                            idParti = idParti + "\n" + s;
                                doc.ReplaceText("@MODEL@", idParti);//in una lista di parti ci possono essere parti anche di tipo differente
                            }
                            string seriali = "";
                            string flowChart = "";
                            int totSerPerLine = 0;
                            if (collList != null)
                            {
                                collList = collList.OrderBy(o => o.numFlowChart).ToList();
                                foreach (RMA_Collimatore c in collList)
                                    if (seriali.Equals(""))
                                    {
                                        seriali = seriali + c.seriale;
                                        totSerPerLine++;
                                        flowChart = flowChart + c.numFlowChart;
                                    }
                                    else
                                    {
                                        if (!flowChart.Contains(c.numFlowChart))
                                        {
                                            flowChart = flowChart + "\n\n" + c.numFlowChart;
                                            seriali = seriali + "\n\n" + c.seriale;
                                            totSerPerLine = 1;
                                        }
                                        else
                                        {
                                            if (totSerPerLine <= 3)
                                            {
                                                seriali = seriali + "," + c.seriale;
                                                totSerPerLine++;
                                            }
                                            else
                                            {
                                                seriali = seriali + ",\n" + c.seriale;
                                                flowChart = flowChart + "\n";
                                                totSerPerLine = 1;
                                            }
                                        }

                                    }
                            }
                            else if (partList != null)
                            {
                                partList = partList.OrderBy(o => o.numFlowChart).ToList();
                                foreach (RMA_Parte p in partList)
                                    if (seriali.Equals(""))
                                    {
                                        seriali = seriali + p.serialeParte.Trim();
                                        totSerPerLine++;
                                        flowChart = flowChart + p.numFlowChart;
                                    }
                                    else
                                    {
                                        if (!flowChart.Contains(p.numFlowChart))
                                        {
                                            flowChart = flowChart + "\n\n" + p.numFlowChart;
                                            seriali = seriali + "\n\n" + p.serialeParte.Trim();
                                            totSerPerLine = 1;
                                        }
                                        else
                                        {
                                            if (!seriali.Contains(p.serialeParte.Trim()))
                                            {
                                                if (totSerPerLine <= 3)
                                                {
                                                    seriali = seriali + "," + p.serialeParte.Trim();
                                                    totSerPerLine++;
                                                }
                                                else
                                                {
                                                    seriali = seriali + ",\n" + p.serialeParte.Trim();
                                                    flowChart = flowChart + "\n";
                                                    totSerPerLine = 1;
                                                }
                                            }
                                        }
                                    }
                            }
                            doc.ReplaceText("@SERIALS@", seriali);
                            doc.ReplaceText("@FLOW@", flowChart);
                            if (collList != null)
                                doc.ReplaceText("@TOTAL@", collList.Count.ToString());
                            else if (partList != null)
                                doc.ReplaceText("@TOTAL@", partList.Count.ToString());
                            if (itemToEdit[k].idRMA.Contains("CNC"))
                            {
                                if (infoCli.dataArrivoCollim.ToShortDateString() != null && !infoCli.dataArrivoCollim.ToShortDateString().Equals("01/01/1753"))
                                    doc.ReplaceText("@ARRDATA@", infoCli.dataArrivoCollim.ToShortDateString());
                                else
                                    doc.ReplaceText("@ARRDATA@", "N/A");
                                if (infoCli.decisioneCliente != null)
                                    doc.ReplaceText("@CUSTDEC@", infoCli.decisioneCliente);
                                else
                                    doc.ReplaceText("@CUSTDEC@", "");
                                if (infoCli.codCollCli != null)
                                    doc.ReplaceText("@CUSCOLCODE@", infoCli.codCollCli);
                                else
                                    doc.ReplaceText("@CUSCOLCODE@", "");
                                if (infoCli.SCARCliente != null)
                                    doc.ReplaceText("@CUSTSCAR@", infoCli.SCARCliente);
                                else
                                    doc.ReplaceText("@CUSTSCAR@", "");
                                if (infoCli.numScarCli != null)
                                    doc.ReplaceText("@CUSTSCARNR@", infoCli.numScarCli);
                                else
                                    doc.ReplaceText("@CUSTSCARNR@", "");
                                if (infoCli.ordRiparazione != null)
                                    doc.ReplaceText("@REPORD@", infoCli.ordRiparazione);
                                else
                                    doc.ReplaceText("@REPORD@", "");
                                if (infoCli.dataOrdineRip.ToShortDateString() != null && !infoCli.dataOrdineRip.ToShortDateString().Equals("01/01/1753"))
                                    doc.ReplaceText("@REPORDATE@", infoCli.dataOrdineRip.ToShortDateString());
                                else
                                    doc.ReplaceText("@REPORDATE@", "N/A");
                                if (infoCli.docReso != null)
                                    doc.ReplaceText("@DELNOTE@", infoCli.docReso);
                                else
                                    doc.ReplaceText("@DELNOTE@", "");
                                if (infoCli.dataDocReso.ToShortDateString() != null && !infoCli.dataDocReso.ToShortDateString().Equals("01/01/1753"))
                                    doc.ReplaceText("@DELNOTEDATE@", infoCli.dataDocReso.ToShortDateString());
                                else
                                    doc.ReplaceText("@DELNOTEDATE@", "N/A");
                                if (infoCli.noteCliente != null)
                                    doc.ReplaceText("@CUSDEF@", infoCli.noteCliente.ToString());
                                else
                                    doc.ReplaceText("@CUSDEF@", "");
                            }
                            else
                                doc.ReplaceText("@CUSCOLCODE@", "");
                            doc.ReplaceText("@GENNOTES@", rmaGen.note);
                            ////sezione Analisi Del Problema
                            //inserisco le righe nella tabella
                            if (itemToEdit[k].idRMA.Contains("NCI"))
                                actualRow = 8;
                            else
                                actualRow = 13;
                            int totLineeDifetto = 5;
                            if (probList != null)
                            {
                                if (probList.Count >= 1)
                                {
                                    for (int j = 1; j < probList.Count; j++)
                                    {
                                        for (int i = actualRow, delta = 0; i < actualRow + totLineeDifetto; i++, delta++)
                                        {
                                            table.InsertRow(table.Rows[i], actualRow + totLineeDifetto + delta);
                                        }
                                    }
                                }
                                //riempio le righe nella tabella con i dati opportuni
                                for (int j = 0, delta = 0; j < probList.Count; j++, delta = delta + totLineeDifetto)
                                {
                                    List<RMA_EsitiTestPreAnalisi> esitiTestListFromDB = new List<RMA_EsitiTestPreAnalisi>();
                                    esitiTestListFromDB = (List<RMA_EsitiTestPreAnalisi>)conn.CreateCommand("SELECT * FROM [Autotest].[dbo].[RMA_EsitiTestPreAnalisi] WHERE [Autotest].[dbo].[RMA_EsitiTestPreAnalisi].[idRMA] = '" + probList[j].idRMA + "' AND [Autotest].[dbo].[RMA_EsitiTestPreAnalisi].[idProblema] = '" + probList[j].idProblema + "'", esitiTestListFromDB);
                                    string pbAnPerf = "";
                                    if (esitiTestListFromDB != null)
                                        foreach (RMA_EsitiTestPreAnalisi tst in esitiTestListFromDB)
                                            if (pbAnPerf.Equals(""))
                                                pbAnPerf = pbAnPerf + tst.descTestDaEseguire + " : " + tst.esitoTest;
                                            else
                                                pbAnPerf = pbAnPerf + "\n" + tst.descTestDaEseguire + " : " + tst.esitoTest;
                                    else
                                        pbAnPerf = "N/A";
                                    table.Rows[actualRow + (0 + delta)].Cells[1].ReplaceText("@ANTSTPERF@", pbAnPerf);
                                    table.Rows[actualRow + (1 + delta)].Cells[1].ReplaceText("@RALCODEF@", probList[j].difRiscDaRalco);
                                    table.Rows[actualRow + (2 + delta)].Cells[1].ReplaceText("@PROBCAUSE@", probList[j].causaProblema.Replace("GENERICO : ", "") + " : " + probList[j].rifFlowChart.Replace("GENERICO : ", "").Replace("N/A : ", ""));
                                    table.Rows[actualRow + (3 + delta)].Cells[1].ReplaceText("@PROBSEV@", probList[j].gravitaProblema);
                                    string responsabili = "";
                                    int idx = 1;
                                    foreach (string r in probList[j].nomeResponsabile)
                                    {
                                        if (responsabili.Equals("") && !r.Equals(""))
                                            responsabili = responsabili + idx.ToString() + " - " + r;
                                        else
                                            if (!r.Equals(""))
                                            responsabili = responsabili + ", " + idx.ToString() + " - " + r;
                                        idx++;
                                    }
                                    if (responsabili.Equals(""))
                                        table.Rows[actualRow + (4 + delta)].Cells[1].ReplaceText("@RESPS@", "N/A");
                                    else
                                        table.Rows[actualRow + (4 + delta)].Cells[1].ReplaceText("@RESPS@", responsabili);
                                }
                                actualRow = actualRow + (totLineeDifetto * probList.Count) + 1;
                            }
                            else
                                actualRow = actualRow + (totLineeDifetto) + 1;
                            //sezione Riparazione Effettuata
                            //inserisco le righe nella tabella
                            int totLineeRiparazione = 1;
                            if (ripList != null)
                            {
                                if (ripList.Count >= 1)
                                {
                                    for (int j = 1; j < ripList.Count; j++)
                                    {
                                        for (int i = actualRow, delta = 0; i < actualRow + totLineeRiparazione; i++, delta++)
                                        {
                                            table.InsertRow(table.Rows[i], actualRow + totLineeRiparazione + delta);
                                        }
                                    }
                                }
                                //riempio le righe nella tabella con i dati opportuni
                                for (int j = 0, delta = 0; j < ripList.Count; j++, delta = delta + totLineeRiparazione)
                                {
                                    table.Rows[actualRow + (0 + delta)].Cells[1].ReplaceText("##", ripList[j].descRip + ": " + ripList[j].descParte);
                                }
                            }
                            //Sezione test Da Eseguire
                            string testDesc = "";
                            if (testDaEseguireList != null)
                            {
                                testDesc = "";
                                foreach (RMA_TestDaEseguire t in testDaEseguireList)
                                {
                                    if (testDesc.Equals(""))
                                        testDesc = testDesc + t.descTestDaEseguire;
                                    else
                                        testDesc = testDesc + "\n" + t.descTestDaEseguire;
                                }
                            }
                            else
                                testDesc = "N/A";
                            doc.ReplaceText("@TESTDESC@", testDesc);
                            //rimuovo eventuali bookmarks residui
                            //doc.ReplaceText("##", "");
                            //Sezione Impatto della Segnalazione
                            doc.ReplaceText("@SECUR@", noteQualita.sicurezzaProdotto);
                            doc.ReplaceText("@INCID@", noteQualita.segnalazIncidente);
                            doc.ReplaceText("@SINOTE@", noteQualita.invioNotaInf);
                            doc.ReplaceText("@RISKEV@", noteQualita.revDocValutazRischi);
                            doc.ReplaceText("@PCACT@", noteQualita.statoAzCorrett);
                            doc.ReplaceText("@CAREF@", noteQualita.rifAzCorrett);
                            doc.ReplaceText("@CARNOTES@", noteQualita.note);
                            //Sezione Informazioni Sul trasporto
                            if (itemToEdit[k].idRMA.Contains("CNC"))
                            {
                                doc.ReplaceText("@SHIPFEES@", infoCli.speseSpedizione);
                                doc.ReplaceText("@SHIPNOTES@", infoCli.tipoTrasporto);
                                doc.ReplaceText("@PACKTYPE@", infoCli.tipoImballaggio);
                                doc.ReplaceText("@PACKNOTES@", infoCli.noteImballaggio);
                                if (infoCli.dataSpedizione.ToString().Equals("01/01/1753 00:00:00"))
                                    doc.ReplaceText("@SHIPDATE@", "");
                                else
                                    doc.ReplaceText("@SHIPDATE@", infoCli.dataSpedizione.ToShortDateString());
                            }
                            //Sezione Garanzia
                            if (itemToEdit[k].idRMA.Contains("CNC"))
                            {
                                if (garanziaRalco.statoGaranzia != null)
                                    if (garanziaRalco.azioniSupplRalco != null && garanziaRalco.azioniSupplRalco != "")
                                        doc.ReplaceText("@WARRSTAT@", garanziaRalco.azioniSupplRalco);
                                    else
                                        doc.ReplaceText("@WARRSTAT@", garanziaRalco.statoGaranzia);
                                else
                                    doc.ReplaceText("@WARRSTAT@", "");
                                if (garanziaRalco.noteGaranziaRalco != null)
                                    doc.ReplaceText("@WARRNOTES@", garanziaRalco.noteGaranziaRalco);
                                else
                                    doc.ReplaceText("@WARRNOTES@", "");
                            }
                            //condizione usata solo nel caso di stampa di RMA parziali
                            doc.ReplaceText("##", "N/A");

                            //salvataggio del file nella cartella temporanea locale
                            if (!Directory.Exists(Environment.CurrentDirectory + @"\NonConformita\"))
                                Directory.CreateDirectory(Environment.CurrentDirectory + @"\NonConformita\");
                            doc.SaveAs(Environment.CurrentDirectory + @"\NonConformita\nonConformita_TMP_" + k + ".docx");
                            // Open in Word:
                            Process.Start("WINWORD.EXE", Environment.CurrentDirectory + @"\NonConformita\nonConformita_TMP_" + k + ".docx");
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
                else
                    MessageBox.Show("Selezionare un RMA da stampare", "Informazione", MessageBoxButtons.OK, MessageBoxIcon.Information);
                rmaList.Reverse();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_ExportCNC_Click(object sender, EventArgs e)
        {
            try
            {
                //salvataggio del file nella cartella locale
                if (!Directory.Exists(Environment.CurrentDirectory + @"\Report\"))
                    Directory.CreateDirectory(Environment.CurrentDirectory + @"\Report\");
                List<string> lines;
                if (Globals.user.getUserCredential().Equals("NonConfInt"))
                     lines = new List<string>() { "RMA;CLIENTE;MODELLO COLLIMATORE;TOT;NUMERO SERIALE;FLOW CHART;DATA CREAZIONE;ULTIMA MODIFICA;AGGIORNATO DA;DATA SCADENZA;PRIORITA;STATO;CAUSE PROBLEMA;DIFETTI RISCONTRATI;" };
                else
                    lines = new List<string>() { "RMA;CLIENTE;MODELLO COLLIMATORE;TOT;NUMERO SERIALE;FLOW CHART;DATA CREAZIONE;ULTIMA MODIFICA;AGGIORNATO DA;DATA SCADENZA;PRIORITA;STATO;NUMERO SCAR;DOCUMENTO DI RESO;ORDINE DI RIPARAZIONE;NOTE CLIENTE;GARANZIA;CAUSE PROBLEMA;DIFETTI RISCONTRATI;PROVENIENZA;" };
                string line = "";
                foreach (DataGridViewRow row in dgv_ListaNonConf.Rows)
                {
                    string[] modelloColl = row.Cells[3].Value.ToString().Split('\n');
                    string[] numSeriale = row.Cells[5].Value.ToString().Split('\n');
                    string[] flowChart = row.Cells[6].Value.ToString().Split('\n');
                    if (!numSeriale.Length.Equals(modelloColl.Length))
                    {
                        numSeriale = row.Cells[3].Value.ToString().Split('\n');//faccio in modo che numseriale contenga un numero di elementi pari a modelloColl
                        for (int i = 0; i < numSeriale.Length; i++)
                            numSeriale[i] = "";//metto a "" ogni elemento di numSeriale
                    }
                    for (int i = 0; i < numSeriale.Length; i++)
                    {
                        if (numSeriale[i].Equals("1804684"))
                            Console.WriteLine(numSeriale[i]);
                        line = "";
                        if (Globals.user.getUserCredential().Equals("NonConfInt"))
                            line = line + row.Cells[1].Value.ToString() + ";" + row.Cells[2].Value.ToString() + ";" + modelloColl[i] + ";" + row.Cells[4].Value.ToString() + ";" + numSeriale[i] + ";" + flowChart[i] + ";" + row.Cells[7].Value.ToString() + ";" + row.Cells[8].Value.ToString() + ";" + row.Cells[9].Value.ToString() + ";" + row.Cells[10].Value.ToString() + ";" + row.Cells[11].Value.ToString() + ";" + row.Cells[12].Value.ToString() + ";";
                        else if (Globals.user.getUserCredential().Equals("NonConfExt") || Globals.user.getUserCredential().Equals("admin") || Globals.user.getUserCredential().Equals("admin"))
                            line = line + row.Cells[1].Value.ToString() + ";" + row.Cells[2].Value.ToString() + ";" + modelloColl[i] + ";" + row.Cells[4].Value.ToString() + ";" + numSeriale[i] + ";" + (flowChart.Length.Equals(numSeriale.Length) ? flowChart[i]: flowChart[0]) + ";" + row.Cells[7].Value.ToString() + ";" + row.Cells[8].Value.ToString() + ";" + row.Cells[9].Value.ToString() + ";" + row.Cells[10].Value.ToString().Replace("01/01/1753", "") + ";" + row.Cells[11].Value.ToString() + ";" + row.Cells[12].Value.ToString() + ";" + row.Cells[13].Value.ToString() + ";" + row.Cells[14].Value.ToString() + ";" + row.Cells[15].Value.ToString() + ";" + row.Cells[16].Value.ToString() + ";" + row.Cells[17].Value.ToString() + ";" + row.Cells[18].Value.ToString() + ";" + row.Cells[19].Value.ToString() + ";" + ";" + row.Cells[20].Value.ToString() + ";";
                        line = line.Replace("\n", " ").Replace("\t", " ").Replace("\r"," ").Replace(Environment.NewLine, " ");
                        lines.Add(line);
                    }
                }
                string dateTime = DateTime.Now.ToShortDateString();
                System.IO.File.WriteAllLines(System.IO.Path.GetDirectoryName(Application.ExecutablePath) + @"\Report\" + "Extract_" + dateTime.ToString().Replace("/", "_") + ".csv", lines);
                //MessageBox.Show("Report salvato correttamente", "Informazione", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Process.Start("EXCEL.EXE", System.IO.Path.GetDirectoryName(Application.ExecutablePath) + @"\Report\" + "Extract_" + dateTime.ToString().Replace("/", "_") + ".csv");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Form_ServiceManager_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                removeBakFiles();
                Application.Exit();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Cmb_RMASelection_TextChanged(object sender, EventArgs e)
        {
            try
            {
                bool areAllRmaSelected = false;
                if (cmb_RMASelection.SelectedItem.Equals("Seleziona Tutti"))
                    areAllRmaSelected = true;
                else
                    areAllRmaSelected = false;
                foreach (DataGridViewRow row in dgv_ListaNonConf.Rows)
                    row.Cells[0].Value = areAllRmaSelected;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //private void Tb_ricerca_Click(object sender, EventArgs e)
        //{
        //    tb_ricerca.Text = "";
        //    tb_ricerca.ForeColor = Color.Black;
        //}

        //private void Tb_ricerca_Leave(object sender, EventArgs e)
        //{
        //    if (tb_ricerca.Text.Equals(""))
        //    {
        //        tb_ricerca.Text = "Cerca...";
        //        tb_ricerca.ForeColor = SystemColors.ControlDark;
        //    }
        //}

    }
}