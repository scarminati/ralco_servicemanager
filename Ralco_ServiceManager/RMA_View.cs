﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ralco_ServiceManager
{
    public class RMA_View
    {
        public string idRMA { get; set; }
        public string tipoRMA { get; set; }
        public List<String> modelloCollim { get; set; }
        public List<String> serialeCollim { get; set; }
        public List<String> numFlowChart { get; set; }
        public DateTime dataCreazione { get; set; }
        public string creatoDa { get; set; }
        public DateTime dataAggiornamento { get; set; }
        public string aggiornatoDa { get; set; }
        public DateTime dataScadenza { get; set; }
        public string priorita { get; set; }
        public string stato { get; set; }
        public string location { get; set; }//RalcoIT, RalcoUSA,...
        public string nomeCliente { get; set; }
        public string numSCAR { get; set; }
        public string docReso { get; set; }
        public string numOrdRip { get; set; }
        public string noteCliente { get; set; }
        public string statoGaranzia { get; set; }
        public List<String> difRiscDaRalco { get; set; }
        public List<String> causaProblema { get; set; }
        public decimal costoRiparazione { get; set; }
        public string provCollimatore { get; set; }

        public RMA_View()
        {
            //do nothing...
        }

        public RMA_View(RMA_View rma)
        {
            this.idRMA = rma.idRMA;
            this.tipoRMA = rma.tipoRMA;
            this.modelloCollim = rma.modelloCollim;
            this.serialeCollim = rma.serialeCollim;
            this.numFlowChart = rma.numFlowChart;
            this.dataCreazione = DateTime.Now;
            this.creatoDa = rma.creatoDa;
            this.dataAggiornamento = DateTime.Now;
            this.aggiornatoDa = rma.aggiornatoDa;
            this.dataScadenza = rma.dataScadenza;
            this.priorita = rma.priorita;
            this.stato = rma.stato;
            this.location = rma.location;
            this.nomeCliente = rma.nomeCliente;
            this.numSCAR = numSCAR;
            this.docReso = docReso;
            this.numOrdRip = numOrdRip;
            this.noteCliente = noteCliente;
            this.statoGaranzia = statoGaranzia;
            this.difRiscDaRalco = difRiscDaRalco;
            this.causaProblema = causaProblema;
            this.costoRiparazione = costoRiparazione;
            this.provCollimatore = provCollimatore;
        }

    }
}
