﻿namespace Ralco_ServiceManager
{
    partial class Form_Preventivo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Preventivo));
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.dgv_ListaArticoliPreventivo = new System.Windows.Forms.DataGridView();
            this.chk_Art = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.idArticolo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.qta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descArticolo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.prezzo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.toolStrip_MenuPreventivo = new System.Windows.Forms.ToolStrip();
            this.btn_RefreshPreventivo = new System.Windows.Forms.ToolStripButton();
            this.btn_importRiparazioni = new System.Windows.Forms.ToolStripButton();
            this.btn_AddArticolo = new System.Windows.Forms.ToolStripButton();
            this.btn_AddExtraCosts = new System.Windows.Forms.ToolStripButton();
            this.btn_DeleteArticolo = new System.Windows.Forms.ToolStripButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tb_TotalePrev = new System.Windows.Forms.MaskedTextBox();
            this.rb_totPreventivo = new System.Windows.Forms.RadioButton();
            this.lbl_TotaleCostoRalco = new System.Windows.Forms.Label();
            this.lbl_totConsuntivo = new System.Windows.Forms.Label();
            this.btn_cancel = new System.Windows.Forms.Button();
            this.btn_OK = new System.Windows.Forms.Button();
            this.gb_InfoGenerali = new System.Windows.Forms.GroupBox();
            this.lbl_RevPrev = new System.Windows.Forms.Label();
            this.cmb_statoPrev = new System.Windows.Forms.ComboBox();
            this.lbl_stato = new System.Windows.Forms.Label();
            this.lbl_IDPrev = new System.Windows.Forms.Label();
            this.lbl_IDPreventivo = new System.Windows.Forms.Label();
            this.lbl_revisione = new System.Windows.Forms.Label();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ListaArticoliPreventivo)).BeginInit();
            this.groupBox6.SuspendLayout();
            this.toolStrip_MenuPreventivo.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.gb_InfoGenerali.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.dgv_ListaArticoliPreventivo);
            this.groupBox5.Location = new System.Drawing.Point(12, 148);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(680, 364);
            this.groupBox5.TabIndex = 10;
            this.groupBox5.TabStop = false;
            // 
            // dgv_ListaArticoliPreventivo
            // 
            this.dgv_ListaArticoliPreventivo.AllowUserToAddRows = false;
            this.dgv_ListaArticoliPreventivo.AllowUserToOrderColumns = true;
            this.dgv_ListaArticoliPreventivo.BackgroundColor = System.Drawing.SystemColors.ActiveBorder;
            this.dgv_ListaArticoliPreventivo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_ListaArticoliPreventivo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.chk_Art,
            this.idArticolo,
            this.qta,
            this.descArticolo,
            this.prezzo});
            this.dgv_ListaArticoliPreventivo.Cursor = System.Windows.Forms.Cursors.Default;
            this.dgv_ListaArticoliPreventivo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_ListaArticoliPreventivo.Location = new System.Drawing.Point(3, 16);
            this.dgv_ListaArticoliPreventivo.Name = "dgv_ListaArticoliPreventivo";
            this.dgv_ListaArticoliPreventivo.ReadOnly = true;
            this.dgv_ListaArticoliPreventivo.RowHeadersVisible = false;
            this.dgv_ListaArticoliPreventivo.Size = new System.Drawing.Size(674, 345);
            this.dgv_ListaArticoliPreventivo.TabIndex = 6;
            this.dgv_ListaArticoliPreventivo.Click += new System.EventHandler(this.dgv_ListaArticoliPreventivo_Click);
            // 
            // chk_Art
            // 
            this.chk_Art.HeaderText = "";
            this.chk_Art.Name = "chk_Art";
            this.chk_Art.ReadOnly = true;
            this.chk_Art.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.chk_Art.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.chk_Art.Width = 30;
            // 
            // idArticolo
            // 
            this.idArticolo.HeaderText = "ID Articolo";
            this.idArticolo.Name = "idArticolo";
            this.idArticolo.ReadOnly = true;
            this.idArticolo.Width = 90;
            // 
            // qta
            // 
            this.qta.HeaderText = "Quantità";
            this.qta.Name = "qta";
            this.qta.ReadOnly = true;
            this.qta.Width = 70;
            // 
            // descArticolo
            // 
            this.descArticolo.HeaderText = "Descrizione";
            this.descArticolo.Name = "descArticolo";
            this.descArticolo.ReadOnly = true;
            this.descArticolo.Width = 380;
            // 
            // prezzo
            // 
            this.prezzo.HeaderText = "Prezzo (€)";
            this.prezzo.Name = "prezzo";
            this.prezzo.ReadOnly = true;
            this.prezzo.Width = 90;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.toolStrip_MenuPreventivo);
            this.groupBox6.Location = new System.Drawing.Point(12, 72);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(386, 70);
            this.groupBox6.TabIndex = 9;
            this.groupBox6.TabStop = false;
            // 
            // toolStrip_MenuPreventivo
            // 
            this.toolStrip_MenuPreventivo.AutoSize = false;
            this.toolStrip_MenuPreventivo.BackColor = System.Drawing.SystemColors.Control;
            this.toolStrip_MenuPreventivo.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip_MenuPreventivo.ImageScalingSize = new System.Drawing.Size(47, 47);
            this.toolStrip_MenuPreventivo.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btn_RefreshPreventivo,
            this.btn_importRiparazioni,
            this.btn_AddArticolo,
            this.btn_AddExtraCosts,
            this.btn_DeleteArticolo});
            this.toolStrip_MenuPreventivo.Location = new System.Drawing.Point(3, 16);
            this.toolStrip_MenuPreventivo.Name = "toolStrip_MenuPreventivo";
            this.toolStrip_MenuPreventivo.Size = new System.Drawing.Size(380, 50);
            this.toolStrip_MenuPreventivo.TabIndex = 117;
            this.toolStrip_MenuPreventivo.Text = "toolStrip1";
            // 
            // btn_RefreshPreventivo
            // 
            this.btn_RefreshPreventivo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_RefreshPreventivo.Image = ((System.Drawing.Image)(resources.GetObject("btn_RefreshPreventivo.Image")));
            this.btn_RefreshPreventivo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_RefreshPreventivo.Name = "btn_RefreshPreventivo";
            this.btn_RefreshPreventivo.Size = new System.Drawing.Size(51, 47);
            this.btn_RefreshPreventivo.Text = "Evidenzia";
            this.btn_RefreshPreventivo.ToolTipText = "Evidenzia";
            this.btn_RefreshPreventivo.Click += new System.EventHandler(this.btn_RefreshPreventivo_Click);
            // 
            // btn_importRiparazioni
            // 
            this.btn_importRiparazioni.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_importRiparazioni.Image = ((System.Drawing.Image)(resources.GetObject("btn_importRiparazioni.Image")));
            this.btn_importRiparazioni.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_importRiparazioni.Name = "btn_importRiparazioni";
            this.btn_importRiparazioni.Size = new System.Drawing.Size(51, 47);
            this.btn_importRiparazioni.Text = "toolStripButton4";
            this.btn_importRiparazioni.ToolTipText = "Esporta Magazzino";
            this.btn_importRiparazioni.Click += new System.EventHandler(this.btn_importRiparazioni_Click);
            // 
            // btn_AddArticolo
            // 
            this.btn_AddArticolo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_AddArticolo.Image = ((System.Drawing.Image)(resources.GetObject("btn_AddArticolo.Image")));
            this.btn_AddArticolo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_AddArticolo.Name = "btn_AddArticolo";
            this.btn_AddArticolo.Size = new System.Drawing.Size(51, 47);
            this.btn_AddArticolo.Text = "toolStripButton3";
            this.btn_AddArticolo.Click += new System.EventHandler(this.btn_AddArticolo_Click);
            // 
            // btn_AddExtraCosts
            // 
            this.btn_AddExtraCosts.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_AddExtraCosts.Image = ((System.Drawing.Image)(resources.GetObject("btn_AddExtraCosts.Image")));
            this.btn_AddExtraCosts.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_AddExtraCosts.Name = "btn_AddExtraCosts";
            this.btn_AddExtraCosts.Size = new System.Drawing.Size(51, 47);
            this.btn_AddExtraCosts.Text = "toolStripButton3";
            this.btn_AddExtraCosts.Click += new System.EventHandler(this.btn_Extra_Click);
            // 
            // btn_DeleteArticolo
            // 
            this.btn_DeleteArticolo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_DeleteArticolo.Image = ((System.Drawing.Image)(resources.GetObject("btn_DeleteArticolo.Image")));
            this.btn_DeleteArticolo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_DeleteArticolo.Name = "btn_DeleteArticolo";
            this.btn_DeleteArticolo.Size = new System.Drawing.Size(51, 47);
            this.btn_DeleteArticolo.Text = "toolStripButton3";
            this.btn_DeleteArticolo.Click += new System.EventHandler(this.btn_DeleteArticolo_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tb_TotalePrev);
            this.groupBox1.Controls.Add(this.rb_totPreventivo);
            this.groupBox1.Controls.Add(this.lbl_TotaleCostoRalco);
            this.groupBox1.Controls.Add(this.lbl_totConsuntivo);
            this.groupBox1.Location = new System.Drawing.Point(402, 72);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(290, 70);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            // 
            // tb_TotalePrev
            // 
            this.tb_TotalePrev.Location = new System.Drawing.Point(127, 14);
            this.tb_TotalePrev.MaximumSize = new System.Drawing.Size(150, 20);
            this.tb_TotalePrev.MinimumSize = new System.Drawing.Size(150, 20);
            this.tb_TotalePrev.Name = "tb_TotalePrev";
            this.tb_TotalePrev.ReadOnly = true;
            this.tb_TotalePrev.Size = new System.Drawing.Size(150, 20);
            this.tb_TotalePrev.TabIndex = 105;
            this.tb_TotalePrev.Text = "0";
            // 
            // rb_totPreventivo
            // 
            this.rb_totPreventivo.AutoSize = true;
            this.rb_totPreventivo.Location = new System.Drawing.Point(8, 15);
            this.rb_totPreventivo.Name = "rb_totPreventivo";
            this.rb_totPreventivo.Size = new System.Drawing.Size(109, 17);
            this.rb_totPreventivo.TabIndex = 11;
            this.rb_totPreventivo.TabStop = true;
            this.rb_totPreventivo.Text = "Totale Preventivo";
            this.rb_totPreventivo.UseVisualStyleBackColor = true;
            this.rb_totPreventivo.CheckedChanged += new System.EventHandler(this.rb_TotPreventivo_CheckedChanged);
            // 
            // lbl_TotaleCostoRalco
            // 
            this.lbl_TotaleCostoRalco.AutoSize = true;
            this.lbl_TotaleCostoRalco.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_TotaleCostoRalco.Location = new System.Drawing.Point(127, 44);
            this.lbl_TotaleCostoRalco.MaximumSize = new System.Drawing.Size(150, 20);
            this.lbl_TotaleCostoRalco.MinimumSize = new System.Drawing.Size(150, 20);
            this.lbl_TotaleCostoRalco.Name = "lbl_TotaleCostoRalco";
            this.lbl_TotaleCostoRalco.Size = new System.Drawing.Size(150, 20);
            this.lbl_TotaleCostoRalco.TabIndex = 10;
            this.lbl_TotaleCostoRalco.Text = "0";
            this.lbl_TotaleCostoRalco.Visible = false;
            // 
            // lbl_totConsuntivo
            // 
            this.lbl_totConsuntivo.AutoSize = true;
            this.lbl_totConsuntivo.Location = new System.Drawing.Point(5, 47);
            this.lbl_totConsuntivo.Name = "lbl_totConsuntivo";
            this.lbl_totConsuntivo.Size = new System.Drawing.Size(93, 13);
            this.lbl_totConsuntivo.TabIndex = 1;
            this.lbl_totConsuntivo.Text = "Totale Consuntivo";
            this.lbl_totConsuntivo.Visible = false;
            // 
            // btn_cancel
            // 
            this.btn_cancel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_cancel.BackgroundImage")));
            this.btn_cancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_cancel.Location = new System.Drawing.Point(368, 530);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(50, 50);
            this.btn_cancel.TabIndex = 103;
            this.btn_cancel.UseVisualStyleBackColor = true;
            this.btn_cancel.Click += new System.EventHandler(this.btn_cancelPreventivo_Click);
            // 
            // btn_OK
            // 
            this.btn_OK.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_OK.BackgroundImage")));
            this.btn_OK.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_OK.Location = new System.Drawing.Point(263, 530);
            this.btn_OK.Name = "btn_OK";
            this.btn_OK.Size = new System.Drawing.Size(50, 50);
            this.btn_OK.TabIndex = 102;
            this.btn_OK.UseVisualStyleBackColor = true;
            this.btn_OK.Click += new System.EventHandler(this.btn_okPreventivo_Click);
            // 
            // gb_InfoGenerali
            // 
            this.gb_InfoGenerali.Controls.Add(this.lbl_RevPrev);
            this.gb_InfoGenerali.Controls.Add(this.cmb_statoPrev);
            this.gb_InfoGenerali.Controls.Add(this.lbl_stato);
            this.gb_InfoGenerali.Controls.Add(this.lbl_IDPrev);
            this.gb_InfoGenerali.Controls.Add(this.lbl_IDPreventivo);
            this.gb_InfoGenerali.Controls.Add(this.lbl_revisione);
            this.gb_InfoGenerali.Location = new System.Drawing.Point(12, 5);
            this.gb_InfoGenerali.Name = "gb_InfoGenerali";
            this.gb_InfoGenerali.Size = new System.Drawing.Size(680, 61);
            this.gb_InfoGenerali.TabIndex = 104;
            this.gb_InfoGenerali.TabStop = false;
            this.gb_InfoGenerali.Text = "Informazioni Generali";
            // 
            // lbl_RevPrev
            // 
            this.lbl_RevPrev.AutoSize = true;
            this.lbl_RevPrev.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_RevPrev.Location = new System.Drawing.Point(332, 22);
            this.lbl_RevPrev.MaximumSize = new System.Drawing.Size(120, 20);
            this.lbl_RevPrev.MinimumSize = new System.Drawing.Size(120, 20);
            this.lbl_RevPrev.Name = "lbl_RevPrev";
            this.lbl_RevPrev.Size = new System.Drawing.Size(120, 20);
            this.lbl_RevPrev.TabIndex = 2;
            // 
            // cmb_statoPrev
            // 
            this.cmb_statoPrev.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmb_statoPrev.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_statoPrev.FormattingEnabled = true;
            this.cmb_statoPrev.Items.AddRange(new object[] {
            "In Completamento",
            "In Valutazione",
            "Approvato",
            "Respinto",
            "Annullato"});
            this.cmb_statoPrev.Location = new System.Drawing.Point(540, 22);
            this.cmb_statoPrev.Name = "cmb_statoPrev";
            this.cmb_statoPrev.Size = new System.Drawing.Size(120, 21);
            this.cmb_statoPrev.TabIndex = 3;
            // 
            // lbl_stato
            // 
            this.lbl_stato.AutoSize = true;
            this.lbl_stato.Location = new System.Drawing.Point(484, 25);
            this.lbl_stato.Name = "lbl_stato";
            this.lbl_stato.Size = new System.Drawing.Size(32, 13);
            this.lbl_stato.TabIndex = 10;
            this.lbl_stato.Text = "Stato";
            // 
            // lbl_IDPrev
            // 
            this.lbl_IDPrev.AutoSize = true;
            this.lbl_IDPrev.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_IDPrev.Location = new System.Drawing.Point(112, 22);
            this.lbl_IDPrev.MaximumSize = new System.Drawing.Size(120, 20);
            this.lbl_IDPrev.MinimumSize = new System.Drawing.Size(120, 20);
            this.lbl_IDPrev.Name = "lbl_IDPrev";
            this.lbl_IDPrev.Size = new System.Drawing.Size(120, 20);
            this.lbl_IDPrev.TabIndex = 1;
            // 
            // lbl_IDPreventivo
            // 
            this.lbl_IDPreventivo.AutoSize = true;
            this.lbl_IDPreventivo.Location = new System.Drawing.Point(16, 25);
            this.lbl_IDPreventivo.Name = "lbl_IDPreventivo";
            this.lbl_IDPreventivo.Size = new System.Drawing.Size(72, 13);
            this.lbl_IDPreventivo.TabIndex = 0;
            this.lbl_IDPreventivo.Text = "ID Preventivo";
            // 
            // lbl_revisione
            // 
            this.lbl_revisione.AutoSize = true;
            this.lbl_revisione.Location = new System.Drawing.Point(253, 25);
            this.lbl_revisione.Name = "lbl_revisione";
            this.lbl_revisione.Size = new System.Drawing.Size(54, 13);
            this.lbl_revisione.TabIndex = 8;
            this.lbl_revisione.Text = "Revisione";
            // 
            // Form_Preventivo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(703, 598);
            this.Controls.Add(this.gb_InfoGenerali);
            this.Controls.Add(this.btn_cancel);
            this.Controls.Add(this.btn_OK);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox6);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(719, 636);
            this.MinimumSize = new System.Drawing.Size(719, 636);
            this.Name = "Form_Preventivo";
            this.Text = "Preventivo";
            this.groupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ListaArticoliPreventivo)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.toolStrip_MenuPreventivo.ResumeLayout(false);
            this.toolStrip_MenuPreventivo.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.gb_InfoGenerali.ResumeLayout(false);
            this.gb_InfoGenerali.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.DataGridView dgv_ListaArticoliPreventivo;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btn_cancel;
        private System.Windows.Forms.Button btn_OK;
        private System.Windows.Forms.Label lbl_totConsuntivo;
        private System.Windows.Forms.Label lbl_TotaleCostoRalco;
        private System.Windows.Forms.GroupBox gb_InfoGenerali;
        private System.Windows.Forms.Label lbl_RevPrev;
        private System.Windows.Forms.ComboBox cmb_statoPrev;
        private System.Windows.Forms.Label lbl_stato;
        private System.Windows.Forms.Label lbl_IDPrev;
        private System.Windows.Forms.Label lbl_IDPreventivo;
        private System.Windows.Forms.Label lbl_revisione;
        private System.Windows.Forms.RadioButton rb_totPreventivo;
        private System.Windows.Forms.MaskedTextBox tb_TotalePrev;
        private System.Windows.Forms.DataGridViewCheckBoxColumn chk_Art;
        private System.Windows.Forms.DataGridViewTextBoxColumn idArticolo;
        private System.Windows.Forms.DataGridViewTextBoxColumn qta;
        private System.Windows.Forms.DataGridViewTextBoxColumn descArticolo;
        private System.Windows.Forms.DataGridViewTextBoxColumn prezzo;
        private System.Windows.Forms.ToolStrip toolStrip_MenuPreventivo;
        private System.Windows.Forms.ToolStripButton btn_RefreshPreventivo;
        private System.Windows.Forms.ToolStripButton btn_importRiparazioni;
        private System.Windows.Forms.ToolStripButton btn_AddArticolo;
        private System.Windows.Forms.ToolStripButton btn_AddExtraCosts;
        private System.Windows.Forms.ToolStripButton btn_DeleteArticolo;
    }
}