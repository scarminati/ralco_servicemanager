﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ralco_ServiceManager
{
    public class RMA_GaranziaRalco
    {
        public int idProgressivo { get; set; }
        public string idRMA { get; set; }
        public string provCollimatore { get; set; }
        public string anniVitaCollimatore { get; set; }
        public string respDannoCollimatore { get; set; }
        public string noteGaranziaRalco { get; set; }
        public string statoGaranzia { get; set; }
        public string azioniSupplRalco { get; set; }
    }
}
