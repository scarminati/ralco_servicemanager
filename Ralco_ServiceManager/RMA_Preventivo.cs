﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ralco_ServiceManager
{
    public class RMA_Preventivo
    {
        public int idProgressivo { get; set; }
        public string idRMA { get; set; }
        public int numPreventivo { get; set; }
        public string revisionePrev { get; set; }
        public DateTime dataAggPrev { get; set; }
        public string stato { get; set; }
        public Decimal totale { get; set; }
        public Decimal totaleCostoRalco { get; set; }
        public List<string> idArticolo { get; set; }
        public string nomeTecnicoRalco { get; set; }

        //costruttore di default
        public RMA_Preventivo()
        {
            //do nothing...
        }

        //overload del costruttore di default
        public RMA_Preventivo(RMA_Preventivo p)
        {
            this.idProgressivo = p.idProgressivo;
            this.idRMA = p.idRMA;
            this.numPreventivo = p.numPreventivo;
            this.revisionePrev = p.revisionePrev;
            this.dataAggPrev = p.dataAggPrev;
            this.stato = p.stato;
            this.totale = p.totale;
            this.totaleCostoRalco = p.totaleCostoRalco;
            //this.idArticolo = new List<String>();
            //foreach (String s in p.idArticolo)
            //    this.idArticolo.Add(s);
            this.nomeTecnicoRalco = p.nomeTecnicoRalco;
        }
    }
}
