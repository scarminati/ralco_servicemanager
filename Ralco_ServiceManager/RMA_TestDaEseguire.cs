﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ralco_ServiceManager
{
    public class RMA_TestDaEseguire
    {
        public Int64 idProgressivo { get; set; }
        public string idRMA { get; set; }
        public string descTestDaEseguire { get; set; }

        //costruttore di default
        public RMA_TestDaEseguire()
        {
            //do nothing...
        }

        //overload costruttore di default
        public RMA_TestDaEseguire(RMA_TestDaEseguire test)
        {
            this.idProgressivo = test.idProgressivo;
            this.idRMA = test.idRMA;
            this.descTestDaEseguire = test.descTestDaEseguire;
        }
    }
}
