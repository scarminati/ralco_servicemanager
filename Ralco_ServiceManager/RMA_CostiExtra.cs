﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ralco_ServiceManager
{
    public class RMA_CostiExtra :IEquatable <RMA_CostiExtra>
    {
        public int idExtra { get; set; }
        public string descrizione { get; set; }
        public Decimal prezzoUnitario { get; set; }
        public int quantita { get; set; }
        private SQLConnector conn;

        //costruttore di default
        public RMA_CostiExtra()
        {
            //do nothing...
        }

        //override del costruttore di default
        public RMA_CostiExtra(string idArticolo, string descrizione, decimal prezzo, int quantita)
        {
            this.idExtra = Convert.ToInt32(idArticolo);
            this.descrizione = descrizione;
            this.prezzoUnitario = prezzo;
            this.quantita = quantita;
        }

        public bool Equals(RMA_CostiExtra obj)
        {
            if (obj == null)
                return false;
            else if (obj.idExtra.Equals(this.idExtra))
                return true;
            else
                return false;
        }
    }
}
