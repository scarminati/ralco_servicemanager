﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Ralco_ServiceManager
{
    public partial class Form_AzioneSupplementareDB : Form
    {
        private SQLConnector conn;
        public Form_AzioneSupplementareDB()
        {
            try
            {
                InitializeComponent();
                conn = new SQLConnector();
                setWidgetDescriptions();
                fillDataGridView();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void setWidgetDescriptions()
        {
            try
            {
                #region Form
                this.Text = Utils.resourcemanager.GetString("Form_AzioneSupplementareDB");
                #endregion
                #region TooltipsPerBottoni
                ToolTip tooltip = new System.Windows.Forms.ToolTip();
                tooltip.SetToolTip(btn_addAzioneSupplDB, Utils.resourcemanager.GetString("btn_addAzioneSupplDB"));
                tooltip.SetToolTip(btn_OK, Utils.resourcemanager.GetString("btn_OK"));
                tooltip.SetToolTip(btn_cancel, Utils.resourcemanager.GetString("btn_cancel"));
                #endregion
                #region Groupbox
                gb_AddAzioneAlDB.Text = Utils.resourcemanager.GetString("gb_AddAzioneAlDB");
                #endregion
                #region DataGridViewColumns
                dgv_listaAzioniSupplDB.Columns["descAzione"].HeaderText = Utils.resourcemanager.GetString("dgv_listaAzioniSupplDB_descAzione");
                #endregion
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void fillDataGridView()
        {
            try
            {
                List<String> azSupplList = new List<String>();
                azSupplList = (List<String>)conn.CreateCommand("SELECT [descrizioneAzioneSuppl] FROM [Autotest].[dbo].[RMA_ElencoAzioniSupplementari]", azSupplList);
                dgv_listaAzioniSupplDB.Rows.Clear();
                foreach (string azSuppl in azSupplList)
                    this.dgv_listaAzioniSupplDB.Rows.Insert(0, false, azSuppl);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_addAzioneSupplDB_Click(object sender, EventArgs e)
        {
            try
            {
                List<String> azSupplList = new List<String>();
                azSupplList = (List<String>)conn.CreateCommand("SELECT * FROM [Autotest].[dbo].[RMA_ElencoAzioniSupplementari] WHERE [descrizioneAzioneSuppl] = '" + tb_descAzSuppl.Text + "'", azSupplList);
                if (azSupplList == null || azSupplList.Count.Equals(0))
                {
                    DialogResult dialogResult = MessageBox.Show("Sei sicuro di voler inserire l'azione supplementare nel database?", "Aggiungi Azione Supplementare Al Database", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        conn.CreateCommand("INSERT INTO [Autotest].[dbo].[RMA_ElencoAzioniSupplementari] " +
                        "([descrizioneAzioneSuppl]) " +
                        "VALUES ('" + tb_descAzSuppl.Text + "')", null);
                    }
                    fillDataGridView();
                }
                else
                {
                    MessageBox.Show("Azione supplementare già presente nel Database", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            try
            {
                this.DialogResult = DialogResult.OK;
                this.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_cancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.DialogResult = DialogResult.Cancel;
                this.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
