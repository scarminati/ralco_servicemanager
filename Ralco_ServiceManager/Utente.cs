﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ralco_ServiceManager
{
    public class Utente
    {
        public int idUtente { get; set; }
        public int idApplicativo { get; set; }
        public string nome { get; set; }
        public string cognome { get; set; }
        public string password { get; set; }
        public string credenziale { get; set; }
        public string location { get; set; }
        public string language { get; set; }

        public Utente()
        {

        }

        public Utente(Utente user)
        {
            this.idUtente = user.idUtente;
            this.idApplicativo = user.idApplicativo;
            this.nome = user.nome;
            this.cognome = user.cognome;
            this.password = user.password;
            this.credenziale = user.credenziale;
            this.location = user.location;
            this.language = user.language;
        }

        //metodi di uso comune
        public string getFullUserName()
        {
            return (this.nome + " " + this.cognome);
        }

        public string getReverseFullUserName()
        {
            return (this.cognome + " " + this.nome);
        }

        public string getUserCredential()
        {
            return (this.credenziale);
        }

        public string getUserLocation()
        {
            return (this.location);
        }
    }
}
