﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;

namespace Ralco_ServiceManager
{
    public partial class Form_FlowChart : Form
    {
        public RMA_Articolo articolo { get; set; }
        private SQLConnector conn;
        private List<RMA_DifettoRiscontrato> difList;
        public Form_FlowChart(List<RMA_DifettoRiscontrato> difList)
        {
            try
            {
                InitializeComponent();
                conn = new SQLConnector();
                this.difList = new List<RMA_DifettoRiscontrato>(difList);
                setWidgetDescriptions();
                setTreeView();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void setWidgetDescriptions()
        {
            try
            {
                #region Form
                this.Text = Utils.resourcemanager.GetString("Form_FlowChart");
                #endregion
                #region TooltipsPerBottoni
                ToolTip tooltip = new System.Windows.Forms.ToolTip();
                tooltip.SetToolTip(btn_OK, Utils.resourcemanager.GetString("btn_OK"));
                tooltip.SetToolTip(btn_cancel, Utils.resourcemanager.GetString("btn_cancel"));
                #endregion
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void setTreeView()
        {
            try
            {
                tv_FlowChart.Nodes.Clear();
                List<string> listFasi = new List<string>();
                listFasi = getListValues("SELECT [Autotest].[dbo].[RMA_FasiProduzione].[descrizFaseProduzione] FROM [Autotest].[dbo].[RMA_FasiProduzione]");
                if (listFasi != null)
                {
                    tv_FlowChart.BeginUpdate();
                    foreach (string s in listFasi)
                        tv_FlowChart.Nodes.Add(s);
                    foreach (RMA_DifettoRiscontrato d in difList)
                        for (int i = 0; i < tv_FlowChart.Nodes.Count; i++)
                            if (tv_FlowChart.Nodes[i].Text.Equals(d.fase))
                                tv_FlowChart.Nodes[i].Nodes.Add(d.codParte + "     " + d.descDifetto);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private List<string> getListValues(string SQLquery)
        {
            try
            {
                List<string> listValues = new List<string>();
                listValues.Clear();
                listValues = (List<string>)conn.CreateCommand(SQLquery, listValues);
                return listValues;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            try
            {
                this.DialogResult = DialogResult.OK;
                this.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_cancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.DialogResult = DialogResult.Cancel;
                this.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
