﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ralco_ServiceManager
{
    public partial class Form_Log : Form
    {
        public RMA_Log log { get; set; }
        private SQLConnector conn;
        private List<RMA_Articolo> artListDB;
        private string idRMA;
        private bool isNewLog = true;
        public Form_Log(String idRMA, RMA_Log log_FromForm)
        {
            try
            {
                InitializeComponent();
                setWidgetDescriptions();
                conn = new SQLConnector();
                this.idRMA = idRMA;
                if (log_FromForm != null)
                {
                    isNewLog = false;
                    log = new RMA_Log(log_FromForm);
                    fillForm();
                }
                else
                {
                    log = new RMA_Log(this.idRMA);
                    fillForm();
                }
                    
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void setWidgetDescriptions()
        {
            try
            {
                #region Form
                this.Text = Utils.resourcemanager.GetString("Form_Log");
                #endregion
                #region TooltipsPerBottoni
                ToolTip tooltip = new System.Windows.Forms.ToolTip();
                tooltip.SetToolTip(btn_OK, Utils.resourcemanager.GetString("btn_OK"));
                tooltip.SetToolTip(btn_cancel, Utils.resourcemanager.GetString("btn_cancel"));
                #endregion
                #region Label
                lbl_aggDa.Text = Utils.resourcemanager.GetString("lbl_aggDa");
                lbl_dataAgg.Text = Utils.resourcemanager.GetString("lbl_dataAgg");
                lbl_statoRMA.Text = Utils.resourcemanager.GetString("lbl_statoRMA");
                lbl_note.Text = Utils.resourcemanager.GetString("lbl_note");
                #endregion
                #region Groupbox
                grp_dettRip.Text = Utils.resourcemanager.GetString("grp_dettRip");
                #endregion
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //usato nel caso di import di LOG
        private void fillForm()
        {
            try
            {
                lbl_aggiornatoDa.Text = log.aggiornatoDa;
                dtp_dataAggiornamento.Value = log.dataAggiornamento;
                cmb_statoRMA.SelectedItem = log.stato;
                tb_noteLog.Text = log.note;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //assegna i valori dei vari campi del form agli attributi del problema
        private void updateVariabilePubblica()
        {
            try
            {
                log.idRMA = this.idRMA;
                log.dataAggiornamento = dtp_dataAggiornamento.Value;
                log.aggiornatoDa = lbl_aggiornatoDa.Text;
                log.stato = cmb_statoRMA.SelectedItem.ToString();
                log.note = Utils.parseUselessChars(tb_noteLog.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void commitDB()
        {
            try
            {
                if (isNewLog)
                {
                    conn.CreateCommand("INSERT INTO [Autotest].[dbo].[RMA_Log] " +
                    "([idRMA],[dataAggiornamento],[aggiornatoDa],[stato],[note]) " +
                    "VALUES ('" + log.idRMA + "','" + log.dataAggiornamento.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "','" + log.aggiornatoDa + "','" + log.stato + "','" + log.note.Replace("'", "") + "')", null);
                }
                else
                {
                    conn.CreateCommand("UPDATE [Autotest].[dbo].[RMA_Log] SET" +
                    "[idRMA] = '" + log.idRMA + "'" +
                    ",[dataAggiornamento] = '" + log.dataAggiornamento.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "'" +
                    ",[aggiornatoDa] = '" + log.aggiornatoDa + "'" +
                    ",[stato] = '" + log.stato + "'" +
                    ",[note] = '" + log.note + "'" +
                    " WHERE ([Autotest].[dbo].[RMA_Log].[idRMA] = '" + log.idRMA + "' AND [Autotest].[dbo].[RMA_Log].[idProgressivo] = '" + log.idProgressivo + "')", null);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_okRiparazione_Click(object sender, EventArgs e)
        {
            try
            {
                updateVariabilePubblica();
                commitDB();
                this.DialogResult = DialogResult.OK;
                this.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_cancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.DialogResult = DialogResult.Cancel;
                this.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
