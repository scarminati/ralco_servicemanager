﻿namespace Ralco_ServiceManager
{
    partial class Form_Riparazione
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Riparazione));
            this.lbl_descRip = new System.Windows.Forms.Label();
            this.lbl_parteSost = new System.Windows.Forms.Label();
            this.lbl_note = new System.Windows.Forms.Label();
            this.gb_dettRip = new System.Windows.Forms.GroupBox();
            this.tb_descParte = new System.Windows.Forms.TextBox();
            this.rb_descrizioneParte = new System.Windows.Forms.RadioButton();
            this.nud_Quantita = new System.Windows.Forms.NumericUpDown();
            this.lbl_qta = new System.Windows.Forms.Label();
            this.cmb_statoRiparazione = new System.Windows.Forms.ComboBox();
            this.lbl_statoRip = new System.Windows.Forms.Label();
            this.tb_noteRiparazione = new System.Windows.Forms.TextBox();
            this.cmb_parteSostituita = new System.Windows.Forms.ComboBox();
            this.cmb_descRiparazione = new System.Windows.Forms.ComboBox();
            this.btn_OK = new System.Windows.Forms.Button();
            this.btn_cancel = new System.Windows.Forms.Button();
            this.gb_dettRip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nud_Quantita)).BeginInit();
            this.SuspendLayout();
            // 
            // lbl_descRip
            // 
            this.lbl_descRip.AutoSize = true;
            this.lbl_descRip.Location = new System.Drawing.Point(8, 35);
            this.lbl_descRip.Name = "lbl_descRip";
            this.lbl_descRip.Size = new System.Drawing.Size(116, 13);
            this.lbl_descRip.TabIndex = 1;
            this.lbl_descRip.Text = "Descrizione riparazione";
            // 
            // lbl_parteSost
            // 
            this.lbl_parteSost.AutoSize = true;
            this.lbl_parteSost.Location = new System.Drawing.Point(8, 70);
            this.lbl_parteSost.Name = "lbl_parteSost";
            this.lbl_parteSost.Size = new System.Drawing.Size(76, 13);
            this.lbl_parteSost.TabIndex = 3;
            this.lbl_parteSost.Text = "Parte sostituita";
            // 
            // lbl_note
            // 
            this.lbl_note.AutoSize = true;
            this.lbl_note.Location = new System.Drawing.Point(8, 286);
            this.lbl_note.Name = "lbl_note";
            this.lbl_note.Size = new System.Drawing.Size(30, 13);
            this.lbl_note.TabIndex = 4;
            this.lbl_note.Text = "Note";
            // 
            // gb_dettRip
            // 
            this.gb_dettRip.Controls.Add(this.tb_descParte);
            this.gb_dettRip.Controls.Add(this.rb_descrizioneParte);
            this.gb_dettRip.Controls.Add(this.nud_Quantita);
            this.gb_dettRip.Controls.Add(this.lbl_qta);
            this.gb_dettRip.Controls.Add(this.cmb_statoRiparazione);
            this.gb_dettRip.Controls.Add(this.lbl_statoRip);
            this.gb_dettRip.Controls.Add(this.tb_noteRiparazione);
            this.gb_dettRip.Controls.Add(this.cmb_parteSostituita);
            this.gb_dettRip.Controls.Add(this.cmb_descRiparazione);
            this.gb_dettRip.Controls.Add(this.lbl_parteSost);
            this.gb_dettRip.Controls.Add(this.lbl_descRip);
            this.gb_dettRip.Controls.Add(this.lbl_note);
            this.gb_dettRip.Location = new System.Drawing.Point(12, 12);
            this.gb_dettRip.Name = "gb_dettRip";
            this.gb_dettRip.Size = new System.Drawing.Size(472, 400);
            this.gb_dettRip.TabIndex = 7;
            this.gb_dettRip.TabStop = false;
            this.gb_dettRip.Text = "Dettagli della riparazione";
            // 
            // tb_descParte
            // 
            this.tb_descParte.Enabled = false;
            this.tb_descParte.Location = new System.Drawing.Point(130, 100);
            this.tb_descParte.Multiline = true;
            this.tb_descParte.Name = "tb_descParte";
            this.tb_descParte.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tb_descParte.Size = new System.Drawing.Size(333, 53);
            this.tb_descParte.TabIndex = 12;
            this.tb_descParte.Text = "N/A";
            // 
            // rb_descrizioneParte
            // 
            this.rb_descrizioneParte.AutoSize = true;
            this.rb_descrizioneParte.Location = new System.Drawing.Point(11, 117);
            this.rb_descrizioneParte.Name = "rb_descrizioneParte";
            this.rb_descrizioneParte.Size = new System.Drawing.Size(108, 17);
            this.rb_descrizioneParte.TabIndex = 11;
            this.rb_descrizioneParte.TabStop = true;
            this.rb_descrizioneParte.Text = "Descrizione Parte";
            this.rb_descrizioneParte.UseVisualStyleBackColor = true;
            this.rb_descrizioneParte.CheckedChanged += new System.EventHandler(this.rb_descrizioneParte_CheckedChanged);
            // 
            // nud_Quantita
            // 
            this.nud_Quantita.Location = new System.Drawing.Point(130, 167);
            this.nud_Quantita.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nud_Quantita.Name = "nud_Quantita";
            this.nud_Quantita.Size = new System.Drawing.Size(333, 20);
            this.nud_Quantita.TabIndex = 10;
            this.nud_Quantita.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lbl_qta
            // 
            this.lbl_qta.AutoSize = true;
            this.lbl_qta.Location = new System.Drawing.Point(8, 169);
            this.lbl_qta.Name = "lbl_qta";
            this.lbl_qta.Size = new System.Drawing.Size(47, 13);
            this.lbl_qta.TabIndex = 9;
            this.lbl_qta.Text = "Quantità";
            // 
            // cmb_statoRiparazione
            // 
            this.cmb_statoRiparazione.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmb_statoRiparazione.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_statoRiparazione.FormattingEnabled = true;
            this.cmb_statoRiparazione.Location = new System.Drawing.Point(130, 205);
            this.cmb_statoRiparazione.Name = "cmb_statoRiparazione";
            this.cmb_statoRiparazione.Size = new System.Drawing.Size(333, 21);
            this.cmb_statoRiparazione.Sorted = true;
            this.cmb_statoRiparazione.TabIndex = 4;
            // 
            // lbl_statoRip
            // 
            this.lbl_statoRip.AutoSize = true;
            this.lbl_statoRip.Location = new System.Drawing.Point(8, 206);
            this.lbl_statoRip.Name = "lbl_statoRip";
            this.lbl_statoRip.Size = new System.Drawing.Size(91, 13);
            this.lbl_statoRip.TabIndex = 8;
            this.lbl_statoRip.Text = "Stato Riparazione";
            // 
            // tb_noteRiparazione
            // 
            this.tb_noteRiparazione.Location = new System.Drawing.Point(130, 245);
            this.tb_noteRiparazione.Multiline = true;
            this.tb_noteRiparazione.Name = "tb_noteRiparazione";
            this.tb_noteRiparazione.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tb_noteRiparazione.Size = new System.Drawing.Size(333, 143);
            this.tb_noteRiparazione.TabIndex = 5;
            this.tb_noteRiparazione.Text = "N/A";
            // 
            // cmb_parteSostituita
            // 
            this.cmb_parteSostituita.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmb_parteSostituita.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_parteSostituita.FormattingEnabled = true;
            this.cmb_parteSostituita.Location = new System.Drawing.Point(130, 65);
            this.cmb_parteSostituita.Name = "cmb_parteSostituita";
            this.cmb_parteSostituita.Size = new System.Drawing.Size(333, 21);
            this.cmb_parteSostituita.Sorted = true;
            this.cmb_parteSostituita.TabIndex = 2;
            this.cmb_parteSostituita.SelectedIndexChanged += new System.EventHandler(this.cmb_parteSostituita_SelectedIndexChanged);
            // 
            // cmb_descRiparazione
            // 
            this.cmb_descRiparazione.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmb_descRiparazione.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_descRiparazione.FormattingEnabled = true;
            this.cmb_descRiparazione.Location = new System.Drawing.Point(130, 30);
            this.cmb_descRiparazione.Name = "cmb_descRiparazione";
            this.cmb_descRiparazione.Size = new System.Drawing.Size(333, 21);
            this.cmb_descRiparazione.Sorted = true;
            this.cmb_descRiparazione.TabIndex = 1;
            // 
            // btn_OK
            // 
            this.btn_OK.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_OK.BackgroundImage")));
            this.btn_OK.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_OK.Location = new System.Drawing.Point(164, 435);
            this.btn_OK.Name = "btn_OK";
            this.btn_OK.Size = new System.Drawing.Size(50, 50);
            this.btn_OK.TabIndex = 6;
            this.btn_OK.UseVisualStyleBackColor = true;
            this.btn_OK.Click += new System.EventHandler(this.btn_okRiparazione_Click);
            // 
            // btn_cancel
            // 
            this.btn_cancel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_cancel.BackgroundImage")));
            this.btn_cancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_cancel.Location = new System.Drawing.Point(269, 435);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(50, 50);
            this.btn_cancel.TabIndex = 7;
            this.btn_cancel.UseVisualStyleBackColor = true;
            this.btn_cancel.Click += new System.EventHandler(this.btn_cancelRiparazione_Click);
            // 
            // Form_Riparazione
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(496, 507);
            this.Controls.Add(this.btn_cancel);
            this.Controls.Add(this.btn_OK);
            this.Controls.Add(this.gb_dettRip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(512, 545);
            this.MinimumSize = new System.Drawing.Size(512, 545);
            this.Name = "Form_Riparazione";
            this.Text = "Dettagli Riparazione";
            this.gb_dettRip.ResumeLayout(false);
            this.gb_dettRip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nud_Quantita)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lbl_descRip;
        private System.Windows.Forms.Label lbl_parteSost;
        private System.Windows.Forms.Label lbl_note;
        private System.Windows.Forms.GroupBox gb_dettRip;
        private System.Windows.Forms.ComboBox cmb_parteSostituita;
        private System.Windows.Forms.ComboBox cmb_descRiparazione;
        private System.Windows.Forms.TextBox tb_noteRiparazione;
        private System.Windows.Forms.Button btn_OK;
        private System.Windows.Forms.ComboBox cmb_statoRiparazione;
        private System.Windows.Forms.Label lbl_statoRip;
        private System.Windows.Forms.Button btn_cancel;
        private System.Windows.Forms.Label lbl_qta;
        private System.Windows.Forms.NumericUpDown nud_Quantita;
        private System.Windows.Forms.RadioButton rb_descrizioneParte;
        private System.Windows.Forms.TextBox tb_descParte;
    }
}