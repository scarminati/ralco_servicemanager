﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;

namespace Ralco_ServiceManager
{
    public partial class Form_Articolo : Form
    {
        public RMA_Articolo articolo { get; set; }
        private SQLConnector conn;
        public Form_Articolo(List<RMA_Articolo> artList)
        {
            try
            {
                InitializeComponent();
                conn = new SQLConnector();
                setWidgetDescriptions();
                setCmbArticoli(artList);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Imposta le descrizioni per i vari widget (bottoni, ...) a seconda della lingua dell'utente (impostata con il Utils.setResourceManager();)
        /// </summary>
        private void setWidgetDescriptions()
        {
            try
            {
                #region Form
                this.Text = Utils.resourcemanager.GetString("Form_Articolo");
                #endregion
                #region TooltipsPerBottoni
                ToolTip tooltip = new System.Windows.Forms.ToolTip();
                tooltip.SetToolTip(btn_OK, Utils.resourcemanager.GetString("btn_OK"));
                tooltip.SetToolTip(btn_cancel, Utils.resourcemanager.GetString("btn_cancel"));
                #endregion
                #region Label
                lbl_IDArticolo.Text = Utils.resourcemanager.GetString("lbl_IDArticolo");
                lbl_Desc.Text = Utils.resourcemanager.GetString("lbl_Desc");
                lbl_prUnit.Text = Utils.resourcemanager.GetString("lbl_prUnit");
                lbl_qta.Text = Utils.resourcemanager.GetString("lbl_qta");
                lbl_prTot.Text = Utils.resourcemanager.GetString("lbl_prTot");
                #endregion
                #region Groupbox
                grp_RSRO_Rif.Text = Utils.resourcemanager.GetString("grp_RSRO_Rif");
                #endregion
                #region DataGridViewColumns
                dgv_ListaRifRSRO.Columns["codice"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaNonConf_IDRMA");
                dgv_ListaRifRSRO.Columns["descr"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaNonConf_Cliente");
                #endregion
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void setCmbArticoli(List<RMA_Articolo> artList)
        {
            try
            {
                cmb_IDArticoli.Items.Clear();
                foreach (RMA_Articolo a in artList)
                    cmb_IDArticoli.Items.Add(a.idArticolo);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cmb_IDArticoli_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cmb_IDArticoli.SelectedItem != null)
                {
                    articolo = new RMA_Articolo();
                    articolo = (RMA_Articolo)conn.CreateCommand("SELECT DISTINCT ID_AZIENDA, ID_ARTICOLO, UPPER (DESCRIZIONE), ID_SCELTA, ID_SCELTA FROM [archivi].[dbo].[vistaListaArticoliPanth] WHERE ID_LINGUA = 'it' AND ID_ARTICOLO = '" + cmb_IDArticoli.SelectedItem + "'", articolo);
                    if (articolo != null)
                    {
                        articolo.idArticolo = articolo.idArticolo;
                        nud_Quantita.Value = 1;
                        fillForm();
                        //articolo.quantita = Convert.ToInt32(nud_Quantita.Value);
                        //articolo.calculatePrice(articolo);
                        //articolo.addArticleEnglishDescription();
                        //lbl_descrizione.Text = articolo.descrizione;
                        //lbl_prezzoUnit.Text = articolo.prezzoUnitario.ToString();
                        //articolo.prezzoTotale = (articolo.prezzoUnitario * (nud_Quantita.Value));
                        //lbl_prezzoTot.Text = articolo.prezzoTotale.ToString();
                    }
                    else
                    {
                        MessageBox.Show("ID articolo non corretto", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                    MessageBox.Show("ID articolo non corretto", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void fillForm()
        {
            try
            {
                articolo.quantita = Convert.ToInt32(nud_Quantita.Value);
                articolo.calculatePrice(articolo);
                articolo.addArticleEnglishDescription();
                lbl_descrizione.Text = Utils.parseUselessChars(articolo.descrizione);
                lbl_prezzoUnit.Text = articolo.prezzoUnitario.ToString();
                lbl_prezzoTot.Text = (articolo.prezzoUnitario * (nud_Quantita.Value)).ToString();
                dgv_ListaRifRSRO.Rows.Clear();
                List<RMA_Articolo> rsro_Rif = new List<RMA_Articolo>();
                rsro_Rif = (List<RMA_Articolo>)conn.CreateCommand("SELECT [ID_AZIENDA],[PADRE],[DSC_PADRE],[PROGRESSIVO],[COSTO] FROM [archivi].[dbo].[VISTA_SM_MODPRO_PANTH] WHERE [FIGLIO] = '" + cmb_IDArticoli.SelectedItem.ToString() + "'", rsro_Rif);
                if (rsro_Rif != null)
                {
                    foreach (RMA_Articolo r in rsro_Rif)
                        if (r.idArticolo.StartsWith("RS") || r.idArticolo.StartsWith("RO"))
                            dgv_ListaRifRSRO.Rows.Insert(0, false, r.idArticolo, r.descrizione);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void nud_Quantita_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                //mettere la condizione sul prezzo totale : se MP, prezzotot = (prezzoUnit*5)*quantità; se Gruppo, usare gli RS.
                fillForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            try
            {
                if (articolo.idArticolo.Equals("") || articolo.descrizione.Equals(""))
                    MessageBox.Show("Seleziona almeno un articolo da inserire nel preventivo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else if (nud_Quantita.Value.Equals(0))
                    MessageBox.Show("Seleziona almeno un pezzo per l'articolo selezionato", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                {
                    this.DialogResult = DialogResult.OK;
                    this.Dispose();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_cancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.DialogResult = DialogResult.Cancel;
                this.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
