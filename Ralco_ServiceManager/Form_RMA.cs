﻿#define USEPANTHERA
//#undef USEPANTHERA
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Reflection;
using Xceed.Words.NET;
using System.Net.Mail;
using System.Diagnostics;
using System.Drawing.Printing;
using System.Configuration;
using Zen.Barcode;
using System.Security.Principal;

namespace Ralco_ServiceManager
{
    public partial class Form_RMA : Form
    {
        //variabili usate per il tab Generali
        public RMA_Generali rmaGen { get; set; }
        public List<RMA_Collimatore> collList { get; set; }
        public List<RMA_Parte> partList { get; set; }
        private List<int> partiAdded { get; set; }
        private List<String> serialiAdded { get; set; }
        private List<String> numFlowChart { get; set; }
        //variabili usate per il tab Informativa Cliente
        public RMA_InformativaCliente infoCli { get; set; }
        //variabile usata per il tab Allegati
        public List<RMA_Allegato> allegatiList { get; set; }
        //variabili usate per il tab Analisi del Problema
        public List<RMA_Problema> probList { get; set; }
        public List<RMA_Problema> probRemovedFromList { get; set; }
        public List<RMA_DifettoRiscontrato> difList { get; set; }
        //variabili usate per il tab Garanzia
        public RMA_GaranziaRalco garanziaRalco { get; set; }
        //variabili usate per il tab Preventivi
        public List<RMA_Preventivo> prevList { get; set; }
        public List<RMA_Preventivo> prevRemovedFromList { get; set; }
        //variabili usate per il tab Riparazione
        public List<RMA_Riparazione> ripList { get; set; }
        public List<RMA_Riparazione> ripRemovedFromList { get; set; }
        //variabili usate per il tab FasiDaRieseguire
        public RMA_FaseProduzione minFasiDaRiesList { get; set; }
        //variabili usate per il tab TestDaRieseguire
        public List<RMA_TestDaEseguire> testDaEseguireList { get; set; }
        //variabili usate per il tab Azioni Supplementari
        public List<RMA_EsitiAzioniSupplementari> esitiAzioniSupplementariList { get; set; }
        //variabili usate per il tab Note Qualita
        public RMA_NoteQualita noteQualita { get; set; }
        //variabili usate per il tab Note RMA
        public List<RMA_Log> logList { get; set; }
        //variabili usate per il form
        private List<String> nomeFaseDaRieseguire;
        private List<String> nomeTestDaEseguire;
        private List<RMA_ClientiRalco> cliList;
        private Object obj = null;
        private RMA_View rma_fromForm;
        private string idRMA;
        private SQLConnector conn;
        private const decimal COEFF_MOLT = 2.5m;
        private bool firstSave = true;//parametro usato dal bottone saveRMA
        private StreamReader streamToPrint;
        private System.Drawing.Font printFont;
        List<string> listAzioniSuppl { get; set; }
        private bool isClonedRMA = false;
        public Form_RMA(RMA_View rma, params bool[] isClonedRMA)
        {
            try
            {
                InitializeComponent();
                conn = new SQLConnector();
                //inizializzazione variabili delle varie list usate in fase di insert nel database alla pressione del tasto OK
                this.rma_fromForm = rma;
                collList = new List<RMA_Collimatore>();
                partList = new List<RMA_Parte>();
                allegatiList = new List<RMA_Allegato>();
                probList = new List<RMA_Problema>();
                prevList = new List<RMA_Preventivo>();
                ripList = new List<RMA_Riparazione>();
                difList = new List<RMA_DifettoRiscontrato>();
                minFasiDaRiesList = new RMA_FaseProduzione();
                testDaEseguireList = new List<RMA_TestDaEseguire>();
                esitiAzioniSupplementariList = new List<RMA_EsitiAzioniSupplementari>();
                nomeFaseDaRieseguire = new List<String>();
                nomeFaseDaRieseguire = (List<String>)conn.CreateCommand("SELECT [descrizFaseProduzione] FROM [Autotest].[dbo].[RMA_FasiProduzione]", nomeFaseDaRieseguire);
                probRemovedFromList = new List<RMA_Problema>();
                prevRemovedFromList = new List<RMA_Preventivo>();
                ripRemovedFromList = new List<RMA_Riparazione>();
                partiAdded = new List<int>();
                serialiAdded = new List<string>();
                numFlowChart = new List<string>();
                //imposto il valore iniziale delle datetimepicker alla data odierna.
                dtp_DataArrivoCollimatore.Value = DateTime.Now;
                dtp_dataScadenzaRMA.Value = DateTime.Now;
                dtp_dataOrdRip.Value = DateTime.Now;
                dtp_dataDocReso.Value = DateTime.Now;
                dtp_dataSpedizione.Value = DateTime.Now;
                //e setto i valori delle textbox corrispondenti a ""
                tb_DataArrivoCollimatore.Text = "  /  /";
                tb_DataScadenzaRMA.Text = "  /  /";
                tb_DataOrdineRiparazione.Text = "  /  /";
                tb_DataDocumentoDiReso.Text = "  /  /";
                tb_DataSpedizione.Text = "  /  /";
                cmb_CollSelection.SelectedIndex = 0;
                cmb_ParteSelection.SelectedIndex = 0;
                cmb_PrevPrezzi.SelectedIndex = 1;
                setDifferenzeForm();
                setCmbListaClienti();
                setWidgetDescriptions();//da completare
                if (rma_fromForm == null)
                {
                    calcolaIDRMA();
                }
                else
                {
                    refreshFormRMA();
                    if (!isClonedRMA.Length.Equals(0) && isClonedRMA[0].Equals(true))
                    {
                        calcolaIDRMA();
                        if (collList != null)
                        {
                            Form_Collimatore fc = new Form_Collimatore(idRMA, collList);
                            fc.Dispose();
                        }
                        if (partList != null)
                        {
                            Form_Parte fp = new Form_Parte(idRMA, partList);
                            fp.Dispose();
                        }
                        if (probList != null)
                        {
                            Form_Problema fp = new Form_Problema(idRMA, probList);
                            fp.Dispose();
                        }
                        if (ripList != null)
                        {
                            Form_Riparazione fp = new Form_Riparazione(idRMA, ripList);
                            fp.Dispose();
                        }
                        if (prevList != null)
                        {
                            Form_Preventivo fp = new Form_Preventivo(idRMA, prevList);
                            fp.Dispose();
                        }
                        rmaGen.idRMA = idRMA;
                        infoCli.idRMA = idRMA;
                        garanziaRalco.idRMA = idRMA;
                        noteQualita.idRMA = idRMA;
                        this.isClonedRMA = isClonedRMA[0];
                        rma_fromForm = null;
                        commitDB(0);
                        rma_fromForm = rma;
                        rma_fromForm.idRMA = idRMA;
                    }
                    else
                    {
                        idRMA = rma.idRMA;
                    }
                    cmb_statoRMA.BackColor = Color.LightSalmon;
                }
                refreshTableFasiDaRieseguire();
                initializeTableTestDaEseguire();
                tabctrl_RMA_SelectedIndexChanged(null, null);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        /// <summary>
        /// Restituisce una lista di stringhe in cui:
        /// posizione 0 = seriali
        /// posizione 1 = flow chart
        /// </summary>
        private List<String> getSerialiEFlowChart()
        {
            try
            {
                List<String> dataString = new List<String>();
                string seriali = "";
                string flowChart = "";
                int totSerPerLine = 0;
                if (collList != null)
                {
                    foreach (RMA_Collimatore c in collList)
                        if (seriali.Equals(""))
                        {
                            seriali = seriali + c.seriale;
                            totSerPerLine++;
                            flowChart = flowChart + c.numFlowChart;
                        }
                        else
                        {
                            if (!flowChart.Contains(c.numFlowChart))
                            {
                                flowChart = flowChart + "\n\n" + c.numFlowChart;
                                seriali = seriali + "\n\n" + c.seriale;
                                totSerPerLine = 1;
                            }
                            else
                            {
                                if (totSerPerLine <= 3)
                                {
                                    seriali = seriali + "," + c.seriale;
                                    totSerPerLine++;
                                }
                                else
                                {
                                    seriali = seriali + ",\n" + c.seriale;
                                    flowChart = flowChart + "\n";
                                    totSerPerLine = 1;
                                }
                            }

                        }
                }
                else if (partList != null)
                {
                    foreach (RMA_Parte p in partList)
                        if (seriali.Equals(""))
                        {
                            seriali = seriali + p.serialeParte;
                            totSerPerLine++;
                            flowChart = flowChart + p.numFlowChart;
                        }
                        else
                        {
                            if (!flowChart.Contains(p.numFlowChart))
                            {
                                flowChart = flowChart + "\n\n" + p.numFlowChart;
                                seriali = seriali + "\n\n" + p.serialeParte;
                                totSerPerLine = 1;
                            }
                            else
                            {
                                if (totSerPerLine <= 3)
                                {
                                    seriali = seriali + "," + p.serialeParte;
                                    totSerPerLine++;
                                }
                                else
                                {
                                    seriali = seriali + ",\n" + p.serialeParte;
                                    flowChart = flowChart + "\n";
                                    totSerPerLine = 1;
                                }
                            }

                        }
                }
                dataString.Add(seriali);
                dataString.Add(flowChart);
                return (dataString);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return (null);
            }
        }

        private void dtp_DataArrivoCollimatore_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                tb_DataArrivoCollimatore.Text = dtp_DataArrivoCollimatore.Value.ToShortDateString();
                if (tb_DataScadenzaRMA.Text.Equals("  /  /"))
                {
                    tb_DataScadenzaRMA.Text = dtp_DataArrivoCollimatore.Value.AddDays(38).ToShortDateString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dtp_dataScadenzaRMA_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                tb_DataScadenzaRMA.Text = dtp_dataScadenzaRMA.Value.ToShortDateString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dtp_dataOrdRip_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                tb_DataOrdineRiparazione.Text = dtp_dataOrdRip.Value.ToShortDateString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dtp_dataDocReso_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                tb_DataDocumentoDiReso.Text = dtp_dataDocReso.Value.ToShortDateString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dtp_dataSpedizione_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                tb_DataSpedizione.Text = dtp_dataSpedizione.Value.ToShortDateString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void setDifferenzeForm()
        {
            try
            {
                cmb_tipoRMA.Items.Clear();
                if (Globals.user.getUserCredential().Equals("admin"))
                {
                    cmb_tipoRMA.Items.Add("Esterno");
                    cmb_tipoRMA.Items.Add("Interno");
                    if (rma_fromForm == null)
                        cmb_statoRMA.SelectedIndex = 0;
                }
                else
                {
                    if (rma_fromForm != null)
                    {
                        //Edit CNC
                        cmb_tipoRMA.Items.Add(rma_fromForm.tipoRMA);
                        if (rma_fromForm.tipoRMA.Equals("Esterno"))
                        {
                            if (Globals.user.getUserCredential().Equals("NonConfInt"))
                            {
                                //tabctrl_RMA.TabPages.Remove(tab_infoCliente);
                                //tabctrl_RMA.TabPages.Remove(tab_detGaranzia);
                                tabctrl_RMA.TabPages.Remove(tab_Preventivi);
                                tabctrl_RMA.TabPages.Remove(tab_Trasporto);
                                //tabctrl_RMA.TabPages.Remove(tab_NoteRMA);
                            }
                        }
                        else if (rma_fromForm.tipoRMA.Equals("Interno"))
                        {
                            tabctrl_RMA.TabPages.Remove(tab_infoCliente);
                            tabctrl_RMA.TabPages.Remove(tab_detGaranzia);
                            tabctrl_RMA.TabPages.Remove(tab_AzioniSuppl);
                            tabctrl_RMA.TabPages.Remove(tab_Preventivi);
                            tabctrl_RMA.TabPages.Remove(tab_Trasporto);
                            //tabctrl_RMA.TabPages.Remove(tab_NoteRMA);
                        }
                    }
                    else
                    {
                        //Nuovo CNC
                        if (Globals.user.getUserCredential().Equals("NonConfExt") || Globals.user.getUserCredential().Equals("admin"))
                        {
                            cmb_tipoRMA.Items.Add("Esterno");
                            //combobox del tab_detGaranzia, non visibile nel caso di apertura di nuovo CNC 
                            cmb_provCollimatore.SelectedIndex = -1;
                            cmb_anniVitaCollimatore.SelectedIndex = -1;
                            cmb_respDannoCollimatore.SelectedIndex = -1;
                            cmb_azioniSupplRalco.SelectedIndex = -1;
                        }
                        else if (Globals.user.getUserCredential().Equals("NonConfInt"))
                        {
                            cmb_tipoRMA.Items.Add("Interno");
                            //Non visualizzo i tab che non servono nel caso di apertura di un nuovo CNC INT
                            tabctrl_RMA.TabPages.Remove(tab_infoCliente);
                            tabctrl_RMA.TabPages.Remove(tab_detGaranzia);
                            tabctrl_RMA.TabPages.Remove(tab_AzioniSuppl);
                            tabctrl_RMA.TabPages.Remove(tab_Preventivi);
                            tabctrl_RMA.TabPages.Remove(tab_Trasporto);
                            //tabctrl_RMA.TabPages.Remove(tab_NoteRMA);
                        }
                        //Valori di default del form validi per entrambi i casi di nuovo CNC
                        cmb_statoRMA.SelectedIndex = 0;
                        cmb_sicProdotto.SelectedIndex = 1;
                        cmb_segIncidente.SelectedIndex = 1;
                        cmb_invioNotaInf.SelectedIndex = 1;
                        cmb_revDocValutRischi.SelectedIndex = 1;
                        cmb_statoAzCorr.SelectedIndex = 1;
                    }
                    cmb_tipoRMA.DropDownStyle = ComboBoxStyle.Simple;
                    cmb_tipoRMA.Enabled = false;
                }
                cmb_tipoRMA.SelectedIndex = 0;
                cmb_prioritaRMA.SelectedIndex = 2;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cmb_statoRMA_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cmb_statoRMA.SelectedItem.Equals("Riparazione Completata"))
                {
                    if (ripList != null && !ripList.Count.Equals(0))
                        foreach (RMA_Riparazione r in ripList)
                            r.statoRip = "Completata";
                }
                else if (cmb_statoRMA.SelectedItem.Equals("Chiuso"))
                {
                    cmb_prioritaRMA.SelectedItem = "3 - Bassa";
                }
                //simulo il cambio del tab per aggiornare le variabili
                tabctrl_RMA_SelectedIndexChanged(null, null);
                //coloro lo sfondo della combobox di bianco
                cmb_statoRMA.BackColor = Color.White;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void setCmbListaClienti()
        {
            try
            {
                cmb_nomeCliente.Items.Clear();
                cliList = new List<RMA_ClientiRalco>();
                cliList = (List<RMA_ClientiRalco>)conn.CreateCommand("SELECT * FROM [archivi].[dbo].[VISTA_SM_CLIENTI] order by ID", cliList);
                foreach (RMA_ClientiRalco cli in cliList)
                    cmb_nomeCliente.Items.Add(cli.nomeCliente.TrimStart().TrimEnd());
                cmb_nomeCliente.Sorted = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //attivata ogni volta che modifico il valore della combobox
        private void cmb_nomeCliente_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                lbl_idCliente.Text = "";
                lbl_idCliente.Text = cliList.First(r => r.nomeCliente.TrimEnd().Equals(cmb_nomeCliente.SelectedItem.ToString())).IdCliente.ToString();
                setTipoImballaggio();
                dgv_azioniSupplementari.Rows.Clear();
                btn_RefreshAzSuppl_Click(null, null);
            }
            catch (Exception ex)
            {
                lbl_idCliente.Text = "N/A";
            }
            try
            {
                lbl_codCollCliente.Text = "";
                RMA_Collimatore coll = new RMA_Collimatore();
                coll = (RMA_Collimatore)conn.CreateCommand("SELECT PROGR, nFlow,'" + collList[0].seriale + "' AS Serial, Model, dtCEf, " +
                                                             "Idcliente, Ragsoc, CodCli, NumeroFormattato, Da_N, A_N, " +
                                                              "Qta, Data FROM [archivi].[dbo].[VISTA_SM_FC_2] WHERE [archivi].[dbo].[VISTA_SM_FC_2].[Model] = '" + collList[0].modelloColl + "' AND [RagSoc] = " + "(SELECT [RagSoc] FROM [archivi].[dbo].[Clienti] WHERE [Cliente] = " + lbl_idCliente.Text + ")" , coll);
                lbl_codCollCliente.Text = coll.codCollCliente;
                setTipoImballaggio();
            }
            catch (Exception ex)
            {
                lbl_codCollCliente.Text = "N/A";
            }
        }

        private void setTipoImballaggio()
        {
            try
            {
                if (cmb_nomeCliente.SelectedItem.ToString().Contains("Carestream"))
                    cmb_tipoImballaggio.SelectedItem = "Imballo Kodak / Kodak packaging";
                else if (cmb_nomeCliente.SelectedItem.ToString().Contains("Philips"))
                    cmb_tipoImballaggio.SelectedItem = "Cassa di legno / Wood box";
                else
                    cmb_tipoImballaggio.SelectedItem = "Instapack";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void setWidgetDescriptions()
        {
            try
            {
                #region TooltipsPerBottoni
                ToolTip tooltip = new System.Windows.Forms.ToolTip();
                tooltip.SetToolTip(btn_saveRMA, Utils.resourcemanager.GetString("btn_saveRMA"));
                tooltip.SetToolTip(btn_SaveAndClose, Utils.resourcemanager.GetString("btn_SaveAndCloseRMA"));
                tooltip.SetToolTip(btn_cancel, Utils.resourcemanager.GetString("btn_cancel"));
                #region TabCollimatore
                btn_RefreshCollRMA.ToolTipText = Utils.resourcemanager.GetString("btn_RefreshCollRMA");
                btn_AddCollRMA.ToolTipText = Utils.resourcemanager.GetString("btn_AddCollRMA");
                btn_CancelCollRMA.ToolTipText = Utils.resourcemanager.GetString("btn_CancelCollRMA");
                btn_StampaEtichetteColl.ToolTipText = Utils.resourcemanager.GetString("btn_StampaEtichetteColl");
                btn_visualizzaScansioneFlowChart.ToolTipText = Utils.resourcemanager.GetString("btn_visualizzaScansioneFlowChart");
                btn_visualizzaFlowTree.ToolTipText = Utils.resourcemanager.GetString("btn_visualizzaFlowTree");
                btn_DHR.ToolTipText = Utils.resourcemanager.GetString("btn_DHR");
                #endregion
                #region TabParte
                btn_RefreshParteRMA.ToolTipText = Utils.resourcemanager.GetString("btn_RefreshParteRMA");
                btn_AddParteRMA.ToolTipText = Utils.resourcemanager.GetString("btn_AddParteRMA");
                btn_CancelParteRMA.ToolTipText = Utils.resourcemanager.GetString("btn_CancelParteRMA");
                btn_EditParteRMA.ToolTipText = Utils.resourcemanager.GetString("btn_EditParteRMA");
                btn_StampaEtichetteParte.ToolTipText = Utils.resourcemanager.GetString("btn_StampaEtichetteParte");
                #endregion
                #region TabAllegati
                btn_RefreshAllegati.ToolTipText = Utils.resourcemanager.GetString("btn_RefreshAllegati");
                btn_AddAllegati.ToolTipText = Utils.resourcemanager.GetString("btn_AddAllegati");
                btn_DeleteAllegati.ToolTipText = Utils.resourcemanager.GetString("btn_DeleteAllegati");
                btn_viewAllegati.ToolTipText = Utils.resourcemanager.GetString("btn_viewAllegati");
                #endregion
                #region TabAnalisiProblema
                btn_RefreshProbl.ToolTipText = Utils.resourcemanager.GetString("btn_RefreshProbl");
                btn_importProbFromTemplateDB.ToolTipText = Utils.resourcemanager.GetString("btn_importProbFromTemplateDB");
                btn_AddProbl.ToolTipText = Utils.resourcemanager.GetString("btn_AddProbl");
                btn_CloneProblema.ToolTipText = Utils.resourcemanager.GetString("btn_CloneProblema");
                btn_DeleteProbl.ToolTipText = Utils.resourcemanager.GetString("btn_DeleteProbl");
                btn_EditProbl.ToolTipText = Utils.resourcemanager.GetString("btn_EditProbl");
                btn_addTemplateProbDB.ToolTipText = Utils.resourcemanager.GetString("btn_addTemplateProbDB");
                btn_editTemplateProbDB.ToolTipText = Utils.resourcemanager.GetString("btn_editTemplateProbDB");
                #endregion
                #region TabRiparazioni
                btn_RefreshRiparazione.ToolTipText = Utils.resourcemanager.GetString("btn_RefreshRiparazione");
                btn_AddRiparazione.ToolTipText = Utils.resourcemanager.GetString("btn_AddRiparazione");
                btn_importRipFromTemplateDB.ToolTipText = Utils.resourcemanager.GetString("btn_importRipFromTemplateDB");
                btn_CloneRiparazione.ToolTipText = Utils.resourcemanager.GetString("btn_CloneRiparazione");
                btn_DeleteRiparazione.ToolTipText = Utils.resourcemanager.GetString("btn_DeleteRiparazione");
                btn_EditRiparazione.ToolTipText = Utils.resourcemanager.GetString("btn_EditRiparazione");
                btn_addTemplateRipDB.ToolTipText = Utils.resourcemanager.GetString("btn_addTemplateRipDB");
                btn_editTemplateRipDB.ToolTipText = Utils.resourcemanager.GetString("btn_editTemplateRipDB");
                btn_PrintRiparazioni.ToolTipText = Utils.resourcemanager.GetString("btn_PrintRiparazioni");
                #endregion
                #region TabPreventivi
                btn_RefreshPreventivi.ToolTipText = Utils.resourcemanager.GetString("btn_RefreshPreventivi");
                btn_AddPreventivo.ToolTipText = Utils.resourcemanager.GetString("btn_AddPreventivo");
                btn_ClonePreventivo.ToolTipText = Utils.resourcemanager.GetString("btn_ClonePreventivo");
                btn_DeletePreventivo.ToolTipText = Utils.resourcemanager.GetString("btn_DeletePreventivo");
                btn_EditPreventivo.ToolTipText = Utils.resourcemanager.GetString("btn_EditPreventivo");
                btn_PrintPreventivo.ToolTipText = Utils.resourcemanager.GetString("btn_PrintPreventivo");
                #endregion
                #region TabAzioniSupplementari
                btn_RefreshAzSuppl.ToolTipText = Utils.resourcemanager.GetString("btn_RefreshAzSuppl");
                btn_GestisciAzSuppl.ToolTipText = Utils.resourcemanager.GetString("btn_EditAzSuppl");
                #endregion
                #region TabNoteRMA
                btn_RefreshNoteRMA.ToolTipText = Utils.resourcemanager.GetString("btn_RefreshNoteRMA");
                btn_AddNoteRMA.ToolTipText = Utils.resourcemanager.GetString("btn_AddNoteRMA");
                btn_DeleteNoteRMA.ToolTipText = Utils.resourcemanager.GetString("btn_DeleteNoteRMA");
                btn_EditNoteRMA.ToolTipText = Utils.resourcemanager.GetString("btn_EditNoteRMA");
                btn_PrintGenericInfo.ToolTipText = Utils.resourcemanager.GetString("btn_PrintGenericInfo");
                #endregion
                #endregion
                #region Label
                lbl_priorita.Text = Utils.resourcemanager.GetString("lbl_priorita");
                lbl_stato.Text = Utils.resourcemanager.GetString("lbl_stato");
                lbl_numeroRMA.Text = Utils.resourcemanager.GetString("lbl_numeroRMA");
                lbl_dataCreazRMA.Text = Utils.resourcemanager.GetString("lbl_dataCreazRMA");
                lbl_tipoRMA.Text = Utils.resourcemanager.GetString("lbl_tipoRMA");
                lbl_noteGen.Text = Utils.resourcemanager.GetString("lbl_noteGen");
                lbl_nomeCliente.Text = Utils.resourcemanager.GetString("lbl_nomeCliente");
                lbl_idCli.Text = Utils.resourcemanager.GetString("lbl_idCli");
                lbl_codCollCli.Text = Utils.resourcemanager.GetString("lbl_codCollCli");
                lbl_decisCli.Text = Utils.resourcemanager.GetString("lbl_decisCli");
                lbl_dataArrColl.Text = Utils.resourcemanager.GetString("lbl_dataArrColl");
                lbl_dataScadRMA.Text = Utils.resourcemanager.GetString("lbl_dataScadRMA");
                lbl_SCARcli.Text = Utils.resourcemanager.GetString("lbl_SCARcli");
                lbl_numSCARcli.Text = Utils.resourcemanager.GetString("lbl_numSCARcli");
                lbl_ordRip.Text = Utils.resourcemanager.GetString("lbl_ordRip");
                lbl_dataOrdRip.Text = Utils.resourcemanager.GetString("lbl_dataOrdRip");
                lbl_docReso.Text = Utils.resourcemanager.GetString("lbl_docReso");
                lbl_dataDocReso.Text = Utils.resourcemanager.GetString("lbl_dataDocReso");
                lbl_difRiscCli.Text = Utils.resourcemanager.GetString("lbl_difRiscCli");
                lbl_provColl.Text = Utils.resourcemanager.GetString("lbl_provColl");
                lbl_anniVitaColl.Text = Utils.resourcemanager.GetString("lbl_anniVitaColl");
                lbl_respDannoColl.Text = Utils.resourcemanager.GetString("lbl_respDannoColl");
                lbl_note.Text = Utils.resourcemanager.GetString("lbl_note");
                lbl_azSupplRalco.Text = Utils.resourcemanager.GetString("lbl_azSupplRalco");
                lbl_dataSped.Text = Utils.resourcemanager.GetString("lbl_dataSped");
                lbl_speseSped.Text = Utils.resourcemanager.GetString("lbl_speseSped");
                lbl_noteTrasporto.Text = Utils.resourcemanager.GetString("lbl_noteTrasporto");
                lbl_sicProd.Text = Utils.resourcemanager.GetString("lbl_sicProd");
                lbl_segnIncid.Text = Utils.resourcemanager.GetString("lbl_segnIncid");
                lbl_invioNotaInf.Text = Utils.resourcemanager.GetString("lbl_invioNotaInf");
                lbl_revDocValutazRischi.Text = Utils.resourcemanager.GetString("lbl_revDocValutazRischi");
                lbl_statoAzCorr.Text = Utils.resourcemanager.GetString("lbl_stato");
                lbl_rif.Text = Utils.resourcemanager.GetString("lbl_rif");
                lbl_descAzCorr.Text = Utils.resourcemanager.GetString("lbl_Desc");
                lbl_noteQualita.Text = Utils.resourcemanager.GetString("lbl_noteQualita");
                #endregion
                #region Groupbox
                gb_InfoGenerali.Text = Utils.resourcemanager.GetString("gb_InfoGenerali");
                gb_dettagli.Text = Utils.resourcemanager.GetString("gb_dettagli");
                gb_impattoSCAR.Text = Utils.resourcemanager.GetString("gb_impattoSCAR");
                gb_azCorrPrev.Text = Utils.resourcemanager.GetString("gb_azCorrPrev");
                #endregion
                #region tabControl
                tabctrl_RMA.TabPages["tab_generalInfo"].Text = Utils.resourcemanager.GetString("tabctrl_RMA_tab_generalInfo");
                tabctrl_RMA.TabPages["tab_Analisi"].Text = Utils.resourcemanager.GetString("tabctrl_RMA_tab_Analisi");
                tabctrl_RMA.TabPages["tab_FasiDaRieseguire"].Text = Utils.resourcemanager.GetString("tabctrl_RMA_tab_FasiDaRieseguire");
                tabctrl_RMA.TabPages["tab_TestDaEseguire"].Text = Utils.resourcemanager.GetString("tabctrl_RMA_tab_TestDaEseguire");
                tabctrl_RMA.TabPages["tab_Riparazioni"].Text = Utils.resourcemanager.GetString("tabctrl_RMA_tab_Riparazioni");
                if (Globals.user.getUserCredential().Equals("NonConfExt") || Globals.user.getUserCredential().Equals("admin"))
                {
                    tabctrl_RMA.TabPages["tab_infoCliente"].Text = Utils.resourcemanager.GetString("tabctrl_RMA_tab_infoCliente");
                    tabctrl_RMA.TabPages["tab_Allegati"].Text = Utils.resourcemanager.GetString("tabctrl_RMA_tab_Allegati");
                    tabctrl_RMA.TabPages["tab_detGaranzia"].Text = Utils.resourcemanager.GetString("tabctrl_RMA_tab_detGaranzia");
                    tabctrl_RMA.TabPages["tab_Preventivi"].Text = Utils.resourcemanager.GetString("tabctrl_RMA_tab_Preventivi");
                    tabctrl_RMA.TabPages["tab_AzioniSuppl"].Text = Utils.resourcemanager.GetString("tabctrl_RMA_tab_AzioniSuppl");
                    tabctrl_RMA.TabPages["tab_Trasporto"].Text = Utils.resourcemanager.GetString("tabctrl_RMA_tab_Trasporto");
                }
                tabctrl_RMA.TabPages["tab_Qualita"].Text = Utils.resourcemanager.GetString("tabctrl_RMA_tab_Qualita");
                tabctrl_RMA.TabPages["tab_NoteRMA"].Text = Utils.resourcemanager.GetString("tabctrl_RMA_tab_NoteRMA");
                tabctrl_ItemGenerali.TabPages["tabCollimatore"].Text = Utils.resourcemanager.GetString("tabctrl_ItemGenerali_tabCollimatore");
                tabctrl_ItemGenerali.TabPages["tabParte"].Text = Utils.resourcemanager.GetString("tabctrl_ItemGenerali_tabParte");
                #endregion
                #region Radio Button
                //rb_SelectALL_Coll.Text = Utils.resourcemanager.GetString("rb_SelectAll");
                //rb_DeselectALL_Coll.Text = Utils.resourcemanager.GetString("rb_DeselectAll");
                //rb_SelectALL_Parte.Text = Utils.resourcemanager.GetString("rb_SelectAll");
                //rb_DeselectALL_Parte.Text = Utils.resourcemanager.GetString("rb_DeselectAll");
                rb_statoGaranzia.Text = Utils.resourcemanager.GetString("rb_statoGaranzia");
                #endregion
                #region DataGridViewColumns
                dgv_ListaCollimatoriRMA.Columns["serialNr"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaCollimatoriRMA_serialNr");
                dgv_ListaCollimatoriRMA.Columns["modello"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaCollimatoriRMA_modello");
                dgv_ListaCollimatoriRMA.Columns["dataProd"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaCollimatoriRMA_dataProd");
                dgv_ListaCollimatoriRMA.Columns["cliente"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaCollimatoriRMA_cliente");
                dgv_ListaCollimatoriRMA.Columns["codColl"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaCollimatoriRMA_codColl");
                dgv_ListaCollimatoriRMA.Columns["numFC"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaCollimatoriRMA_numFC");
                dgv_ListaCollimatoriRMA.Columns["da_SN"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaCollimatoriRMA_da_SN");
                dgv_ListaCollimatoriRMA.Columns["al_SN"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaCollimatoriRMA_al_SN");
                dgv_ListaCollimatoriRMA.Columns["qta"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaCollimatoriRMA_qta");
                dgv_ListaPartiRMA.Columns["idParte"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaPartiRMA_idParte");
                dgv_ListaPartiRMA.Columns["descrizParte"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaPartiRMA_descrizParte");
                dgv_ListaPartiRMA.Columns["SNParte"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaPartiRMA_SNParte");
                dgv_ListaPartiRMA.Columns["cliParte"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaPartiRMA_cliParte");
                dgv_ListaPartiRMA.Columns["FC_Parte"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaPartiRMA_FC_Parte");
                dgv_ListaPartiRMA.Columns["numBolla"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaPartiRMA_numBolla");
                dgv_ListaPartiRMA.Columns["dataBolla"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaPartiRMA_dataBolla");
                dgv_ListaPartiRMA.Columns["dataVendita"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaPartiRMA_dataVendita");
                dgv_ListaPartiRMA.Columns["qtyPart"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaPartiRMA_qtyPart");
                dgv_ListaAllegatiRMA.Columns["nomeFile"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaAllegatiRMA_nomeFile");
                dgv_ListaProblemiRMA.Columns["difettoProbl"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaProblemiRMA_difettoProbl");
                dgv_ListaProblemiRMA.Columns["dataAggProb"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaProblemiRMA_dataAggProb");
                dgv_ListaProblemiRMA.Columns["gravitaProb"].HeaderText = Utils.resourcemanager.GetString("dgv_ListaProblemiRMA_gravitaProb");
                #endregion
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void calcolaIDRMA()
        {
            try
            {
                if (!cb_modManNumRMA.Checked)
                {
                    lbl_NumRMA.Text = "";
                    idRMA = "";
                    string label = "";
                    if (Globals.user.getUserCredential().Equals("NonConfExt") || Globals.user.getUserCredential().Equals("admin"))
                        label = "CNC";
                    else if (Globals.user.getUserCredential().Equals("NonConfInt"))
                        label = "NCI";
                    string lastID_RMA = "";
                    lastID_RMA = (string)conn.CreateCommand("SELECT TOP 1 idRMA FROM [Autotest].[dbo].[RMA_Main] WHERE [Autotest].[dbo].[RMA_Main].[idRMA] LIKE '%" + label + "%/" + DateTime.Now.ToString("yy") + "' ORDER BY [Autotest].[dbo].[RMA_Main].[idRMA] DESC", lastID_RMA);
                    if (!lastID_RMA.Equals(""))
                    {
                        string[] RMA_ID = lastID_RMA.Split('/');
                        int lastCNCnr = Convert.ToInt16(RMA_ID[1]) + 1;
                        idRMA = RMA_ID[0] + "/" + (lastCNCnr).ToString().PadLeft(4, '0') + "/" + RMA_ID[2];
                    }
                    else//caso database vuoto oppure caso primo CNC dell'anno
                    {
                        idRMA = label + "/0001/" + DateTime.Now.ToString("yy");
                    }
                    lbl_NumRMA.Text = idRMA;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void refreshFormRMA()
        {
            try
            {
                #region RMA_Generali
                rmaGen = new RMA_Generali();
                rmaGen = (RMA_Generali)conn.CreateCommand("SELECT * FROM [Autotest].[dbo].[RMA_Generali] WHERE [Autotest].[dbo].[RMA_Generali].[idRMA] = '" + rma_fromForm.idRMA + "'", rmaGen);
                if (rmaGen != null)
                {
                    lbl_NumRMA.Text = rmaGen.idRMA;
                    lbl_DataCreazioneRMA.Text = rmaGen.dataCreazione.ToString();
                    cmb_tipoRMA.SelectedItem = rmaGen.tipoRMA;
                    cmb_prioritaRMA.SelectedItem = rmaGen.priorita;
                    tb_noteGenerali.Text = Utils.parseUselessChars(rmaGen.note);
                    cmb_statoRMA.SelectedItem = rmaGen.stato;
                }
                #endregion
                #region Lista Collimatori
                collList.Clear();
                collList = (List<RMA_Collimatore>)conn.CreateCommand("SELECT * FROM [Autotest].[dbo].[RMA_Collimatore] WHERE [Autotest].[dbo].[RMA_Collimatore].[idRMA] = '" + rma_fromForm.idRMA + "'", collList);
                if (collList != null)
                    btn_RefreshCollRMA_Click(null, null);
                #endregion
                #region Lista Parti
                partList.Clear();
                partList = (List<RMA_Parte>)conn.CreateCommand("SELECT * FROM [Autotest].[dbo].[RMA_Parte] WHERE [Autotest].[dbo].[RMA_Parte].[idRMA] = '" + rma_fromForm.idRMA + "'", partList);
                if (partList != null)
                    btn_RefreshParteRMA_Click(null, null);
                #endregion
                #region RMA_InformativaCliente
                infoCli = new RMA_InformativaCliente();
                infoCli = (RMA_InformativaCliente)conn.CreateCommand("SELECT * FROM [Autotest].[dbo].[RMA_InformativaCliente] WHERE [Autotest].[dbo].[RMA_InformativaCliente].[idRMA] = '" + rma_fromForm.idRMA + "'", infoCli);
                if (infoCli != null)
                {
                    cmb_nomeCliente.SelectedItem = infoCli.cliente.nomeCliente.TrimStart().TrimEnd();
                    //lbl_idCliente.Text = infoCli.cliente.IdCliente.ToString();
                    cmb_decisCliente.SelectedIndex = cmb_decisCliente.FindString(infoCli.decisioneCliente);
                    if (infoCli.dataArrivoCollim != dtp_DataArrivoCollimatore.MinDate)
                        tb_DataArrivoCollimatore.Text = infoCli.dataArrivoCollim.ToShortDateString();
                    else
                        tb_DataArrivoCollimatore.Text = "  /  /";
                    if (infoCli.dataScadenzaRMA != dtp_dataScadenzaRMA.MinDate)
                        tb_DataScadenzaRMA.Text = infoCli.dataScadenzaRMA.ToShortDateString();
                    else
                        tb_DataScadenzaRMA.Text = "  /  /";
                    tb_scarCliente.Text = infoCli.SCARCliente;
                    tb_numScarCLI.Text = infoCli.numScarCli;
                    tb_ordRiparaz.Text = infoCli.ordRiparazione;
                    if (infoCli.dataOrdineRip != dtp_dataOrdRip.MinDate)
                        tb_DataOrdineRiparazione.Text = infoCli.dataOrdineRip.ToShortDateString();
                    else
                        tb_DataOrdineRiparazione.Text = "  /  /";
                    tb_docReso.Text = infoCli.docReso;
                    if (infoCli.dataDocReso != dtp_dataDocReso.MinDate)
                        tb_DataDocumentoDiReso.Text = infoCli.dataDocReso.ToShortDateString();
                    else
                        tb_DataDocumentoDiReso.Text = "  /  /";
                    if (infoCli.dataSpedizione != dtp_dataSpedizione.MinDate)
                        tb_DataSpedizione.Text = infoCli.dataSpedizione.ToShortDateString();
                    else
                        tb_DataSpedizione.Text = "  /  /";
                    cmb_speseSpediz.SelectedIndex = cmb_speseSpediz.FindString(infoCli.speseSpedizione);
                    tb_noteTrasporto.Text = infoCli.tipoTrasporto.Replace("'", "");
                    tb_noteCliente.Text = infoCli.noteCliente.Replace("'", "");
                    cmb_tipoImballaggio.SelectedItem = infoCli.tipoImballaggio.ToString();
                    tb_noteImballaggio.Text = infoCli.noteImballaggio.Replace("'", "");
                    btn_RefreshCollRMA_Click(null, null);
                }
                #endregion
                #region RMA_Allegati
                allegatiList = (List<RMA_Allegato>)conn.CreateCommand("SELECT * FROM [Autotest].[dbo].[RMA_Allegati] WHERE [Autotest].[dbo].[RMA_Allegati].[idRMA] = '" + rma_fromForm.idRMA + "'", allegatiList);
                btn_RefreshAllegati_Click(null, null);
                #endregion
                #region RMA_Problema
                probList.Clear();
                probList = (List<RMA_Problema>)conn.CreateCommand("SELECT * FROM [Autotest].[dbo].[RMA_Problema] WHERE [Autotest].[dbo].[RMA_Problema].[idRMA] = '" + rma_fromForm.idRMA + "'", probList);
                #endregion
                //Tab Analisi del Problema

                difList = new List<RMA_DifettoRiscontrato>();
                if (collList!= null)
                {
                    if (!collList.Count.Equals(0))
                    {
                        List<RMA_DifettoRiscontrato> tempDifList = new List<RMA_DifettoRiscontrato>();
                        tempDifList = (List<RMA_DifettoRiscontrato>)conn.CreateCommand("SELECT DISTINCT [dscSez],[DscArt],[codArt] FROM [archivi].[dbo].[listaFlowControlli] WHERE [archivi].[dbo].[listaFlowControlli].Modello = '" + collList[0].modelloColl + "'", tempDifList);
                        if (tempDifList != null)
                            foreach (RMA_DifettoRiscontrato d in tempDifList)
                                if (!difList.Contains(d))
                                    difList.Add(d);
                        tempDifList = new List<RMA_DifettoRiscontrato>();
                        tempDifList = (List<RMA_DifettoRiscontrato>)conn.CreateCommand("SELECT DISTINCT [dscSez],[DscArt],[codArt] FROM [archivi].[dbo].[listaFlowLavorazioni] WHERE [archivi].[dbo].[listaFlowLavorazioni].Modello = '" + collList[0].modelloColl + "'", tempDifList);
                        if (tempDifList != null)
                            foreach (RMA_DifettoRiscontrato d in tempDifList)
                                if (!difList.Contains(d))
                                    difList.Add(d);
                        //tempDifList = new List<RMA_DifettoRiscontrato>();
                        //tempDifList = (List<RMA_DifettoRiscontrato>)conn.CreateCommand("SELECT DISTINCT [dscSez],[DscArt],[codArt] FROM [archivi].[dbo].[VISTA_SM_STORICO_GRUPPI_CONTROLLI] WHERE [archivi].[dbo].[VISTA_SM_STORICO_GRUPPI_CONTROLLI].Modello = '" + collList[0].modelloColl + "'", tempDifList);
                        //if (tempDifList != null)
                        //    foreach (RMA_DifettoRiscontrato d in tempDifList)
                        //        if (!difList.Contains(d))
                        //            difList.Add(d);
                    }
                }
                if (partList != null)
                {
                    if (!partList.Count.Equals(0))
                    {
                        List<RMA_DifettoRiscontrato> tempDifList = new List<RMA_DifettoRiscontrato>();
                        tempDifList = (List<RMA_DifettoRiscontrato>)conn.CreateCommand("SELECT DISTINCT [dscSez],[DscArt],[codArt] FROM [archivi].[dbo].[listaFlowControlli] WHERE [archivi].[dbo].[listaFlowControlli].Modello = '" + partList[0].descParte + "'", tempDifList);
                        if (tempDifList != null)
                            foreach (RMA_DifettoRiscontrato d in tempDifList)
                                if (!difList.Contains(d))
                                    difList.Add(d);
                        tempDifList = new List<RMA_DifettoRiscontrato>();
                        tempDifList = (List<RMA_DifettoRiscontrato>)conn.CreateCommand("SELECT DISTINCT [dscSez],[DscArt],[codArt] FROM [archivi].[dbo].[listaFlowLavorazioni] WHERE [archivi].[dbo].[listaFlowLavorazioni].Modello = '" + partList[0].descParte + "'", tempDifList);
                        if (tempDifList != null)
                            foreach (RMA_DifettoRiscontrato d in tempDifList)
                                if (!difList.Contains(d))
                                    difList.Add(d);
                        //tempDifList = new List<RMA_DifettoRiscontrato>();
                        //tempDifList = (List<RMA_DifettoRiscontrato>)conn.CreateCommand("SELECT DISTINCT [dscSez],[DscArt],[codArt] FROM [archivi].[dbo].[VISTA_SM_STORICO_GRUPPI_CONTROLLI] WHERE [archivi].[dbo].[VISTA_SM_STORICO_GRUPPI_CONTROLLI].Modello = '" + partList[0].descParte + "'", tempDifList);
                        //if (tempDifList != null)
                        //    foreach (RMA_DifettoRiscontrato d in tempDifList)
                        //        if (!difList.Contains(d))
                        //            difList.Add(d);
                    }
                }
                if (difList != null)
                    difList = difList.Distinct().ToList();
                //potrei ottenere null dalla query nel caso in cui un RMA non abbia associato alcun problema. Ma in questo caso devo re-inizializzare la lista dei problemi.
                if (probList == null)
                    probList = new List<RMA_Problema>();
                //Tab Garanzia Ralco, da riepmire solo nel caso di non conformità Esterne
                if (Globals.user.getUserCredential().Equals("NonConfExt") || Globals.user.getUserCredential().Equals("admin") || rmaGen.tipoRMA.Equals("Esterno"))
                {
                    garanziaRalco = new RMA_GaranziaRalco();
                    garanziaRalco = (RMA_GaranziaRalco)conn.CreateCommand("SELECT * FROM [Autotest].[dbo].[RMA_GaranziaRalco] WHERE [Autotest].[dbo].[RMA_GaranziaRalco].[idRMA] = '" + rma_fromForm.idRMA + "'", garanziaRalco);
                    if (garanziaRalco != null)
                    {
                        if (!garanziaRalco.provCollimatore.Equals(""))
                            cmb_provCollimatore.SelectedIndex = cmb_provCollimatore.FindString(garanziaRalco.provCollimatore);
                        if (!garanziaRalco.anniVitaCollimatore.Equals(""))
                            cmb_anniVitaCollimatore.SelectedIndex = cmb_anniVitaCollimatore.FindString(garanziaRalco.anniVitaCollimatore);
                        if (!garanziaRalco.respDannoCollimatore.Equals(""))
                            cmb_respDannoCollimatore.SelectedIndex = cmb_respDannoCollimatore.FindString(garanziaRalco.respDannoCollimatore);
                        tb_noteGaranziaRalco.Text = garanziaRalco.noteGaranziaRalco.Replace("'", "");
                        if (!garanziaRalco.statoGaranzia.Equals(""))
                            cmb_statoGaranzia.SelectedIndex = cmb_statoGaranzia.FindString(garanziaRalco.statoGaranzia);
                        if (!garanziaRalco.azioniSupplRalco.Equals(""))
                            cmb_azioniSupplRalco.SelectedIndex = cmb_azioniSupplRalco.FindString(garanziaRalco.azioniSupplRalco);
                    }
                }
                //Tab Fasi Da Rieseguire
                minFasiDaRiesList = (RMA_FaseProduzione)conn.CreateCommand("SELECT * FROM [Autotest].[dbo].[RMA_FasiProduzioneDaRieseguire] WHERE [Autotest].[dbo].[RMA_FasiProduzioneDaRieseguire].[idRMA] = '" + rma_fromForm.idRMA + "'", minFasiDaRiesList);
                //Tab Test Da Rieseguire non è necessario aggiornarla.
                //Tab Preventivi, da riepmire solo nel caso di non conformità Esterne
                if (Globals.user.getUserCredential().Equals("NonConfExt") || Globals.user.getUserCredential().Equals("admin"))
                {
                    prevList.Clear();
                    prevList = (List<RMA_Preventivo>)conn.CreateCommand("SELECT * FROM [Autotest].[dbo].[RMA_Preventivo] WHERE [Autotest].[dbo].[RMA_Preventivo].[idRMA] = '" + rma_fromForm.idRMA + "'", prevList);
                    //potrei ottenere null dalla query nel caso in cui un RMA non abbia associato alcun problema. Ma in questo caso devo re-inizializzare la lista dei problemi.
                    if (prevList == null)
                        prevList = new List<RMA_Preventivo>();
                }
                ripList.Clear();
                ripList = (List<RMA_Riparazione>)conn.CreateCommand("SELECT * FROM [Autotest].[dbo].[RMA_Riparazione] WHERE [Autotest].[dbo].[RMA_Riparazione].[idRMA] = '" + rma_fromForm.idRMA + "'", ripList);
                //Tab Riparazioni
                if (ripList == null)
                    ripList = new List<RMA_Riparazione>();
                //potrei ottenere null dalla query nel caso in cui un RMA non abbia associato alcun problema. Ma in questo caso devo re-inizializzare la lista dei problemi.
                //Tab Note Qualità
                noteQualita = new RMA_NoteQualita();
                noteQualita = (RMA_NoteQualita)conn.CreateCommand("SELECT * FROM [Autotest].[dbo].[RMA_NoteQualita] WHERE [Autotest].[dbo].[RMA_NoteQualita].[idRMA] = '" + rma_fromForm.idRMA + "'", noteQualita);
                cmb_sicProdotto.SelectedIndex = cmb_sicProdotto.FindString(noteQualita.sicurezzaProdotto);
                cmb_segIncidente.SelectedIndex = cmb_segIncidente.FindString(noteQualita.segnalazIncidente);
                cmb_invioNotaInf.SelectedIndex = cmb_invioNotaInf.FindString(noteQualita.invioNotaInf);
                cmb_revDocValutRischi.SelectedIndex = cmb_revDocValutRischi.FindString(noteQualita.revDocValutazRischi);
                cmb_statoAzCorr.SelectedIndex = cmb_statoAzCorr.FindString(noteQualita.statoAzCorrett);
                cmb_rifAzCorr.SelectedIndex = cmb_rifAzCorr.FindString(noteQualita.rifAzCorrett);
                lbl_descAzCorrett.Text = noteQualita.descAzCorrett;
                tb_noteQualita.Text = noteQualita.note.Replace("'", "");
                //Tab Note RMA, da riepmire solo nel caso di non conformità Esterne
                refreshTableLogRMA();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //aggiorno le variabili ogni volta che cambio il TAB oppure, in generale, prima di eseguire il commit sul DB
        private void tabctrl_RMA_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //calcolaIDRMA();
                //Tabella  RMA_Generali
                rmaGen = new RMA_Generali();
                rmaGen.idRMA = lbl_NumRMA.Text;
                if (cmb_tipoRMA.SelectedItem != null)
                    rmaGen.tipoRMA = cmb_tipoRMA.SelectedItem.ToString();
                else
                    rmaGen.tipoRMA = "";
                rmaGen.dataCreazione = DateTime.Now;
                rmaGen.creatoDa = Globals.user.getFullUserName();
                rmaGen.dataAggiornamento = DateTime.Now;
                rmaGen.aggiornatoDa = Globals.user.getFullUserName();
                rmaGen.location = Globals.user.getUserLocation();
                if (cmb_prioritaRMA.SelectedItem != null)
                    rmaGen.priorita = cmb_prioritaRMA.SelectedItem.ToString();
                else
                    rmaGen.priorita = "";
                rmaGen.note = Utils.parseUselessChars(tb_noteGenerali.Text);
                if (cmb_statoRMA.SelectedItem != null)
                    rmaGen.stato = cmb_statoRMA.SelectedItem.ToString();
                else
                    rmaGen.stato = "";
                if (collList != null && !collList.Count.Equals(0))
                {
                    if (rmaGen.seriali == null)
                        rmaGen.seriali = new List<string>();
                    numFlowChart.Clear();
                    foreach (RMA_Collimatore c in collList)
                    {
                        rmaGen.seriali.Add(c.seriale);
                        numFlowChart.Add(c.numFlowChart);
                        //condizione necessaria per distinguere gli RMA nuovi da quelli già inseriti
                        if (c.idRMA != null)
                            if (!c.idRMA.Equals(lbl_NumRMA.Text))
                                c.idRMA = null;
                    }
                }
                if (partList != null && !partList.Count.Equals(0))
                {
                    if (rmaGen.progrParte == null)
                        rmaGen.progrParte = new List<int>();
                    foreach (RMA_Parte p in partList)
                    {
                        rmaGen.progrParte.Add(p.idProgressivo);
                        //condizione necessaria per distinguere gli RMA nuovi da quelli già inseriti
                        if (p.idRMA != null)
                            if (!p.idRMA.Equals(lbl_NumRMA.Text))
                                p.idRMA = null;
                    }
                }
                //Tabella  RMA_InformativaCliente, da riepmire solo nel caso di non conformità Esterne
                if (Globals.user.getUserCredential().Equals("NonConfExt") || Globals.user.getUserCredential().Equals("admin"))
                {
                    infoCli = new RMA_InformativaCliente();
                    infoCli.cliente = new RMA_ClientiRalco();
                    infoCli.idRMA = lbl_NumRMA.Text;
                    if (cmb_nomeCliente.SelectedItem != null)
                    {
                        infoCli.cliente = cliList.First(r => r.nomeCliente.TrimEnd().Equals(cmb_nomeCliente.SelectedItem.ToString()));
                    }
                    //if (!lbl_idCliente.Text.Equals (""))
                    //    infoCli.cliente.IdCliente = Convert.ToInt32(lbl_idCliente.Text);
                    if (cmb_decisCliente.SelectedItem != null)
                        infoCli.decisioneCliente = cmb_decisCliente.SelectedItem.ToString();
                    else
                        infoCli.decisioneCliente = "";
                    if (!tb_DataArrivoCollimatore.Text.Equals("  /  /"))
                        infoCli.dataArrivoCollim = Convert.ToDateTime(tb_DataArrivoCollimatore.Text);
                    else
                        infoCli.dataArrivoCollim = dtp_DataArrivoCollimatore.MinDate;
                    if (!tb_DataScadenzaRMA.Text.Equals("  /  /"))
                        infoCli.dataScadenzaRMA = Convert.ToDateTime(tb_DataScadenzaRMA.Text);
                    else
                        infoCli.dataScadenzaRMA = dtp_dataScadenzaRMA.MinDate;
                    infoCli.SCARCliente = Utils.parseUselessChars(tb_scarCliente.Text);
                    infoCli.numScarCli = Utils.parseUselessChars(tb_numScarCLI.Text);
                    infoCli.ordRiparazione = Utils.parseUselessChars(tb_ordRiparaz.Text);
                    if (!tb_DataOrdineRiparazione.Text.Equals("  /  /"))
                        infoCli.dataOrdineRip = Convert.ToDateTime(tb_DataOrdineRiparazione.Text);
                    else
                        infoCli.dataOrdineRip = dtp_DataArrivoCollimatore.MinDate;
                    infoCli.docReso = Utils.parseUselessChars(tb_docReso.Text);
                    if (!tb_DataDocumentoDiReso.Text.Equals("  /  /"))
                        infoCli.dataDocReso = Convert.ToDateTime(tb_DataDocumentoDiReso.Text);
                    else
                        infoCli.dataDocReso = dtp_dataDocReso.MinDate;
                    if (!tb_DataSpedizione.Text.Equals("  /  /"))
                        infoCli.dataSpedizione = Convert.ToDateTime(tb_DataSpedizione.Text);
                    else
                        infoCli.dataSpedizione = dtp_dataSpedizione.MinDate;
                    if (cmb_speseSpediz.SelectedItem != null)
                        infoCli.speseSpedizione = cmb_speseSpediz.SelectedItem.ToString();
                    else
                        infoCli.speseSpedizione = "";
                    infoCli.tipoTrasporto = Utils.parseUselessChars(tb_noteTrasporto.Text);
                    infoCli.noteCliente = Utils.parseUselessChars(tb_noteCliente.Text);
                    infoCli.codCollCli = lbl_codCollCliente.Text;
                    if (cmb_tipoImballaggio.SelectedItem != null)
                        infoCli.tipoImballaggio = cmb_tipoImballaggio.SelectedItem.ToString();
                    else
                        infoCli.tipoImballaggio = "";
                    infoCli.noteImballaggio = Utils.parseUselessChars(tb_noteImballaggio.Text);
                }
                //Tabella Allegati
                btn_RefreshAllegati_Click(null, null);
                //Tabella Riparazioni
                btn_RefreshRiparazione_Click(null, null);
                //Tabella RMA_Problema
                btn_RefreshProbl_Click(null, null);
                difList = new List<RMA_DifettoRiscontrato>();
                if (collList != null)
                {
                    if (!collList.Count.Equals(0))
                    {
                        List<RMA_DifettoRiscontrato> tempDifList = new List<RMA_DifettoRiscontrato>();
                        tempDifList = (List<RMA_DifettoRiscontrato>)conn.CreateCommand("SELECT DISTINCT [dscSez],[DscArt],[codArt] FROM [archivi].[dbo].[listaFlowControlli] WHERE [archivi].[dbo].[listaFlowControlli].Modello = '" + collList[0].modelloColl + "'", tempDifList);
                        if (tempDifList != null)
                            foreach (RMA_DifettoRiscontrato d in tempDifList)
                                if (!difList.Contains(d))
                                    difList.Add(d);
                        tempDifList = new List<RMA_DifettoRiscontrato>();
                        tempDifList = (List<RMA_DifettoRiscontrato>)conn.CreateCommand("SELECT DISTINCT [dscSez],[DscArt],[codArt] FROM [archivi].[dbo].[listaFlowLavorazioni] WHERE [archivi].[dbo].[listaFlowLavorazioni].Modello = '" + collList[0].modelloColl + "'", tempDifList);
                        if (tempDifList != null)
                            foreach (RMA_DifettoRiscontrato d in tempDifList)
                                if (!difList.Contains(d))
                                    difList.Add(d);
                        //tempDifList = new List<RMA_DifettoRiscontrato>();
                        //tempDifList = (List<RMA_DifettoRiscontrato>)conn.CreateCommand("SELECT DISTINCT [dscSez],[DscArt],[codArt] FROM [archivi].[dbo].[VISTA_SM_STORICO_GRUPPI_CONTROLLI] WHERE [archivi].[dbo].[VISTA_SM_STORICO_GRUPPI_CONTROLLI].Modello = '" + collList[0].modelloColl + "'", tempDifList);
                        //if (tempDifList != null)
                        //    foreach (RMA_DifettoRiscontrato d in tempDifList)
                        //        if (!difList.Contains(d))
                        //            difList.Add(d);
                    }
                }
                if (partList != null)
                {
                    if (!partList.Count.Equals(0))
                    {
                        List<RMA_DifettoRiscontrato> tempDifList = new List<RMA_DifettoRiscontrato>();
                        tempDifList = (List<RMA_DifettoRiscontrato>)conn.CreateCommand("SELECT [dscSez],[DscArt],[codArt] FROM [archivi].[dbo].[listaFlowControlli] WHERE [archivi].[dbo].[listaFlowControlli].Modello = '" + partList[0].descParte + "'", tempDifList);
                        if (tempDifList != null)
                            foreach (RMA_DifettoRiscontrato d in tempDifList)
                                if (!difList.Contains(d))
                                    difList.Add(d);
                        tempDifList = new List<RMA_DifettoRiscontrato>();
                        tempDifList = (List<RMA_DifettoRiscontrato>)conn.CreateCommand("SELECT [dscSez],[DscArt],[codArt] FROM [archivi].[dbo].[listaFlowLavorazioni] WHERE [archivi].[dbo].[listaFlowLavorazioni].Modello = '" + partList[0].descParte + "'", tempDifList);
                        if (tempDifList != null)
                            foreach (RMA_DifettoRiscontrato d in tempDifList)
                                if (!difList.Contains(d))
                                    difList.Add(d);
                        //tempDifList = new List<RMA_DifettoRiscontrato>();
                        //tempDifList = (List<RMA_DifettoRiscontrato>)conn.CreateCommand("SELECT DISTINCT [dscSez],[DscArt],[codArt] FROM [archivi].[dbo].[VISTA_SM_STORICO_GRUPPI_CONTROLLI] WHERE [archivi].[dbo].[VISTA_SM_STORICO_GRUPPI_CONTROLLI].Modello = '" + partList[0].descParte + "'", tempDifList);
                        //if (tempDifList != null)
                        //    foreach (RMA_DifettoRiscontrato d in tempDifList)
                        //        if (!difList.Contains(d))
                        //            difList.Add(d);
                    }
                }
                if (difList != null)
                    difList = difList.Distinct().ToList();
                //Tabella RMA_garanziaRalco, da riepmire solo nel caso di non conformità Esterne
                if (Globals.user.getUserCredential().Equals("NonConfExt") || Globals.user.getUserCredential().Equals("admin"))
                {
                    garanziaRalco = new RMA_GaranziaRalco();
                    garanziaRalco.idRMA = lbl_NumRMA.Text;
                    if (cmb_provCollimatore.SelectedItem != null)
                        garanziaRalco.provCollimatore = cmb_provCollimatore.SelectedItem.ToString();
                    else
                        garanziaRalco.provCollimatore = "";
                    if (cmb_anniVitaCollimatore.SelectedItem != null)
                        garanziaRalco.anniVitaCollimatore = cmb_anniVitaCollimatore.SelectedItem.ToString();
                    else
                        garanziaRalco.anniVitaCollimatore = "";
                    if (cmb_respDannoCollimatore.SelectedItem != null)
                        garanziaRalco.respDannoCollimatore = cmb_respDannoCollimatore.SelectedItem.ToString();
                    else
                        garanziaRalco.respDannoCollimatore = "";
                    garanziaRalco.noteGaranziaRalco = Utils.parseUselessChars(tb_noteGaranziaRalco.Text);
                    garanziaRalco.statoGaranzia = cmb_statoGaranzia.Text;
                    if (cmb_azioniSupplRalco.SelectedItem != null)
                        garanziaRalco.azioniSupplRalco = cmb_azioniSupplRalco.SelectedItem.ToString();
                    else
                        garanziaRalco.azioniSupplRalco = "";
                }
                //Tabella RMA_Preventivo, da riepmire solo nel caso di non conformità Esterne
                if (Globals.user.getUserCredential().Equals("NonConfExt") || Globals.user.getUserCredential().Equals("admin"))
                    btn_RefreshPrev_Click(null, null);
                //Fasi da rieseguire

                if (minFasiDaRiesList.minFaseProd == null)
                {
                    minFasiDaRiesList.minFaseProd = new List<int>();
                }
                else
                    minFasiDaRiesList.minFaseProd.Clear();
                //int index = 0;
                foreach (DataGridViewRow r in dgv_FasiDaRieseguire.Rows)
                {
                    minFasiDaRiesList.minFaseProd.Reverse();
                    minFasiDaRiesList.minFaseProd.Add(Convert.ToInt32(r.Cells["minLavoro"].Value));
                    minFasiDaRiesList.minFaseProd.Reverse();
                }
                refreshTableFasiDaRieseguire();
                //Test Da Rieseguire
                dgv_testDaEseguire.Refresh();
                //Tab Azioni Supplementari
                initializeTableAzioniSupplementari();
                //Tabella RMA_NoteQualita
                noteQualita = new RMA_NoteQualita();
                noteQualita.idRMA = lbl_NumRMA.Text;
                if (cmb_sicProdotto.SelectedItem != null)
                    noteQualita.sicurezzaProdotto = cmb_sicProdotto.SelectedItem.ToString();
                else
                    noteQualita.sicurezzaProdotto = "";
                if (cmb_segIncidente.SelectedItem != null)
                    noteQualita.segnalazIncidente = cmb_segIncidente.SelectedItem.ToString();
                else
                    noteQualita.segnalazIncidente = "";
                if (cmb_invioNotaInf.SelectedItem != null)
                    noteQualita.invioNotaInf = cmb_invioNotaInf.SelectedItem.ToString();
                else
                    noteQualita.invioNotaInf = "";
                if (cmb_revDocValutRischi.SelectedItem != null)
                    noteQualita.revDocValutazRischi = cmb_revDocValutRischi.SelectedItem.ToString();
                else
                    noteQualita.revDocValutazRischi = "";
                if (cmb_statoAzCorr.SelectedItem != null)
                    noteQualita.statoAzCorrett = cmb_statoAzCorr.SelectedItem.ToString();
                else
                    noteQualita.statoAzCorrett = "";
                if (cmb_rifAzCorr.SelectedItem != null)
                    noteQualita.rifAzCorrett = cmb_rifAzCorr.SelectedItem.ToString();
                else
                    noteQualita.rifAzCorrett = "";
                noteQualita.descAzCorrett = lbl_descAzCorrett.Text;
                noteQualita.note = Utils.parseUselessChars(tb_noteQualita.Text);
                //logRMA, da riepmire solo nel caso di non conformità Esterne
                //if (Globals.user.getUserCredential().Equals("NonConfExt") || Globals.user.getUserCredential().Equals("admin"))
                refreshTableLogRMA();
            }
            catch (Exception ex)
            {
                //do nothing...
            }
        }

        //impostare gmail fornendo l'accesso App meno sicure (impostazione già settata per la seguente casella di posta)
        // ID : ralcoservicemanager@gmail.com
        // PWD : R@Lco$erv!ces
        private void sendMail(List<string> recipientsAddressesList, string testSubject, string testBody)
        {
            try
            {
                SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
                client.EnableSsl = true;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Credentials = new System.Net.NetworkCredential("ralcoservicemanager@gmail.com", "R@Lco$erv!ces");
                if (recipientsAddressesList != null)
                    foreach (string address in recipientsAddressesList)
                        client.Send("ralcoservicemanager@gmail.com", address, testSubject, testBody);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void btn_saveRMA_Click(object sender, EventArgs e)
        {
            try
            {
                commitDB(0);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_saveAndCloseRMA_Click(object sender, EventArgs e)
        {
            try
            {
                commitDB(1);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void commitDB(int main)
        {
            //if (cmb_statoRMA.BackColor.Equals(Color.Red))
            //    MessageBox.Show("Modifica lo stato dell'RMA prima di procedere", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //else
            //{
            try
            {
                //modifico automaticamente lo stato dell'RMA
                tabctrl_RMA_SelectedIndexChanged(null, null);
                if (!noteQualita.statoAzCorrett.Equals(""))
                //if(cmb_statoAzCorr.SelectedItem.Equals("Da Valutare"))
                    cmb_statoRMA.SelectedItem = "Attesa Valutazione Note Qualità";
                //caso ADD new RMA
                if (rma_fromForm == null && firstSave == true)
                {
                    calcolaIDRMA();
                    conn.CreateCommand("INSERT INTO [Autotest].[dbo].[RMA_Main] " +
                    "([idRMA],[idGenerali],[idInfoCliente],[idAnalisiProblema],[idPreventivo],[idRiparaz],[idNoteQualita]) " +
                    "VALUES ('" + lbl_NumRMA.Text + "','" + "" + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0 + "')", null);
                    if ((collList != null && !collList.Count.Equals(0)) || (partList != null && !partList.Count.Equals(0)))
                    {
                        //Tabella RMA_Collimatore
                        if (rmaGen.seriali != null)
                        //if (collList != null && !collList.Count.Equals(0))
                        {
                            //Tabella RMA_Generali
                            foreach (string s in rmaGen.seriali)
                            {
                                conn.CreateCommand("INSERT INTO [Autotest].[dbo].[RMA_Generali] " +
                                "([idRMA],[tipoRMA],[dataCreazione],[creatoDa],[dataAggiornamento],[aggiornatoDa],[location],[priorita],[note],[stato],[serialeColl]) " +
                                "VALUES ('" + rmaGen.idRMA + "','" + rmaGen.tipoRMA + "','" + rmaGen.dataCreazione.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "','" + rmaGen.creatoDa + "','" + rmaGen.dataAggiornamento.ToString("yyyy-MM-dd HH:mm:ss.fffffff")
                                + "','" + rmaGen.aggiornatoDa + "','" + rmaGen.location + "','" + rmaGen.priorita
                                + "','" + rmaGen.note + "','" + rmaGen.stato + "','" + s + "')", null);
                            }
                        }
                        if (rmaGen.progrParte != null)
                        //if (partList != null && !partList.Count.Equals(0))
                        {
                            //Tabella RMA_Generali
                            foreach (Int16 i in rmaGen.progrParte)
                            {
                                conn.CreateCommand("INSERT INTO [Autotest].[dbo].[RMA_Generali] " +
                                "([idRMA],[tipoRMA],[dataCreazione],[creatoDa],[dataAggiornamento],[aggiornatoDa],[location],[priorita],[note],[stato],[progrParte]) " +
                                "VALUES ('" + rmaGen.idRMA + "','" + rmaGen.tipoRMA + "','" + rmaGen.dataCreazione.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "','" + rmaGen.creatoDa + "','" + rmaGen.dataAggiornamento.ToString("yyyy-MM-dd HH:mm:ss.fffffff")
                                + "','" + rmaGen.aggiornatoDa + "','" + rmaGen.location + "','" + rmaGen.priorita
                                + "','" + rmaGen.note + "','" + rmaGen.stato + "','" + i + "')", null);
                            }
                        }
                        if (Globals.user.getUserCredential().Equals("NonConfExt") || Globals.user.getUserCredential().Equals("admin"))
                        {
                            if (infoCli != null && infoCli.cliente != null)
                            {
                                //Tabella RMA_InformativaCliente
                                conn.CreateCommand("INSERT INTO [Autotest].[dbo].[RMA_InformativaCliente] " +
                                "([idRMA],[nomeCliente],[idCliente],[decisioneCliente],[numScarCli],[SCARCliente],[ordRiparazione],[dataOrdineRip],[docReso],[dataSpedizione],[speseSpedizione],[noteCliente],[dataArrivoCollim],[dataScadenzaRMA],[dataDocReso],[tipoTrasporto],[codCollCli],[tipoImballaggio],[noteImballaggio]) " +
                                "VALUES ('" + infoCli.idRMA + "','" + infoCli.cliente.nomeCliente + "'," + infoCli.cliente.IdCliente + ",'" + infoCli.decisioneCliente + "','" + infoCli.numScarCli + "','" + infoCli.SCARCliente + "','" + infoCli.ordRiparazione + "','" + infoCli.dataOrdineRip.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "','" + infoCli.docReso + "','" + infoCli.dataSpedizione.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "','" + infoCli.speseSpedizione + "','" + infoCli.noteCliente + "','" + infoCli.dataArrivoCollim.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "','" + infoCli.dataScadenzaRMA.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "','" + infoCli.dataDocReso.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "','" + infoCli.tipoTrasporto + "','" + infoCli.codCollCli + "','" + infoCli.tipoImballaggio + "','" + infoCli.noteImballaggio + "')", null);
                            }
                        }
                        //Tabella RMA_Allegati
                        if (allegatiList != null)
                        {
                            if (!Directory.Exists(@"\\ralcosrv2\public\RMA\" + lbl_NumRMA.Text.Replace("/", "_")))
                                Directory.CreateDirectory(@"\\ralcosrv2\public\RMA\" + lbl_NumRMA.Text.Replace("/", "_"));
                            foreach (RMA_Allegato a in allegatiList)
                            {
                                string pathDest = @"\\ralcosrv2\public\RMA\" + lbl_NumRMA.Text.Replace("/", "_") + @"\" + Path.GetFileName(a.pathAllegato);
                                File.Copy(a.pathAllegato, pathDest, true);
                                a.pathAllegato = pathDest;
                                conn.CreateCommand("INSERT INTO [Autotest].[dbo].[RMA_Allegati] " +
                                "([idRMA],[pathAllegato]) " +
                                "VALUES ('" + lbl_NumRMA.Text + "','" + a.pathAllegato + "')", null);
                            }
                        }
                        if (Globals.user.getUserCredential().Equals("NonConfExt") || Globals.user.getUserCredential().Equals("admin"))
                        {
                            //Tabella RMA_GaranziaRalco
                            conn.CreateCommand("INSERT INTO [Autotest].[dbo].[RMA_GaranziaRalco] " +
                            "([idRMA],[provCollimatore],[anniVitaCollimatore],[respDannoCollimatore],[noteGaranziaRalco],[statoGaranzia],[azioniSupplRalco]) " +
                            "VALUES ('" + garanziaRalco.idRMA + "','" + garanziaRalco.provCollimatore + "','" + garanziaRalco.anniVitaCollimatore + "','" + garanziaRalco.respDannoCollimatore + "','" + garanziaRalco.noteGaranziaRalco + "','" + garanziaRalco.statoGaranzia + "','" + garanziaRalco.azioniSupplRalco + "')", null);
                        }
                        //Tabella RMA_NoteQualita
                        conn.CreateCommand("INSERT INTO [Autotest].[dbo].[RMA_NoteQualita] " +
                        "([idRMA],[sicurezzaProdotto],[segnalazIncidente],[invioNotaInf],[revDocValutazRischi],[statoAzCorrett],[rifAzCorrett],[descAzCorrett],[note]) " +
                        "VALUES ('" + noteQualita.idRMA + "','" + noteQualita.sicurezzaProdotto + "','" + noteQualita.segnalazIncidente + "','" + noteQualita.invioNotaInf + "','" + noteQualita.revDocValutazRischi + "','" + noteQualita.statoAzCorrett + "','" + noteQualita.rifAzCorrett + "','" + noteQualita.descAzCorrett + "','" + noteQualita.note + "')", null);
                        //Tabella RMA_FasiProduzioneDaRieseguire
                        minFasiDaRiesList.idRMA = lbl_NumRMA.Text;
                        conn.CreateCommand("INSERT INTO [Autotest].[dbo].[RMA_FasiProduzioneDaRieseguire] " +
                        "([idRMA],[minutiLavoroFase1],[minutiLavoroFase2],[minutiLavoroFase3],[minutiLavoroFase4],[minutiLavoroFase5],[minutiLavoroFase6],[minutiLavoroFase7],[minutiLavoroFase8],[minutiLavoroFase9],[minutiLavoroFase10],[minutiLavoroFase11],[minutiLavoroFase12]) " +
                        "VALUES ('" + minFasiDaRiesList.idRMA + "'," + minFasiDaRiesList.minFaseProd[0] + "," + minFasiDaRiesList.minFaseProd[1] + "," + minFasiDaRiesList.minFaseProd[2] + "," + minFasiDaRiesList.minFaseProd[3] + "," + minFasiDaRiesList.minFaseProd[4] + "," + minFasiDaRiesList.minFaseProd[5] + "," + minFasiDaRiesList.minFaseProd[6] + "," + minFasiDaRiesList.minFaseProd[7] + "," + minFasiDaRiesList.minFaseProd[8] + "," + minFasiDaRiesList.minFaseProd[9] + "," + minFasiDaRiesList.minFaseProd[10] + "," + minFasiDaRiesList.minFaseProd[11] + ")", null);
                        firstSave = false;
                        serialiAdded.Clear();
                        partiAdded.Clear();
                    }
                    else
                    {
                        MessageBox.Show("L'RMA deve contenere almeno un collimatore o una parte", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else//caso EDIT RMA - FACCIO UPDATE delle tabelle o INSERT dove occorre
                {
                    //Tabella RMA_Generali
                    if (rmaGen.seriali != null)
                    {
                        foreach (string s in rmaGen.seriali)
                        {
                            if (serialiAdded.Contains(s))
                            {
                                conn.CreateCommand("INSERT INTO [Autotest].[dbo].[RMA_Generali] " +
                                "([idRMA],[tipoRMA],[dataCreazione],[creatoDa],[dataAggiornamento],[aggiornatoDa],[location],[priorita],[note],[stato],[serialeColl]) " +
                                "VALUES ('" + rmaGen.idRMA + "','" + rmaGen.tipoRMA + "','" + rmaGen.dataCreazione.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "','" + rmaGen.creatoDa + "','" + rmaGen.dataAggiornamento.ToString("yyyy-MM-dd HH:mm:ss.fffffff")
                                + "','" + rmaGen.aggiornatoDa + "','" + rmaGen.location + "','" + rmaGen.priorita
                                + "','" + rmaGen.note + "','" + rmaGen.stato + "','" + s + "')", null);
                            }
                            else
                            {
                                conn.CreateCommand("UPDATE [Autotest].[dbo].[RMA_Generali] SET " +
                                "[idRMA] = '" + rmaGen.idRMA + "'" +
                                ",[tipoRMA] = '" + rmaGen.tipoRMA + "'" +
                                ",[dataAggiornamento] = '" + rmaGen.dataAggiornamento.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "'" +
                                ",[aggiornatoDa] = '" + rmaGen.aggiornatoDa + "'" +
                                ",[location] = '" + rmaGen.location + "'" +
                                ",[priorita] = '" + rmaGen.priorita + "'" +
                                ",[note] = '" + rmaGen.note + "'" +
                                ",[stato] = '" + rmaGen.stato + "'" +
                                " WHERE ([Autotest].[dbo].[RMA_Generali].[idRMA] = '" + rmaGen.idRMA + "' AND [Autotest].[dbo].[RMA_Generali].[serialeColl] = '" + s + "')", null);
                            }
                        }
                    }
                    if (rmaGen.progrParte != null)
                    {
                        foreach (int p in rmaGen.progrParte)
                        {
                            if (partiAdded.Contains(p))
                            {
                                conn.CreateCommand("INSERT INTO [Autotest].[dbo].[RMA_Generali] " +
                                "([idRMA],[tipoRMA],[dataCreazione],[creatoDa],[dataAggiornamento],[aggiornatoDa],[location],[priorita],[note],[stato],[progrParte]) " +
                                "VALUES ('" + rmaGen.idRMA + "','" + rmaGen.tipoRMA + "','" + rmaGen.dataCreazione.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "','" + rmaGen.creatoDa + "','" + rmaGen.dataAggiornamento.ToString("yyyy-MM-dd HH:mm:ss.fffffff")
                                + "','" + rmaGen.aggiornatoDa + "','" + rmaGen.location + "','" + rmaGen.priorita
                                + "','" + rmaGen.note + "','" + rmaGen.stato + "','" + p + "')", null);
                            }
                            else
                            {
                                conn.CreateCommand("UPDATE [Autotest].[dbo].[RMA_Generali] SET " +
                                "[idRMA] = '" + rmaGen.idRMA + "'" +
                                ",[tipoRMA] = '" + rmaGen.tipoRMA + "'" +
                                ",[dataAggiornamento] = '" + rmaGen.dataAggiornamento.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "'" +
                                ",[aggiornatoDa] = '" + rmaGen.aggiornatoDa + "'" +
                                ",[location] = '" + rmaGen.location + "'" +
                                ",[priorita] = '" + rmaGen.priorita + "'" +
                                ",[note] = '" + rmaGen.note + "'" +
                                ",[stato] = '" + rmaGen.stato + "'" +
                                " WHERE ([Autotest].[dbo].[RMA_Generali].[idRMA] = '" + rmaGen.idRMA + "' AND [Autotest].[dbo].[RMA_Generali].[progrParte] = '" + p + "')", null);
                            }
                        }
                    }
                    if (Globals.user.getUserCredential().Equals("NonConfExt") || Globals.user.getUserCredential().Equals("admin"))
                    {
                        //Tabella RMA_InformativaCliente
                        conn.CreateCommand("UPDATE [Autotest].[dbo].[RMA_InformativaCliente] SET " +
                        "[idRMA] = '" + infoCli.idRMA + "'" +
                        ",[nomeCliente] = '" + infoCli.cliente.nomeCliente + "'" +
                        ",[idCliente] = " + infoCli.cliente.IdCliente + "" +
                        ",[decisioneCliente] = '" + infoCli.decisioneCliente + "'" +
                        ",[numScarCli] = '" + infoCli.numScarCli + "'" +
                        ",[SCARCliente] = '" + infoCli.SCARCliente + "'" +
                        ",[ordRiparazione] = '" + infoCli.ordRiparazione + "'" +
                        ",[dataOrdineRip] = '" + infoCli.dataOrdineRip.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "'" +
                        ",[docReso] = '" + infoCli.docReso + "'" +
                        ",[dataSpedizione] = '" + infoCli.dataSpedizione.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "'" +
                        ",[speseSpedizione] = '" + infoCli.speseSpedizione + "'" +
                        ",[noteCliente] = '" + infoCli.noteCliente + "'" +
                        ",[dataArrivoCollim] = '" + infoCli.dataArrivoCollim.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "'" +
                        ",[dataScadenzaRMA] = '" + infoCli.dataScadenzaRMA.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "'" +
                        ",[dataDocReso] = '" + infoCli.dataDocReso.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "'" +
                        ",[tipoTrasporto] = '" + infoCli.tipoTrasporto + "'" +
                        ",[codCollCli] = '" + infoCli.codCollCli + "'" +
                        ",[tipoImballaggio] = '" + infoCli.tipoImballaggio + "'" +
                        ",[noteImballaggio] = '" + infoCli.noteImballaggio + "'" +
                        " WHERE ([Autotest].[dbo].[RMA_InformativaCliente].[idRMA] = '" + infoCli.idRMA + "')", null);
#if USEPANTHERA
                        if (!infoCli.cliente.IdCliente.Equals(0))
                        {
                            int count = 0;
                            count = (int)conn.CreateCommand("SELECT COUNT ([ID_RMA]) FROM [Autotest].[dbo].[RMA_TES] WHERE ID_RMA = '" + infoCli.idRMA.Replace('/', '_') + "'", count);
                            if (count.Equals(0))
                                conn.CreateCommand("INSERT INTO [Autotest].[dbo].[RMA_TES] " +
                                                    "([ID_AZIENDA],[ID_RMA],[R_CLIENTE],[R_ARTICOLO],[QUANTITA],[ST_PANTHERA],[ST_SWS],[NOTE]) " +
                                                    "VALUES ('001','" + infoCli.idRMA.Replace('/', '_') + "','" + infoCli.cliente.IdCliente.ToString().PadLeft(6, '0') + "','" + collList[0].modelloColl + "','" + collList.Count + "','" + "0" + "','" + cmb_statoRMA.SelectedIndex.ToString() + "','" + "N/A" + "')", null);
                            else
                                conn.CreateCommand("UPDATE [Autotest].[dbo].[RMA_TES] SET " +
                                "[R_CLIENTE] = '" + infoCli.cliente.IdCliente.ToString().PadLeft(6, '0') + "'" +
                                ",[R_ARTICOLO] = '" + collList[0].modelloColl + "'" +
                                ",[QUANTITA] = '" + collList.Count + "'" +
                                ",[ST_SWS] = '" + cmb_statoRMA.SelectedIndex.ToString() + "'" +
                                "WHERE ([Autotest].[dbo].[RMA_TES].[ID_AZIENDA] = '001' AND [Autotest].[dbo].[RMA_TES].[ID_RMA] = '" + infoCli.idRMA.Replace('/', '_') + "')", null);
                        }
#endif
                    }
                    //Tabella RMA_Allegati
                    if (allegatiList != null)
                    {
                        if (!Directory.Exists(@"\\ralcosrv2\public\RMA\" + lbl_NumRMA.Text.Replace("/", "_")))
                            Directory.CreateDirectory(@"\\ralcosrv2\public\RMA\" + lbl_NumRMA.Text.Replace("/", "_"));
                        foreach (RMA_Allegato a in allegatiList)
                        {
                            if (!a.pathAllegato.Contains(@"\\ralcosrv2\public\RMA\" + lbl_NumRMA.Text.Replace("/", "_")))
                            {
                                //faccio il salvataggio (in overwrite) solo se l'allegato non è già presente. Non faccio update della tabella, in quanto il path non viene modificato (o c'è, o manca)
                                string pathDest = @"\\ralcosrv2\public\RMA\" + lbl_NumRMA.Text.Replace("/", "_") + @"\" + Path.GetFileName(a.pathAllegato);
                                File.Copy(a.pathAllegato, pathDest, true);
                                a.pathAllegato = pathDest;
                                conn.CreateCommand("INSERT INTO [Autotest].[dbo].[RMA_Allegati] " +
                                "([idRMA],[pathAllegato]) " +
                                "VALUES ('" + lbl_NumRMA.Text + "','" + a.pathAllegato + "')", null);
                            }
                        }
                    }
                    if (Globals.user.getUserCredential().Equals("NonConfExt") || Globals.user.getUserCredential().Equals("admin"))
                    {
                        //Tabella RMA_GaranziaRalco
                        conn.CreateCommand("UPDATE [Autotest].[dbo].[RMA_GaranziaRalco] SET " +
                        "[idRMA] = '" + garanziaRalco.idRMA + "'" +
                        ",[provCollimatore] = '" + garanziaRalco.provCollimatore + "'" +
                        ",[anniVitaCollimatore] = '" + garanziaRalco.anniVitaCollimatore + "'" +
                        ",[respDannoCollimatore] = '" + garanziaRalco.respDannoCollimatore + "'" +
                        ",[noteGaranziaRalco] = '" + garanziaRalco.noteGaranziaRalco + "'" +
                        ",[statoGaranzia] = '" + garanziaRalco.statoGaranzia + "'" +
                        ",[azioniSupplRalco] = '" + garanziaRalco.azioniSupplRalco + "'" +
                        " WHERE ([Autotest].[dbo].[RMA_GaranziaRalco].[idRMA] = '" + garanziaRalco.idRMA + "')", null);
                    }
                    //Tabella RMA_NoteQualita
                    conn.CreateCommand("UPDATE [Autotest].[dbo].[RMA_NoteQualita] SET " +
                    "[idRMA] = '" + noteQualita.idRMA + "'" +
                    ",[sicurezzaProdotto] = '" + noteQualita.sicurezzaProdotto + "'" +
                    ",[segnalazIncidente] = '" + noteQualita.segnalazIncidente + "'" +
                    ",[invioNotaInf] = '" + noteQualita.invioNotaInf + "'" +
                    ",[revDocValutazRischi] = '" + noteQualita.revDocValutazRischi + "'" +
                    ",[statoAzCorrett] = '" + noteQualita.statoAzCorrett + "'" +
                    ",[rifAzCorrett] = '" + noteQualita.rifAzCorrett + "'" +
                    ",[descAzCorrett] = '" + noteQualita.descAzCorrett + "'" +
                    ",[note] = '" + noteQualita.note + "'" +
                    " WHERE ([Autotest].[dbo].[RMA_NoteQualita].[idRMA] = '" + noteQualita.idRMA + "')", null);
                    //Tabella RMA_FasiProduzioneDaRieseguire
                    //ho aggiunto un nuovo problema
                    //minFasiDaRiesList.minFaseProd.Reverse();
                    if (minFasiDaRiesList.idRMA == null)
                    {
                        minFasiDaRiesList.idRMA = lbl_NumRMA.Text;
                        conn.CreateCommand("INSERT INTO [Autotest].[dbo].[RMA_FasiProduzioneDaRieseguire] " +
                        "([idRMA],[minutiLavoroFase1],[minutiLavoroFase2],[minutiLavoroFase3],[minutiLavoroFase4],[minutiLavoroFase5],[minutiLavoroFase6],[minutiLavoroFase7],[minutiLavoroFase8],[minutiLavoroFase9],[minutiLavoroFase10],[minutiLavoroFase11],[minutiLavoroFase12]) " +
                        "VALUES ('" + minFasiDaRiesList.idRMA + "'," + minFasiDaRiesList.minFaseProd[0] + "," + minFasiDaRiesList.minFaseProd[1] + "," + minFasiDaRiesList.minFaseProd[2] + "," + minFasiDaRiesList.minFaseProd[3] + "," + minFasiDaRiesList.minFaseProd[4] + "," + minFasiDaRiesList.minFaseProd[5] + "," + minFasiDaRiesList.minFaseProd[6] + "," + minFasiDaRiesList.minFaseProd[7] + "," + minFasiDaRiesList.minFaseProd[8] + "," + minFasiDaRiesList.minFaseProd[9] + "," + minFasiDaRiesList.minFaseProd[10] + "," + minFasiDaRiesList.minFaseProd[11] + ")", null);
                    }
                    else
                    {
                        //aggiorno i dati del problema
                        conn.CreateCommand("UPDATE [Autotest].[dbo].[RMA_FasiProduzioneDaRieseguire] SET" +
                        "[idRMA] = '" + minFasiDaRiesList.idRMA + "'" +
                        ",[minutiLavoroFase1] = " + minFasiDaRiesList.minFaseProd[0] + "" +
                        ",[minutiLavoroFase2] = " + minFasiDaRiesList.minFaseProd[1] + "" +
                        ",[minutiLavoroFase3] = " + minFasiDaRiesList.minFaseProd[2] + "" +
                        ",[minutiLavoroFase4] = " + minFasiDaRiesList.minFaseProd[3] + "" +
                        ",[minutiLavoroFase5] = " + minFasiDaRiesList.minFaseProd[4] + "" +
                        ",[minutiLavoroFase6] = " + minFasiDaRiesList.minFaseProd[5] + "" +
                        ",[minutiLavoroFase7] = " + minFasiDaRiesList.minFaseProd[6] + "" +
                        ",[minutiLavoroFase8] = " + minFasiDaRiesList.minFaseProd[7] + "" +
                        ",[minutiLavoroFase9] = " + minFasiDaRiesList.minFaseProd[8] + "" +
                        ",[minutiLavoroFase10] = " + minFasiDaRiesList.minFaseProd[9] + "" +
                        ",[minutiLavoroFase11] = " + minFasiDaRiesList.minFaseProd[10] + "" +
                        ",[minutiLavoroFase12] = " + minFasiDaRiesList.minFaseProd[11] + "" +
                        " WHERE ([Autotest].[dbo].[RMA_FasiProduzioneDaRieseguire].[idRMA] = '" + minFasiDaRiesList.idRMA /*+ "' AND [Autotest].[dbo].[RMA_FasiProduzioneDaRieseguire].[idProgressivo] = '" + minFasiDaRiesList.idProgressivo*/ + "')", null);
                    }
                    serialiAdded.Clear();
                    partiAdded.Clear();
                }
                //Tabella RMA_TestDaEseguire salvo solo quando chiudo anche la finestra
                if (testDaEseguireList != null)
                    testDaEseguireList.Clear();
                else
                    testDaEseguireList = new List<RMA_TestDaEseguire>();
                testDaEseguireList = (List<RMA_TestDaEseguire>)conn.CreateCommand("SELECT * FROM [Autotest].[dbo].[RMA_TestDaEseguire] WHERE [Autotest].[dbo].[RMA_TestDaEseguire].[idRMA] = '" + lbl_NumRMA.Text + "'", testDaEseguireList);
                foreach (DataGridViewRow r in dgv_testDaEseguire.Rows)
                {
                    if (r.Cells[0].Value.Equals(true))
                    {
                        if (testDaEseguireList == null || testDaEseguireList.Count.Equals(0))
                            conn.CreateCommand("INSERT INTO [Autotest].[dbo].[RMA_TestDaEseguire] " +
                            "([idRMA],[descTestDaEseguire]) " +
                            "VALUES ('" + lbl_NumRMA.Text + "','" + r.Cells[1].Value + "')", null);
                        else
                        {
                            bool found = false;
                            foreach (RMA_TestDaEseguire test in testDaEseguireList)
                                if (test.descTestDaEseguire.Equals(r.Cells[1].Value))
                                {
                                    found = true;
                                    break;
                                }
                            if (!found)
                                conn.CreateCommand("INSERT INTO [Autotest].[dbo].[RMA_TestDaEseguire] " +
                                "([idRMA],[descTestDaEseguire]) " +
                                "VALUES ('" + lbl_NumRMA.Text + "','" + r.Cells[1].Value + "')", null);
                        }
                    }
                    else
                    {
                        if (testDaEseguireList != null)
                        {
                            bool found = false;
                            foreach (RMA_TestDaEseguire test in testDaEseguireList)
                                if (test.descTestDaEseguire.Equals(r.Cells[1].Value))
                                {
                                    found = true;
                                    break;
                                }
                            if (found)
                                conn.CreateCommand("DELETE FROM [Autotest].[dbo].[RMA_TestDaEseguire] " +
                                "WHERE [Autotest].[dbo].[RMA_TestDaEseguire].[idRMA] = '" + lbl_NumRMA.Text + "' AND [Autotest].[dbo].[RMA_TestDaEseguire].[descTestDaEseguire] ='" + r.Cells[1].Value + "'", null);
                        }
                    }
                }
                //faccio commit sulla tabella azioni supplementari se e solo se non si tratta di un RMA clonato. Questo per evitare il caso in cui si clona un RMA e poi si cambia cliente e modello collimatore.
                //if (!isClonedRMA)
                //{
                //    //Tabella RMA_AzioniSupplementari salvo solo quando chiudo anche la finestra
                //    if (esitiAzioniSupplementariList != null)
                //        esitiAzioniSupplementariList.Clear();
                //    else
                //        esitiAzioniSupplementariList = new List<RMA_EsitiAzioniSupplementari>();
                //    esitiAzioniSupplementariList = (List<RMA_EsitiAzioniSupplementari>)conn.CreateCommand("SELECT * FROM [Autotest].[dbo].[RMA_EsitiAzioniSupplementari] WHERE [Autotest].[dbo].[RMA_EsitiAzioniSupplementari].[idRMA] = '" + lbl_NumRMA.Text + "'", esitiAzioniSupplementariList);
                //    if (esitiAzioniSupplementariList == null || esitiAzioniSupplementariList.Count.Equals(0))
                //        foreach (DataGridViewRow r in dgv_azioniSupplementari.Rows)
                //            conn.CreateCommand("INSERT INTO [Autotest].[dbo].[RMA_EsitiAzioniSupplementari] " +
                //            "([idRMA],[descAzSuppl],[esitoAzSuppl]) " +
                //            "VALUES ('" + lbl_NumRMA.Text + "','" + Utils.parseUselessChars(r.Cells[1].Value.ToString()) + "','" + Utils.parseUselessChars(r.Cells[2].Value.ToString()) + "')", null);
                //    else
                //    {
                //        int idx = 0;
                //        foreach (RMA_EsitiAzioniSupplementari a in esitiAzioniSupplementariList)
                //        {
                //            conn.CreateCommand("UPDATE [Autotest].[dbo].[RMA_EsitiAzioniSupplementari] SET" +
                //            "[esitoAzSuppl] =  '" + Utils.parseUselessChars(dgv_azioniSupplementari.Rows[idx].Cells[2].Value.ToString()) + "'" +
                //            "WHERE [idRMA] = '" + lbl_NumRMA.Text + "' AND [idProgressivo] = '" + a.idProgressivo + "'", null);
                //            idx++;
                //        }
                //    }
                //}
                sendEmail();
                switch (main)
                {
                    case 1:
                        this.DialogResult = DialogResult.OK;
                        this.Dispose();
                        break;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void sendEmail()
        {
            try
            {
                //invio di e-mail automatiche a seconda dello stato
                List<String> recipientsAddressesList = new List<String>();
                string testSubject = "";
                string testBody = "";
                if (cmb_statoRMA.SelectedItem.Equals("Preventivo Approvato") && !!cmb_statoRMA.BackColor.Equals(Color.Salmon) && !dgv_ListaRiparazRMA.Rows.Count.Equals(0))
                {
                    recipientsAddressesList.Add("l.redaelli@ralco.it");
                    recipientsAddressesList.Add("a.pisu@ralco.it");
                    testSubject = "Scarico Pezzi Da Magazzino";
                    testBody = "Scaricare dal magazzino i seguenti articoli : \n";
                    foreach (DataGridViewRow row in dgv_ListaRiparazRMA.Rows)
                    {
                        testBody = testBody + "ARTICOLO : " + row.Cells.GetCellValueFromColumnHeader("Parte") + " QUANTITA' : " + row.Cells.GetCellValueFromColumnHeader("Quantità")  + "\n";
                    }
                }
                else if (cmb_statoRMA.SelectedItem.Equals("Chiuso") && !!cmb_statoRMA.BackColor.Equals(Color.Salmon))
                {
                    recipientsAddressesList.Add("s.conti@ralco.it");
                    testSubject = "RMA " + lbl_NumRMA + "Chiuso";
                    testBody = "Verificare se per l'RMA in oggetto sono necessarie azioni correttive.";
                }
                if (!recipientsAddressesList.Count.Equals(0))
                    sendMail(recipientsAddressesList, testSubject, testBody);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Cerco in RMA_Generali l'idRMA. Se non lo trovo, significa che non ho mai premuto save -> rimuovo dalla sola tabella RMA_Main this.idRMA, valorizzato durante la calcolaIdRMA();
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_cancelRMA_Click(object sender, EventArgs e)
        {
            try
            {
                formCancel();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Form_RMA_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                formCancel();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void formCancel()
        {
            try
            {
                string idRMAToDelete = (string)conn.CreateCommand("SELECT [idRMA] FROM [Autotest].[dbo].[RMA_Generali] WHERE [Autotest].[dbo].[RMA_Generali].[idRMA] = '" + lbl_NumRMA.Text + "'", idRMA);
                if (idRMAToDelete.Equals(""))//non ho mai premuto Save -> idRMA è presente SOLO in Form_Main
                    this.DialogResult = DialogResult.Abort;
                else
                    this.DialogResult = DialogResult.Cancel;
                this.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void rb_statoGaranzia_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                cmb_statoGaranzia.Enabled = rb_statoGaranzia.Checked;
                cmb_statoGaranzia.DropDownStyle = ComboBoxStyle.DropDown;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

#region Generali

#region GeneralMethods
        private void refreshDataGridView(int main)
        {
            /*DATAGRIDVIEW IN REFRESH
             main = 0 -> dgv_ListaCollimatoriRMA
             main = 1 -> dgv_ListaPartiRMA*/
            try
            {
                switch (main)
                {
                    case 0:
                        {
                            dgv_ListaCollimatoriRMA.Rows.Clear();
                            if (collList != null)
                            {
                                foreach (RMA_Collimatore c in collList)
                                {
                                    dgv_ListaCollimatoriRMA.Rows.Insert(0, true, c.seriale, c.modelloColl, c.dataProduzione.ToShortDateString(), c.cliente.nomeCliente, c.codCollCliente, c.numFlowChart, c.da_N, c.a_N, c.qta);
                                }
                            }
                            dgv_ListaCollimatoriRMA.Refresh();
                        }
                        break;
                    case 1:
                        {
                            dgv_ListaPartiRMA.Rows.Clear();
                            if (partList != null)
                            {
                                foreach (RMA_Parte p in partList)
                                {
                                    dgv_ListaPartiRMA.Rows.Insert(0, true, p.idParte, p.descParte, p.serialeParte, p.nomeCliente, p.numFlowChart, p.numBolla, p.dataBolla, p.dataVendita, p.quantita);
                                    if (p.dataBolla.Equals(DateTime.MinValue))//(r.dataScadenza.ToString().Equals("01/01/1753 00:00:00") || r.dataScadenza.ToString().Equals("01/01/0001 00:00:00"))
                                    {
                                        //se la data è quella minima, non la faccio visualizzare (il valore deve essere sempre inserito come data, altrimenti non si può eseguire l'ordinamento)
                                        this.dgv_ListaPartiRMA.Rows[0].Cells[7].Style.ForeColor = Color.Transparent;
                                        this.dgv_ListaPartiRMA.Rows[0].Cells[7].Style.SelectionForeColor = Color.Transparent;
                                    }
                                    if (p.dataVendita.Equals(DateTime.MinValue))//(r.dataScadenza.ToString().Equals("01/01/1753 00:00:00") || r.dataScadenza.ToString().Equals("01/01/0001 00:00:00"))
                                    {
                                        //se la data è quella minima, non la faccio visualizzare (il valore deve essere sempre inserito come data, altrimenti non si può eseguire l'ordinamento)
                                        this.dgv_ListaPartiRMA.Rows[0].Cells[8].Style.ForeColor = Color.Transparent;
                                        this.dgv_ListaPartiRMA.Rows[0].Cells[8].Style.SelectionForeColor = Color.Transparent;
                                    }
                                }
                            }
                            dgv_ListaPartiRMA.Refresh();
                        }
                        break;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void printBarCode(string numRMA)
        {
            try
            {
                using (WindowsImpersonationContext wic = WindowsIdentity.Impersonate(IntPtr.Zero))
                {
                    //code to send printdocument to the printer
                    PrintDocument document = new PrintDocument();
                    BarcodeDraw bdraw = BarcodeDrawFactory.GetSymbology(BarcodeSymbology.Code128);
                    PrintDialog pdlg = new PrintDialog();
                    string dymoName = ConfigurationManager.AppSettings["DYMO"];
                    if (dymoName != null)
                    {
                        pdlg.PrinterSettings.PrinterName = dymoName;
                        document.PrinterSettings.PrinterName = pdlg.PrinterSettings.PrinterName;
                        document.DefaultPageSettings.Landscape = true;//stampa orizzontalmente
                        string barcodeInfo = "";
                        System.Drawing.Image barcodeImage = bdraw.Draw(numRMA, 50);
                        document.PrintPage += delegate (object sender, PrintPageEventArgs e)
                        {
                            e.Graphics.DrawImage(barcodeImage, 5, 15);
                            e.Graphics.DrawString(numRMA, new System.Drawing.Font("arial", 8), new SolidBrush(Color.Black), 50, 70);
                        };
                        document.Print();
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void printLabels()
        {
            try
            {
                using (WindowsImpersonationContext wic = WindowsIdentity.Impersonate(IntPtr.Zero))
                {
                    //code to send printdocument to the printer
                    streamToPrint = new StreamReader(Environment.CurrentDirectory + @"\TMP\etichetta_TMP.txt");
                    try
                    {
                        printFont = new System.Drawing.Font("Arial", 7);
                        PrintDocument pd = new PrintDocument();
                        PrintDialog pdlg = new PrintDialog();
                        string dymoName = ConfigurationManager.AppSettings["DYMO"];
                        if (dymoName != null)
                        {
                            pdlg.PrinterSettings.PrinterName = dymoName;
                            pd.PrinterSettings.PrinterName = pdlg.PrinterSettings.PrinterName;
                            pd.PrintPage += new PrintPageEventHandler(this.pd_PrintPage);
                            pd.DefaultPageSettings.Landscape = true;//stampa orizzontalmente
                            pd.Print();
                        }
                    }
                    finally
                    {
                        streamToPrint.Close();
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void pd_PrintPage(object sender, PrintPageEventArgs ev)
        {
            try
            {
                float linesPerPage = 0;
                float yPos = 0;
                int count = 0;
                float leftMargin = 0;
                float topMargin = 10;
                float offset = 0;
                String line = null;

                // Calculate the number of lines per page.
                linesPerPage = 5;// ev.MarginBounds.Height / printFont.GetHeight(ev.Graphics);

                // Iterate over the file, printing each line.
                while (count <= linesPerPage && ((line = streamToPrint.ReadLine()) != null))
                {
                    yPos = topMargin + (count * printFont.GetHeight(ev.Graphics)) + offset;
                    ev.Graphics.DrawString(line, printFont, Brushes.Black, leftMargin, yPos, new StringFormat());
                    count++;
                    offset = offset + 5;
                }

                // If more lines exist, print another page.
                if (line != null)
                    ev.HasMorePages = true;
                else
                    ev.HasMorePages = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

#endregion

#region Collimatori
        private void dgv_ListaCollimatoriRMA_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                Utils.DataGridView_CellContentClick(dgv_ListaCollimatoriRMA, 1);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Cmb_CollSelection_TextChanged(object sender, EventArgs e)
        {
            try
            {
                bool areAllRmaSelected = false;
                if (cmb_CollSelection.SelectedItem.Equals("Seleziona Tutti"))
                    areAllRmaSelected = true;
                else
                    areAllRmaSelected = false;
                foreach (DataGridViewRow row in dgv_ListaCollimatoriRMA.Rows)
                    row.Cells[0].Value = areAllRmaSelected;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_RefreshCollRMA_Click(object sender, EventArgs e)
        {
            try
            {
                refreshDataGridView(0);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_AddCollRMA_Click(object sender, EventArgs e)
        {
            try
            {
                using (var form = new Form_Collimatore(lbl_NumRMA.Text))
                {
                    var result = form.ShowDialog();
                    if (result == DialogResult.OK && !form.collimators.Count.Equals(0))
                    {
                        collList.AddRange(form.collimators);
                        serialiAdded.Clear();
                        foreach (RMA_Collimatore c in form.collimators)
                            serialiAdded.Add(c.seriale);
                        string s = (String)conn.CreateCommand("SELECT Dsc FROM [archivi].[dbo].[VISTA_SM_CLIPANTH_CLIOLDRALCO] WHERE Ragsoc LIKE '%" + form.collimators[0].cliente.nomeCliente + "%'", cmb_nomeCliente.Text);
                        btn_saveRMA_Click(null, null);
                        cmb_nomeCliente.SelectedIndex = cmb_nomeCliente.FindString(s.TrimEnd());
                        dgv_azioniSupplementari.Rows.Clear();
                        btn_RefreshAzSuppl_Click(null, null);
                    }
                }
                btn_RefreshCollRMA_Click(sender, e);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_CancelCollRMA_Click(object sender, EventArgs e)
        {
            try
            {
                tabctrl_RMA_SelectedIndexChanged(null, null);
                int totElements = 0;
                if (collList != null)
                    totElements += collList.Count();
                if (partList != null)
                    totElements += partList.Count();
                if (totElements == 1)
                    MessageBox.Show("Inserire un elemento prima di rimuovere il collimatore selezionato", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                {
                    foreach (DataGridViewRow row in dgv_ListaCollimatoriRMA.Rows)
                    {
                        if (row.Cells["chk_coll"].Value != null && row.Cells["chk_coll"].Value.Equals(true))
                        {
                            var itemToRemove = collList.SingleOrDefault(r => r.seriale.Equals(row.Cells["serialNr"].Value));
                            if (itemToRemove != null)
                            {
                                collList.Remove(itemToRemove);
                                if (rma_fromForm != null)
                                {
                                    conn.CreateCommand("DELETE FROM [Autotest].[dbo].[RMA_Collimatore] " +
                                    "WHERE [Autotest].[dbo].[RMA_Collimatore].[idRMA] = '" + itemToRemove.idRMA + "' AND [Autotest].[dbo].[RMA_Collimatore].[serialeColl] ='" + itemToRemove.seriale + "'", null);
                                    conn.CreateCommand("DELETE FROM [Autotest].[dbo].[RMA_Generali] " +
                                    "WHERE [Autotest].[dbo].[RMA_Generali].[idRMA] = '" + itemToRemove.idRMA + "' AND [Autotest].[dbo].[RMA_Generali].[serialeColl] ='" + itemToRemove.seriale + "'", null);

                                    #if USEPANTHERA
                                    //cancello i dati dalla vista RMA_TES_MATRICOLE
                                    conn.CreateCommand("DELETE FROM[Autotest].[dbo].[RMA_TES_MATRICOLE] " +
                                    "WHERE ([Autotest].[dbo].[RMA_TES_MATRICOLE].[ID_AZIENDA] = '001' AND [Autotest].[dbo].[RMA_TES_MATRICOLE].[ID_RMA] = '" + itemToRemove.idRMA.Replace('/', '_') + "' AND [Autotest].[dbo].[RMA_TES_MATRICOLE].[ID_MATRICOLA] = '" + itemToRemove.seriale + "' AND [Autotest].[dbo].[RMA_TES_MATRICOLE].[ID_ARTICOLO] = '" + itemToRemove.modelloColl + "')", null);
                                    #endif
                                }
                            }
                        }
                    }
                    btn_RefreshCollRMA_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_StampaEtichetteColl_Click(object sender, EventArgs e)
        {
            try
            {
                int index = 0;//indice usato per determinare quale elemento della lista modificare
                collList.Reverse();//inverto l'ordine degli elementi della lista perchè list.Add aggiunge in Append, mentre nella datagridview aggiungo in testa...
                List<RMA_Collimatore> itemToPrint = new List<RMA_Collimatore>();
                foreach (DataGridViewRow row in dgv_ListaCollimatoriRMA.Rows)
                {
                    if (row.Cells.GetCellValueFromColumnHeader("").Equals(true))
                    {
                        index = row.Index;
                        itemToPrint.Add(collList.Single(c => c.seriale.Equals(row.Cells.GetCellValueFromColumnHeader("Seriale"))));
                    }
                }
                if (itemToPrint != null && !itemToPrint.Count.Equals(0))
                {
                    foreach (RMA_Collimatore c in itemToPrint)
                    {
                        if (!Directory.Exists(Environment.CurrentDirectory + @"\TMP\"))
                            Directory.CreateDirectory(Environment.CurrentDirectory + @"\TMP\");

                        string[] lines = { "N° RMA : " + lbl_NumRMA.Text, "Modello : " + c.modelloColl, "N° Seriale : " + c.seriale, "N° Flow Chart : " + c.numFlowChart, "Cliente : " + cmb_nomeCliente.SelectedItem.ToString().Trim('\r').TrimEnd() };
                        System.IO.File.WriteAllLines(Environment.CurrentDirectory + @"\TMP\etichetta_TMP.txt", lines);
                        printLabels();
                        printBarCode(lbl_NumRMA.Text);
                    }
                }
                else
                    MessageBox.Show("Selezionare almeno un collimatore", "Informazione", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_visualizzaScansioneFlowChart_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow row in dgv_ListaCollimatoriRMA.Rows)
                {
                    if (row.Cells["chk_coll"].Value != null && row.Cells["chk_coll"].Value.Equals(true))
                    {
                        string[] splittedString = row.Cells["numFC"].Value.ToString().Split('/');
                        if (splittedString[1].Contains("1995") || splittedString[1].Contains("1996") || splittedString[1].Contains("1997"))
                            splittedString[1] = splittedString[1] + " Philips";
                        string path = @"\\ralcosrv2\Dati_Ralco\Scansioni\FlowChart\" + splittedString[1];
                        //string path = @"D:\" + splittedString[1];
                        string[] files = System.IO.Directory.GetFiles(path, "*" + splittedString[0] + "*", System.IO.SearchOption.TopDirectoryOnly);
                        if (files.Length > 0)
                        {
                            //file exist
                            foreach (string f in files)
                                Process.Start(f);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_visualizzaFlowTree_Click(object sender, EventArgs e)
        {
            try
            {
                using (var form = new Form_FlowChart(difList))
                {
                    var result = form.ShowDialog();
                    if (result == DialogResult.OK)
                    {
                        form.Close();
                        form.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_DHR_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow row in dgv_ListaCollimatoriRMA.Rows)
                {
                    if (row.Cells["chk_coll"].Value != null && row.Cells["chk_coll"].Value.Equals(true))
                    {
                        string[] splittedString = row.Cells["numFC"].Value.ToString().Split('/');
                        string path = @"\\ralcosrv2\Public\DHR_Collimatori\" + splittedString[1] + "\\" + splittedString[0] + "\\" + row.Cells["serialNr"].Value;
                        if (Directory.Exists(path))
                        {
                            ProcessStartInfo startInfo = new ProcessStartInfo(path, "explorer.exe");
                            Process.Start(startInfo);
                        }
                        else
                            MessageBox.Show("Directory DHR non esistente", "Informazione", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
#endregion

#region Parti
        private void dgv_ListaPartiRMA_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                Utils.DataGridView_CellContentClick(dgv_ListaPartiRMA, 1);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btn_RefreshParteRMA_Click(object sender, EventArgs e)
        {
            try
            {
                refreshDataGridView(1);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btn_AddParteRMA_Click(object sender, EventArgs e)
        {
            try
            {
                using (var form = new Form_Parte(lbl_NumRMA.Text))
                {
                    var result = form.ShowDialog();
                    if (result == DialogResult.OK && !form.partList.Count.Equals(0))
                    {
                        partList.AddRange(form.partList);
                        partiAdded.Clear();
                        foreach (RMA_Parte p in form.partList)
                            partiAdded.Add(p.idProgressivo);
                        btn_saveRMA_Click(null, null);
                    }
                }
                btn_RefreshParteRMA_Click(null, null);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_CancelParteRMA_Click(object sender, EventArgs e)
        {
            try
            {
                tabctrl_RMA_SelectedIndexChanged(null, null);
                int totElements = 0;
                if (collList != null)
                    totElements += collList.Count();
                if (partList != null)
                    totElements += partList.Count();
                if (totElements == 1)
                    MessageBox.Show("Inserire un elemento prima di rimuovere il collimatore selezionato", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                {
                    foreach (DataGridViewRow row in dgv_ListaPartiRMA.Rows)
                    {
                        if (row.Cells["chk_Parte"].Value != null && row.Cells["chk_Parte"].Value.Equals(true))
                        {
                            RMA_Parte p = new RMA_Parte(row.Cells[1].Value.ToString(), row.Cells[2].Value.ToString(), row.Cells[3].Value.ToString(), row.Cells[4].Value.ToString(), row.Cells[6].Value.ToString(), Convert.ToDateTime(row.Cells[7].Value.ToString()), Convert.ToDateTime(row.Cells[8].Value.ToString()), Convert.ToInt16(row.Cells[9].Value.ToString()), row.Cells[5].Value.ToString());
                            var itemToRemove = partList.SingleOrDefault(r => r.Equals(p));
                            if (itemToRemove != null)
                            {
                                partList.Remove(itemToRemove);
                                if (rma_fromForm != null)
                                {
                                    try
                                    {
                                        int progrParte = 0;
                                        progrParte = (int)conn.CreateCommand("SELECT [idProgressivo] FROM [Autotest].[dbo].[RMA_Parte] WHERE [idRMA] = '" + itemToRemove.idRMA + "'", progrParte);
                                        conn.CreateCommand("DELETE FROM [Autotest].[dbo].[RMA_Parte] " +
                                        "WHERE [Autotest].[dbo].[RMA_Parte].[idProgressivo] = '" + progrParte + "'", null);
                                        conn.CreateCommand("DELETE FROM [Autotest].[dbo].[RMA_Generali] " +
                                        "WHERE [Autotest].[dbo].[RMA_Generali].[idRMA] = '" + itemToRemove.idRMA + "' AND [Autotest].[dbo].[RMA_Generali].[progrParte] ='" + progrParte + "'", null);
                                    }
                                    catch (Exception ex)
                                    {
                                        MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    }
                                }
                            }
                        }
                    }
                    btn_RefreshParteRMA_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_EditParteRMA_Click(object sender, EventArgs e)
        {
            try
            {
                int index = 0;//indice usato per determinare quale elemento della lista modificare
                partList.Reverse();//inverto l'ordine degli elementi della lista perchè list.Add aggiunge in Append, mentre nella datagridview aggiungo in testa...
                List<RMA_Parte> partToEditList = new List<RMA_Parte>();
                foreach (DataGridViewRow row in dgv_ListaPartiRMA.Rows)
                {
                    if (row.Cells.GetCellValueFromColumnHeader("").Equals(true) && !row.Cells.GetCellValueFromColumnHeader("Seriale").Equals(""))
                    {
                        index = row.Index;
                        partToEditList.Add(partList.Single(r => r.idParte.Equals(row.Cells.GetCellValueFromColumnHeader("ID Parte"))));
                    }
                    else
                    {
                        partToEditList.AddRange(partList);
                    }
                }
                if (partToEditList == null && !partToEditList.Count.Equals(0))
                    MessageBox.Show("Selezionare una parte da modificare", "Informazione", MessageBoxButtons.OK, MessageBoxIcon.Information);
                else if (partToEditList.Count >= 2)
                    MessageBox.Show("Selezionare una sola parte da modificare", "Informazione", MessageBoxButtons.OK, MessageBoxIcon.Information);
                else
                {
                    using (var form = new Form_Parte(partToEditList))
                    {
                        var result = form.ShowDialog();
                        if (result == DialogResult.OK)
                        {
                            partList.Clear();
                            partList.AddRange(form.partList);
                        }
                    }
                    partList.Reverse();
                    btn_RefreshParteRMA_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_StampaEtichetteParte_Click(object sender, EventArgs e)
        {
            try
            {
                int index = 0;//indice usato per determinare quale elemento della lista modificare
                partList.Reverse();//inverto l'ordine degli elementi della lista perchè list.Add aggiunge in Append, mentre nella datagridview aggiungo in testa...
                List<RMA_Parte> itemToPrint = new List<RMA_Parte>();
                foreach (DataGridViewRow row in dgv_ListaPartiRMA.Rows)
                {
                    if (row.Cells.GetCellValueFromColumnHeader("").Equals(true))
                    {
                        index = row.Index;
                        if (!row.Cells.GetCellValueFromColumnHeader("Seriale").Equals(""))
                            itemToPrint.Add(partList.Single(c => c.idParte.Equals(row.Cells.GetCellValueFromColumnHeader("ID Parte"))));
                        else
                            itemToPrint.Add(partList[0]);
                    }
                }
                if (itemToPrint != null && !itemToPrint.Count.Equals(0))
                {
                    foreach (RMA_Parte p in itemToPrint)
                    {
                        if (!Directory.Exists(Environment.CurrentDirectory + @"\TMP\"))
                            Directory.CreateDirectory(Environment.CurrentDirectory + @"\TMP\");
                        string[] lines = { "N° RMA : " + lbl_NumRMA.Text, "Modello : " + p.idParte, "N° Seriale : " + p.serialeParte, "N° Flow Chart : " + p.numFlowChart };
                        System.IO.File.WriteAllLines(Environment.CurrentDirectory + @"\TMP\etichetta_TMP.txt", lines);
                        printLabels();
                        printBarCode(lbl_NumRMA.Text);
                    }
                }
                else
                    MessageBox.Show("Selezionare almeno una parte", "Informazione", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Cmb_ParteSelection_TextChanged(object sender, EventArgs e)
        {
            try
            {
                bool areAllRmaSelected = false;
                if (cmb_ParteSelection.SelectedItem.Equals("Seleziona Tutti"))
                    areAllRmaSelected = true;
                else
                    areAllRmaSelected = false;
                foreach (DataGridViewRow row in dgv_ListaPartiRMA.Rows)
                    row.Cells[0].Value = areAllRmaSelected;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
#endregion

#endregion

#region Allegati

        private void dgv_ListaAllegatiRMA_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                Utils.DataGridView_CellContentClick(dgv_ListaAllegatiRMA, 1);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_RefreshAllegati_Click(object sender, EventArgs e)
        {
            try
            {
                dgv_ListaAllegatiRMA.Rows.Clear();
                if (allegatiList != null)
                {
                    foreach (RMA_Allegato a in allegatiList)
                    {
                        dgv_ListaAllegatiRMA.Rows.Insert(0, false, a.pathAllegato);
                    }
                    dgv_ListaAllegatiRMA.Refresh();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_AddAllegati_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog dlg = new OpenFileDialog();
                if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    if (allegatiList == null)
                        allegatiList = new List<RMA_Allegato>();
                    //non inserisco elementi duplicati (identico filename)
                    bool ok = true;
                    foreach (RMA_Allegato a in allegatiList)
                    {
                        if (Path.GetFileName(a.pathAllegato).Equals(Path.GetFileName(dlg.FileName.ToString())))
                        {
                            ok = false;
                            break;
                        }
                    }
                    if (ok)
                        allegatiList.Add(new RMA_Allegato(lbl_NumRMA.Text, dlg.FileName.ToString()));
                    else
                        MessageBox.Show("Allegato già presente", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    btn_RefreshAllegati_Click(null, null);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_DeleteAllegati_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow row in dgv_ListaAllegatiRMA.Rows)
                {
                    if (row.Cells["chkAll"].Value != null && row.Cells["chkAll"].Value.Equals(true))
                    {
                        var itemToRemove = allegatiList.Single(a => a.pathAllegato.Equals(row.Cells.GetCellValueFromColumnHeader("Nome File")));
                        if (itemToRemove != null)
                        {
                            var result = MessageBox.Show("L'elemento selezionato verrà rimosso definitivamente. Sei sicuro di voler procedere?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (result == DialogResult.Yes)
                            {
                                allegatiList.Remove(itemToRemove);
                                //se sono nel caso di modifica dell'RMA, rimuovo l'elemento anche dal DB;
                                if (rma_fromForm != null)
                                {
                                    conn.CreateCommand("DELETE FROM [Autotest].[dbo].[RMA_Allegati] " +
                                    "WHERE [Autotest].[dbo].[RMA_Allegati].[idRMA] = '" + itemToRemove.idRMA + "' AND [Autotest].[dbo].[RMA_Allegati].[idProgressivo] ='" + itemToRemove.idProgressivo + "'", null);
                                    File.Delete(itemToRemove.pathAllegato);
                                }
                            }
                        }
                    }
                }
                btn_RefreshAllegati_Click(null, null);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Btn_viewAllegati_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow row in dgv_ListaAllegatiRMA.Rows)
                {
                    if (row.Cells["chkAll"].Value != null && row.Cells["chkAll"].Value.Equals(true))
                    {
                        Process.Start(row.Cells["nomeFile"].Value.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

#endregion

#region AnalisiProblema

        private void dgv_ListaProblemiRMA_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                Utils.DataGridView_CellContentClick(dgv_ListaProblemiRMA, 1);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_RefreshProbl_Click(object sender, EventArgs e)
        {
            try
            {
                dgv_ListaProblemiRMA.Rows.Clear();
                if (probList != null)
                {
                    foreach (RMA_Problema p in probList)
                    {
                        dgv_ListaProblemiRMA.Rows.Insert(0, false, p.difRiscDaRalco, p.dataAggProb, p.gravitaProblema, p.idProblema);
                    }
                    dgv_ListaProblemiRMA.Refresh();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_AddProbl_Click(object sender, EventArgs e)
        {
            try
            {
                addProblema(false);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_addTemplateProbDB_Click(object sender, EventArgs e)
        {
            try
            {
                addProblema(true);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void addProblema(bool isTemplateDBProblem)
        {
            try
            {
                if (collList != null)
                {
                    if (!collList.Count.Equals(0))
                    {
                        //considero solo il caso in cui collList contiene lo stesso tipo di collimatore
                        using (var form = new Form_Problema(lbl_NumRMA.Text, numFlowChart, difList, isTemplateDBProblem, null))
                        {
                            var result = form.ShowDialog();
                            if (result == DialogResult.OK)
                            {
                                if (form.problema != null)
                                    probList.Add(form.problema);
                            }
                        }
                        btn_RefreshProbl_Click(null, null);
                    }
                    else
                        MessageBox.Show("Inserire almeno un collimatore prima di procedere", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else if (partList != null)
                {
                    if (!partList.Count.Equals(0))
                    {
                        using (var form = new Form_Problema(lbl_NumRMA.Text, numFlowChart, difList, isTemplateDBProblem, null))
                        {
                            var result = form.ShowDialog();
                            if (result == DialogResult.OK)
                            {
                                if (form.problema != null)
                                    probList.Add(form.problema);
                            }
                        }
                        btn_RefreshProbl_Click(null, null);
                    }
                    else
                        MessageBox.Show("Inserire almeno una parte prima di procedere", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_importProbFromTemplateDB_Click(object sender, EventArgs e)
        {
            try
            {
                using (var form = new Form_ListaTemplatesDB(1))
                {
                    var result = form.ShowDialog();
                    if (result == DialogResult.OK)
                    {
                        if (form.problemsList != null && form.problemsList.Count.Equals(1))
                        {
                            using (var innerForm = new Form_Problema(lbl_NumRMA.Text, numFlowChart, difList, false, form.problemsList[0]))
                            {
                                var innerResult = innerForm.ShowDialog();
                                if (innerResult == DialogResult.OK)
                                {
                                    if (innerForm.problema != null)
                                        probList.Add(innerForm.problema);
                                }
                            }
                            probList.Reverse();
                            btn_RefreshProbl_Click(sender, e);
                        }
                    }
                }
                btn_RefreshProbl_Click(null, null);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_CloneProblema_Click(object sender, EventArgs e)
        {
            try
            {
                List<RMA_Problema> itemToClone = new List<RMA_Problema>();
                foreach (DataGridViewRow row in dgv_ListaProblemiRMA.Rows)
                {
                    if (row.Cells.GetCellValueFromColumnHeader("").Equals(true))
                    {
                        itemToClone.Add(probList.Single(r => r.idProblema.Equals(row.Cells.GetCellValueFromColumnHeader("ID PROBLEMA"))));
                    }
                }
                if (itemToClone != null && itemToClone.Count.Equals(1))
                {
                    itemToClone[0].idProblema = 0;
                    using (var form = new Form_Problema(lbl_NumRMA.Text, numFlowChart, difList, false, itemToClone[0]))
                    {
                        var result = form.ShowDialog();
                        if (result == DialogResult.OK)
                        {
                            probList.Add(form.problema);
                        }
                    }
                    btn_RefreshProbl_Click(sender, e);
                }
                else if (itemToClone.Count >= 2)
                    MessageBox.Show("Non è possibile clonare più di un problema per volta", "Informazione", MessageBoxButtons.OK, MessageBoxIcon.Information);
                else
                    MessageBox.Show("Selezionare un problema da clonare", "Informazione", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_DeleteProbl_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow row in dgv_ListaProblemiRMA.Rows)
                {
                    if (row.Cells["chk_Prob"].Value != null && row.Cells["chk_Prob"].Value.Equals(true))
                    {
                        var itemToRemove = probList.Single(r => r.idProblema.Equals(row.Cells.GetCellValueFromColumnHeader("ID PROBLEMA")));
                        if (itemToRemove != null)
                        {
                            var result = MessageBox.Show("L'elemento selezionato verrà rimosso definitivamente. Sei sicuro di voler procedere?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (result == DialogResult.Yes)
                            {
                                probRemovedFromList.Add(itemToRemove);
                                probList.Remove(itemToRemove);
                                //se sono nel caso di modifica dell'RMA, rimuovo l'elemento anche dal DB;
                                if (rma_fromForm != null)
                                {
                                    conn.CreateCommand("DELETE FROM [Autotest].[dbo].[RMA_EsitiTestPreAnalisi] " +
                                    "WHERE [Autotest].[dbo].[RMA_EsitiTestPreAnalisi].[idRMA] = '" + itemToRemove.idRMA + "' AND [Autotest].[dbo].[RMA_EsitiTestPreAnalisi].[idProblema] ='" + itemToRemove.idProblema + "'", null);
                                    conn.CreateCommand("DELETE FROM [Autotest].[dbo].[RMA_Problema] " +
                                    "WHERE [Autotest].[dbo].[RMA_Problema].[idRMA] = '" + itemToRemove.idRMA + "' AND [Autotest].[dbo].[RMA_Problema].[idProblema] ='" + itemToRemove.idProblema + "'", null);
                                }
                            }
                        }
                    }
                }
                btn_RefreshProbl_Click(sender, e);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_EditProbl_Click(object sender, EventArgs e)
        {
            try
            {
                int index = 0;//indice usato per determinare quale elemento della lista modificare
                probList.Reverse();//inverto l'ordine degli elementi della lista perchè list.Add aggiunge in Append, mentre nella datagridview aggiungo in testa...
                List<RMA_Problema> itemToEdit = new List<RMA_Problema>();
                foreach (DataGridViewRow row in dgv_ListaProblemiRMA.Rows)
                {
                    if (row.Cells.GetCellValueFromColumnHeader("").Equals(true))
                    {
                        index = row.Index;
                        itemToEdit.Add(probList.Single(r => r.idProblema.Equals(row.Cells.GetCellValueFromColumnHeader("ID PROBLEMA"))));
                    }
                }
                if (itemToEdit != null && itemToEdit.Count.Equals(1))
                {
                    using (var form = new Form_Problema(lbl_NumRMA.Text, numFlowChart, difList, false, itemToEdit[0]))
                    {
                        var result = form.ShowDialog();
                        if (result == DialogResult.OK)
                        {
                            probList[index] = form.problema;
                        }
                    }
                    probList.Reverse();
                    btn_RefreshProbl_Click(sender, e);
                }
                else if (itemToEdit.Count >= 2)
                    MessageBox.Show("Non è possibile modificare più di un problema per volta", "Informazione", MessageBoxButtons.OK, MessageBoxIcon.Information);
                else
                    MessageBox.Show("Selezionare un problema da modificare", "Informazione", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_editTemplateProbDB_Click(object sender, EventArgs e)
        {
            try
            {
                using (var form = new Form_ListaTemplatesDB(1))
                {
                    var result = form.ShowDialog();
                    if (result == DialogResult.OK)
                    {
                        if (form.problemsList != null && form.problemsList.Count.Equals(1))
                        {
                            using (var innerForm = new Form_Problema("", null, difList, true, form.problemsList[0]))
                            {
                                var innerResult = innerForm.ShowDialog();
                                if (innerResult == DialogResult.OK)
                                {
                                    if (innerForm.problema != null)
                                        probList.Add(innerForm.problema);
                                }
                            }
                            probList.Reverse();
                            btn_RefreshProbl_Click(sender, e);
                        }
                    }
                }
                btn_RefreshProbl_Click(null, null);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cmb_anniVitaCollimatore_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                cmb_statoGaranzia.Text = "";
                logicaDeterminazioneGaranzia();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cmb_respDannoCollimatore_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                logicaDeterminazioneGaranzia();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void logicaDeterminazioneGaranzia()
        {
            try
            {
                switch (cmb_anniVitaCollimatore.SelectedIndex)
                {
                    case 0://- di 2
                        switch (cmb_respDannoCollimatore.SelectedIndex)
                        {
                            case 0://cliente
                                cmb_statoGaranzia.SelectedIndex = 1; //Fuori Garanzia
                                break;
                            case 1://Ralco
                                cmb_statoGaranzia.SelectedIndex = 0; //In Garanzia
                                break;
                        }
                        break;
                    case 1://+ di 2
                        cmb_statoGaranzia.SelectedIndex = 1;//Fuori Garanzia
                        break;
                }
                if (cmb_statoGaranzia.SelectedIndex.Equals(1))
                    cmb_statoAzCorr.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

#endregion

#region Fasi Da Rieseguire

        private void dgv_FasiDaRieseguire_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                Utils.DataGridView_CellContentClick(dgv_FasiDaRieseguire, 2);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void refreshTableFasiDaRieseguire()
        {
            try
            {
                dgv_FasiDaRieseguire.Rows.Clear();
                if (minFasiDaRiesList.minFaseProd == null || minFasiDaRiesList.minFaseProd.Count.Equals(0))
                {
                    foreach (String s in nomeFaseDaRieseguire)
                        dgv_FasiDaRieseguire.Rows.Insert(0, 0, s, "0");
                }
                else
                {
                    int idx = 0;
                    foreach (int m in minFasiDaRiesList.minFaseProd)
                    {
                        bool isChecked = false;
                        if (!m.ToString().Equals("0"))
                            isChecked = true;
                        dgv_FasiDaRieseguire.Rows.Insert(0, isChecked, nomeFaseDaRieseguire[idx].ToString(), m.ToString());
                        idx++;
                    }
                }
                dgv_FasiDaRieseguire.Refresh();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

#endregion

#region Test Da Eseguire

        private void initializeTableTestDaEseguire()
        {
            try
            {
                dgv_testDaEseguire.Rows.Clear();
                nomeTestDaEseguire = new List<String>();
                nomeTestDaEseguire = (List<String>)conn.CreateCommand("SELECT [descrizioneTest] FROM [Autotest].[dbo].[RMA_ElencoTest]", nomeTestDaEseguire);
                nomeTestDaEseguire.Reverse();
                foreach (String s in nomeTestDaEseguire)
                    dgv_testDaEseguire.Rows.Insert(0, false, s);
                testDaEseguireList = (List<RMA_TestDaEseguire>)conn.CreateCommand("SELECT * FROM [Autotest].[dbo].[RMA_TestDaEseguire] WHERE [Autotest].[dbo].[RMA_TestDaEseguire].[idRMA] = '" + lbl_NumRMA.Text + "'", testDaEseguireList);
                if (testDaEseguireList != null)
                {
                    foreach (DataGridViewRow r in dgv_testDaEseguire.Rows)
                        foreach (RMA_TestDaEseguire t in testDaEseguireList)
                            if (t.descTestDaEseguire.Equals(r.Cells[1].Value))
                                r.Cells[0].Value = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

#endregion

#region Riparazioni

        private void dgv_ListaRiparazRMA_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                Utils.DataGridView_CellContentClick(dgv_ListaRiparazRMA, 1);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /*NOTA : il campo ID RIPARAZIONE è nascosto, ed usato solo per selezionare la riparazione corretta mediante il proprio ID dalla lista ripList in fase di modifica / delete / clone*/
        private void btn_RefreshRiparazione_Click(object sender, EventArgs e)
        {
            try
            {
                dgv_ListaRiparazRMA.Rows.Clear();
                if (ripList != null)
                {
                    foreach (RMA_Riparazione r in ripList)
                    {
                        dgv_ListaRiparazRMA.Rows.Insert(0, false, r.descRip, r.idParte + " " + r.descParte, r.quantita, r.statoRip, r.noteRip, r.dataAggRip, r.idRiparazione);
                    }
                    dgv_ListaRiparazRMA.Refresh();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_AddRiparazione_Click(object sender, EventArgs e)
        {
            try
            {
                addRiparazione(false);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_addTemplateRipDB_Click(object sender, EventArgs e)
        {
            try
            {
                addRiparazione(true);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void addRiparazione (bool isTemplateDBRiparazione)
        {
            try
            {
                if (collList != null)
                {
                    if (!collList.Count.Equals(0))
                    {
                        RMA_Riparazione r = null;
                        using (var form = new Form_Riparazione(lbl_NumRMA.Text, isTemplateDBRiparazione, r))
                        {
                            var result = form.ShowDialog();
                            if (result == DialogResult.OK)
                            {
                                if (form.riparazione != null)
                                {
                                    ripList.Add(form.riparazione);
                                }
                            }
                        }
                        btn_RefreshRiparazione_Click(null, null);
                    }
                    else
                        MessageBox.Show("Inserire almeno un collimatore prima di procedere", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else if (partList != null)
                {
                    if (!partList.Count.Equals(0))
                    {
                        RMA_Riparazione r = null;
                        using (var form = new Form_Riparazione(lbl_NumRMA.Text, isTemplateDBRiparazione, r))
                        {
                            var result = form.ShowDialog();
                            if (result == DialogResult.OK)
                            {
                                if (form.riparazione != null)
                                    ripList.Add(form.riparazione);
                            }
                        }
                        btn_RefreshRiparazione_Click(null, null);
                    }
                    else
                        MessageBox.Show("Inserire almeno una parte prima di procedere", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_importRipFromTemplateDB_Click(object sender, EventArgs e)
        {
            try
            {
                using (var form = new Form_ListaTemplatesDB(2))
                {
                    var result = form.ShowDialog();
                    if (result == DialogResult.OK)
                    {
                        if (form.repairsList != null && form.repairsList.Count.Equals(1))
                        {
                            using (var innerForm = new Form_Riparazione(lbl_NumRMA.Text, false, form.repairsList[0]))
                            {
                                var innerResult = innerForm.ShowDialog();
                                if (innerResult == DialogResult.OK)
                                {
                                    if (innerForm.riparazione != null)
                                        ripList.Add(innerForm.riparazione);
                                }
                            }
                            ripList.Reverse();
                            btn_RefreshRiparazione_Click(sender, e);
                        }
                    }
                }
                btn_RefreshRiparazione_Click(null, null);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_CloneRiparazione_Click(object sender, EventArgs e)
        {
            try
            {
                List<RMA_Riparazione> itemToClone = new List<RMA_Riparazione>();
                foreach (DataGridViewRow row in dgv_ListaRiparazRMA.Rows)
                {
                    if (row.Cells.GetCellValueFromColumnHeader("").Equals(true))
                    {
                        itemToClone.Add(ripList.Single(r => r.idRiparazione.Equals(row.Cells.GetCellValueFromColumnHeader("ID RIPARAZIONE"))));
                    }
                }
                if (itemToClone != null && itemToClone.Count.Equals(1))
                {
                    itemToClone[0].idRiparazione = 0;
                    using (var form = new Form_Riparazione(lbl_NumRMA.Text, false, itemToClone[0]))
                    {
                        var result = form.ShowDialog();
                        if (result == DialogResult.OK)
                        {
                            ripList.Add(form.riparazione);
                        }
                    }
                    btn_RefreshRiparazione_Click(sender, e);
                }
                else if (itemToClone.Count >= 2)
                    MessageBox.Show("Non è possibile clonare più di una riparazione per volta", "Informazione", MessageBoxButtons.OK, MessageBoxIcon.Information);
                else
                    MessageBox.Show("Selezionare una riparazione da clonare", "Informazione", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_DeleteRiparazione_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow row in dgv_ListaRiparazRMA.Rows)
                {
                    if (row.Cells["chk_Rip"].Value != null && row.Cells["chk_Rip"].Value.Equals(true))
                    {
                        var itemToRemove = ripList.Single(r => r.idRiparazione.Equals(row.Cells.GetCellValueFromColumnHeader("ID RIPARAZIONE")));
                        if (itemToRemove != null)
                        {
                            var result = MessageBox.Show("L'elemento selezionato verrà rimosso definitivamente. Sei sicuro di voler procedere?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (result == DialogResult.Yes)
                            {
                                ripRemovedFromList.Add(itemToRemove);
                                ripList.Remove(itemToRemove);
                                //se sono nel caso di modifica dell'RMA, rimuovo l'elemento anche dal DB;
                                if (rma_fromForm != null)
                                {
                                    try
                                    {
                                        conn.CreateCommand("DELETE FROM [Autotest].[dbo].[RMA_Riparazione] " +
                                        "WHERE [Autotest].[dbo].[RMA_Riparazione].[idRMA] = '" + itemToRemove.idRMA + "' AND [Autotest].[dbo].[RMA_Riparazione].[idRiparazione] ='" + itemToRemove.idRiparazione + "'", null);
                                        #if USEPANTHERA
                                        //cancello i dati dalla vista RMA_RIG_MAT di Panthera
                                        conn.CreateCommand("DELETE FROM[Autotest].[dbo].[RMA_RIG_MAT] " +
                                        "WHERE ([Autotest].[dbo].[RMA_RIG_MAT].[ID_AZIENDA] = '001' AND [Autotest].[dbo].[RMA_RIG_MAT].[ID_RMA] = '" + itemToRemove.idRMA.Replace('/', '_') + "' AND [Autotest].[dbo].[RMA_RIG_MAT].[ID_MATERIALE] = '" + itemToRemove.idParte + "')", null);
                                        #endif
                                    }
                                    catch (Exception ex)
                                    {
                                        MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    }
                                }
                            }
                        }
                    }
                }
                btn_RefreshRiparazione_Click(sender, e);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_EditRiparazione_Click(object sender, EventArgs e)
        {
            try
            {
                int index = 0;//indice usato per determinare quale elemento della lista modificare
                ripList.Reverse();//inverto l'ordine degli elementi della lista perchè list.Add aggiunge in Append, mentre nella datagridview aggiungo in testa...
                List<RMA_Riparazione> itemToEdit = new List<RMA_Riparazione>();
                foreach (DataGridViewRow row in dgv_ListaRiparazRMA.Rows)
                {
                    if (row.Cells.GetCellValueFromColumnHeader("").Equals(true))
                    {
                        index = row.Index;
                        itemToEdit.Add(ripList.Single(r => r.idRiparazione.Equals(row.Cells.GetCellValueFromColumnHeader("ID RIPARAZIONE"))));
                    }
                }
                if (itemToEdit != null && itemToEdit.Count.Equals(1))
                {
                    using (var form = new Form_Riparazione(lbl_NumRMA.Text, false, itemToEdit[0]))
                    {
                        var result = form.ShowDialog();
                        if (result == DialogResult.OK)
                        {
                            ripList[index] = form.riparazione;
                        }
                    }
                    ripList.Reverse();
                    btn_RefreshRiparazione_Click(sender, e);
                }
                else if (itemToEdit.Count >= 2)
                    MessageBox.Show("Non è possibile modificare più di una riparazione per volta", "Informazione", MessageBoxButtons.OK, MessageBoxIcon.Information);
                else
                    MessageBox.Show("Selezionare una riparazione da modificare", "Informazione", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_editTemplateRipDB_Click(object sender, EventArgs e)
        {
            try
            {
                using (var form = new Form_ListaTemplatesDB(2))
                {
                    var result = form.ShowDialog();
                    if (result == DialogResult.OK)
                    {
                        if (form.repairsList != null && form.repairsList.Count.Equals(1))
                        {
                            using (var innerForm = new Form_Riparazione("", true, form.repairsList[0]))
                            {
                                var innerResult = innerForm.ShowDialog();
                                if (innerResult == DialogResult.OK)
                                {
                                    if (innerForm.riparazione != null)
                                        ripList.Add(innerForm.riparazione);
                                }
                            }
                            ripList.Reverse();
                            btn_RefreshRiparazione_Click(sender, e);
                        }
                    }
                }
                btn_RefreshRiparazione_Click(null, null);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_printRiparazioni_Click(object sender, EventArgs e)
        {
            try
            {
                string path = @"\\ralcoserver01\Testi\1 QUALITA' ITALIA-USA\MODULI_FORMS\MGTQ007-3 COLLIMATORE NON CONFORME PRODUZIONE\MGTQ007-3_INFO_RIPARAZIONE.docx";
                var doc = DocX.Load(path);
                var table = doc.Tables[0];
                int actualRow = 0;
                //Sezione informazioni generali
                doc.ReplaceText("@IDRMA@", rmaGen.idRMA);
                //doc.ReplaceText("@TYPE@", rmaGen.tipoRMA);
                doc.ReplaceText("@OWNER@", Globals.user.getFullUserName());
                doc.ReplaceText("@DATE@", rmaGen.dataCreazione.ToShortDateString());
                //doc.ReplaceText("@STATUS@", rmaGen.stato);
                doc.ReplaceText("@CUSTOMER@", infoCli.cliente.nomeCliente.ToString());//in una lista di collimatori ci sono solo collimatori dello stesso tipo
                if (collList != null)
                    doc.ReplaceText("@MODEL@", collList[0].modelloColl.ToString());//in una lista di collimatori ci sono solo collimatori dello stesso tipo
                else if (partList != null)
                {
                    List<String> idPartiList = partList.Select(o => o.idParte).Distinct().ToList();
                    string idParti = "";
                    if (idPartiList != null)
                        foreach (string s in idPartiList)
                            if (idParti == "")
                                idParti = idParti + s;
                            else
                                idParti = idParti + "\n" + s;
                    doc.ReplaceText("@MODEL@", idParti);//in una lista di parti ci possono essere parti anche di tipo differente
                }
                doc.ReplaceText("@CUSCOLCODE@", infoCli.codCollCli);
                List<String> dataString = new List<String>();
                dataString = getSerialiEFlowChart();
                string seriali = dataString[0];
                string flowChart = dataString[1];
                doc.ReplaceText("@SERIALS@", seriali);
                doc.ReplaceText("@FLOW@", flowChart);
                if (collList != null)
                    doc.ReplaceText("@TOTAL@", collList.Count.ToString());
                else if (partList != null)
                    doc.ReplaceText("@TOTAL@", partList.Count.ToString());
                doc.ReplaceText("@GENNOTES@", rmaGen.note);
                actualRow = 8;
                int totLineeRiparazione = 1;
                if (ripList != null)
                {
                    if (ripList.Count >= 1)
                    {
                        for (int j = 1; j < ripList.Count; j++)
                        {
                            for (int i = actualRow, delta = 0; i < actualRow + totLineeRiparazione; i++, delta++)
                            {
                                table.InsertRow(table.Rows[i], actualRow + totLineeRiparazione + delta);
                            }
                        }
                    }
                    //riempio le righe nella tabella con i dati opportuni
                    for (int j = 0, delta = 0; j < ripList.Count; j++, delta = delta + totLineeRiparazione)
                    {
                        table.Rows[actualRow + delta].Cells[0].ReplaceText("@REPTYPE@", ripList[j].descRip);
                        table.Rows[actualRow + delta].Cells[1].ReplaceText("@PARTID@", ripList[j].idParte.Trim());
                        table.Rows[actualRow + delta].Cells[2].ReplaceText("@PARTDESC@", ripList[j].descParte.Trim());
                        table.Rows[actualRow + delta].Cells[3].ReplaceText("@QTY@", ripList[j].quantita.ToString());
                        table.Rows[actualRow + delta].Cells[4].ReplaceText("@REPSTAT@", ripList[j].statoRip);
                        table.Rows[actualRow + delta].Cells[5].ReplaceText("@REPNOTES@", ripList[j].noteRip);
                    }
                }
                //salvataggio del file nella cartella temporanea locale
                if (!Directory.Exists(Environment.CurrentDirectory + @"\NonConformita\"))
                    Directory.CreateDirectory(Environment.CurrentDirectory + @"\NonConformita\");
                doc.SaveAs(Environment.CurrentDirectory + @"\NonConformita\infoRiparazione_TMP.docx");
                // Open in Word:
                Process.Start("WINWORD.EXE", Environment.CurrentDirectory + @"\NonConformita\infoRiparazione_TMP.docx");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

#endregion

#region Azioni Supplementari

        private void initializeTableAzioniSupplementari()
        {
            try
            {
                string query = "";
                //faccio il refresh solo se la dgv non contiene elementi, e quindi solo nel caso di inizializzazione...
                if (dgv_azioniSupplementari.Rows.Count == 0)
                {
                    if (collList != null && !collList.Count.Equals(0) && cmb_nomeCliente.SelectedItem != null)
                    {
                        esitiAzioniSupplementariList = (List<RMA_EsitiAzioniSupplementari>)conn.CreateCommand("SELECT * FROM [Autotest].[dbo].[RMA_EsitiAzioniSupplementari] WHERE [Autotest].[dbo].[RMA_EsitiAzioniSupplementari].[idRMA] = '" + lbl_NumRMA.Text + "'", esitiAzioniSupplementariList);
                        //Caso 1 : Collimatore specifico per un cliente specifico
                        //Caso 2 : Collimatore specifico per un cliente qualsiasi
                        //Caso 3 : Collimatore qualsiasi per un cliente specifico
                        //Caso 4 : Collimatore qualsiasi per un cliente qualsiasi
                        query = "SELECT DISTINCT [descrizioneAzioneSuppl] FROM (" +
                                "(SELECT DISTINCT * FROM [Autotest].[dbo].[SM_VISTA_CLI_MOD_AZSUPPL] WHERE [Autotest].[dbo].[SM_VISTA_CLI_MOD_AZSUPPL].[modelloColl] = '" + collList[0].modelloColl + "' AND [Autotest].[dbo].[SM_VISTA_CLI_MOD_AZSUPPL].[nomeCliente] LIKE '%" + cmb_nomeCliente.SelectedItem.ToString() + "%')"
                        + "UNION (SELECT DISTINCT * FROM [Autotest].[dbo].[SM_VISTA_CLI_MOD_AZSUPPL] WHERE[Autotest].[dbo].[SM_VISTA_CLI_MOD_AZSUPPL].[modelloColl] = '" + collList[0].modelloColl + "' AND [Autotest].[dbo].[SM_VISTA_CLI_MOD_AZSUPPL].[nomeCliente] = '*')"
                        + "UNION (SELECT DISTINCT * FROM [Autotest].[dbo].[SM_VISTA_CLI_MOD_AZSUPPL] WHERE[Autotest].[dbo].[SM_VISTA_CLI_MOD_AZSUPPL].[modelloColl] = '*' AND [Autotest].[dbo].[SM_VISTA_CLI_MOD_AZSUPPL].[nomeCliente] LIKE '%" + cmb_nomeCliente.SelectedItem.ToString() + "%')"
                        + "UNION (SELECT DISTINCT * FROM [Autotest].[dbo].[SM_VISTA_CLI_MOD_AZSUPPL] WHERE[Autotest].[dbo].[SM_VISTA_CLI_MOD_AZSUPPL].[modelloColl] = '*' AND [Autotest].[dbo].[SM_VISTA_CLI_MOD_AZSUPPL].[nomeCliente] = '*')) AS TableTemp";
                        dgv_azioniSupplementari.Rows.Clear();
                        List<String> azSupplList = new List<String>();
                        azSupplList = (List<String>)conn.CreateCommand(query, azSupplList);
                        if (azSupplList != null && !azSupplList.Count.Equals(0))
                        {
                            foreach (String az in azSupplList)
                            {
                                dgv_azioniSupplementari.Rows.Insert(0, true, az);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgv_azioniSupplementari_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            try
            {
                if (dgv_azioniSupplementari.CurrentCell.Value != null)
                {
                    if (dgv_azioniSupplementari.CurrentCell.Value.Equals("Aggiungi a Note Trasporto"))
                        if (tb_noteTrasporto.Text.Equals("N/A"))
                            tb_noteTrasporto.Text = dgv_azioniSupplementari.CurrentRow.Cells.GetCellValueFromColumnHeader("Descrizione Azione Supplementare").ToString();
                        else
                            if (!tb_noteTrasporto.Text.Contains(dgv_azioniSupplementari.CurrentRow.Cells.GetCellValueFromColumnHeader("Descrizione Azione Supplementare").ToString()))
                                tb_noteTrasporto.Text = tb_noteTrasporto.Text + "\n" + dgv_azioniSupplementari.CurrentRow.Cells.GetCellValueFromColumnHeader("Descrizione Azione Supplementare").ToString();
                    if (dgv_azioniSupplementari.CurrentCell.Value.Equals("Aggiungi a Note Imballaggio"))
                        if (tb_noteImballaggio.Text.Equals("N/A"))
                            tb_noteImballaggio.Text = dgv_azioniSupplementari.CurrentRow.Cells.GetCellValueFromColumnHeader("Descrizione Azione Supplementare").ToString();
                        else
                            if (!tb_noteImballaggio.Text.Contains(dgv_azioniSupplementari.CurrentRow.Cells.GetCellValueFromColumnHeader("Descrizione Azione Supplementare").ToString()))
                                tb_noteImballaggio.Text = tb_noteImballaggio.Text + "\n" + dgv_azioniSupplementari.CurrentRow.Cells.GetCellValueFromColumnHeader("Descrizione Azione Supplementare").ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_RefreshAzSuppl_Click(object sender, EventArgs e)
        {
            try
            {
                initializeTableAzioniSupplementari();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_EditAzSuppl_Click(object sender, EventArgs e)
        {
            try
            {
                using (var form = new Form_AzioniSupplementari())
                {
                    var result = form.ShowDialog();
                    if (result == DialogResult.OK)
                    {
                        //condizione necessaria per il refresh
                        dgv_azioniSupplementari.Rows.Clear();
                    }
                }
                btn_RefreshAzSuppl_Click(null, null);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_PrintAzSuppl_Click(object sender, EventArgs e)
        {
            try
            {
                string path = @"\\ralcoserver01\Testi\1 QUALITA' ITALIA-USA\MODULI_FORMS\MGTQ014 LISTA PASSAGGI DI RIPARAZIONE PER COLLIMATORI R 915 S DHHS\MGTQ014_AZIONI_SUPPLEMENTARI.docx";
                var doc = DocX.Load(path);
                var table = doc.Tables[0];
                int actualRow = 0;
                //Sezione informazioni generali
                doc.ReplaceText("@IDRMA@", rmaGen.idRMA);
                //doc.ReplaceText("@TYPE@", rmaGen.tipoRMA);
                doc.ReplaceText("@OWNER@", Globals.user.getFullUserName());
                doc.ReplaceText("@DATE@", rmaGen.dataCreazione.ToShortDateString());
                //doc.ReplaceText("@STATUS@", rmaGen.stato);
                doc.ReplaceText("@CUSTOMER@", infoCli.cliente.nomeCliente.ToString());//in una lista di collimatori ci sono solo collimatori dello stesso tipo
                doc.ReplaceText("@MODEL@", collList[0].modelloColl.ToString());//in una lista di collimatori ci sono solo collimatori dello stesso tipo
                doc.ReplaceText("@CUSCOLCODE@", infoCli.codCollCli);
                List<String> dataString = new List<String>();
                dataString = getSerialiEFlowChart();
                string seriali = dataString[0];
                string flowChart = dataString[1];
                doc.ReplaceText("@SERIALS@", seriali);
                doc.ReplaceText("@FLOW@", flowChart);
                doc.ReplaceText("@TOTAL@", collList.Count.ToString());
                actualRow = 7;
                int totLineeAzioniSuppl = 1;
                if (esitiAzioniSupplementariList == null)
                    esitiAzioniSupplementariList = new List<RMA_EsitiAzioniSupplementari>();
                esitiAzioniSupplementariList = (List<RMA_EsitiAzioniSupplementari>)conn.CreateCommand("SELECT * FROM [Autotest].[dbo].[RMA_EsitiAzioniSupplementari] WHERE [Autotest].[dbo].[RMA_EsitiAzioniSupplementari].[idRMA] = '" + lbl_NumRMA.Text + "'", esitiAzioniSupplementariList);
                if (esitiAzioniSupplementariList != null)
                {
                    if (esitiAzioniSupplementariList.Count >= 1)
                    {
                        for (int j = 1; j < esitiAzioniSupplementariList.Count; j++)
                        {
                            for (int i = actualRow, delta = 0; i < actualRow + totLineeAzioniSuppl; i++, delta++)
                            {
                                table.InsertRow(table.Rows[i], actualRow + totLineeAzioniSuppl + delta);
                            }
                        }
                    }
                    //riempio le righe nella tabella con i dati opportuni
                    for (int j = 0, delta = 0; j < esitiAzioniSupplementariList.Count; j++, delta = delta + totLineeAzioniSuppl)
                    {
                        table.Rows[actualRow + delta].Cells[0].ReplaceText("@ID@",(j+1).ToString());
                        table.Rows[actualRow + delta].Cells[1].ReplaceText("@DESCRIPTION@", esitiAzioniSupplementariList[j].descAzSuppl);
                        table.Rows[actualRow + delta].Cells[2].ReplaceText("@OUTCOME@", esitiAzioniSupplementariList[j].esitoAzSuppl);
                    }
                }
                else
                {
                    table.Rows[actualRow].Cells[0].ReplaceText("@ID@", "N/A");
                    table.Rows[actualRow].Cells[1].ReplaceText("@DESCRIPTION@", "N/A");
                    table.Rows[actualRow].Cells[2].ReplaceText("@OUTCOME@", "N/A");
                }
                //salvataggio del file nella cartella temporanea locale
                if (!Directory.Exists(Environment.CurrentDirectory + @"\NonConformita\"))
                    Directory.CreateDirectory(Environment.CurrentDirectory + @"\NonConformita\");
                doc.SaveAs(Environment.CurrentDirectory + @"\NonConformita\azioniSupplementari_TMP.docx");
                // Open in Word:
                Process.Start("WINWORD.EXE", Environment.CurrentDirectory + @"\NonConformita\azioniSupplementari_TMP.docx");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

#endregion

#region Preventivi

        private void dgv_ListaPreventiviRMA_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                Utils.DataGridView_CellContentClick(dgv_ListaPreventiviRMA, 1);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_RefreshPrev_Click(object sender, EventArgs e)
        {
            try
            {
                dgv_ListaPreventiviRMA.Rows.Clear();
                if (prevList != null)
                {
                    foreach (RMA_Preventivo p in prevList)
                    {
                        dgv_ListaPreventiviRMA.Rows.Insert(0, false, p.numPreventivo, p.revisionePrev, p.dataAggPrev, p.totale, p.stato, p.idProgressivo);
                    }
                    dgv_ListaPreventiviRMA.Refresh();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_AddPreventivo_Click(object sender, EventArgs e)
        {
            try
            {
                if (garanziaRalco.statoGaranzia.Equals(""))
                    MessageBox.Show("Determinare la garanzia del collimatore prima di procedere", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                {
                    bool[] conditions = { true, false, false };
                    if (Globals.user.getUserCredential().Equals("NonConfExt") || Globals.user.getUserCredential().Equals("admin") || Globals.user.getUserCredential().Equals("admin"))
                    {
                        if (collList != null)
                        {
                            using (var form = new Form_Preventivo(lbl_NumRMA.Text, null, null, collList, ripList, minFasiDaRiesList, garanziaRalco, conditions))
                            {
                                var result = form.ShowDialog();
                                if (result == DialogResult.OK)
                                {
                                    prevList.Add(form.preventivo);
                                }
                            }
                        }
                        else if (partList != null)
                        {
                            using (var form = new Form_Preventivo(lbl_NumRMA.Text, null, null, partList, ripList, minFasiDaRiesList, garanziaRalco, conditions))
                            {
                                var result = form.ShowDialog();
                                if (result == DialogResult.OK)
                                {
                                    prevList.Add(form.preventivo);
                                }
                            }
                        }
                    }
                    else
                    {
                        if (collList != null)
                        {
                            using (var form = new Form_Preventivo(lbl_NumRMA.Text, null, infoCli.cliente, collList, ripList, minFasiDaRiesList, garanziaRalco, conditions))
                            {
                                var result = form.ShowDialog();
                                if (result == DialogResult.OK)
                                {
                                    prevList.Add(form.preventivo);
                                }
                            }
                        }
                        else if (partList != null)
                        {
                            using (var form = new Form_Preventivo(lbl_NumRMA.Text, null, infoCli.cliente, partList, ripList, minFasiDaRiesList, garanziaRalco, conditions))
                            {
                                var result = form.ShowDialog();
                                if (result == DialogResult.OK)
                                {
                                    prevList.Add(form.preventivo);
                                }
                            }
                        }
                    }
                    btn_RefreshPrev_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_ClonePreventivo_Click(object sender, EventArgs e)
        {
            try
            {
                bool[] conditions = { false, false, true };
                List<RMA_Preventivo> itemToClone = new List<RMA_Preventivo>();
                foreach (DataGridViewRow row in dgv_ListaPreventiviRMA.Rows)
                {
                    if (row.Cells.GetCellValueFromColumnHeader("").Equals(true))
                    {
                        itemToClone.Add(prevList.Single(r => r.idProgressivo.Equals(row.Cells.GetCellValueFromColumnHeader("ID PREVENTIVO"))));
                    }
                }
                if (itemToClone != null && itemToClone.Count.Equals(1))
                {
                    if (Globals.user.getUserCredential().Equals("NonConfInt"))
                        infoCli = null;
                    using (var form = new Form_Preventivo(lbl_NumRMA.Text, itemToClone[0], infoCli.cliente, collList, ripList, minFasiDaRiesList, garanziaRalco, conditions))
                    {
                        var result = form.ShowDialog();
                        if (result == DialogResult.OK)
                        {
                            prevList.Add(form.preventivo);
                        }
                    }
                    btn_RefreshPrev_Click(sender, e);
                }
                else if (itemToClone.Count >= 2)
                    MessageBox.Show("Non è possibile clonare più di un preventivo per volta", "Informazione", MessageBoxButtons.OK, MessageBoxIcon.Information);
                else
                    MessageBox.Show("Selezionare un preventivo da clonare", "Informazione", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_DeletePreventivo_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow row in dgv_ListaPreventiviRMA.Rows)
                {
                    if (row.Cells["chk_Prev"].Value != null && row.Cells["chk_Prev"].Value.Equals(true))
                    {
                        var itemToRemove = prevList.Single(r => r.idProgressivo.Equals(row.Cells.GetCellValueFromColumnHeader("ID PREVENTIVO")));
                        if (itemToRemove != null)
                        {
                            var result = MessageBox.Show("L'elemento selezionato verrà rimosso definitivamente. Sei sicuro di voler procedere?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (result == DialogResult.Yes)
                            {
                                prevRemovedFromList.Add(itemToRemove);
                                prevList.Remove(itemToRemove);
                                //se sono nel caso di modifica dell'RMA, rimuovo l'elemento anche dal DB;
                                if (rma_fromForm != null)
                                {
                                    try
                                    {
                                        conn.CreateCommand("DELETE FROM [Autotest].[dbo].[RMA_Preventivo] " +
                                        "WHERE [Autotest].[dbo].[RMA_Preventivo].[idRMA] = '" + itemToRemove.idRMA + "' AND [Autotest].[dbo].[RMA_Preventivo].[numPreventivo] ='" + itemToRemove.numPreventivo + "' AND  [Autotest].[dbo].[RMA_Preventivo].[revPreventivo] = '" + itemToRemove.revisionePrev + "'", null);
                                        conn.CreateCommand("DELETE FROM [Autotest].[dbo].[RMA_Articoli_Preventivo] " +
                                        "WHERE [Autotest].[dbo].[RMA_Articoli_Preventivo].[idRMA] = '" + itemToRemove.idRMA + "' AND [Autotest].[dbo].[RMA_Articoli_Preventivo].[numPreventivo] ='" + itemToRemove.numPreventivo + "' AND  [Autotest].[dbo].[RMA_Articoli_Preventivo].[revPreventivo] = '" + itemToRemove.revisionePrev + "'", null);
                                    }
                                    catch (Exception ex)
                                    {
                                        MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    }
                                }
                            }
                        }
                    }
                }
                btn_RefreshPrev_Click(sender, e);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_EditPreventivo_Click(object sender, EventArgs e)
        {
            try
            {
                bool[] conditions = { false, true, false };
                int index = 0;//indice usato per determinare quale elemento della lista modificare
                prevList.Reverse();//inverto l'ordine degli elementi della lista perchè list.Add aggiunge in Append, mentre nella datagridview aggiungo in testa...
                List<RMA_Preventivo> itemToEdit = new List<RMA_Preventivo>();
                foreach (DataGridViewRow row in dgv_ListaPreventiviRMA.Rows)
                {
                    if (row.Cells.GetCellValueFromColumnHeader("").Equals(true))
                    {
                        index = row.Index;
                        itemToEdit.Add(prevList.Single(r => r.numPreventivo.Equals(row.Cells.GetCellValueFromColumnHeader("Preventivo")) && r.revisionePrev.Equals(row.Cells.GetCellValueFromColumnHeader("Revisione"))));
                    }
                }
                if (itemToEdit != null && itemToEdit.Count.Equals(1))
                {
                    if (Globals.user.getUserCredential().Equals("NonConfInt"))
                        infoCli = null;
                    using (var form = new Form_Preventivo(lbl_NumRMA.Text, itemToEdit[0], infoCli.cliente, collList, ripList, minFasiDaRiesList, garanziaRalco, conditions))
                    {
                        var result = form.ShowDialog();
                        if (result == DialogResult.OK)
                        {
                            if (!form.preventivo.numPreventivo.Equals(itemToEdit[0].numPreventivo) || !form.preventivo.revisionePrev.Equals(itemToEdit[0].revisionePrev))
                                //prevList[index] = form.preventivo;
                                prevList.Add(form.preventivo);
                            else
                            {
                                prevList[index].dataAggPrev = form.preventivo.dataAggPrev;
                                prevList[index].totale = form.preventivo.totale;
                                prevList[index].stato = form.preventivo.stato;
                            }
                        }
                    }
                    prevList.Reverse();
                    btn_RefreshPrev_Click(sender, e);
                }
                else if (itemToEdit.Count >= 2)
                    MessageBox.Show("Non è possibile modificare più di un preventivo per volta", "Informazione", MessageBoxButtons.OK, MessageBoxIcon.Information);
                else
                    MessageBox.Show("Selezionare un preventivo da modificare", "Informazione", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_printPreventivo_Click(object sender, EventArgs e)
        {
            try
            {
                int index = 0;//indice usato per determinare quale elemento della lista modificare
                prevList.Reverse();//inverto l'ordine degli elementi della lista perchè list.Add aggiunge in Append, mentre nella datagridview aggiungo in testa...
                List<RMA_Preventivo> itemToEdit = new List<RMA_Preventivo>();
                foreach (DataGridViewRow row in dgv_ListaPreventiviRMA.Rows)
                {
                    if (row.Cells.GetCellValueFromColumnHeader("").Equals(true))
                    {
                        index = row.Index;
                        itemToEdit.Add(prevList.Single(r => r.idProgressivo.Equals(row.Cells.GetCellValueFromColumnHeader("ID PREVENTIVO"))));
                    }
                }
                if (itemToEdit != null && itemToEdit.Count.Equals(1))
                {
                    try
                    {
                        string path = "";
                        if (cmb_PrevPrezzi.SelectedItem.Equals("Con Prezzi"))
                            path = @"\\ralcoserver01\Testi\1 QUALITA' ITALIA-USA\MODULI_FORMS\MGTQ007-4 PREVENTIVO RIPARAZIONE\MGTQ007-4_PreventivoRiparazione_ConPrezzi.docx";
                        else if (cmb_PrevPrezzi.SelectedItem.Equals("Senza Prezzi"))
                            path = @"\\ralcoserver01\Testi\1 QUALITA' ITALIA-USA\MODULI_FORMS\MGTQ007-4 PREVENTIVO RIPARAZIONE\MGTQ007-4_PreventivoRiparazione_SenzaPrezzi.docx";
                        var doc = DocX.Load(path);
                        //Sezione informazioni generali
                        doc.ReplaceText("@NBPR@", lbl_NumRMA.Text.Replace("CNC/", "")); //Perchè vogliono che il numero preventivo coincida con il CNC a cui il preventivo è associato, a meno della notazione CNC/
                                                                                        //if (itemToEdit[0].revisionePrev.Equals("0"))
                                                                                        //    doc.ReplaceText("@RVPR@","");
                                                                                        //else
                                                                                        //    doc.ReplaceText("@RVPR@", itemToEdit[0].revisionePrev.ToString());
                        doc.ReplaceText("@RVPR@", "PREV N° " + itemToEdit[0].numPreventivo.ToString() + "   REV  " + itemToEdit[0].revisionePrev.ToString());
                        doc.ReplaceText("@OPRT@", itemToEdit[0].nomeTecnicoRalco.ToString());
                        doc.ReplaceText("@DEPR@", itemToEdit[0].dataAggPrev.ToString("dd/MM/yyyy"));
                        if (collList != null)
                        {
                            doc.ReplaceText("@MCOL@", collList[0].modelloColl);
                            doc.ReplaceText("@CCOL@", collList[0].codCollCliente);
                        }
                        else if (partList != null)
                        {
                            doc.ReplaceText("@MCOL@", partList[0].descParte);
                            doc.ReplaceText("@CCOL@", partList[0].idParte);
                        }
                        List<String> dataString = new List<String>();
                        dataString = getSerialiEFlowChart();
                        string seriali = dataString[0];
                        string flowChart = dataString[1];
                        doc.ReplaceText("@SCOL@", seriali);
                        doc.ReplaceText("@GRNZ@", cmb_statoGaranzia.Text);
                        doc.ReplaceText("@CUST@", cmb_nomeCliente.SelectedItem.ToString());
                        doc.ReplaceText("@SCAR@", tb_scarCliente.Text);
                        doc.ReplaceText("@NSCAR@", tb_numScarCLI.Text);
                        doc.ReplaceText("@ORRP@", tb_ordRiparaz.Text);
                        //sezione parti sostituite
                        List<RMA_Articolo> artList = new List<RMA_Articolo>();
                        artList = (List<RMA_Articolo>)conn.CreateCommand("SELECT * FROM [Autotest].[dbo].[RMA_Articoli_Preventivo] WHERE [Autotest].[dbo].[RMA_Articoli_Preventivo].[idRMA] = '" + itemToEdit[0].idRMA + "' AND [Autotest].[dbo].[RMA_Articoli_Preventivo].[numPreventivo] = " + itemToEdit[0].numPreventivo + " AND [Autotest].[dbo].[RMA_Articoli_Preventivo].[revPreventivo] = '" + itemToEdit[0].revisionePrev + "'", artList);
                        var table = doc.Tables[1];
                        foreach (RMA_Articolo a in artList)
                        {
                            table.InsertRow();
                            a.addArticleEnglishDescription();
                            table.Rows[table.RowCount - 1].Cells[0].InsertParagraph(a.idArticolo);
                            table.Rows[table.RowCount - 1].Cells[1].InsertParagraph(a.quantita.ToString());
                            table.Rows[table.RowCount - 1].Cells[2].InsertParagraph(a.descrizione);
                            if (cmb_PrevPrezzi.SelectedItem.Equals("Con Prezzi"))
                                table.Rows[table.RowCount - 1].Cells[3].InsertParagraph(Decimal.Round(a.quantita * a.prezzoUnitario * COEFF_MOLT, 2).ToString());
                        }
                        doc.ReplaceText("@TTPR@", Decimal.Round(itemToEdit[0].totale, 2).ToString());
                        //firma digitalizzata
                        string[] nomeFileFirma = new string[] { Globals.user.getFullUserName().ToLower(), Globals.user.getReverseFullUserName().ToLower() };
                        string[] listaFirmeDigitali = Directory.GetFiles(@"\\ralcosrv2\Public\Firme");
                        //faccio i lowercase della dei nomi dei file delle firme
                        listaFirmeDigitali = listaFirmeDigitali.Select(f => f.ToLowerInvariant()).ToArray();
                        string filePathFirma = "";
                        for (int j = 0; j < nomeFileFirma.Length; j++)
                        {
                            for (int i = 0; i < listaFirmeDigitali.Length; i++)
                            {
                                if (listaFirmeDigitali[i].Contains(nomeFileFirma[j]))
                                {
                                    filePathFirma = filePathFirma + listaFirmeDigitali[i];
                                    break;
                                }
                            }
                            if (!filePathFirma.Equals(""))
                                break;
                        }
                        Xceed.Words.NET.Image img;
                        if (!filePathFirma.Equals(""))//ho trovato esattamente l'elemento che mi interessava (il path può essere uno solo)
                        {
                            //cerco nella cartella delle firme digitali quella che contiene il nome operatore
                            img = doc.AddImage(filePathFirma);
                            //img = doc.AddImage(AppDomain.CurrentDomain.BaseDirectory + "\\Templates\\FirmeDigitali\\" + nomeFileFirma + ".jpg");
                            Xceed.Words.NET.Picture picture = img.CreatePicture();
                            Xceed.Words.NET.Paragraph p1 = doc.InsertParagraph();
                            p1.AppendPicture(picture);
                            p1.Alignment = Alignment.right;
                        }
                        //salvataggio del file
                        if (!Directory.Exists(Environment.CurrentDirectory + @"\Preventivi\"))
                            Directory.CreateDirectory(Environment.CurrentDirectory + @"\Preventivi\");
                        doc.SaveAs(Environment.CurrentDirectory + @"\Preventivi\preventivo_TMP.docx");
                        Process.Start("WINWORD.EXE", Environment.CurrentDirectory + @"\Preventivi\preventivo_TMP.docx");
                        //MessageBox.Show("Preventivo salvato correttamente", "Informazione", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else if (itemToEdit.Count >= 2)
                    MessageBox.Show("Non è possibile stampare più di un preventivo per volta", "Informazione", MessageBoxButtons.OK, MessageBoxIcon.Information);
                else
                    MessageBox.Show("Selezionare un preventivo da stampare", "Informazione", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

#endregion

#region Note RMA

        private void dgv_noteRMA_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                Utils.DataGridView_CellContentClick(dgv_noteRMA, 1);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void refreshTableLogRMA()
        {
            try
            {
                logList = new List<RMA_Log>();
                logList = (List<RMA_Log>)conn.CreateCommand("SELECT * FROM [Autotest].[dbo].[RMA_Log] WHERE [Autotest].[dbo].[RMA_Log].[idRMA] = '" + lbl_NumRMA.Text + "'", logList);
                if (logList != null)
                {
                    dgv_noteRMA.Rows.Clear();
                    foreach (RMA_Log l in logList)
                    {
                        dgv_noteRMA.Rows.Insert(0, false, l.dataAggiornamento, l.aggiornatoDa, l.stato, l.note, l.idProgressivo);
                    }
                    dgv_noteRMA.Refresh();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_RefreshNoteRMA_Click(object sender, EventArgs e)
        {
            try
            {
                refreshTableLogRMA();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_AddNoteRMA_Click(object sender, EventArgs e)
        {
            try
            {
                using (var form = new Form_Log(lbl_NumRMA.Text, null))
                {
                    var result = form.ShowDialog();
                    if (result == DialogResult.OK)
                    {
                        if (logList == null)
                            logList = new List<RMA_Log>();
                        logList.Add(form.log);
                    }
                    btn_RefreshNoteRMA_Click(null, null);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_DeleteNoteRMA_Click(object sender, EventArgs e)
        {
            try
            {
                List<RMA_Log> itemsToRemove = new List<RMA_Log>();
                foreach (DataGridViewRow row in dgv_noteRMA.Rows)
                {
                    if (row.Cells.GetCellValueFromColumnHeader("").Equals(true))
                    {
                        DialogResult res = MessageBox.Show("Questa operazione rimuoverà definitivamente la nota relativa all'RMA selezionato dal sistema.\nSei sicuro di voler procedere?", "Error", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (res.Equals(DialogResult.Yes))
                        {
                            itemsToRemove.Add(logList.Single(r => r.idProgressivo.Equals(row.Cells.GetCellValueFromColumnHeader("ID NOTE"))));
                            foreach (var v in itemsToRemove)
                                if (logList.Contains(v))
                                {
                                    Object retVal = null;
                                    retVal = (Object)conn.CreateCommand("DELETE FROM [AutoTest].[dbo].[RMA_Log] WHERE [AutoTest].[dbo].[RMA_Log].[idRMA] = '" + lbl_NumRMA.Text + "' AND [AutoTest].[dbo].[RMA_Log].[dataAggiornamento] LIKE '%" + v.dataAggiornamento.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "%'", retVal);
                                    logList.Remove(v);
                                }
                            refreshTableLogRMA();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_EditNoteRMA_Click(object sender, EventArgs e)
        {
            try
            {
                int index = 0;//indice usato per determinare quale elemento della lista modificare
                logList.Reverse();//inverto l'ordine degli elementi della lista perchè list.Add aggiunge in Append, mentre nella datagridview aggiungo in testa...
                List<RMA_Log> itemToEdit = new List<RMA_Log>();
                foreach (DataGridViewRow row in dgv_noteRMA.Rows)
                {
                    if (row.Cells.GetCellValueFromColumnHeader("").Equals(true))
                    {
                        index = row.Index;
                        itemToEdit.Add(logList.Single(r => r.idProgressivo.Equals(row.Cells.GetCellValueFromColumnHeader("ID NOTE"))));
                    }
                }
                if (itemToEdit != null && itemToEdit.Count.Equals(1))
                {
                    using (var form = new Form_Log(lbl_NumRMA.Text, itemToEdit[0]))
                    {
                        var result = form.ShowDialog();
                        if (result == DialogResult.OK)
                        {
                            logList.RemoveAt(index);
                            logList.Add(new RMA_Log(form.log));
                        }
                    }
                    logList.Reverse();
                    btn_RefreshNoteRMA_Click(null, null);
                }
                else if (itemToEdit.Count >= 2)
                    MessageBox.Show("Non è possibile modificare più di una nota per volta", "Informazione", MessageBoxButtons.OK, MessageBoxIcon.Information);
                else
                    MessageBox.Show("Selezionare una nota da modificare", "Informazione", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_PrintGeneralInfo_Click(object sender, EventArgs e)
        {
            try
            {
                string path = @"\\ralcoserver01\Testi\1 QUALITA' ITALIA-USA\MODULI_FORMS\MGTQ007-3 COLLIMATORE NON CONFORME PRODUZIONE\MGTQ007-3_INFO_GENERICHE_CNC.docx";
                var doc = DocX.Load(path);
                var table = doc.Tables[0];
                int actualRow = 0;
                //Sezione informazioni generali
                doc.ReplaceText("@IDRMA@", rmaGen.idRMA);
                //doc.ReplaceText("@TYPE@", rmaGen.tipoRMA);
                doc.ReplaceText("@OWNER@", Globals.user.getFullUserName());
                doc.ReplaceText("@DATE@", rmaGen.dataCreazione.ToShortDateString());
                doc.ReplaceText("@GENNOTES@", rmaGen.note);
                //Sezione Informativa Cliente
                doc.ReplaceText("@CUSTOMER@", infoCli.cliente.nomeCliente.ToString());//in una lista di collimatori ci sono solo collimatori dello stesso tipo
                if (collList != null)
                    doc.ReplaceText("@MODEL@", collList[0].modelloColl.ToString());//in una lista di collimatori ci sono solo collimatori dello stesso tipo
                else if (partList != null)
                {
                    List<String> idPartiList = partList.Select(o => o.idParte).Distinct().ToList();
                    string idParti = "";
                    if (idPartiList != null)
                        foreach (string s in idPartiList)
                            if (idParti == "")
                                idParti = idParti + s;
                            else
                                idParti = idParti + "\n" + s;
                    doc.ReplaceText("@MODEL@", idParti);//in una lista di parti ci possono essere parti anche di tipo differente
                }
                List<String> dataString = new List<String>();
                dataString = getSerialiEFlowChart();
                string seriali = dataString[0];
                string flowChart = dataString[1];
                doc.ReplaceText("@SERIALS@", seriali);
                doc.ReplaceText("@FLOW@", flowChart);
                if (collList != null)
                    doc.ReplaceText("@TOTAL@", collList.Count.ToString());
                else if (partList != null)
                    doc.ReplaceText("@TOTAL@", partList.Count.ToString());
                if (infoCli != null)
                {
                    if (infoCli.dataArrivoCollim.ToShortDateString() != null && !infoCli.dataArrivoCollim.ToShortDateString().Equals("01/01/1753"))
                        doc.ReplaceText("@ARRDATE@", infoCli.dataArrivoCollim.ToShortDateString());
                    else
                        doc.ReplaceText("@ARRDATE@", "");
                    if (infoCli.dataScadenzaRMA.ToShortDateString() != null && !infoCli.dataScadenzaRMA.ToShortDateString().Equals("01/01/1753"))
                        doc.ReplaceText("@RMAEXPDATE@", infoCli.dataScadenzaRMA.ToShortDateString());
                    else
                        doc.ReplaceText("@RMAEXPDATE@", "");
                    if (infoCli.decisioneCliente != null)
                        doc.ReplaceText("@CUSTDEC@", infoCli.decisioneCliente);
                    else
                        doc.ReplaceText("@CUSTDEC@", "");
                    if (infoCli.codCollCli != null)
                        doc.ReplaceText("@CUSCOLCODE@", infoCli.codCollCli);
                    else
                        doc.ReplaceText("@CUSCOLCODE@", "");
                    if (infoCli.SCARCliente != null)
                        doc.ReplaceText("@CUSTSCAR@", infoCli.SCARCliente);
                    else
                        doc.ReplaceText("@CUSTSCAR@", "");
                    if (infoCli.numScarCli != null)
                        doc.ReplaceText("@CUSTSCARNR@", infoCli.numScarCli);
                    else
                        doc.ReplaceText("@CUSTSCARNR@", "");
                    if (infoCli.ordRiparazione != null)
                        doc.ReplaceText("@REPORD@", infoCli.ordRiparazione);
                    else
                        doc.ReplaceText("@REPORD@", "");
                    if (infoCli.dataOrdineRip.ToShortDateString() != null && !infoCli.dataOrdineRip.ToShortDateString().Equals("01/01/1753"))
                        doc.ReplaceText("@REPORDATE@", infoCli.dataOrdineRip.ToShortDateString());
                    else
                        doc.ReplaceText("@REPORDATE@", "");
                    if (infoCli.docReso != null)
                        doc.ReplaceText("@DELNOTE@", infoCli.docReso);
                    else
                        doc.ReplaceText("@DELNOTE@", "");
                    if (infoCli.dataDocReso.ToShortDateString() != null && !infoCli.dataDocReso.ToShortDateString().Equals("01/01/1753"))
                        doc.ReplaceText("@DELNOTEDATE@", infoCli.dataDocReso.ToShortDateString());
                    else
                        doc.ReplaceText("@DELNOTEDATE@", "");
                    if (infoCli.noteCliente != null)
                        doc.ReplaceText("@CLINOTES@", infoCli.noteCliente);
                    else
                        doc.ReplaceText("@CLINOTES@", "");
                }
                //Sezione Garanzia
                if (garanziaRalco != null)
                {
                    if (garanziaRalco.provCollimatore != null)
                        doc.ReplaceText("@COLLPROV@", garanziaRalco.provCollimatore);
                    else
                        doc.ReplaceText("@COLLPROV@", "");
                    if (garanziaRalco.anniVitaCollimatore != null)
                        doc.ReplaceText("@COLLNUMYEARS@", garanziaRalco.anniVitaCollimatore);
                    else
                        doc.ReplaceText("@COLLNUMYEARS@", "");
                    if (garanziaRalco.respDannoCollimatore != null)
                        doc.ReplaceText("@DMGRESP@", garanziaRalco.respDannoCollimatore);
                    else
                        doc.ReplaceText("@DMGRESP@", "");
                    if (garanziaRalco.azioniSupplRalco != null)
                        doc.ReplaceText("@ADDACTS@", garanziaRalco.azioniSupplRalco);
                    else
                        doc.ReplaceText("@ADDACTS@", "");
                    if (garanziaRalco.statoGaranzia != null)
                        doc.ReplaceText("@WARRSTAT@", garanziaRalco.statoGaranzia);
                    else
                        doc.ReplaceText("@WARRSTAT@", "");
                    if (garanziaRalco.noteGaranziaRalco != null)
                        doc.ReplaceText("@WARRNOTES@", garanziaRalco.noteGaranziaRalco);
                    else
                        doc.ReplaceText("@WARRNOTES@", "");
                }
                //salvataggio del file nella cartella temporanea locale
                if (!Directory.Exists(Environment.CurrentDirectory + @"\NonConformita\"))
                    Directory.CreateDirectory(Environment.CurrentDirectory + @"\NonConformita\");
                doc.SaveAs(Environment.CurrentDirectory + @"\NonConformita\infoGeneriche_TMP.docx");
                // Open in Word:
                Process.Start("WINWORD.EXE", Environment.CurrentDirectory + @"\NonConformita\infoGeneriche_TMP.docx");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        #endregion

        private void Cb_modManNumRMA_CheckedChanged(object sender, EventArgs e)
        {
            if (cb_modManNumRMA.Checked)
                lbl_NumRMA.Text = tb_ModManNumRMA.Text;
        }
    }
}
