﻿namespace Ralco_ServiceManager
{
    partial class Form_CostiExtra
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_CostiExtra));
            this.btn_addExtraToDB = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.btn_OK = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dgv_ListaCostiExtra = new System.Windows.Forms.DataGridView();
            this.chk_Art = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.idExtra = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.qta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descCostoExtra = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.prezzoCostoExtra = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gb_selectExtra = new System.Windows.Forms.GroupBox();
            this.btn_addExtra = new System.Windows.Forms.Button();
            this.cmb_costoExtra = new System.Windows.Forms.ComboBox();
            this.gb_addExtra = new System.Windows.Forms.GroupBox();
            this.lbl_Desc = new System.Windows.Forms.Label();
            this.lbl_prezzoCostoExtra = new System.Windows.Forms.Label();
            this.tb_PrezzoCostiExtra = new System.Windows.Forms.MaskedTextBox();
            this.tb_DescCostiExtra = new System.Windows.Forms.TextBox();
            this.btn_cancel = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ListaCostiExtra)).BeginInit();
            this.gb_selectExtra.SuspendLayout();
            this.gb_addExtra.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_addExtraToDB
            // 
            this.btn_addExtraToDB.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_addExtraToDB.BackgroundImage")));
            this.btn_addExtraToDB.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_addExtraToDB.Location = new System.Drawing.Point(458, 23);
            this.btn_addExtraToDB.Name = "btn_addExtraToDB";
            this.btn_addExtraToDB.Size = new System.Drawing.Size(25, 25);
            this.btn_addExtraToDB.TabIndex = 5;
            this.btn_addExtraToDB.UseVisualStyleBackColor = true;
            this.btn_addExtraToDB.Click += new System.EventHandler(this.btn_addExtraToDB_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 30);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 13);
            this.label3.TabIndex = 2;
            // 
            // btn_OK
            // 
            this.btn_OK.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_OK.BackgroundImage")));
            this.btn_OK.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_OK.Location = new System.Drawing.Point(370, 301);
            this.btn_OK.Name = "btn_OK";
            this.btn_OK.Size = new System.Drawing.Size(50, 50);
            this.btn_OK.TabIndex = 7;
            this.btn_OK.UseVisualStyleBackColor = true;
            this.btn_OK.Click += new System.EventHandler(this.btn_okCostiExtra_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dgv_ListaCostiExtra);
            this.groupBox1.Location = new System.Drawing.Point(18, 77);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(940, 204);
            this.groupBox1.TabIndex = 104;
            this.groupBox1.TabStop = false;
            // 
            // dgv_ListaCostiExtra
            // 
            this.dgv_ListaCostiExtra.AllowUserToAddRows = false;
            this.dgv_ListaCostiExtra.AllowUserToOrderColumns = true;
            this.dgv_ListaCostiExtra.BackgroundColor = System.Drawing.SystemColors.ActiveBorder;
            this.dgv_ListaCostiExtra.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_ListaCostiExtra.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.chk_Art,
            this.idExtra,
            this.qta,
            this.descCostoExtra,
            this.prezzoCostoExtra});
            this.dgv_ListaCostiExtra.Cursor = System.Windows.Forms.Cursors.Default;
            this.dgv_ListaCostiExtra.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_ListaCostiExtra.Location = new System.Drawing.Point(3, 16);
            this.dgv_ListaCostiExtra.Name = "dgv_ListaCostiExtra";
            this.dgv_ListaCostiExtra.ReadOnly = true;
            this.dgv_ListaCostiExtra.RowHeadersVisible = false;
            this.dgv_ListaCostiExtra.Size = new System.Drawing.Size(934, 185);
            this.dgv_ListaCostiExtra.TabIndex = 6;
            this.dgv_ListaCostiExtra.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_ListaCostiExtra_CellContentClick);
            // 
            // chk_Art
            // 
            this.chk_Art.HeaderText = "";
            this.chk_Art.Name = "chk_Art";
            this.chk_Art.ReadOnly = true;
            this.chk_Art.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.chk_Art.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.chk_Art.Width = 30;
            // 
            // idExtra
            // 
            this.idExtra.HeaderText = "ID";
            this.idExtra.Name = "idExtra";
            this.idExtra.ReadOnly = true;
            this.idExtra.Width = 90;
            // 
            // qta
            // 
            this.qta.HeaderText = "Quantità";
            this.qta.Name = "qta";
            this.qta.ReadOnly = true;
            this.qta.Width = 70;
            // 
            // descCostoExtra
            // 
            this.descCostoExtra.HeaderText = "Descrizione";
            this.descCostoExtra.Name = "descCostoExtra";
            this.descCostoExtra.ReadOnly = true;
            this.descCostoExtra.Width = 640;
            // 
            // prezzoCostoExtra
            // 
            this.prezzoCostoExtra.HeaderText = "Prezzo (€)";
            this.prezzoCostoExtra.Name = "prezzoCostoExtra";
            this.prezzoCostoExtra.ReadOnly = true;
            this.prezzoCostoExtra.Width = 90;
            // 
            // gb_selectExtra
            // 
            this.gb_selectExtra.Controls.Add(this.btn_addExtra);
            this.gb_selectExtra.Controls.Add(this.cmb_costoExtra);
            this.gb_selectExtra.Controls.Add(this.label3);
            this.gb_selectExtra.Location = new System.Drawing.Point(18, 12);
            this.gb_selectExtra.Name = "gb_selectExtra";
            this.gb_selectExtra.Size = new System.Drawing.Size(443, 59);
            this.gb_selectExtra.TabIndex = 106;
            this.gb_selectExtra.TabStop = false;
            this.gb_selectExtra.Text = "Seleziona Costi Extra";
            // 
            // btn_addExtra
            // 
            this.btn_addExtra.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_addExtra.BackgroundImage")));
            this.btn_addExtra.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_addExtra.Location = new System.Drawing.Point(408, 23);
            this.btn_addExtra.Name = "btn_addExtra";
            this.btn_addExtra.Size = new System.Drawing.Size(25, 25);
            this.btn_addExtra.TabIndex = 2;
            this.btn_addExtra.UseVisualStyleBackColor = true;
            this.btn_addExtra.Click += new System.EventHandler(this.btn_addExtra_Click);
            // 
            // cmb_costoExtra
            // 
            this.cmb_costoExtra.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmb_costoExtra.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_costoExtra.FormattingEnabled = true;
            this.cmb_costoExtra.Items.AddRange(new object[] {
            "Invio al cliente di materiale per la riparazione",
            "Invio al cliente di personale Ralco per riparazione On Site",
            "Riparazione effettuata da Ralco",
            "Riparazione effettuata dal cliente"});
            this.cmb_costoExtra.Location = new System.Drawing.Point(9, 24);
            this.cmb_costoExtra.Name = "cmb_costoExtra";
            this.cmb_costoExtra.Size = new System.Drawing.Size(393, 21);
            this.cmb_costoExtra.Sorted = true;
            this.cmb_costoExtra.TabIndex = 1;
            // 
            // gb_addExtra
            // 
            this.gb_addExtra.Controls.Add(this.lbl_Desc);
            this.gb_addExtra.Controls.Add(this.lbl_prezzoCostoExtra);
            this.gb_addExtra.Controls.Add(this.tb_PrezzoCostiExtra);
            this.gb_addExtra.Controls.Add(this.tb_DescCostiExtra);
            this.gb_addExtra.Controls.Add(this.btn_addExtraToDB);
            this.gb_addExtra.Location = new System.Drawing.Point(467, 12);
            this.gb_addExtra.Name = "gb_addExtra";
            this.gb_addExtra.Size = new System.Drawing.Size(488, 59);
            this.gb_addExtra.TabIndex = 107;
            this.gb_addExtra.TabStop = false;
            this.gb_addExtra.Text = "Aggiungi Costi Extra Al Database";
            // 
            // lbl_Desc
            // 
            this.lbl_Desc.AutoSize = true;
            this.lbl_Desc.Location = new System.Drawing.Point(9, 28);
            this.lbl_Desc.Name = "lbl_Desc";
            this.lbl_Desc.Size = new System.Drawing.Size(62, 13);
            this.lbl_Desc.TabIndex = 7;
            this.lbl_Desc.Text = "Descrizione";
            // 
            // lbl_prezzoCostoExtra
            // 
            this.lbl_prezzoCostoExtra.AutoSize = true;
            this.lbl_prezzoCostoExtra.Location = new System.Drawing.Point(335, 28);
            this.lbl_prezzoCostoExtra.Name = "lbl_prezzoCostoExtra";
            this.lbl_prezzoCostoExtra.Size = new System.Drawing.Size(54, 13);
            this.lbl_prezzoCostoExtra.TabIndex = 6;
            this.lbl_prezzoCostoExtra.Text = "Prezzo (€)";
            // 
            // tb_PrezzoCostiExtra
            // 
            this.tb_PrezzoCostiExtra.Location = new System.Drawing.Point(398, 25);
            this.tb_PrezzoCostiExtra.Mask = "9999.99";
            this.tb_PrezzoCostiExtra.Name = "tb_PrezzoCostiExtra";
            this.tb_PrezzoCostiExtra.Size = new System.Drawing.Size(51, 20);
            this.tb_PrezzoCostiExtra.TabIndex = 4;
            // 
            // tb_DescCostiExtra
            // 
            this.tb_DescCostiExtra.Location = new System.Drawing.Point(81, 25);
            this.tb_DescCostiExtra.Name = "tb_DescCostiExtra";
            this.tb_DescCostiExtra.Size = new System.Drawing.Size(248, 20);
            this.tb_DescCostiExtra.TabIndex = 3;
            // 
            // btn_cancel
            // 
            this.btn_cancel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_cancel.BackgroundImage")));
            this.btn_cancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_cancel.Location = new System.Drawing.Point(475, 301);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(50, 50);
            this.btn_cancel.TabIndex = 112;
            this.btn_cancel.UseVisualStyleBackColor = true;
            this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
            // 
            // Form_CostiExtra
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(965, 363);
            this.Controls.Add(this.btn_cancel);
            this.Controls.Add(this.gb_addExtra);
            this.Controls.Add(this.gb_selectExtra);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btn_OK);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(981, 401);
            this.MinimumSize = new System.Drawing.Size(981, 401);
            this.Name = "Form_CostiExtra";
            this.Text = "Costi Extra";
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ListaCostiExtra)).EndInit();
            this.gb_selectExtra.ResumeLayout(false);
            this.gb_selectExtra.PerformLayout();
            this.gb_addExtra.ResumeLayout(false);
            this.gb_addExtra.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btn_addExtraToDB;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btn_OK;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox gb_selectExtra;
        private System.Windows.Forms.GroupBox gb_addExtra;
        private System.Windows.Forms.ComboBox cmb_costoExtra;
        private System.Windows.Forms.TextBox tb_DescCostiExtra;
        private System.Windows.Forms.DataGridView dgv_ListaCostiExtra;
        private System.Windows.Forms.Button btn_addExtra;
        private System.Windows.Forms.Label lbl_Desc;
        private System.Windows.Forms.Label lbl_prezzoCostoExtra;
        private System.Windows.Forms.MaskedTextBox tb_PrezzoCostiExtra;
        private System.Windows.Forms.Button btn_cancel;
        private System.Windows.Forms.DataGridViewCheckBoxColumn chk_Art;
        private System.Windows.Forms.DataGridViewTextBoxColumn idExtra;
        private System.Windows.Forms.DataGridViewTextBoxColumn qta;
        private System.Windows.Forms.DataGridViewTextBoxColumn descCostoExtra;
        private System.Windows.Forms.DataGridViewTextBoxColumn prezzoCostoExtra;
    }
}