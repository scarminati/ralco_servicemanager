﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ralco_ServiceManager
{
    public class RMA_AzioneSupplementare
    {
        public int idAzioneSuppl { get; set; }
        public string descrizioneAzioneSuppl { get; set; }
        public string nomeCliente { get; set; }
        public string modelloColl { get; set; }

        public RMA_AzioneSupplementare ()
        {
            //override costruttore di default
        }

        public RMA_AzioneSupplementare(int idAzioneSuppl, string descrizioneAzioneSuppl, string nomeCliente, string modelloColl)
        {
            this.idAzioneSuppl = idAzioneSuppl;
            this.descrizioneAzioneSuppl = descrizioneAzioneSuppl;
            this.nomeCliente = nomeCliente;
            this.modelloColl = modelloColl;
        }
    }
}
