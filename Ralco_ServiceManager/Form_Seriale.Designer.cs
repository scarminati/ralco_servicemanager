﻿namespace Ralco_ServiceManager
{
    partial class Form_Seriale
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Seriale));
            this.lbl_SN = new System.Windows.Forms.Label();
            this.btn_AddSeriale = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tb_Seriale = new System.Windows.Forms.MaskedTextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tb_alSeriale = new System.Windows.Forms.MaskedTextBox();
            this.lbl_toSN = new System.Windows.Forms.Label();
            this.tb_dalSeriale = new System.Windows.Forms.MaskedTextBox();
            this.btn_AddManySerials = new System.Windows.Forms.Button();
            this.lbl_fromSN = new System.Windows.Forms.Label();
            this.dgv_listaSeriali = new System.Windows.Forms.DataGridView();
            this.chk_Respo = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.numSer = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btn_OK = new System.Windows.Forms.Button();
            this.btn_cancel = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_listaSeriali)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbl_SN
            // 
            this.lbl_SN.AutoSize = true;
            this.lbl_SN.Location = new System.Drawing.Point(11, 22);
            this.lbl_SN.Name = "lbl_SN";
            this.lbl_SN.Size = new System.Drawing.Size(79, 13);
            this.lbl_SN.TabIndex = 3;
            this.lbl_SN.Text = "Numero Seriale";
            // 
            // btn_AddSeriale
            // 
            this.btn_AddSeriale.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_AddSeriale.BackgroundImage")));
            this.btn_AddSeriale.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_AddSeriale.Location = new System.Drawing.Point(208, 19);
            this.btn_AddSeriale.Name = "btn_AddSeriale";
            this.btn_AddSeriale.Size = new System.Drawing.Size(25, 25);
            this.btn_AddSeriale.TabIndex = 2;
            this.btn_AddSeriale.UseVisualStyleBackColor = true;
            this.btn_AddSeriale.Click += new System.EventHandler(this.btn_AddSerial_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tb_Seriale);
            this.groupBox1.Controls.Add(this.btn_AddSeriale);
            this.groupBox1.Controls.Add(this.lbl_SN);
            this.groupBox1.Location = new System.Drawing.Point(11, 7);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(248, 56);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            // 
            // tb_Seriale
            // 
            this.tb_Seriale.Location = new System.Drawing.Point(114, 22);
            this.tb_Seriale.Mask = "9999999999";
            this.tb_Seriale.Name = "tb_Seriale";
            this.tb_Seriale.Size = new System.Drawing.Size(70, 20);
            this.tb_Seriale.TabIndex = 1;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tb_alSeriale);
            this.groupBox2.Controls.Add(this.lbl_toSN);
            this.groupBox2.Controls.Add(this.tb_dalSeriale);
            this.groupBox2.Controls.Add(this.btn_AddManySerials);
            this.groupBox2.Controls.Add(this.lbl_fromSN);
            this.groupBox2.Location = new System.Drawing.Point(265, 7);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(511, 56);
            this.groupBox2.TabIndex = 9;
            this.groupBox2.TabStop = false;
            // 
            // tb_alSeriale
            // 
            this.tb_alSeriale.Location = new System.Drawing.Point(373, 19);
            this.tb_alSeriale.Mask = "9999999999";
            this.tb_alSeriale.Name = "tb_alSeriale";
            this.tb_alSeriale.Size = new System.Drawing.Size(70, 20);
            this.tb_alSeriale.TabIndex = 4;
            // 
            // lbl_toSN
            // 
            this.lbl_toSN.AutoSize = true;
            this.lbl_toSN.Location = new System.Drawing.Point(247, 22);
            this.lbl_toSN.Name = "lbl_toSN";
            this.lbl_toSN.Size = new System.Drawing.Size(91, 13);
            this.lbl_toSN.TabIndex = 9;
            this.lbl_toSN.Text = "Al Numero Seriale";
            // 
            // tb_dalSeriale
            // 
            this.tb_dalSeriale.Location = new System.Drawing.Point(144, 19);
            this.tb_dalSeriale.Mask = "9999999999";
            this.tb_dalSeriale.Name = "tb_dalSeriale";
            this.tb_dalSeriale.Size = new System.Drawing.Size(70, 20);
            this.tb_dalSeriale.TabIndex = 3;
            // 
            // btn_AddManySerials
            // 
            this.btn_AddManySerials.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_AddManySerials.BackgroundImage")));
            this.btn_AddManySerials.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_AddManySerials.Location = new System.Drawing.Point(472, 19);
            this.btn_AddManySerials.Name = "btn_AddManySerials";
            this.btn_AddManySerials.Size = new System.Drawing.Size(25, 25);
            this.btn_AddManySerials.TabIndex = 5;
            this.btn_AddManySerials.UseVisualStyleBackColor = true;
            this.btn_AddManySerials.Click += new System.EventHandler(this.btn_AddManySerials_Click);
            // 
            // lbl_fromSN
            // 
            this.lbl_fromSN.AutoSize = true;
            this.lbl_fromSN.Location = new System.Drawing.Point(16, 22);
            this.lbl_fromSN.Name = "lbl_fromSN";
            this.lbl_fromSN.Size = new System.Drawing.Size(98, 13);
            this.lbl_fromSN.TabIndex = 3;
            this.lbl_fromSN.Text = "Dal Numero Seriale";
            // 
            // dgv_listaSeriali
            // 
            this.dgv_listaSeriali.AllowUserToAddRows = false;
            this.dgv_listaSeriali.AllowUserToOrderColumns = true;
            this.dgv_listaSeriali.BackgroundColor = System.Drawing.SystemColors.ActiveBorder;
            this.dgv_listaSeriali.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_listaSeriali.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.chk_Respo,
            this.numSer});
            this.dgv_listaSeriali.Cursor = System.Windows.Forms.Cursors.Default;
            this.dgv_listaSeriali.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_listaSeriali.Location = new System.Drawing.Point(3, 16);
            this.dgv_listaSeriali.Name = "dgv_listaSeriali";
            this.dgv_listaSeriali.ReadOnly = true;
            this.dgv_listaSeriali.RowHeadersVisible = false;
            this.dgv_listaSeriali.Size = new System.Drawing.Size(759, 172);
            this.dgv_listaSeriali.TabIndex = 6;
            // 
            // chk_Respo
            // 
            this.chk_Respo.HeaderText = "";
            this.chk_Respo.Name = "chk_Respo";
            this.chk_Respo.ReadOnly = true;
            this.chk_Respo.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.chk_Respo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.chk_Respo.Width = 30;
            // 
            // numSer
            // 
            this.numSer.HeaderText = "Seriale";
            this.numSer.Name = "numSer";
            this.numSer.ReadOnly = true;
            this.numSer.Width = 300;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.dgv_listaSeriali);
            this.groupBox3.Location = new System.Drawing.Point(11, 69);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(765, 191);
            this.groupBox3.TabIndex = 110;
            this.groupBox3.TabStop = false;
            // 
            // btn_OK
            // 
            this.btn_OK.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_OK.BackgroundImage")));
            this.btn_OK.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_OK.Location = new System.Drawing.Point(294, 270);
            this.btn_OK.Name = "btn_OK";
            this.btn_OK.Size = new System.Drawing.Size(50, 50);
            this.btn_OK.TabIndex = 7;
            this.btn_OK.UseVisualStyleBackColor = true;
            this.btn_OK.Click += new System.EventHandler(this.btn_ok_Click);
            // 
            // btn_cancel
            // 
            this.btn_cancel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_cancel.BackgroundImage")));
            this.btn_cancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_cancel.Location = new System.Drawing.Point(399, 270);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(50, 50);
            this.btn_cancel.TabIndex = 111;
            this.btn_cancel.UseVisualStyleBackColor = true;
            this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
            // 
            // Form_Seriale
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 332);
            this.Controls.Add(this.btn_cancel);
            this.Controls.Add(this.btn_OK);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(800, 370);
            this.MinimumSize = new System.Drawing.Size(800, 370);
            this.Name = "Form_Seriale";
            this.Text = "Aggiungi Seriale";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_listaSeriali)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lbl_SN;
        private System.Windows.Forms.Button btn_AddSeriale;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.MaskedTextBox tb_Seriale;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.MaskedTextBox tb_dalSeriale;
        private System.Windows.Forms.Button btn_AddManySerials;
        private System.Windows.Forms.Label lbl_fromSN;
        private System.Windows.Forms.MaskedTextBox tb_alSeriale;
        private System.Windows.Forms.Label lbl_toSN;
        private System.Windows.Forms.DataGridView dgv_listaSeriali;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btn_OK;
        private System.Windows.Forms.DataGridViewCheckBoxColumn chk_Respo;
        private System.Windows.Forms.DataGridViewTextBoxColumn numSer;
        private System.Windows.Forms.Button btn_cancel;
    }
}