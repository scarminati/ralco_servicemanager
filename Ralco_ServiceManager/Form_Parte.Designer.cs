﻿namespace Ralco_ServiceManager
{
    partial class Form_Parte
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Parte));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tb_FC = new System.Windows.Forms.MaskedTextBox();
            this.lbl_FC = new System.Windows.Forms.Label();
            this.btn_RemoveSeriale = new System.Windows.Forms.Button();
            this.tb_numBolla = new System.Windows.Forms.TextBox();
            this.btn_AddSeriale = new System.Windows.Forms.Button();
            this.lbl_numBolla = new System.Windows.Forms.Label();
            this.dgv_listaSeriali = new System.Windows.Forms.DataGridView();
            this.chk_Ser = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.numSer = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmb_nomeCliente = new System.Windows.Forms.ComboBox();
            this.lbl_nomeCli = new System.Windows.Forms.Label();
            this.tb_dataVendita = new System.Windows.Forms.TextBox();
            this.lbl_dataVendita = new System.Windows.Forms.Label();
            this.dtp_dataVendita = new System.Windows.Forms.DateTimePicker();
            this.tb_dataBolla = new System.Windows.Forms.TextBox();
            this.lbl_dataBolla = new System.Windows.Forms.Label();
            this.dtp_dataBolla = new System.Windows.Forms.DateTimePicker();
            this.cmb_IDParti = new System.Windows.Forms.ComboBox();
            this.lbl_qta = new System.Windows.Forms.Label();
            this.nud_Quantita = new System.Windows.Forms.NumericUpDown();
            this.lbl_descrizione = new System.Windows.Forms.Label();
            this.lbl_IDParte = new System.Windows.Forms.Label();
            this.lbl_Desc = new System.Windows.Forms.Label();
            this.btn_cancel = new System.Windows.Forms.Button();
            this.btn_OK = new System.Windows.Forms.Button();
            this.grp_RSRO_Rif = new System.Windows.Forms.GroupBox();
            this.dgv_ListaRifRSRO = new System.Windows.Forms.DataGridView();
            this.chk_Rip = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.codice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_listaSeriali)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_Quantita)).BeginInit();
            this.grp_RSRO_Rif.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ListaRifRSRO)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tb_FC);
            this.groupBox1.Controls.Add(this.lbl_FC);
            this.groupBox1.Controls.Add(this.btn_RemoveSeriale);
            this.groupBox1.Controls.Add(this.tb_numBolla);
            this.groupBox1.Controls.Add(this.btn_AddSeriale);
            this.groupBox1.Controls.Add(this.lbl_numBolla);
            this.groupBox1.Controls.Add(this.dgv_listaSeriali);
            this.groupBox1.Controls.Add(this.cmb_nomeCliente);
            this.groupBox1.Controls.Add(this.lbl_nomeCli);
            this.groupBox1.Controls.Add(this.tb_dataVendita);
            this.groupBox1.Controls.Add(this.lbl_dataVendita);
            this.groupBox1.Controls.Add(this.dtp_dataVendita);
            this.groupBox1.Controls.Add(this.tb_dataBolla);
            this.groupBox1.Controls.Add(this.lbl_dataBolla);
            this.groupBox1.Controls.Add(this.dtp_dataBolla);
            this.groupBox1.Controls.Add(this.cmb_IDParti);
            this.groupBox1.Controls.Add(this.lbl_qta);
            this.groupBox1.Controls.Add(this.nud_Quantita);
            this.groupBox1.Controls.Add(this.lbl_descrizione);
            this.groupBox1.Controls.Add(this.lbl_IDParte);
            this.groupBox1.Controls.Add(this.lbl_Desc);
            this.groupBox1.Location = new System.Drawing.Point(12, 3);
            this.groupBox1.MaximumSize = new System.Drawing.Size(564, 428);
            this.groupBox1.MinimumSize = new System.Drawing.Size(564, 428);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(564, 428);
            this.groupBox1.TabIndex = 107;
            this.groupBox1.TabStop = false;
            // 
            // tb_FC
            // 
            this.tb_FC.Location = new System.Drawing.Point(351, 53);
            this.tb_FC.Mask = "9999/9999";
            this.tb_FC.MaximumSize = new System.Drawing.Size(185, 20);
            this.tb_FC.MinimumSize = new System.Drawing.Size(185, 20);
            this.tb_FC.Name = "tb_FC";
            this.tb_FC.Size = new System.Drawing.Size(185, 20);
            this.tb_FC.TabIndex = 6;
            // 
            // lbl_FC
            // 
            this.lbl_FC.AutoSize = true;
            this.lbl_FC.Location = new System.Drawing.Point(403, 16);
            this.lbl_FC.Name = "lbl_FC";
            this.lbl_FC.Size = new System.Drawing.Size(57, 13);
            this.lbl_FC.TabIndex = 1003;
            this.lbl_FC.Text = "Flow Chart";
            // 
            // btn_RemoveSeriale
            // 
            this.btn_RemoveSeriale.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_RemoveSeriale.BackgroundImage")));
            this.btn_RemoveSeriale.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_RemoveSeriale.Location = new System.Drawing.Point(21, 277);
            this.btn_RemoveSeriale.Name = "btn_RemoveSeriale";
            this.btn_RemoveSeriale.Size = new System.Drawing.Size(50, 50);
            this.btn_RemoveSeriale.TabIndex = 4;
            this.btn_RemoveSeriale.UseVisualStyleBackColor = true;
            this.btn_RemoveSeriale.Click += new System.EventHandler(this.btn_CancelSeriale_Click);
            // 
            // tb_numBolla
            // 
            this.tb_numBolla.Location = new System.Drawing.Point(351, 228);
            this.tb_numBolla.MaximumSize = new System.Drawing.Size(185, 21);
            this.tb_numBolla.MinimumSize = new System.Drawing.Size(185, 21);
            this.tb_numBolla.Name = "tb_numBolla";
            this.tb_numBolla.Size = new System.Drawing.Size(185, 20);
            this.tb_numBolla.TabIndex = 8;
            this.tb_numBolla.Text = "N/A";
            // 
            // btn_AddSeriale
            // 
            this.btn_AddSeriale.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_AddSeriale.BackgroundImage")));
            this.btn_AddSeriale.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_AddSeriale.Location = new System.Drawing.Point(21, 219);
            this.btn_AddSeriale.Name = "btn_AddSeriale";
            this.btn_AddSeriale.Size = new System.Drawing.Size(50, 50);
            this.btn_AddSeriale.TabIndex = 3;
            this.btn_AddSeriale.UseVisualStyleBackColor = true;
            this.btn_AddSeriale.Click += new System.EventHandler(this.btn_AddSeriale_Click);
            // 
            // lbl_numBolla
            // 
            this.lbl_numBolla.AutoSize = true;
            this.lbl_numBolla.Location = new System.Drawing.Point(403, 188);
            this.lbl_numBolla.Name = "lbl_numBolla";
            this.lbl_numBolla.Size = new System.Drawing.Size(70, 13);
            this.lbl_numBolla.TabIndex = 45;
            this.lbl_numBolla.Text = "Numero Bolla";
            // 
            // dgv_listaSeriali
            // 
            this.dgv_listaSeriali.AllowUserToAddRows = false;
            this.dgv_listaSeriali.AllowUserToOrderColumns = true;
            this.dgv_listaSeriali.BackgroundColor = System.Drawing.SystemColors.ActiveBorder;
            this.dgv_listaSeriali.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_listaSeriali.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.chk_Ser,
            this.numSer});
            this.dgv_listaSeriali.Cursor = System.Windows.Forms.Cursors.Default;
            this.dgv_listaSeriali.Location = new System.Drawing.Point(96, 219);
            this.dgv_listaSeriali.Name = "dgv_listaSeriali";
            this.dgv_listaSeriali.ReadOnly = true;
            this.dgv_listaSeriali.RowHeadersVisible = false;
            this.dgv_listaSeriali.Size = new System.Drawing.Size(185, 108);
            this.dgv_listaSeriali.TabIndex = 108;
            // 
            // chk_Ser
            // 
            this.chk_Ser.HeaderText = "";
            this.chk_Ser.Name = "chk_Ser";
            this.chk_Ser.ReadOnly = true;
            this.chk_Ser.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.chk_Ser.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.chk_Ser.Width = 30;
            // 
            // numSer
            // 
            this.numSer.HeaderText = "Seriale";
            this.numSer.Name = "numSer";
            this.numSer.ReadOnly = true;
            this.numSer.Width = 140;
            // 
            // cmb_nomeCliente
            // 
            this.cmb_nomeCliente.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmb_nomeCliente.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_nomeCliente.FormattingEnabled = true;
            this.cmb_nomeCliente.Items.AddRange(new object[] {
            "Invio al cliente di materiale per la riparazione",
            "Invio al cliente di personale Ralco per riparazione On Site",
            "Riparazione effettuata da Ralco",
            "Riparazione effettuata dal cliente"});
            this.cmb_nomeCliente.Location = new System.Drawing.Point(351, 140);
            this.cmb_nomeCliente.MaximumSize = new System.Drawing.Size(185, 0);
            this.cmb_nomeCliente.MinimumSize = new System.Drawing.Size(185, 0);
            this.cmb_nomeCliente.Name = "cmb_nomeCliente";
            this.cmb_nomeCliente.Size = new System.Drawing.Size(185, 21);
            this.cmb_nomeCliente.Sorted = true;
            this.cmb_nomeCliente.TabIndex = 7;
            // 
            // lbl_nomeCli
            // 
            this.lbl_nomeCli.AutoSize = true;
            this.lbl_nomeCli.Location = new System.Drawing.Point(403, 103);
            this.lbl_nomeCli.Name = "lbl_nomeCli";
            this.lbl_nomeCli.Size = new System.Drawing.Size(70, 13);
            this.lbl_nomeCli.TabIndex = 44;
            this.lbl_nomeCli.Text = "Nome Cliente";
            // 
            // tb_dataVendita
            // 
            this.tb_dataVendita.BackColor = System.Drawing.SystemColors.Window;
            this.tb_dataVendita.Location = new System.Drawing.Point(351, 386);
            this.tb_dataVendita.Name = "tb_dataVendita";
            this.tb_dataVendita.ReadOnly = true;
            this.tb_dataVendita.Size = new System.Drawing.Size(146, 20);
            this.tb_dataVendita.TabIndex = 1001;
            // 
            // lbl_dataVendita
            // 
            this.lbl_dataVendita.AutoSize = true;
            this.lbl_dataVendita.Location = new System.Drawing.Point(403, 353);
            this.lbl_dataVendita.Name = "lbl_dataVendita";
            this.lbl_dataVendita.Size = new System.Drawing.Size(80, 13);
            this.lbl_dataVendita.TabIndex = 40;
            this.lbl_dataVendita.Text = "Data di Vendita";
            // 
            // dtp_dataVendita
            // 
            this.dtp_dataVendita.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_dataVendita.Location = new System.Drawing.Point(351, 386);
            this.dtp_dataVendita.MaximumSize = new System.Drawing.Size(185, 21);
            this.dtp_dataVendita.MinimumSize = new System.Drawing.Size(185, 21);
            this.dtp_dataVendita.Name = "dtp_dataVendita";
            this.dtp_dataVendita.Size = new System.Drawing.Size(185, 21);
            this.dtp_dataVendita.TabIndex = 10;
            this.dtp_dataVendita.Value = new System.DateTime(2018, 4, 19, 0, 0, 0, 0);
            this.dtp_dataVendita.ValueChanged += new System.EventHandler(this.dtp_dataVendita_ValueChanged);
            // 
            // tb_dataBolla
            // 
            this.tb_dataBolla.BackColor = System.Drawing.SystemColors.Window;
            this.tb_dataBolla.Location = new System.Drawing.Point(351, 308);
            this.tb_dataBolla.Name = "tb_dataBolla";
            this.tb_dataBolla.ReadOnly = true;
            this.tb_dataBolla.Size = new System.Drawing.Size(146, 20);
            this.tb_dataBolla.TabIndex = 1000;
            // 
            // lbl_dataBolla
            // 
            this.lbl_dataBolla.AutoSize = true;
            this.lbl_dataBolla.Location = new System.Drawing.Point(403, 272);
            this.lbl_dataBolla.Name = "lbl_dataBolla";
            this.lbl_dataBolla.Size = new System.Drawing.Size(67, 13);
            this.lbl_dataBolla.TabIndex = 37;
            this.lbl_dataBolla.Text = "Data di Bolla";
            // 
            // dtp_dataBolla
            // 
            this.dtp_dataBolla.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_dataBolla.Location = new System.Drawing.Point(351, 308);
            this.dtp_dataBolla.MaximumSize = new System.Drawing.Size(185, 21);
            this.dtp_dataBolla.MinimumSize = new System.Drawing.Size(185, 21);
            this.dtp_dataBolla.Name = "dtp_dataBolla";
            this.dtp_dataBolla.Size = new System.Drawing.Size(185, 21);
            this.dtp_dataBolla.TabIndex = 9;
            this.dtp_dataBolla.Value = new System.DateTime(2018, 4, 19, 0, 0, 0, 0);
            this.dtp_dataBolla.ValueChanged += new System.EventHandler(this.dtp_dataBolla_ValueChanged);
            // 
            // cmb_IDParti
            // 
            this.cmb_IDParti.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmb_IDParti.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_IDParti.FormattingEnabled = true;
            this.cmb_IDParti.Location = new System.Drawing.Point(96, 53);
            this.cmb_IDParti.Name = "cmb_IDParti";
            this.cmb_IDParti.Size = new System.Drawing.Size(185, 21);
            this.cmb_IDParti.TabIndex = 1;
            this.cmb_IDParti.SelectedIndexChanged += new System.EventHandler(this.cmb_IDParti_SelectedIndexChanged);
            // 
            // lbl_qta
            // 
            this.lbl_qta.AutoSize = true;
            this.lbl_qta.Location = new System.Drawing.Point(149, 353);
            this.lbl_qta.Name = "lbl_qta";
            this.lbl_qta.Size = new System.Drawing.Size(47, 13);
            this.lbl_qta.TabIndex = 2;
            this.lbl_qta.Text = "Quantità";
            // 
            // nud_Quantita
            // 
            this.nud_Quantita.Location = new System.Drawing.Point(96, 387);
            this.nud_Quantita.Name = "nud_Quantita";
            this.nud_Quantita.Size = new System.Drawing.Size(185, 20);
            this.nud_Quantita.TabIndex = 5;
            // 
            // lbl_descrizione
            // 
            this.lbl_descrizione.AutoSize = true;
            this.lbl_descrizione.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_descrizione.Location = new System.Drawing.Point(96, 121);
            this.lbl_descrizione.MaximumSize = new System.Drawing.Size(185, 70);
            this.lbl_descrizione.MinimumSize = new System.Drawing.Size(185, 70);
            this.lbl_descrizione.Name = "lbl_descrizione";
            this.lbl_descrizione.Size = new System.Drawing.Size(185, 70);
            this.lbl_descrizione.TabIndex = 2;
            // 
            // lbl_IDParte
            // 
            this.lbl_IDParte.AutoSize = true;
            this.lbl_IDParte.Location = new System.Drawing.Point(149, 16);
            this.lbl_IDParte.Name = "lbl_IDParte";
            this.lbl_IDParte.Size = new System.Drawing.Size(46, 13);
            this.lbl_IDParte.TabIndex = 0;
            this.lbl_IDParte.Text = "ID Parte";
            // 
            // lbl_Desc
            // 
            this.lbl_Desc.AutoSize = true;
            this.lbl_Desc.Location = new System.Drawing.Point(149, 90);
            this.lbl_Desc.Name = "lbl_Desc";
            this.lbl_Desc.Size = new System.Drawing.Size(62, 13);
            this.lbl_Desc.TabIndex = 1;
            this.lbl_Desc.Text = "Descrizione";
            // 
            // btn_cancel
            // 
            this.btn_cancel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_cancel.BackgroundImage")));
            this.btn_cancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_cancel.Location = new System.Drawing.Point(548, 452);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(50, 50);
            this.btn_cancel.TabIndex = 11;
            this.btn_cancel.UseVisualStyleBackColor = true;
            this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
            // 
            // btn_OK
            // 
            this.btn_OK.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_OK.BackgroundImage")));
            this.btn_OK.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_OK.Location = new System.Drawing.Point(443, 452);
            this.btn_OK.Name = "btn_OK";
            this.btn_OK.Size = new System.Drawing.Size(50, 50);
            this.btn_OK.TabIndex = 10;
            this.btn_OK.UseVisualStyleBackColor = true;
            this.btn_OK.Click += new System.EventHandler(this.btn_saveAndClose_Click);
            // 
            // grp_RSRO_Rif
            // 
            this.grp_RSRO_Rif.Controls.Add(this.dgv_ListaRifRSRO);
            this.grp_RSRO_Rif.Location = new System.Drawing.Point(582, 3);
            this.grp_RSRO_Rif.Name = "grp_RSRO_Rif";
            this.grp_RSRO_Rif.Size = new System.Drawing.Size(455, 428);
            this.grp_RSRO_Rif.TabIndex = 109;
            this.grp_RSRO_Rif.TabStop = false;
            this.grp_RSRO_Rif.Text = "RS / RO Di Riferimento";
            // 
            // dgv_ListaRifRSRO
            // 
            this.dgv_ListaRifRSRO.AllowUserToAddRows = false;
            this.dgv_ListaRifRSRO.AllowUserToOrderColumns = true;
            this.dgv_ListaRifRSRO.BackgroundColor = System.Drawing.SystemColors.ActiveBorder;
            this.dgv_ListaRifRSRO.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_ListaRifRSRO.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.chk_Rip,
            this.codice,
            this.descr});
            this.dgv_ListaRifRSRO.Cursor = System.Windows.Forms.Cursors.Default;
            this.dgv_ListaRifRSRO.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_ListaRifRSRO.Location = new System.Drawing.Point(3, 16);
            this.dgv_ListaRifRSRO.Name = "dgv_ListaRifRSRO";
            this.dgv_ListaRifRSRO.ReadOnly = true;
            this.dgv_ListaRifRSRO.RowHeadersVisible = false;
            this.dgv_ListaRifRSRO.Size = new System.Drawing.Size(449, 409);
            this.dgv_ListaRifRSRO.TabIndex = 7;
            // 
            // chk_Rip
            // 
            this.chk_Rip.HeaderText = "";
            this.chk_Rip.Name = "chk_Rip";
            this.chk_Rip.ReadOnly = true;
            this.chk_Rip.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.chk_Rip.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.chk_Rip.Width = 30;
            // 
            // codice
            // 
            this.codice.HeaderText = "Codice";
            this.codice.Name = "codice";
            this.codice.ReadOnly = true;
            this.codice.Width = 150;
            // 
            // descr
            // 
            this.descr.HeaderText = "Descrizione";
            this.descr.Name = "descr";
            this.descr.ReadOnly = true;
            this.descr.Width = 250;
            // 
            // Form_Parte
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1048, 514);
            this.Controls.Add(this.grp_RSRO_Rif);
            this.Controls.Add(this.btn_cancel);
            this.Controls.Add(this.btn_OK);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(1064, 552);
            this.MinimumSize = new System.Drawing.Size(1064, 552);
            this.Name = "Form_Parte";
            this.Text = "Aggiungi Parte";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_listaSeriali)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_Quantita)).EndInit();
            this.grp_RSRO_Rif.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ListaRifRSRO)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cmb_IDParti;
        private System.Windows.Forms.Label lbl_descrizione;
        private System.Windows.Forms.Label lbl_IDParte;
        private System.Windows.Forms.Label lbl_Desc;
        private System.Windows.Forms.Label lbl_qta;
        private System.Windows.Forms.NumericUpDown nud_Quantita;
        private System.Windows.Forms.TextBox tb_dataVendita;
        private System.Windows.Forms.Label lbl_dataVendita;
        private System.Windows.Forms.DateTimePicker dtp_dataVendita;
        private System.Windows.Forms.TextBox tb_dataBolla;
        private System.Windows.Forms.Label lbl_dataBolla;
        private System.Windows.Forms.DateTimePicker dtp_dataBolla;
        private System.Windows.Forms.ComboBox cmb_nomeCliente;
        private System.Windows.Forms.Label lbl_nomeCli;
        private System.Windows.Forms.Label lbl_numBolla;
        private System.Windows.Forms.TextBox tb_numBolla;
        private System.Windows.Forms.Button btn_RemoveSeriale;
        private System.Windows.Forms.Button btn_AddSeriale;
        private System.Windows.Forms.DataGridView dgv_listaSeriali;
        private System.Windows.Forms.DataGridViewCheckBoxColumn chk_Ser;
        private System.Windows.Forms.DataGridViewTextBoxColumn numSer;
        private System.Windows.Forms.Button btn_cancel;
        private System.Windows.Forms.Button btn_OK;
        private System.Windows.Forms.MaskedTextBox tb_FC;
        private System.Windows.Forms.Label lbl_FC;
        private System.Windows.Forms.GroupBox grp_RSRO_Rif;
        private System.Windows.Forms.DataGridView dgv_ListaRifRSRO;
        private System.Windows.Forms.DataGridViewCheckBoxColumn chk_Rip;
        private System.Windows.Forms.DataGridViewTextBoxColumn codice;
        private System.Windows.Forms.DataGridViewTextBoxColumn descr;
    }
}