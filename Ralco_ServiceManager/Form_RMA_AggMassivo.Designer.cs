﻿namespace Ralco_ServiceManager
{
    partial class Form_RMA_AggMassivo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_RMA_AggMassivo));
            this.rb_appendDifRiscCli = new System.Windows.Forms.TabControl();
            this.tab_generalInfo = new System.Windows.Forms.TabPage();
            this.gb_InfoGenerali = new System.Windows.Forms.GroupBox();
            this.rb_replaceNoteGenerali = new System.Windows.Forms.RadioButton();
            this.rb_appendNoteGenerali = new System.Windows.Forms.RadioButton();
            this.tb_noteGenerali = new System.Windows.Forms.TextBox();
            this.tab_infoCliente = new System.Windows.Forms.TabPage();
            this.gb_dettagli = new System.Windows.Forms.GroupBox();
            this.rb_replaceDifRiscCli = new System.Windows.Forms.RadioButton();
            this.rb_appendDifRiscCliente = new System.Windows.Forms.RadioButton();
            this.tb_DataOrdineRiparazione = new System.Windows.Forms.MaskedTextBox();
            this.lbl_codCollCli = new System.Windows.Forms.Label();
            this.tb_DataDocumentoDiReso = new System.Windows.Forms.MaskedTextBox();
            this.lbl_idCli = new System.Windows.Forms.Label();
            this.tb_DataScadenzaRMA = new System.Windows.Forms.MaskedTextBox();
            this.tb_DataArrivoCollimatore = new System.Windows.Forms.MaskedTextBox();
            this.lbl_codCollCliente = new System.Windows.Forms.Label();
            this.lbl_idCliente = new System.Windows.Forms.Label();
            this.lbl_dataDocReso = new System.Windows.Forms.Label();
            this.dtp_dataDocReso = new System.Windows.Forms.DateTimePicker();
            this.dtp_DataArrivoCollimatore = new System.Windows.Forms.DateTimePicker();
            this.lbl_dataArrColl = new System.Windows.Forms.Label();
            this.dtp_dataScadenzaRMA = new System.Windows.Forms.DateTimePicker();
            this.lbl_dataScadRMA = new System.Windows.Forms.Label();
            this.dtp_dataOrdRip = new System.Windows.Forms.DateTimePicker();
            this.lbl_dataOrdRip = new System.Windows.Forms.Label();
            this.lbl_ordRip = new System.Windows.Forms.Label();
            this.cmb_nomeCliente = new System.Windows.Forms.ComboBox();
            this.tb_ordRiparaz = new System.Windows.Forms.TextBox();
            this.lbl_nomeCliente = new System.Windows.Forms.Label();
            this.tb_numScarCLI = new System.Windows.Forms.TextBox();
            this.lbl_numSCARcli = new System.Windows.Forms.Label();
            this.cmb_decisCliente = new System.Windows.Forms.ComboBox();
            this.lbl_decisCli = new System.Windows.Forms.Label();
            this.tb_noteCliente = new System.Windows.Forms.TextBox();
            this.tb_docReso = new System.Windows.Forms.TextBox();
            this.tb_scarCliente = new System.Windows.Forms.TextBox();
            this.lbl_docReso = new System.Windows.Forms.Label();
            this.lbl_SCARcli = new System.Windows.Forms.Label();
            this.tab_detGaranzia = new System.Windows.Forms.TabPage();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.rb_replaceNoteGaranzia = new System.Windows.Forms.RadioButton();
            this.rb_appendNoteGaranzia = new System.Windows.Forms.RadioButton();
            this.cmb_statoGaranzia = new System.Windows.Forms.ComboBox();
            this.rb_statoGaranzia = new System.Windows.Forms.RadioButton();
            this.cmb_azioniSupplRalco = new System.Windows.Forms.ComboBox();
            this.lbl_azSupplRalco = new System.Windows.Forms.Label();
            this.cmb_respDannoCollimatore = new System.Windows.Forms.ComboBox();
            this.tb_noteGaranziaRalco = new System.Windows.Forms.TextBox();
            this.lbl_respDannoColl = new System.Windows.Forms.Label();
            this.cmb_provCollimatore = new System.Windows.Forms.ComboBox();
            this.lbl_provColl = new System.Windows.Forms.Label();
            this.cmb_anniVitaCollimatore = new System.Windows.Forms.ComboBox();
            this.lbl_anniVitaColl = new System.Windows.Forms.Label();
            this.tab_Trasporto = new System.Windows.Forms.TabPage();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.rb_replaceNoteImballaggio = new System.Windows.Forms.RadioButton();
            this.rb_appendNoteImballaggio = new System.Windows.Forms.RadioButton();
            this.tb_noteImballaggio = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.rb_replaceNoteTrasporto = new System.Windows.Forms.RadioButton();
            this.rb_appendNoteTrasporto = new System.Windows.Forms.RadioButton();
            this.cmb_tipoImballaggio = new System.Windows.Forms.ComboBox();
            this.tb_noteTrasporto = new System.Windows.Forms.TextBox();
            this.tb_DataSpedizione = new System.Windows.Forms.MaskedTextBox();
            this.lbl_dataSped = new System.Windows.Forms.Label();
            this.lbl_speseSped = new System.Windows.Forms.Label();
            this.cmb_speseSpediz = new System.Windows.Forms.ComboBox();
            this.dtp_dataSpedizione = new System.Windows.Forms.DateTimePicker();
            this.tab_Qualita = new System.Windows.Forms.TabPage();
            this.rb_replaceNoteQualita = new System.Windows.Forms.RadioButton();
            this.rb_appendNoteQualita = new System.Windows.Forms.RadioButton();
            this.tb_noteQualita = new System.Windows.Forms.TextBox();
            this.gb_azCorrPrev = new System.Windows.Forms.GroupBox();
            this.cmb_rifAzCorr = new System.Windows.Forms.ComboBox();
            this.lbl_descAzCorrett = new System.Windows.Forms.Label();
            this.lbl_descAzCorr = new System.Windows.Forms.Label();
            this.cmb_statoAzCorr = new System.Windows.Forms.ComboBox();
            this.lbl_statoAzCorr = new System.Windows.Forms.Label();
            this.lbl_rif = new System.Windows.Forms.Label();
            this.gb_impattoSCAR = new System.Windows.Forms.GroupBox();
            this.cmb_revDocValutRischi = new System.Windows.Forms.ComboBox();
            this.cmb_segIncidente = new System.Windows.Forms.ComboBox();
            this.lbl_revDocValutazRischi = new System.Windows.Forms.Label();
            this.lbl_segnIncid = new System.Windows.Forms.Label();
            this.cmb_invioNotaInf = new System.Windows.Forms.ComboBox();
            this.cmb_sicProdotto = new System.Windows.Forms.ComboBox();
            this.lbl_invioNotaInf = new System.Windows.Forms.Label();
            this.lbl_sicProd = new System.Windows.Forms.Label();
            this.tab_NoteRMA = new System.Windows.Forms.TabPage();
            this.lbl_note = new System.Windows.Forms.Label();
            this.tb_dataAggiornamento = new System.Windows.Forms.MaskedTextBox();
            this.dtp_dataAggiornamento = new System.Windows.Forms.DateTimePicker();
            this.lbl_dataAgg = new System.Windows.Forms.Label();
            this.cmb_statoNotaRMA = new System.Windows.Forms.ComboBox();
            this.lbl_statoRMA = new System.Windows.Forms.Label();
            this.tb_noteLog = new System.Windows.Forms.TextBox();
            this.cmb_prioritaRMA = new System.Windows.Forms.ComboBox();
            this.lbl_priorita = new System.Windows.Forms.Label();
            this.cmb_statoRMA = new System.Windows.Forms.ComboBox();
            this.lbl_stato = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.btn_SaveAndClose = new System.Windows.Forms.Button();
            this.btn_cancel = new System.Windows.Forms.Button();
            this.btn_saveRMA = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rb_appendDifRiscCli.SuspendLayout();
            this.tab_generalInfo.SuspendLayout();
            this.gb_InfoGenerali.SuspendLayout();
            this.tab_infoCliente.SuspendLayout();
            this.gb_dettagli.SuspendLayout();
            this.tab_detGaranzia.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.tab_Trasporto.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.tab_Qualita.SuspendLayout();
            this.gb_azCorrPrev.SuspendLayout();
            this.gb_impattoSCAR.SuspendLayout();
            this.tab_NoteRMA.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // rb_appendDifRiscCli
            // 
            this.rb_appendDifRiscCli.Controls.Add(this.tab_generalInfo);
            this.rb_appendDifRiscCli.Controls.Add(this.tab_infoCliente);
            this.rb_appendDifRiscCli.Controls.Add(this.tab_detGaranzia);
            this.rb_appendDifRiscCli.Controls.Add(this.tab_Trasporto);
            this.rb_appendDifRiscCli.Controls.Add(this.tab_Qualita);
            this.rb_appendDifRiscCli.Controls.Add(this.tab_NoteRMA);
            this.rb_appendDifRiscCli.Location = new System.Drawing.Point(12, 60);
            this.rb_appendDifRiscCli.Name = "rb_appendDifRiscCli";
            this.rb_appendDifRiscCli.SelectedIndex = 0;
            this.rb_appendDifRiscCli.Size = new System.Drawing.Size(1054, 438);
            this.rb_appendDifRiscCli.TabIndex = 3;
            // 
            // tab_generalInfo
            // 
            this.tab_generalInfo.BackColor = System.Drawing.Color.Transparent;
            this.tab_generalInfo.Controls.Add(this.gb_InfoGenerali);
            this.tab_generalInfo.Location = new System.Drawing.Point(4, 22);
            this.tab_generalInfo.Name = "tab_generalInfo";
            this.tab_generalInfo.Padding = new System.Windows.Forms.Padding(3);
            this.tab_generalInfo.Size = new System.Drawing.Size(1046, 412);
            this.tab_generalInfo.TabIndex = 0;
            this.tab_generalInfo.Text = "Generali";
            // 
            // gb_InfoGenerali
            // 
            this.gb_InfoGenerali.Controls.Add(this.rb_replaceNoteGenerali);
            this.gb_InfoGenerali.Controls.Add(this.rb_appendNoteGenerali);
            this.gb_InfoGenerali.Controls.Add(this.tb_noteGenerali);
            this.gb_InfoGenerali.Location = new System.Drawing.Point(6, 6);
            this.gb_InfoGenerali.Name = "gb_InfoGenerali";
            this.gb_InfoGenerali.Size = new System.Drawing.Size(1034, 400);
            this.gb_InfoGenerali.TabIndex = 10;
            this.gb_InfoGenerali.TabStop = false;
            this.gb_InfoGenerali.Text = "Informazioni Generali";
            // 
            // rb_replaceNoteGenerali
            // 
            this.rb_replaceNoteGenerali.AutoSize = true;
            this.rb_replaceNoteGenerali.Location = new System.Drawing.Point(14, 207);
            this.rb_replaceNoteGenerali.Name = "rb_replaceNoteGenerali";
            this.rb_replaceNoteGenerali.Size = new System.Drawing.Size(106, 30);
            this.rb_replaceNoteGenerali.TabIndex = 16;
            this.rb_replaceNoteGenerali.TabStop = true;
            this.rb_replaceNoteGenerali.Text = "Sovrascrivi Note \r\nGenerali";
            this.rb_replaceNoteGenerali.UseVisualStyleBackColor = true;
            this.rb_replaceNoteGenerali.CheckedChanged += new System.EventHandler(this.rb_replaceNoteGenerali_CheckedChanged);
            // 
            // rb_appendNoteGenerali
            // 
            this.rb_appendNoteGenerali.AutoSize = true;
            this.rb_appendNoteGenerali.Checked = true;
            this.rb_appendNoteGenerali.Location = new System.Drawing.Point(14, 166);
            this.rb_appendNoteGenerali.Name = "rb_appendNoteGenerali";
            this.rb_appendNoteGenerali.Size = new System.Drawing.Size(95, 30);
            this.rb_appendNoteGenerali.TabIndex = 15;
            this.rb_appendNoteGenerali.TabStop = true;
            this.rb_appendNoteGenerali.Text = "Aggiungi Note \r\nGenerali";
            this.rb_appendNoteGenerali.UseVisualStyleBackColor = true;
            this.rb_appendNoteGenerali.CheckedChanged += new System.EventHandler(this.rb_appendNoteGenerali_CheckedChanged);
            // 
            // tb_noteGenerali
            // 
            this.tb_noteGenerali.Location = new System.Drawing.Point(165, 16);
            this.tb_noteGenerali.Multiline = true;
            this.tb_noteGenerali.Name = "tb_noteGenerali";
            this.tb_noteGenerali.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tb_noteGenerali.Size = new System.Drawing.Size(850, 373);
            this.tb_noteGenerali.TabIndex = 4;
            // 
            // tab_infoCliente
            // 
            this.tab_infoCliente.BackColor = System.Drawing.Color.Transparent;
            this.tab_infoCliente.Controls.Add(this.gb_dettagli);
            this.tab_infoCliente.Location = new System.Drawing.Point(4, 22);
            this.tab_infoCliente.Name = "tab_infoCliente";
            this.tab_infoCliente.Padding = new System.Windows.Forms.Padding(3);
            this.tab_infoCliente.Size = new System.Drawing.Size(1046, 412);
            this.tab_infoCliente.TabIndex = 1;
            this.tab_infoCliente.Text = "Informativa Cliente";
            // 
            // gb_dettagli
            // 
            this.gb_dettagli.Controls.Add(this.rb_replaceDifRiscCli);
            this.gb_dettagli.Controls.Add(this.rb_appendDifRiscCliente);
            this.gb_dettagli.Controls.Add(this.tb_DataOrdineRiparazione);
            this.gb_dettagli.Controls.Add(this.lbl_codCollCli);
            this.gb_dettagli.Controls.Add(this.tb_DataDocumentoDiReso);
            this.gb_dettagli.Controls.Add(this.lbl_idCli);
            this.gb_dettagli.Controls.Add(this.tb_DataScadenzaRMA);
            this.gb_dettagli.Controls.Add(this.tb_DataArrivoCollimatore);
            this.gb_dettagli.Controls.Add(this.lbl_codCollCliente);
            this.gb_dettagli.Controls.Add(this.lbl_idCliente);
            this.gb_dettagli.Controls.Add(this.lbl_dataDocReso);
            this.gb_dettagli.Controls.Add(this.dtp_dataDocReso);
            this.gb_dettagli.Controls.Add(this.dtp_DataArrivoCollimatore);
            this.gb_dettagli.Controls.Add(this.lbl_dataArrColl);
            this.gb_dettagli.Controls.Add(this.dtp_dataScadenzaRMA);
            this.gb_dettagli.Controls.Add(this.lbl_dataScadRMA);
            this.gb_dettagli.Controls.Add(this.dtp_dataOrdRip);
            this.gb_dettagli.Controls.Add(this.lbl_dataOrdRip);
            this.gb_dettagli.Controls.Add(this.lbl_ordRip);
            this.gb_dettagli.Controls.Add(this.cmb_nomeCliente);
            this.gb_dettagli.Controls.Add(this.tb_ordRiparaz);
            this.gb_dettagli.Controls.Add(this.lbl_nomeCliente);
            this.gb_dettagli.Controls.Add(this.tb_numScarCLI);
            this.gb_dettagli.Controls.Add(this.lbl_numSCARcli);
            this.gb_dettagli.Controls.Add(this.cmb_decisCliente);
            this.gb_dettagli.Controls.Add(this.lbl_decisCli);
            this.gb_dettagli.Controls.Add(this.tb_noteCliente);
            this.gb_dettagli.Controls.Add(this.tb_docReso);
            this.gb_dettagli.Controls.Add(this.tb_scarCliente);
            this.gb_dettagli.Controls.Add(this.lbl_docReso);
            this.gb_dettagli.Controls.Add(this.lbl_SCARcli);
            this.gb_dettagli.Location = new System.Drawing.Point(6, 6);
            this.gb_dettagli.Name = "gb_dettagli";
            this.gb_dettagli.Size = new System.Drawing.Size(1034, 400);
            this.gb_dettagli.TabIndex = 0;
            this.gb_dettagli.TabStop = false;
            this.gb_dettagli.Text = "Dettagli";
            // 
            // rb_replaceDifRiscCli
            // 
            this.rb_replaceDifRiscCli.AutoSize = true;
            this.rb_replaceDifRiscCli.Location = new System.Drawing.Point(6, 335);
            this.rb_replaceDifRiscCli.Name = "rb_replaceDifRiscCli";
            this.rb_replaceDifRiscCli.Size = new System.Drawing.Size(133, 30);
            this.rb_replaceDifRiscCli.TabIndex = 36;
            this.rb_replaceDifRiscCli.TabStop = true;
            this.rb_replaceDifRiscCli.Text = "Sovrascrivi Difetto \r\nRiscontrato Dal Cliente";
            this.rb_replaceDifRiscCli.UseVisualStyleBackColor = true;
            this.rb_replaceDifRiscCli.CheckedChanged += new System.EventHandler(this.rb_replaceDifRiscCli_CheckedChanged);
            // 
            // rb_appendDifRiscCliente
            // 
            this.rb_appendDifRiscCliente.AutoSize = true;
            this.rb_appendDifRiscCliente.Checked = true;
            this.rb_appendDifRiscCliente.Location = new System.Drawing.Point(6, 299);
            this.rb_appendDifRiscCliente.Name = "rb_appendDifRiscCliente";
            this.rb_appendDifRiscCliente.Size = new System.Drawing.Size(133, 30);
            this.rb_appendDifRiscCliente.TabIndex = 35;
            this.rb_appendDifRiscCliente.TabStop = true;
            this.rb_appendDifRiscCliente.Text = "Aggiungi Difetto \r\nRiscontrato Dal Cliente";
            this.rb_appendDifRiscCliente.UseVisualStyleBackColor = true;
            this.rb_appendDifRiscCliente.CheckedChanged += new System.EventHandler(this.rb_appendDifRiscCliente_CheckedChanged);
            // 
            // tb_DataOrdineRiparazione
            // 
            this.tb_DataOrdineRiparazione.Location = new System.Drawing.Point(755, 185);
            this.tb_DataOrdineRiparazione.Mask = "00/00/0000";
            this.tb_DataOrdineRiparazione.Name = "tb_DataOrdineRiparazione";
            this.tb_DataOrdineRiparazione.Size = new System.Drawing.Size(211, 20);
            this.tb_DataOrdineRiparazione.TabIndex = 8;
            this.tb_DataOrdineRiparazione.ValidatingType = typeof(System.DateTime);
            // 
            // lbl_codCollCli
            // 
            this.lbl_codCollCli.AutoSize = true;
            this.lbl_codCollCli.Location = new System.Drawing.Point(11, 62);
            this.lbl_codCollCli.Name = "lbl_codCollCli";
            this.lbl_codCollCli.Size = new System.Drawing.Size(129, 13);
            this.lbl_codCollCli.TabIndex = 34;
            this.lbl_codCollCli.Text = "Codice Collimatore Cliente";
            // 
            // tb_DataDocumentoDiReso
            // 
            this.tb_DataDocumentoDiReso.Location = new System.Drawing.Point(755, 226);
            this.tb_DataDocumentoDiReso.Mask = "00/00/0000";
            this.tb_DataDocumentoDiReso.Name = "tb_DataDocumentoDiReso";
            this.tb_DataDocumentoDiReso.Size = new System.Drawing.Size(211, 20);
            this.tb_DataDocumentoDiReso.TabIndex = 10;
            this.tb_DataDocumentoDiReso.ValidatingType = typeof(System.DateTime);
            // 
            // lbl_idCli
            // 
            this.lbl_idCli.AutoSize = true;
            this.lbl_idCli.Location = new System.Drawing.Point(566, 23);
            this.lbl_idCli.Name = "lbl_idCli";
            this.lbl_idCli.Size = new System.Drawing.Size(51, 13);
            this.lbl_idCli.TabIndex = 33;
            this.lbl_idCli.Text = "Id Cliente";
            // 
            // tb_DataScadenzaRMA
            // 
            this.tb_DataScadenzaRMA.Location = new System.Drawing.Point(755, 98);
            this.tb_DataScadenzaRMA.Mask = "00/00/0000";
            this.tb_DataScadenzaRMA.Name = "tb_DataScadenzaRMA";
            this.tb_DataScadenzaRMA.Size = new System.Drawing.Size(211, 20);
            this.tb_DataScadenzaRMA.TabIndex = 4;
            this.tb_DataScadenzaRMA.ValidatingType = typeof(System.DateTime);
            // 
            // tb_DataArrivoCollimatore
            // 
            this.tb_DataArrivoCollimatore.Location = new System.Drawing.Point(170, 98);
            this.tb_DataArrivoCollimatore.Mask = "00/00/0000";
            this.tb_DataArrivoCollimatore.Name = "tb_DataArrivoCollimatore";
            this.tb_DataArrivoCollimatore.Size = new System.Drawing.Size(211, 20);
            this.tb_DataArrivoCollimatore.TabIndex = 3;
            this.tb_DataArrivoCollimatore.ValidatingType = typeof(System.DateTime);
            // 
            // lbl_codCollCliente
            // 
            this.lbl_codCollCliente.AutoSize = true;
            this.lbl_codCollCliente.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_codCollCliente.Location = new System.Drawing.Point(170, 59);
            this.lbl_codCollCliente.MaximumSize = new System.Drawing.Size(250, 20);
            this.lbl_codCollCliente.MinimumSize = new System.Drawing.Size(250, 20);
            this.lbl_codCollCliente.Name = "lbl_codCollCliente";
            this.lbl_codCollCliente.Size = new System.Drawing.Size(250, 20);
            this.lbl_codCollCliente.TabIndex = 32;
            // 
            // lbl_idCliente
            // 
            this.lbl_idCliente.AutoSize = true;
            this.lbl_idCliente.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_idCliente.Location = new System.Drawing.Point(755, 20);
            this.lbl_idCliente.MaximumSize = new System.Drawing.Size(250, 20);
            this.lbl_idCliente.MinimumSize = new System.Drawing.Size(250, 20);
            this.lbl_idCliente.Name = "lbl_idCliente";
            this.lbl_idCliente.Size = new System.Drawing.Size(250, 20);
            this.lbl_idCliente.TabIndex = 31;
            // 
            // lbl_dataDocReso
            // 
            this.lbl_dataDocReso.AutoSize = true;
            this.lbl_dataDocReso.Location = new System.Drawing.Point(566, 229);
            this.lbl_dataDocReso.Name = "lbl_dataDocReso";
            this.lbl_dataDocReso.Size = new System.Drawing.Size(127, 13);
            this.lbl_dataDocReso.TabIndex = 30;
            this.lbl_dataDocReso.Text = "Data Documento di Reso";
            // 
            // dtp_dataDocReso
            // 
            this.dtp_dataDocReso.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_dataDocReso.Location = new System.Drawing.Point(755, 226);
            this.dtp_dataDocReso.MaximumSize = new System.Drawing.Size(250, 20);
            this.dtp_dataDocReso.MinimumSize = new System.Drawing.Size(250, 20);
            this.dtp_dataDocReso.Name = "dtp_dataDocReso";
            this.dtp_dataDocReso.Size = new System.Drawing.Size(250, 20);
            this.dtp_dataDocReso.TabIndex = 10;
            this.dtp_dataDocReso.Value = new System.DateTime(2018, 4, 19, 0, 0, 0, 0);
            this.dtp_dataDocReso.ValueChanged += new System.EventHandler(this.dtp_dataDocReso_ValueChanged);
            // 
            // dtp_DataArrivoCollimatore
            // 
            this.dtp_DataArrivoCollimatore.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_DataArrivoCollimatore.Location = new System.Drawing.Point(170, 98);
            this.dtp_DataArrivoCollimatore.Name = "dtp_DataArrivoCollimatore";
            this.dtp_DataArrivoCollimatore.Size = new System.Drawing.Size(250, 20);
            this.dtp_DataArrivoCollimatore.TabIndex = 3;
            this.dtp_DataArrivoCollimatore.Value = new System.DateTime(2018, 4, 19, 0, 0, 0, 0);
            this.dtp_DataArrivoCollimatore.ValueChanged += new System.EventHandler(this.dtp_DataArrivoCollimatore_ValueChanged);
            // 
            // lbl_dataArrColl
            // 
            this.lbl_dataArrColl.AutoSize = true;
            this.lbl_dataArrColl.Location = new System.Drawing.Point(11, 104);
            this.lbl_dataArrColl.Name = "lbl_dataArrColl";
            this.lbl_dataArrColl.Size = new System.Drawing.Size(114, 13);
            this.lbl_dataArrColl.TabIndex = 28;
            this.lbl_dataArrColl.Text = "Data Arrivo Collimatore";
            // 
            // dtp_dataScadenzaRMA
            // 
            this.dtp_dataScadenzaRMA.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_dataScadenzaRMA.Location = new System.Drawing.Point(755, 98);
            this.dtp_dataScadenzaRMA.MaximumSize = new System.Drawing.Size(250, 20);
            this.dtp_dataScadenzaRMA.MinimumSize = new System.Drawing.Size(250, 20);
            this.dtp_dataScadenzaRMA.Name = "dtp_dataScadenzaRMA";
            this.dtp_dataScadenzaRMA.Size = new System.Drawing.Size(250, 20);
            this.dtp_dataScadenzaRMA.TabIndex = 4;
            this.dtp_dataScadenzaRMA.Value = new System.DateTime(2018, 4, 19, 0, 0, 0, 0);
            this.dtp_dataScadenzaRMA.ValueChanged += new System.EventHandler(this.dtp_dataScadenzaRMA_ValueChanged);
            // 
            // lbl_dataScadRMA
            // 
            this.lbl_dataScadRMA.AutoSize = true;
            this.lbl_dataScadRMA.Location = new System.Drawing.Point(566, 104);
            this.lbl_dataScadRMA.Name = "lbl_dataScadRMA";
            this.lbl_dataScadRMA.Size = new System.Drawing.Size(108, 13);
            this.lbl_dataScadRMA.TabIndex = 13;
            this.lbl_dataScadRMA.Text = "Data Scadenza RMA";
            // 
            // dtp_dataOrdRip
            // 
            this.dtp_dataOrdRip.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_dataOrdRip.Location = new System.Drawing.Point(755, 185);
            this.dtp_dataOrdRip.MaximumSize = new System.Drawing.Size(250, 20);
            this.dtp_dataOrdRip.MinimumSize = new System.Drawing.Size(250, 20);
            this.dtp_dataOrdRip.Name = "dtp_dataOrdRip";
            this.dtp_dataOrdRip.Size = new System.Drawing.Size(250, 20);
            this.dtp_dataOrdRip.TabIndex = 8;
            this.dtp_dataOrdRip.Value = new System.DateTime(2018, 4, 19, 0, 0, 0, 0);
            this.dtp_dataOrdRip.ValueChanged += new System.EventHandler(this.dtp_dataOrdRip_ValueChanged);
            // 
            // lbl_dataOrdRip
            // 
            this.lbl_dataOrdRip.AutoSize = true;
            this.lbl_dataOrdRip.Location = new System.Drawing.Point(566, 188);
            this.lbl_dataOrdRip.Name = "lbl_dataOrdRip";
            this.lbl_dataOrdRip.Size = new System.Drawing.Size(123, 13);
            this.lbl_dataOrdRip.TabIndex = 20;
            this.lbl_dataOrdRip.Text = "Data Ordine Riparazione";
            // 
            // lbl_ordRip
            // 
            this.lbl_ordRip.AutoSize = true;
            this.lbl_ordRip.Location = new System.Drawing.Point(11, 188);
            this.lbl_ordRip.Name = "lbl_ordRip";
            this.lbl_ordRip.Size = new System.Drawing.Size(97, 13);
            this.lbl_ordRip.TabIndex = 4;
            this.lbl_ordRip.Text = "Ordine Riparazione";
            // 
            // cmb_nomeCliente
            // 
            this.cmb_nomeCliente.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmb_nomeCliente.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_nomeCliente.FormattingEnabled = true;
            this.cmb_nomeCliente.Items.AddRange(new object[] {
            "Invio al cliente di materiale per la riparazione",
            "Invio al cliente di personale Ralco per riparazione On Site",
            "Riparazione effettuata da Ralco",
            "Riparazione effettuata dal cliente"});
            this.cmb_nomeCliente.Location = new System.Drawing.Point(170, 17);
            this.cmb_nomeCliente.MaximumSize = new System.Drawing.Size(250, 0);
            this.cmb_nomeCliente.MinimumSize = new System.Drawing.Size(250, 0);
            this.cmb_nomeCliente.Name = "cmb_nomeCliente";
            this.cmb_nomeCliente.Size = new System.Drawing.Size(250, 21);
            this.cmb_nomeCliente.Sorted = true;
            this.cmb_nomeCliente.TabIndex = 1;
            this.cmb_nomeCliente.SelectedIndexChanged += new System.EventHandler(this.cmb_nomeCliente_SelectedIndexChanged);
            // 
            // tb_ordRiparaz
            // 
            this.tb_ordRiparaz.Location = new System.Drawing.Point(170, 185);
            this.tb_ordRiparaz.MaximumSize = new System.Drawing.Size(250, 20);
            this.tb_ordRiparaz.MinimumSize = new System.Drawing.Size(250, 20);
            this.tb_ordRiparaz.Name = "tb_ordRiparaz";
            this.tb_ordRiparaz.Size = new System.Drawing.Size(250, 20);
            this.tb_ordRiparaz.TabIndex = 7;
            // 
            // lbl_nomeCliente
            // 
            this.lbl_nomeCliente.AutoSize = true;
            this.lbl_nomeCliente.Location = new System.Drawing.Point(11, 20);
            this.lbl_nomeCliente.Name = "lbl_nomeCliente";
            this.lbl_nomeCliente.Size = new System.Drawing.Size(70, 13);
            this.lbl_nomeCliente.TabIndex = 23;
            this.lbl_nomeCliente.Text = "Nome Cliente";
            // 
            // tb_numScarCLI
            // 
            this.tb_numScarCLI.Location = new System.Drawing.Point(755, 142);
            this.tb_numScarCLI.MaximumSize = new System.Drawing.Size(250, 20);
            this.tb_numScarCLI.MinimumSize = new System.Drawing.Size(250, 20);
            this.tb_numScarCLI.Name = "tb_numScarCLI";
            this.tb_numScarCLI.Size = new System.Drawing.Size(250, 20);
            this.tb_numScarCLI.TabIndex = 6;
            // 
            // lbl_numSCARcli
            // 
            this.lbl_numSCARcli.AutoSize = true;
            this.lbl_numSCARcli.Location = new System.Drawing.Point(566, 146);
            this.lbl_numSCARcli.Name = "lbl_numSCARcli";
            this.lbl_numSCARcli.Size = new System.Drawing.Size(111, 13);
            this.lbl_numSCARcli.TabIndex = 18;
            this.lbl_numSCARcli.Text = "Numero SCAR Cliente";
            // 
            // cmb_decisCliente
            // 
            this.cmb_decisCliente.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmb_decisCliente.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_decisCliente.FormattingEnabled = true;
            this.cmb_decisCliente.Items.AddRange(new object[] {
            "Invio al cliente di materiale per la riparazione / Sending repairment material to" +
                " the customer",
            "Invio al cliente di personale Ralco per riparazione On Site / Sending Ralco perso" +
                "nnel to the customer for repairment On Site",
            "Riparazione effettuata dal cliente / Repairment carried out by the customer",
            "Riparazione effettuata in Ralco / Repairment carried out by Ralco"});
            this.cmb_decisCliente.Location = new System.Drawing.Point(755, 62);
            this.cmb_decisCliente.MaximumSize = new System.Drawing.Size(250, 0);
            this.cmb_decisCliente.MinimumSize = new System.Drawing.Size(250, 0);
            this.cmb_decisCliente.Name = "cmb_decisCliente";
            this.cmb_decisCliente.Size = new System.Drawing.Size(250, 21);
            this.cmb_decisCliente.Sorted = true;
            this.cmb_decisCliente.TabIndex = 2;
            // 
            // lbl_decisCli
            // 
            this.lbl_decisCli.AutoSize = true;
            this.lbl_decisCli.Location = new System.Drawing.Point(566, 65);
            this.lbl_decisCli.Name = "lbl_decisCli";
            this.lbl_decisCli.Size = new System.Drawing.Size(89, 13);
            this.lbl_decisCli.TabIndex = 16;
            this.lbl_decisCli.Text = "Decisione Cliente";
            // 
            // tb_noteCliente
            // 
            this.tb_noteCliente.Location = new System.Drawing.Point(170, 278);
            this.tb_noteCliente.Multiline = true;
            this.tb_noteCliente.Name = "tb_noteCliente";
            this.tb_noteCliente.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tb_noteCliente.Size = new System.Drawing.Size(842, 110);
            this.tb_noteCliente.TabIndex = 11;
            // 
            // tb_docReso
            // 
            this.tb_docReso.Location = new System.Drawing.Point(170, 226);
            this.tb_docReso.MaximumSize = new System.Drawing.Size(250, 20);
            this.tb_docReso.MinimumSize = new System.Drawing.Size(250, 20);
            this.tb_docReso.Name = "tb_docReso";
            this.tb_docReso.Size = new System.Drawing.Size(250, 20);
            this.tb_docReso.TabIndex = 9;
            // 
            // tb_scarCliente
            // 
            this.tb_scarCliente.Location = new System.Drawing.Point(170, 143);
            this.tb_scarCliente.MaximumSize = new System.Drawing.Size(250, 20);
            this.tb_scarCliente.MinimumSize = new System.Drawing.Size(250, 20);
            this.tb_scarCliente.Name = "tb_scarCliente";
            this.tb_scarCliente.Size = new System.Drawing.Size(250, 20);
            this.tb_scarCliente.TabIndex = 5;
            // 
            // lbl_docReso
            // 
            this.lbl_docReso.AutoSize = true;
            this.lbl_docReso.Location = new System.Drawing.Point(11, 229);
            this.lbl_docReso.Name = "lbl_docReso";
            this.lbl_docReso.Size = new System.Drawing.Size(101, 13);
            this.lbl_docReso.TabIndex = 3;
            this.lbl_docReso.Text = "Documento di Reso";
            // 
            // lbl_SCARcli
            // 
            this.lbl_SCARcli.AutoSize = true;
            this.lbl_SCARcli.Location = new System.Drawing.Point(11, 146);
            this.lbl_SCARcli.Name = "lbl_SCARcli";
            this.lbl_SCARcli.Size = new System.Drawing.Size(71, 13);
            this.lbl_SCARcli.TabIndex = 1;
            this.lbl_SCARcli.Text = "SCAR Cliente";
            // 
            // tab_detGaranzia
            // 
            this.tab_detGaranzia.BackColor = System.Drawing.Color.Transparent;
            this.tab_detGaranzia.Controls.Add(this.groupBox12);
            this.tab_detGaranzia.Location = new System.Drawing.Point(4, 22);
            this.tab_detGaranzia.Name = "tab_detGaranzia";
            this.tab_detGaranzia.Size = new System.Drawing.Size(1046, 412);
            this.tab_detGaranzia.TabIndex = 7;
            this.tab_detGaranzia.Text = "Garanzia";
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.rb_replaceNoteGaranzia);
            this.groupBox12.Controls.Add(this.rb_appendNoteGaranzia);
            this.groupBox12.Controls.Add(this.cmb_statoGaranzia);
            this.groupBox12.Controls.Add(this.rb_statoGaranzia);
            this.groupBox12.Controls.Add(this.cmb_azioniSupplRalco);
            this.groupBox12.Controls.Add(this.lbl_azSupplRalco);
            this.groupBox12.Controls.Add(this.cmb_respDannoCollimatore);
            this.groupBox12.Controls.Add(this.tb_noteGaranziaRalco);
            this.groupBox12.Controls.Add(this.lbl_respDannoColl);
            this.groupBox12.Controls.Add(this.cmb_provCollimatore);
            this.groupBox12.Controls.Add(this.lbl_provColl);
            this.groupBox12.Controls.Add(this.cmb_anniVitaCollimatore);
            this.groupBox12.Controls.Add(this.lbl_anniVitaColl);
            this.groupBox12.Location = new System.Drawing.Point(6, 6);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(1037, 400);
            this.groupBox12.TabIndex = 8;
            this.groupBox12.TabStop = false;
            // 
            // rb_replaceNoteGaranzia
            // 
            this.rb_replaceNoteGaranzia.AutoSize = true;
            this.rb_replaceNoteGaranzia.Location = new System.Drawing.Point(97, 230);
            this.rb_replaceNoteGaranzia.Name = "rb_replaceNoteGaranzia";
            this.rb_replaceNoteGaranzia.Size = new System.Drawing.Size(106, 30);
            this.rb_replaceNoteGaranzia.TabIndex = 27;
            this.rb_replaceNoteGaranzia.TabStop = true;
            this.rb_replaceNoteGaranzia.Text = "Sovrascrivi Note \r\nGaranzia";
            this.rb_replaceNoteGaranzia.UseVisualStyleBackColor = true;
            this.rb_replaceNoteGaranzia.CheckedChanged += new System.EventHandler(this.rb_replaceNoteGaranzia_CheckedChanged);
            // 
            // rb_appendNoteGaranzia
            // 
            this.rb_appendNoteGaranzia.AutoSize = true;
            this.rb_appendNoteGaranzia.Checked = true;
            this.rb_appendNoteGaranzia.Location = new System.Drawing.Point(97, 189);
            this.rb_appendNoteGaranzia.Name = "rb_appendNoteGaranzia";
            this.rb_appendNoteGaranzia.Size = new System.Drawing.Size(95, 30);
            this.rb_appendNoteGaranzia.TabIndex = 26;
            this.rb_appendNoteGaranzia.TabStop = true;
            this.rb_appendNoteGaranzia.Text = "Aggiungi Note \r\nGaranzia";
            this.rb_appendNoteGaranzia.UseVisualStyleBackColor = true;
            this.rb_appendNoteGaranzia.CheckedChanged += new System.EventHandler(this.rb_appendNoteGaranzia_CheckedChanged);
            // 
            // cmb_statoGaranzia
            // 
            this.cmb_statoGaranzia.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmb_statoGaranzia.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple;
            this.cmb_statoGaranzia.Enabled = false;
            this.cmb_statoGaranzia.FormattingEnabled = true;
            this.cmb_statoGaranzia.Items.AddRange(new object[] {
            "In Garanzia / In Warranty",
            "Fuori Garanzia / Out Of Warranty"});
            this.cmb_statoGaranzia.Location = new System.Drawing.Point(497, 314);
            this.cmb_statoGaranzia.Name = "cmb_statoGaranzia";
            this.cmb_statoGaranzia.Size = new System.Drawing.Size(405, 21);
            this.cmb_statoGaranzia.TabIndex = 25;
            // 
            // rb_statoGaranzia
            // 
            this.rb_statoGaranzia.AutoSize = true;
            this.rb_statoGaranzia.Location = new System.Drawing.Point(97, 315);
            this.rb_statoGaranzia.Name = "rb_statoGaranzia";
            this.rb_statoGaranzia.Size = new System.Drawing.Size(95, 17);
            this.rb_statoGaranzia.TabIndex = 24;
            this.rb_statoGaranzia.TabStop = true;
            this.rb_statoGaranzia.Text = "Stato Garanzia";
            this.rb_statoGaranzia.UseVisualStyleBackColor = true;
            this.rb_statoGaranzia.CheckedChanged += new System.EventHandler(this.rb_statoGaranzia_CheckedChanged);
            // 
            // cmb_azioniSupplRalco
            // 
            this.cmb_azioniSupplRalco.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmb_azioniSupplRalco.FormattingEnabled = true;
            this.cmb_azioniSupplRalco.Items.AddRange(new object[] {
            "Garanzia con addebito / Warranty with charge",
            "Riparazione in deroga Ralco / Repair in derogation"});
            this.cmb_azioniSupplRalco.Location = new System.Drawing.Point(497, 363);
            this.cmb_azioniSupplRalco.Name = "cmb_azioniSupplRalco";
            this.cmb_azioniSupplRalco.Size = new System.Drawing.Size(405, 21);
            this.cmb_azioniSupplRalco.Sorted = true;
            this.cmb_azioniSupplRalco.TabIndex = 6;
            // 
            // lbl_azSupplRalco
            // 
            this.lbl_azSupplRalco.AutoSize = true;
            this.lbl_azSupplRalco.Location = new System.Drawing.Point(94, 363);
            this.lbl_azSupplRalco.Name = "lbl_azSupplRalco";
            this.lbl_azSupplRalco.Size = new System.Drawing.Size(134, 13);
            this.lbl_azSupplRalco.TabIndex = 22;
            this.lbl_azSupplRalco.Text = "Azioni supplementari Ralco";
            // 
            // cmb_respDannoCollimatore
            // 
            this.cmb_respDannoCollimatore.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmb_respDannoCollimatore.FormattingEnabled = true;
            this.cmb_respDannoCollimatore.Items.AddRange(new object[] {
            "Cliente / Customer",
            "Ralco"});
            this.cmb_respDannoCollimatore.Location = new System.Drawing.Point(497, 111);
            this.cmb_respDannoCollimatore.Name = "cmb_respDannoCollimatore";
            this.cmb_respDannoCollimatore.Size = new System.Drawing.Size(405, 21);
            this.cmb_respDannoCollimatore.Sorted = true;
            this.cmb_respDannoCollimatore.TabIndex = 3;
            this.cmb_respDannoCollimatore.SelectedIndexChanged += new System.EventHandler(this.cmb_respDannoCollimatore_SelectedIndexChanged);
            // 
            // tb_noteGaranziaRalco
            // 
            this.tb_noteGaranziaRalco.Location = new System.Drawing.Point(497, 159);
            this.tb_noteGaranziaRalco.Multiline = true;
            this.tb_noteGaranziaRalco.Name = "tb_noteGaranziaRalco";
            this.tb_noteGaranziaRalco.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tb_noteGaranziaRalco.Size = new System.Drawing.Size(405, 130);
            this.tb_noteGaranziaRalco.TabIndex = 4;
            // 
            // lbl_respDannoColl
            // 
            this.lbl_respDannoColl.AutoSize = true;
            this.lbl_respDannoColl.Location = new System.Drawing.Point(94, 111);
            this.lbl_respDannoColl.Name = "lbl_respDannoColl";
            this.lbl_respDannoColl.Size = new System.Drawing.Size(245, 13);
            this.lbl_respDannoColl.TabIndex = 17;
            this.lbl_respDannoColl.Text = "Responsabilità del danneggiamento del collimatore";
            // 
            // cmb_provCollimatore
            // 
            this.cmb_provCollimatore.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmb_provCollimatore.FormattingEnabled = true;
            this.cmb_provCollimatore.Items.AddRange(new object[] {
            "Cliente / Customer",
            "Service"});
            this.cmb_provCollimatore.Location = new System.Drawing.Point(497, 24);
            this.cmb_provCollimatore.Name = "cmb_provCollimatore";
            this.cmb_provCollimatore.Size = new System.Drawing.Size(405, 21);
            this.cmb_provCollimatore.Sorted = true;
            this.cmb_provCollimatore.TabIndex = 1;
            // 
            // lbl_provColl
            // 
            this.lbl_provColl.AutoSize = true;
            this.lbl_provColl.Location = new System.Drawing.Point(94, 24);
            this.lbl_provColl.Name = "lbl_provColl";
            this.lbl_provColl.Size = new System.Drawing.Size(136, 13);
            this.lbl_provColl.TabIndex = 16;
            this.lbl_provColl.Text = "Provenienza del collimatore";
            // 
            // cmb_anniVitaCollimatore
            // 
            this.cmb_anniVitaCollimatore.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmb_anniVitaCollimatore.FormattingEnabled = true;
            this.cmb_anniVitaCollimatore.Items.AddRange(new object[] {
            "Meno di 2 / Less than 2",
            "Più di 2 / More than 2"});
            this.cmb_anniVitaCollimatore.Location = new System.Drawing.Point(497, 66);
            this.cmb_anniVitaCollimatore.Name = "cmb_anniVitaCollimatore";
            this.cmb_anniVitaCollimatore.Size = new System.Drawing.Size(405, 21);
            this.cmb_anniVitaCollimatore.Sorted = true;
            this.cmb_anniVitaCollimatore.TabIndex = 2;
            this.cmb_anniVitaCollimatore.SelectedIndexChanged += new System.EventHandler(this.cmb_anniVitaCollimatore_SelectedIndexChanged);
            // 
            // lbl_anniVitaColl
            // 
            this.lbl_anniVitaColl.AutoSize = true;
            this.lbl_anniVitaColl.Location = new System.Drawing.Point(94, 66);
            this.lbl_anniVitaColl.Name = "lbl_anniVitaColl";
            this.lbl_anniVitaColl.Size = new System.Drawing.Size(129, 13);
            this.lbl_anniVitaColl.TabIndex = 7;
            this.lbl_anniVitaColl.Text = "Anni di vita del collimatore";
            // 
            // tab_Trasporto
            // 
            this.tab_Trasporto.BackColor = System.Drawing.Color.Transparent;
            this.tab_Trasporto.Controls.Add(this.groupBox14);
            this.tab_Trasporto.Location = new System.Drawing.Point(4, 22);
            this.tab_Trasporto.Name = "tab_Trasporto";
            this.tab_Trasporto.Size = new System.Drawing.Size(1046, 412);
            this.tab_Trasporto.TabIndex = 9;
            this.tab_Trasporto.Text = "Trasporto";
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.groupBox2);
            this.groupBox14.Controls.Add(this.groupBox1);
            this.groupBox14.Controls.Add(this.label1);
            this.groupBox14.Controls.Add(this.cmb_tipoImballaggio);
            this.groupBox14.Controls.Add(this.tb_DataSpedizione);
            this.groupBox14.Controls.Add(this.lbl_dataSped);
            this.groupBox14.Controls.Add(this.lbl_speseSped);
            this.groupBox14.Controls.Add(this.cmb_speseSpediz);
            this.groupBox14.Controls.Add(this.dtp_dataSpedizione);
            this.groupBox14.Location = new System.Drawing.Point(6, 6);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(1037, 400);
            this.groupBox14.TabIndex = 33;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "Dettagli";
            // 
            // rb_replaceNoteImballaggio
            // 
            this.rb_replaceNoteImballaggio.AutoSize = true;
            this.rb_replaceNoteImballaggio.Location = new System.Drawing.Point(19, 81);
            this.rb_replaceNoteImballaggio.Name = "rb_replaceNoteImballaggio";
            this.rb_replaceNoteImballaggio.Size = new System.Drawing.Size(106, 30);
            this.rb_replaceNoteImballaggio.TabIndex = 6;
            this.rb_replaceNoteImballaggio.TabStop = true;
            this.rb_replaceNoteImballaggio.Text = "Sovrascrivi Note \r\nImballaggio";
            this.rb_replaceNoteImballaggio.UseVisualStyleBackColor = true;
            // 
            // rb_appendNoteImballaggio
            // 
            this.rb_appendNoteImballaggio.AutoSize = true;
            this.rb_appendNoteImballaggio.Checked = true;
            this.rb_appendNoteImballaggio.Location = new System.Drawing.Point(19, 40);
            this.rb_appendNoteImballaggio.Name = "rb_appendNoteImballaggio";
            this.rb_appendNoteImballaggio.Size = new System.Drawing.Size(95, 30);
            this.rb_appendNoteImballaggio.TabIndex = 5;
            this.rb_appendNoteImballaggio.TabStop = true;
            this.rb_appendNoteImballaggio.Text = "Aggiungi Note \r\nImballaggio";
            this.rb_appendNoteImballaggio.UseVisualStyleBackColor = true;
            // 
            // tb_noteImballaggio
            // 
            this.tb_noteImballaggio.Location = new System.Drawing.Point(131, 14);
            this.tb_noteImballaggio.Multiline = true;
            this.tb_noteImballaggio.Name = "tb_noteImballaggio";
            this.tb_noteImballaggio.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tb_noteImballaggio.Size = new System.Drawing.Size(872, 140);
            this.tb_noteImballaggio.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(721, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 13);
            this.label1.TabIndex = 38;
            this.label1.Text = "Tipo Imballaggio";
            // 
            // rb_replaceNoteTrasporto
            // 
            this.rb_replaceNoteTrasporto.AutoSize = true;
            this.rb_replaceNoteTrasporto.Location = new System.Drawing.Point(19, 84);
            this.rb_replaceNoteTrasporto.Name = "rb_replaceNoteTrasporto";
            this.rb_replaceNoteTrasporto.Size = new System.Drawing.Size(106, 30);
            this.rb_replaceNoteTrasporto.TabIndex = 9;
            this.rb_replaceNoteTrasporto.TabStop = true;
            this.rb_replaceNoteTrasporto.Text = "Sovrascrivi Note \r\nTrasporto";
            this.rb_replaceNoteTrasporto.UseVisualStyleBackColor = true;
            // 
            // rb_appendNoteTrasporto
            // 
            this.rb_appendNoteTrasporto.AutoSize = true;
            this.rb_appendNoteTrasporto.Checked = true;
            this.rb_appendNoteTrasporto.Location = new System.Drawing.Point(19, 43);
            this.rb_appendNoteTrasporto.Name = "rb_appendNoteTrasporto";
            this.rb_appendNoteTrasporto.Size = new System.Drawing.Size(95, 30);
            this.rb_appendNoteTrasporto.TabIndex = 8;
            this.rb_appendNoteTrasporto.TabStop = true;
            this.rb_appendNoteTrasporto.Text = "Aggiungi Note \r\nTrasporto";
            this.rb_appendNoteTrasporto.UseVisualStyleBackColor = true;
            // 
            // cmb_tipoImballaggio
            // 
            this.cmb_tipoImballaggio.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmb_tipoImballaggio.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_tipoImballaggio.FormattingEnabled = true;
            this.cmb_tipoImballaggio.ItemHeight = 13;
            this.cmb_tipoImballaggio.Items.AddRange(new object[] {
            "Carta / Paper",
            "Cassa di legno / Wood box",
            "Imballo Kodak / Kodak packaging",
            "Instapack"});
            this.cmb_tipoImballaggio.Location = new System.Drawing.Point(842, 26);
            this.cmb_tipoImballaggio.MaximumSize = new System.Drawing.Size(180, 0);
            this.cmb_tipoImballaggio.MinimumSize = new System.Drawing.Size(180, 0);
            this.cmb_tipoImballaggio.Name = "cmb_tipoImballaggio";
            this.cmb_tipoImballaggio.Size = new System.Drawing.Size(180, 21);
            this.cmb_tipoImballaggio.Sorted = true;
            this.cmb_tipoImballaggio.TabIndex = 4;
            // 
            // tb_noteTrasporto
            // 
            this.tb_noteTrasporto.Location = new System.Drawing.Point(131, 14);
            this.tb_noteTrasporto.Multiline = true;
            this.tb_noteTrasporto.Name = "tb_noteTrasporto";
            this.tb_noteTrasporto.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tb_noteTrasporto.Size = new System.Drawing.Size(872, 140);
            this.tb_noteTrasporto.TabIndex = 10;
            // 
            // tb_DataSpedizione
            // 
            this.tb_DataSpedizione.Location = new System.Drawing.Point(150, 26);
            this.tb_DataSpedizione.Mask = "00/00/0000";
            this.tb_DataSpedizione.Name = "tb_DataSpedizione";
            this.tb_DataSpedizione.Size = new System.Drawing.Size(141, 20);
            this.tb_DataSpedizione.TabIndex = 1;
            this.tb_DataSpedizione.ValidatingType = typeof(System.DateTime);
            // 
            // lbl_dataSped
            // 
            this.lbl_dataSped.AutoSize = true;
            this.lbl_dataSped.Location = new System.Drawing.Point(22, 29);
            this.lbl_dataSped.Name = "lbl_dataSped";
            this.lbl_dataSped.Size = new System.Drawing.Size(96, 13);
            this.lbl_dataSped.TabIndex = 28;
            this.lbl_dataSped.Text = "Data di Spedizione";
            // 
            // lbl_speseSped
            // 
            this.lbl_speseSped.AutoSize = true;
            this.lbl_speseSped.Location = new System.Drawing.Point(369, 29);
            this.lbl_speseSped.Name = "lbl_speseSped";
            this.lbl_speseSped.Size = new System.Drawing.Size(103, 13);
            this.lbl_speseSped.TabIndex = 27;
            this.lbl_speseSped.Text = "Spese di Spedizione";
            // 
            // cmb_speseSpediz
            // 
            this.cmb_speseSpediz.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmb_speseSpediz.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_speseSpediz.FormattingEnabled = true;
            this.cmb_speseSpediz.ItemHeight = 13;
            this.cmb_speseSpediz.Items.AddRange(new object[] {
            "Cliente / Customer",
            "Ralco"});
            this.cmb_speseSpediz.Location = new System.Drawing.Point(504, 26);
            this.cmb_speseSpediz.MaximumSize = new System.Drawing.Size(180, 0);
            this.cmb_speseSpediz.MinimumSize = new System.Drawing.Size(180, 0);
            this.cmb_speseSpediz.Name = "cmb_speseSpediz";
            this.cmb_speseSpediz.Size = new System.Drawing.Size(180, 21);
            this.cmb_speseSpediz.Sorted = true;
            this.cmb_speseSpediz.TabIndex = 3;
            // 
            // dtp_dataSpedizione
            // 
            this.dtp_dataSpedizione.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_dataSpedizione.Location = new System.Drawing.Point(150, 26);
            this.dtp_dataSpedizione.MaximumSize = new System.Drawing.Size(180, 20);
            this.dtp_dataSpedizione.MinimumSize = new System.Drawing.Size(180, 20);
            this.dtp_dataSpedizione.Name = "dtp_dataSpedizione";
            this.dtp_dataSpedizione.Size = new System.Drawing.Size(180, 20);
            this.dtp_dataSpedizione.TabIndex = 2;
            this.dtp_dataSpedizione.Value = new System.DateTime(2018, 4, 19, 0, 0, 0, 0);
            this.dtp_dataSpedizione.ValueChanged += new System.EventHandler(this.dtp_dataSpedizione_ValueChanged);
            // 
            // tab_Qualita
            // 
            this.tab_Qualita.BackColor = System.Drawing.Color.Transparent;
            this.tab_Qualita.Controls.Add(this.rb_replaceNoteQualita);
            this.tab_Qualita.Controls.Add(this.rb_appendNoteQualita);
            this.tab_Qualita.Controls.Add(this.tb_noteQualita);
            this.tab_Qualita.Controls.Add(this.gb_azCorrPrev);
            this.tab_Qualita.Controls.Add(this.gb_impattoSCAR);
            this.tab_Qualita.Location = new System.Drawing.Point(4, 22);
            this.tab_Qualita.Name = "tab_Qualita";
            this.tab_Qualita.Size = new System.Drawing.Size(1046, 412);
            this.tab_Qualita.TabIndex = 3;
            this.tab_Qualita.Text = "Note Qualità";
            // 
            // rb_replaceNoteQualita
            // 
            this.rb_replaceNoteQualita.AutoSize = true;
            this.rb_replaceNoteQualita.Location = new System.Drawing.Point(15, 347);
            this.rb_replaceNoteQualita.Name = "rb_replaceNoteQualita";
            this.rb_replaceNoteQualita.Size = new System.Drawing.Size(106, 30);
            this.rb_replaceNoteQualita.TabIndex = 37;
            this.rb_replaceNoteQualita.TabStop = true;
            this.rb_replaceNoteQualita.Text = "Sovrascrivi Note \r\nQualità";
            this.rb_replaceNoteQualita.UseVisualStyleBackColor = true;
            this.rb_replaceNoteQualita.CheckedChanged += new System.EventHandler(this.rb_replaceNoteQualita_CheckedChanged);
            // 
            // rb_appendNoteQualita
            // 
            this.rb_appendNoteQualita.AutoSize = true;
            this.rb_appendNoteQualita.Checked = true;
            this.rb_appendNoteQualita.Location = new System.Drawing.Point(15, 306);
            this.rb_appendNoteQualita.Name = "rb_appendNoteQualita";
            this.rb_appendNoteQualita.Size = new System.Drawing.Size(95, 30);
            this.rb_appendNoteQualita.TabIndex = 36;
            this.rb_appendNoteQualita.TabStop = true;
            this.rb_appendNoteQualita.Text = "Aggiungi Note \r\nQualità";
            this.rb_appendNoteQualita.UseVisualStyleBackColor = true;
            this.rb_appendNoteQualita.CheckedChanged += new System.EventHandler(this.rb_appendNoteQualita_CheckedChanged);
            // 
            // tb_noteQualita
            // 
            this.tb_noteQualita.Location = new System.Drawing.Point(127, 271);
            this.tb_noteQualita.Multiline = true;
            this.tb_noteQualita.Name = "tb_noteQualita";
            this.tb_noteQualita.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tb_noteQualita.Size = new System.Drawing.Size(899, 127);
            this.tb_noteQualita.TabIndex = 8;
            // 
            // gb_azCorrPrev
            // 
            this.gb_azCorrPrev.Controls.Add(this.cmb_rifAzCorr);
            this.gb_azCorrPrev.Controls.Add(this.lbl_descAzCorrett);
            this.gb_azCorrPrev.Controls.Add(this.lbl_descAzCorr);
            this.gb_azCorrPrev.Controls.Add(this.cmb_statoAzCorr);
            this.gb_azCorrPrev.Controls.Add(this.lbl_statoAzCorr);
            this.gb_azCorrPrev.Controls.Add(this.lbl_rif);
            this.gb_azCorrPrev.Location = new System.Drawing.Point(6, 158);
            this.gb_azCorrPrev.Name = "gb_azCorrPrev";
            this.gb_azCorrPrev.Size = new System.Drawing.Size(1037, 102);
            this.gb_azCorrPrev.TabIndex = 1;
            this.gb_azCorrPrev.TabStop = false;
            this.gb_azCorrPrev.Text = "Azione Correttiva / Preventiva";
            // 
            // cmb_rifAzCorr
            // 
            this.cmb_rifAzCorr.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmb_rifAzCorr.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_rifAzCorr.FormattingEnabled = true;
            this.cmb_rifAzCorr.Location = new System.Drawing.Point(770, 24);
            this.cmb_rifAzCorr.MaximumSize = new System.Drawing.Size(250, 0);
            this.cmb_rifAzCorr.MinimumSize = new System.Drawing.Size(250, 0);
            this.cmb_rifAzCorr.Name = "cmb_rifAzCorr";
            this.cmb_rifAzCorr.Size = new System.Drawing.Size(250, 21);
            this.cmb_rifAzCorr.Sorted = true;
            this.cmb_rifAzCorr.TabIndex = 6;
            // 
            // lbl_descAzCorrett
            // 
            this.lbl_descAzCorrett.AutoSize = true;
            this.lbl_descAzCorrett.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_descAzCorrett.Location = new System.Drawing.Point(121, 71);
            this.lbl_descAzCorrett.MaximumSize = new System.Drawing.Size(895, 20);
            this.lbl_descAzCorrett.MinimumSize = new System.Drawing.Size(895, 20);
            this.lbl_descAzCorrett.Name = "lbl_descAzCorrett";
            this.lbl_descAzCorrett.Size = new System.Drawing.Size(895, 20);
            this.lbl_descAzCorrett.TabIndex = 7;
            // 
            // lbl_descAzCorr
            // 
            this.lbl_descAzCorr.AutoSize = true;
            this.lbl_descAzCorr.Location = new System.Drawing.Point(22, 71);
            this.lbl_descAzCorr.Name = "lbl_descAzCorr";
            this.lbl_descAzCorr.Size = new System.Drawing.Size(62, 13);
            this.lbl_descAzCorr.TabIndex = 14;
            this.lbl_descAzCorr.Text = "Descrizione";
            // 
            // cmb_statoAzCorr
            // 
            this.cmb_statoAzCorr.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmb_statoAzCorr.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_statoAzCorr.FormattingEnabled = true;
            this.cmb_statoAzCorr.Items.AddRange(new object[] {
            "Da Emettere / To Be Emitted",
            "Da Valutare / To Be Evaluated",
            "Emessa / Emitted",
            "Non Necessaria / Not Necessary"});
            this.cmb_statoAzCorr.Location = new System.Drawing.Point(121, 29);
            this.cmb_statoAzCorr.MaximumSize = new System.Drawing.Size(250, 0);
            this.cmb_statoAzCorr.MinimumSize = new System.Drawing.Size(250, 0);
            this.cmb_statoAzCorr.Name = "cmb_statoAzCorr";
            this.cmb_statoAzCorr.Size = new System.Drawing.Size(250, 21);
            this.cmb_statoAzCorr.Sorted = true;
            this.cmb_statoAzCorr.TabIndex = 5;
            // 
            // lbl_statoAzCorr
            // 
            this.lbl_statoAzCorr.AutoSize = true;
            this.lbl_statoAzCorr.Location = new System.Drawing.Point(22, 32);
            this.lbl_statoAzCorr.Name = "lbl_statoAzCorr";
            this.lbl_statoAzCorr.Size = new System.Drawing.Size(32, 13);
            this.lbl_statoAzCorr.TabIndex = 13;
            this.lbl_statoAzCorr.Text = "Stato";
            // 
            // lbl_rif
            // 
            this.lbl_rif.AutoSize = true;
            this.lbl_rif.Location = new System.Drawing.Point(644, 29);
            this.lbl_rif.Name = "lbl_rif";
            this.lbl_rif.Size = new System.Drawing.Size(60, 13);
            this.lbl_rif.TabIndex = 12;
            this.lbl_rif.Text = "Riferimento";
            // 
            // gb_impattoSCAR
            // 
            this.gb_impattoSCAR.Controls.Add(this.cmb_revDocValutRischi);
            this.gb_impattoSCAR.Controls.Add(this.cmb_segIncidente);
            this.gb_impattoSCAR.Controls.Add(this.lbl_revDocValutazRischi);
            this.gb_impattoSCAR.Controls.Add(this.lbl_segnIncid);
            this.gb_impattoSCAR.Controls.Add(this.cmb_invioNotaInf);
            this.gb_impattoSCAR.Controls.Add(this.cmb_sicProdotto);
            this.gb_impattoSCAR.Controls.Add(this.lbl_invioNotaInf);
            this.gb_impattoSCAR.Controls.Add(this.lbl_sicProd);
            this.gb_impattoSCAR.Location = new System.Drawing.Point(6, 6);
            this.gb_impattoSCAR.Name = "gb_impattoSCAR";
            this.gb_impattoSCAR.Size = new System.Drawing.Size(1037, 146);
            this.gb_impattoSCAR.TabIndex = 0;
            this.gb_impattoSCAR.TabStop = false;
            this.gb_impattoSCAR.Text = "Impatto SCAR";
            // 
            // cmb_revDocValutRischi
            // 
            this.cmb_revDocValutRischi.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmb_revDocValutRischi.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_revDocValutRischi.FormattingEnabled = true;
            this.cmb_revDocValutRischi.Items.AddRange(new object[] {
            "Necessaria / Necessary",
            "Non Necessaria / Not Necessary"});
            this.cmb_revDocValutRischi.Location = new System.Drawing.Point(770, 97);
            this.cmb_revDocValutRischi.MaximumSize = new System.Drawing.Size(250, 0);
            this.cmb_revDocValutRischi.MinimumSize = new System.Drawing.Size(250, 0);
            this.cmb_revDocValutRischi.Name = "cmb_revDocValutRischi";
            this.cmb_revDocValutRischi.Size = new System.Drawing.Size(250, 21);
            this.cmb_revDocValutRischi.Sorted = true;
            this.cmb_revDocValutRischi.TabIndex = 4;
            // 
            // cmb_segIncidente
            // 
            this.cmb_segIncidente.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmb_segIncidente.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_segIncidente.FormattingEnabled = true;
            this.cmb_segIncidente.Items.AddRange(new object[] {
            "Necessaria / Necessary",
            "Non Necessaria / Not Necessary"});
            this.cmb_segIncidente.Location = new System.Drawing.Point(770, 37);
            this.cmb_segIncidente.MaximumSize = new System.Drawing.Size(250, 0);
            this.cmb_segIncidente.MinimumSize = new System.Drawing.Size(250, 0);
            this.cmb_segIncidente.Name = "cmb_segIncidente";
            this.cmb_segIncidente.Size = new System.Drawing.Size(250, 21);
            this.cmb_segIncidente.Sorted = true;
            this.cmb_segIncidente.TabIndex = 2;
            // 
            // lbl_revDocValutazRischi
            // 
            this.lbl_revDocValutazRischi.AutoSize = true;
            this.lbl_revDocValutazRischi.Location = new System.Drawing.Point(644, 92);
            this.lbl_revDocValutazRischi.Name = "lbl_revDocValutazRischi";
            this.lbl_revDocValutazRischi.Size = new System.Drawing.Size(110, 26);
            this.lbl_revDocValutazRischi.TabIndex = 22;
            this.lbl_revDocValutazRischi.Text = "Revisione documento\r\nvalutazione dei rischi";
            // 
            // lbl_segnIncid
            // 
            this.lbl_segnIncid.AutoSize = true;
            this.lbl_segnIncid.Location = new System.Drawing.Point(644, 35);
            this.lbl_segnIncid.Name = "lbl_segnIncid";
            this.lbl_segnIncid.Size = new System.Drawing.Size(71, 26);
            this.lbl_segnIncid.TabIndex = 20;
            this.lbl_segnIncid.Text = "Segnalazione\r\ndell\'incidente";
            // 
            // cmb_invioNotaInf
            // 
            this.cmb_invioNotaInf.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmb_invioNotaInf.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_invioNotaInf.FormattingEnabled = true;
            this.cmb_invioNotaInf.Items.AddRange(new object[] {
            "Necessaria / Necessary",
            "Non Necessaria / Not Necessary"});
            this.cmb_invioNotaInf.Location = new System.Drawing.Point(121, 97);
            this.cmb_invioNotaInf.MaximumSize = new System.Drawing.Size(250, 0);
            this.cmb_invioNotaInf.MinimumSize = new System.Drawing.Size(250, 0);
            this.cmb_invioNotaInf.Name = "cmb_invioNotaInf";
            this.cmb_invioNotaInf.Size = new System.Drawing.Size(250, 21);
            this.cmb_invioNotaInf.Sorted = true;
            this.cmb_invioNotaInf.TabIndex = 3;
            // 
            // cmb_sicProdotto
            // 
            this.cmb_sicProdotto.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmb_sicProdotto.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_sicProdotto.FormattingEnabled = true;
            this.cmb_sicProdotto.Items.AddRange(new object[] {
            "Compromessa / Compromised",
            "Non Compromessa / Not Compromised"});
            this.cmb_sicProdotto.Location = new System.Drawing.Point(121, 37);
            this.cmb_sicProdotto.MaximumSize = new System.Drawing.Size(250, 0);
            this.cmb_sicProdotto.MinimumSize = new System.Drawing.Size(250, 0);
            this.cmb_sicProdotto.Name = "cmb_sicProdotto";
            this.cmb_sicProdotto.Size = new System.Drawing.Size(250, 21);
            this.cmb_sicProdotto.Sorted = true;
            this.cmb_sicProdotto.TabIndex = 1;
            // 
            // lbl_invioNotaInf
            // 
            this.lbl_invioNotaInf.AutoSize = true;
            this.lbl_invioNotaInf.Location = new System.Drawing.Point(22, 92);
            this.lbl_invioNotaInf.Name = "lbl_invioNotaInf";
            this.lbl_invioNotaInf.Size = new System.Drawing.Size(59, 26);
            this.lbl_invioNotaInf.TabIndex = 16;
            this.lbl_invioNotaInf.Text = "Invio Nota\r\nInformativa";
            // 
            // lbl_sicProd
            // 
            this.lbl_sicProd.AutoSize = true;
            this.lbl_sicProd.Location = new System.Drawing.Point(22, 35);
            this.lbl_sicProd.Name = "lbl_sicProd";
            this.lbl_sicProd.Size = new System.Drawing.Size(70, 26);
            this.lbl_sicProd.TabIndex = 14;
            this.lbl_sicProd.Text = "Sicurezza del\r\nprodotto";
            // 
            // tab_NoteRMA
            // 
            this.tab_NoteRMA.BackColor = System.Drawing.Color.Transparent;
            this.tab_NoteRMA.Controls.Add(this.lbl_note);
            this.tab_NoteRMA.Controls.Add(this.tb_dataAggiornamento);
            this.tab_NoteRMA.Controls.Add(this.dtp_dataAggiornamento);
            this.tab_NoteRMA.Controls.Add(this.lbl_dataAgg);
            this.tab_NoteRMA.Controls.Add(this.cmb_statoNotaRMA);
            this.tab_NoteRMA.Controls.Add(this.lbl_statoRMA);
            this.tab_NoteRMA.Controls.Add(this.tb_noteLog);
            this.tab_NoteRMA.Location = new System.Drawing.Point(4, 22);
            this.tab_NoteRMA.Name = "tab_NoteRMA";
            this.tab_NoteRMA.Size = new System.Drawing.Size(1046, 412);
            this.tab_NoteRMA.TabIndex = 10;
            this.tab_NoteRMA.Text = "Note RMA";
            // 
            // lbl_note
            // 
            this.lbl_note.AutoSize = true;
            this.lbl_note.Location = new System.Drawing.Point(21, 223);
            this.lbl_note.Name = "lbl_note";
            this.lbl_note.Size = new System.Drawing.Size(57, 13);
            this.lbl_note.TabIndex = 41;
            this.lbl_note.Text = "Note RMA";
            // 
            // tb_dataAggiornamento
            // 
            this.tb_dataAggiornamento.Location = new System.Drawing.Point(181, 24);
            this.tb_dataAggiornamento.Mask = "00/00/0000";
            this.tb_dataAggiornamento.Name = "tb_dataAggiornamento";
            this.tb_dataAggiornamento.Size = new System.Drawing.Size(211, 20);
            this.tb_dataAggiornamento.TabIndex = 40;
            this.tb_dataAggiornamento.ValidatingType = typeof(System.DateTime);
            // 
            // dtp_dataAggiornamento
            // 
            this.dtp_dataAggiornamento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtp_dataAggiornamento.Location = new System.Drawing.Point(181, 24);
            this.dtp_dataAggiornamento.MaximumSize = new System.Drawing.Size(250, 20);
            this.dtp_dataAggiornamento.MinimumSize = new System.Drawing.Size(250, 20);
            this.dtp_dataAggiornamento.Name = "dtp_dataAggiornamento";
            this.dtp_dataAggiornamento.Size = new System.Drawing.Size(250, 20);
            this.dtp_dataAggiornamento.TabIndex = 9;
            this.dtp_dataAggiornamento.Value = new System.DateTime(2018, 1, 16, 13, 29, 36, 0);
            this.dtp_dataAggiornamento.ValueChanged += new System.EventHandler(this.dtp_dataAggiornamento_ValueChanged);
            // 
            // lbl_dataAgg
            // 
            this.lbl_dataAgg.AutoSize = true;
            this.lbl_dataAgg.Location = new System.Drawing.Point(21, 30);
            this.lbl_dataAgg.Name = "lbl_dataAgg";
            this.lbl_dataAgg.Size = new System.Drawing.Size(104, 13);
            this.lbl_dataAgg.TabIndex = 1;
            this.lbl_dataAgg.Text = "Data Aggiornamento";
            // 
            // cmb_statoNotaRMA
            // 
            this.cmb_statoNotaRMA.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmb_statoNotaRMA.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_statoNotaRMA.FormattingEnabled = true;
            this.cmb_statoNotaRMA.Items.AddRange(new object[] {
            "Aperto",
            "In attesa di approvazione",
            "Attesa rientro parte",
            "In riparazione",
            "In Spedizione",
            "Chiuso",
            "Annullato"});
            this.cmb_statoNotaRMA.Location = new System.Drawing.Point(773, 27);
            this.cmb_statoNotaRMA.MaximumSize = new System.Drawing.Size(250, 0);
            this.cmb_statoNotaRMA.MinimumSize = new System.Drawing.Size(250, 0);
            this.cmb_statoNotaRMA.Name = "cmb_statoNotaRMA";
            this.cmb_statoNotaRMA.Size = new System.Drawing.Size(250, 21);
            this.cmb_statoNotaRMA.TabIndex = 4;
            // 
            // lbl_statoRMA
            // 
            this.lbl_statoRMA.AutoSize = true;
            this.lbl_statoRMA.Location = new System.Drawing.Point(613, 30);
            this.lbl_statoRMA.Name = "lbl_statoRMA";
            this.lbl_statoRMA.Size = new System.Drawing.Size(59, 13);
            this.lbl_statoRMA.TabIndex = 8;
            this.lbl_statoRMA.Text = "Stato RMA";
            // 
            // tb_noteLog
            // 
            this.tb_noteLog.Location = new System.Drawing.Point(181, 73);
            this.tb_noteLog.Multiline = true;
            this.tb_noteLog.Name = "tb_noteLog";
            this.tb_noteLog.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tb_noteLog.Size = new System.Drawing.Size(842, 325);
            this.tb_noteLog.TabIndex = 5;
            // 
            // cmb_prioritaRMA
            // 
            this.cmb_prioritaRMA.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmb_prioritaRMA.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_prioritaRMA.FormattingEnabled = true;
            this.cmb_prioritaRMA.Items.AddRange(new object[] {
            "1 - Alta",
            "2 - Media",
            "3 - Bassa"});
            this.cmb_prioritaRMA.Location = new System.Drawing.Point(133, 20);
            this.cmb_prioritaRMA.MaximumSize = new System.Drawing.Size(180, 0);
            this.cmb_prioritaRMA.MinimumSize = new System.Drawing.Size(180, 0);
            this.cmb_prioritaRMA.Name = "cmb_prioritaRMA";
            this.cmb_prioritaRMA.Size = new System.Drawing.Size(180, 21);
            this.cmb_prioritaRMA.TabIndex = 1;
            // 
            // lbl_priorita
            // 
            this.lbl_priorita.AutoSize = true;
            this.lbl_priorita.Location = new System.Drawing.Point(19, 23);
            this.lbl_priorita.Name = "lbl_priorita";
            this.lbl_priorita.Size = new System.Drawing.Size(39, 13);
            this.lbl_priorita.TabIndex = 12;
            this.lbl_priorita.Text = "Priorità";
            // 
            // cmb_statoRMA
            // 
            this.cmb_statoRMA.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmb_statoRMA.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmb_statoRMA.FormattingEnabled = true;
            this.cmb_statoRMA.Items.AddRange(new object[] {
            "Aperto",
            "Attesa Analisi Problema",
            "Emettere Preventivo",
            "Attesa Approvazione Preventivo",
            "Preventivo Approvato",
            "In Riparazione",
            "Riparazione Completata",
            "Parzialmente Evaso",
            "In Spedizione",
            "Attesa Rientro Parte",
            "Chiuso",
            "Annullato"});
            this.cmb_statoRMA.Location = new System.Drawing.Point(882, 20);
            this.cmb_statoRMA.MaximumSize = new System.Drawing.Size(180, 0);
            this.cmb_statoRMA.MinimumSize = new System.Drawing.Size(180, 0);
            this.cmb_statoRMA.Name = "cmb_statoRMA";
            this.cmb_statoRMA.Size = new System.Drawing.Size(180, 21);
            this.cmb_statoRMA.TabIndex = 2;
            this.cmb_statoRMA.SelectedIndexChanged += new System.EventHandler(this.cmb_statoRMA_SelectedIndexChanged);
            // 
            // lbl_stato
            // 
            this.lbl_stato.AutoSize = true;
            this.lbl_stato.Location = new System.Drawing.Point(785, 23);
            this.lbl_stato.Name = "lbl_stato";
            this.lbl_stato.Size = new System.Drawing.Size(32, 13);
            this.lbl_stato.TabIndex = 10;
            this.lbl_stato.Text = "Stato";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(274, 31);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(32, 13);
            this.label10.TabIndex = 9;
            this.label10.Text = "Stato";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(21, 28);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(39, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Priorità";
            // 
            // btn_SaveAndClose
            // 
            this.btn_SaveAndClose.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_SaveAndClose.BackgroundImage")));
            this.btn_SaveAndClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_SaveAndClose.Location = new System.Drawing.Point(467, 513);
            this.btn_SaveAndClose.Name = "btn_SaveAndClose";
            this.btn_SaveAndClose.Size = new System.Drawing.Size(50, 50);
            this.btn_SaveAndClose.TabIndex = 101;
            this.btn_SaveAndClose.UseVisualStyleBackColor = true;
            this.btn_SaveAndClose.Click += new System.EventHandler(this.btn_saveAndCloseRMA_Click);
            // 
            // btn_cancel
            // 
            this.btn_cancel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_cancel.BackgroundImage")));
            this.btn_cancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_cancel.Location = new System.Drawing.Point(572, 513);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(50, 50);
            this.btn_cancel.TabIndex = 102;
            this.btn_cancel.UseVisualStyleBackColor = true;
            this.btn_cancel.Click += new System.EventHandler(this.btn_cancelRMA_Click);
            // 
            // btn_saveRMA
            // 
            this.btn_saveRMA.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_saveRMA.BackgroundImage")));
            this.btn_saveRMA.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_saveRMA.Location = new System.Drawing.Point(362, 513);
            this.btn_saveRMA.Name = "btn_saveRMA";
            this.btn_saveRMA.Size = new System.Drawing.Size(50, 50);
            this.btn_saveRMA.TabIndex = 100;
            this.btn_saveRMA.UseVisualStyleBackColor = true;
            this.btn_saveRMA.Visible = false;
            this.btn_saveRMA.Click += new System.EventHandler(this.btn_saveRMA_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rb_replaceNoteImballaggio);
            this.groupBox1.Controls.Add(this.rb_appendNoteImballaggio);
            this.groupBox1.Controls.Add(this.tb_noteImballaggio);
            this.groupBox1.Location = new System.Drawing.Point(6, 63);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1016, 163);
            this.groupBox1.TabIndex = 39;
            this.groupBox1.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rb_replaceNoteTrasporto);
            this.groupBox2.Controls.Add(this.rb_appendNoteTrasporto);
            this.groupBox2.Controls.Add(this.tb_noteTrasporto);
            this.groupBox2.Location = new System.Drawing.Point(6, 228);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1016, 163);
            this.groupBox2.TabIndex = 40;
            this.groupBox2.TabStop = false;
            // 
            // Form_RMA_AggMassivo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1076, 581);
            this.Controls.Add(this.btn_saveRMA);
            this.Controls.Add(this.btn_cancel);
            this.Controls.Add(this.btn_SaveAndClose);
            this.Controls.Add(this.rb_appendDifRiscCli);
            this.Controls.Add(this.cmb_statoRMA);
            this.Controls.Add(this.cmb_prioritaRMA);
            this.Controls.Add(this.lbl_priorita);
            this.Controls.Add(this.lbl_stato);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(1092, 619);
            this.MinimumSize = new System.Drawing.Size(1092, 619);
            this.Name = "Form_RMA_AggMassivo";
            this.Text = "RMA";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form_RMA_AggMassivo_FormClosing);
            this.rb_appendDifRiscCli.ResumeLayout(false);
            this.tab_generalInfo.ResumeLayout(false);
            this.gb_InfoGenerali.ResumeLayout(false);
            this.gb_InfoGenerali.PerformLayout();
            this.tab_infoCliente.ResumeLayout(false);
            this.gb_dettagli.ResumeLayout(false);
            this.gb_dettagli.PerformLayout();
            this.tab_detGaranzia.ResumeLayout(false);
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.tab_Trasporto.ResumeLayout(false);
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            this.tab_Qualita.ResumeLayout(false);
            this.tab_Qualita.PerformLayout();
            this.gb_azCorrPrev.ResumeLayout(false);
            this.gb_azCorrPrev.PerformLayout();
            this.gb_impattoSCAR.ResumeLayout(false);
            this.gb_impattoSCAR.PerformLayout();
            this.tab_NoteRMA.ResumeLayout(false);
            this.tab_NoteRMA.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl rb_appendDifRiscCli;
        private System.Windows.Forms.TabPage tab_generalInfo;
        private System.Windows.Forms.TabPage tab_infoCliente;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox gb_InfoGenerali;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox gb_dettagli;
        private System.Windows.Forms.Label lbl_ordRip;
        private System.Windows.Forms.Label lbl_docReso;
        private System.Windows.Forms.Label lbl_SCARcli;
        private System.Windows.Forms.TextBox tb_scarCliente;
        private System.Windows.Forms.TextBox tb_ordRiparaz;
        private System.Windows.Forms.TextBox tb_docReso;
        private System.Windows.Forms.TextBox tb_noteCliente;
        private System.Windows.Forms.TabPage tab_Qualita;
        private System.Windows.Forms.GroupBox gb_impattoSCAR;
        private System.Windows.Forms.GroupBox gb_azCorrPrev;
        private System.Windows.Forms.ComboBox cmb_statoAzCorr;
        private System.Windows.Forms.Label lbl_statoAzCorr;
        private System.Windows.Forms.Label lbl_rif;
        private System.Windows.Forms.Label lbl_sicProd;
        private System.Windows.Forms.Label lbl_invioNotaInf;
        private System.Windows.Forms.ComboBox cmb_invioNotaInf;
        private System.Windows.Forms.ComboBox cmb_sicProdotto;
        private System.Windows.Forms.Label lbl_revDocValutazRischi;
        private System.Windows.Forms.Label lbl_segnIncid;
        private System.Windows.Forms.ComboBox cmb_revDocValutRischi;
        private System.Windows.Forms.ComboBox cmb_segIncidente;
        private System.Windows.Forms.TextBox tb_numScarCLI;
        private System.Windows.Forms.Label lbl_numSCARcli;
        private System.Windows.Forms.ComboBox cmb_decisCliente;
        private System.Windows.Forms.Label lbl_decisCli;
        private System.Windows.Forms.Label lbl_dataOrdRip;
        private System.Windows.Forms.DateTimePicker dtp_dataOrdRip;
        private System.Windows.Forms.Button btn_SaveAndClose;
        private System.Windows.Forms.Button btn_cancel;
        private System.Windows.Forms.ComboBox cmb_prioritaRMA;
        private System.Windows.Forms.Label lbl_priorita;
        private System.Windows.Forms.ComboBox cmb_statoRMA;
        private System.Windows.Forms.Label lbl_stato;
        private System.Windows.Forms.DateTimePicker dtp_dataScadenzaRMA;
        private System.Windows.Forms.Label lbl_dataScadRMA;
        private System.Windows.Forms.TextBox tb_noteQualita;
        private System.Windows.Forms.ComboBox cmb_nomeCliente;
        private System.Windows.Forms.Label lbl_nomeCliente;
        private System.Windows.Forms.TabPage tab_detGaranzia;
        private System.Windows.Forms.Label lbl_descAzCorrett;
        private System.Windows.Forms.Label lbl_descAzCorr;
        private System.Windows.Forms.ComboBox cmb_rifAzCorr;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.TextBox tb_noteGaranziaRalco;
        private System.Windows.Forms.Label lbl_respDannoColl;
        private System.Windows.Forms.ComboBox cmb_provCollimatore;
        private System.Windows.Forms.Label lbl_provColl;
        private System.Windows.Forms.ComboBox cmb_anniVitaCollimatore;
        private System.Windows.Forms.Label lbl_anniVitaColl;
        private System.Windows.Forms.ComboBox cmb_respDannoCollimatore;
        private System.Windows.Forms.ComboBox cmb_azioniSupplRalco;
        private System.Windows.Forms.Label lbl_azSupplRalco;
        private System.Windows.Forms.TextBox tb_noteGenerali;
        private System.Windows.Forms.DateTimePicker dtp_DataArrivoCollimatore;
        private System.Windows.Forms.Label lbl_dataArrColl;
        private System.Windows.Forms.Label lbl_dataDocReso;
        private System.Windows.Forms.DateTimePicker dtp_dataDocReso;
        private System.Windows.Forms.TabPage tab_Trasporto;
        private System.Windows.Forms.DateTimePicker dtp_dataSpedizione;
        private System.Windows.Forms.ComboBox cmb_speseSpediz;
        private System.Windows.Forms.Label lbl_dataSped;
        private System.Windows.Forms.Label lbl_speseSped;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.TextBox tb_noteTrasporto;
        private System.Windows.Forms.Label lbl_codCollCliente;
        private System.Windows.Forms.Label lbl_idCliente;
        private System.Windows.Forms.Label lbl_codCollCli;
        private System.Windows.Forms.Label lbl_idCli;
        private System.Windows.Forms.Button btn_saveRMA;
        private System.Windows.Forms.RadioButton rb_statoGaranzia;
        private System.Windows.Forms.ComboBox cmb_statoGaranzia;
        private System.Windows.Forms.MaskedTextBox tb_DataArrivoCollimatore;
        private System.Windows.Forms.MaskedTextBox tb_DataScadenzaRMA;
        private System.Windows.Forms.MaskedTextBox tb_DataDocumentoDiReso;
        private System.Windows.Forms.MaskedTextBox tb_DataSpedizione;
        private System.Windows.Forms.MaskedTextBox tb_DataOrdineRiparazione;
        private System.Windows.Forms.RadioButton rb_replaceNoteGenerali;
        private System.Windows.Forms.RadioButton rb_appendNoteGenerali;
        private System.Windows.Forms.RadioButton rb_replaceDifRiscCli;
        private System.Windows.Forms.RadioButton rb_appendDifRiscCliente;
        private System.Windows.Forms.RadioButton rb_replaceNoteGaranzia;
        private System.Windows.Forms.RadioButton rb_appendNoteGaranzia;
        private System.Windows.Forms.RadioButton rb_replaceNoteTrasporto;
        private System.Windows.Forms.RadioButton rb_appendNoteTrasporto;
        private System.Windows.Forms.RadioButton rb_replaceNoteQualita;
        private System.Windows.Forms.RadioButton rb_appendNoteQualita;
        private System.Windows.Forms.TabPage tab_NoteRMA;
        private System.Windows.Forms.DateTimePicker dtp_dataAggiornamento;
        private System.Windows.Forms.Label lbl_dataAgg;
        private System.Windows.Forms.ComboBox cmb_statoNotaRMA;
        private System.Windows.Forms.Label lbl_statoRMA;
        private System.Windows.Forms.TextBox tb_noteLog;
        private System.Windows.Forms.MaskedTextBox tb_dataAggiornamento;
        private System.Windows.Forms.Label lbl_note;
        private System.Windows.Forms.TextBox tb_noteImballaggio;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmb_tipoImballaggio;
        private System.Windows.Forms.RadioButton rb_replaceNoteImballaggio;
        private System.Windows.Forms.RadioButton rb_appendNoteImballaggio;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
    }
}